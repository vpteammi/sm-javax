package com.sm.javax.netx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.sm.javax.query.Result;

public class BasicNetClient
{
	private ScheduledThreadPoolExecutor executor;
	
	public BasicNetClient (ScheduledThreadPoolExecutor executor)
	{
		super ();
		this.executor = executor;
	}
	
	public BasicNetRequest start (String host, int port, Consumer<Result<String>> responseHandler)
	{
		return new BasicNetRequest (host, port, responseHandler);
	}
	
	// =============================================================================
	public class BasicNetRequest 
	{
		private String                   host;
		private int                      port;
		private StringBuilder            message;
		private Consumer<Result<String>> responseHandler;
		private long                     timeout;
		private ScheduledFuture<?>       terminatorTask;
		private Future<?>                processingTask;
		private Socket socket;

		public BasicNetRequest (String host, int port, Consumer<Result<String>> responseHandler)
		{
			super ();
			this.host = host;
			this.port = port;
			this.responseHandler = responseHandler;
			message = new StringBuilder ();
			timeout = 0L;
		}
		
		public BasicNetRequest timeout (long timeoutMsec)
		{
			if (timeoutMsec >= 0) {
				timeout = timeoutMsec;
			}
			return this;
		}
		
		public BasicNetRequest send (String msg)
		{
			message.append (msg);
			return this;
		}
		
		public void end (String msg)
		{
			send (msg);
			end ();
		}
		
		public void end ()
		{
			try {
				socket = new Socket (host, port);
				OutputStream out = socket.getOutputStream ();
				out.write (message.toString ().getBytes ());
				out.flush ();
				waitForResponse ();
			}
			catch (IOException e) {
				responseHandler.accept (new Result<> (e));
			}
		}
		
		// -----------------------------------------------------------------------------
		private void waitForResponse ()
		{
			if (timeout > 0) {
				terminatorTask = executor.schedule (this::terminateResponseProcessing, timeout, TimeUnit.MILLISECONDS);
			}
			processingTask = executor.submit (this::readResponse);
		}
		
		private synchronized void terminateResponseProcessing ()
		{
			if (processingTask != null) {
				processingTask.cancel (true);
			}
			try { socket.close (); }
			catch (IOException e) { }
		}
		
		private void cancelTerminationTask ()
		{
			if (terminatorTask != null) {
				terminatorTask.cancel (true);
			}
		}
		
		private void readResponse ()
		{
			try (InputStream inp = socket.getInputStream ()) {
				StringBuffer response = new StringBuffer ();
				try (BufferedReader in = new BufferedReader (new InputStreamReader (inp))) {
					String output;		 
					while ((output = in.readLine ()) != null) {
						response.append (output);
					}
					responseHandler.accept (new Result<> (response.toString (), true));
				}
				catch (IOException e) {
					responseHandler.accept (new Result<> (e));
				}
			}
			catch (IOException ex) {
				responseHandler.accept (new Result<> (ex));
			}
			finally {
				try { socket.close (); }
				catch (IOException e) { }
			}
			cancelTerminationTask ();
		}
		
	}
	
}
