package com.sm.javax.netx;

import java.io.File;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import com.sm.javax.langx.ProgrammingError;
import com.sm.javax.langx.Strings;

public class NetUtilities {

	public static URI buildURI(String scheme, String host, int port, String path) {
		try {
			return (new URI(scheme, null, host, port, path, null, null));
		} catch (URISyntaxException e) {
			throw ProgrammingError.shouldNotHappen(e);
		}
	}

	public static URI setFragment(URI uri, String fragment) throws URISyntaxException {
		return (new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), uri.getQuery(), fragment));
	}

	public static URI stripQuery(URI uri) throws URISyntaxException {
		return (new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), null, uri.getFragment()));
	}

	public static URI setQuery(URI uri, String query) throws URISyntaxException {
		return (new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), query, uri.getFragment()));
	}

	public static URI setQueryAndFragment(URI uri, String query, String fragment) throws URISyntaxException {
		return (new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), query, fragment));
	}

	public static URI addToQuery(URI uri, String name, Object value) throws URISyntaxException {
		return (addToQuery(uri, Strings.concat(name, "=", Strings.toString(value))));
	}

	public static URI addToQuery(URI uri, String query) throws URISyntaxException {
		return (new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), Strings.concat(uri.getQuery(), "&", query),
				uri.getFragment()));
	}

	public static URL decodeUrl(URL url) {
		try {
			URI uri = new URI(url.toExternalForm()).normalize();
			uri = new URI(uri.getScheme(), uri.getSchemeSpecificPart(), uri.getFragment());
			return (uri.toURL());
		} catch (URISyntaxException e) {
			return (url);
		} catch (MalformedURLException e) {
			return (url);
		}
	}

	public final static String URI_SCHEME_FILE = "file";

	public static boolean isFileURI(URI uri) {
		return ((uri != null) && (new File(uri) != null));
	}

	public static File toFile(URI uri) throws URISyntaxException {
		try {
			return (new File(uri));
		} catch (IllegalArgumentException e) {
			throw new URISyntaxException(uri.toString(), "URI is not file");
		}
	}

	public static String getQueryParameter(URI uri, String name) {
		return (getQueryParameters(uri).get(name));
	}

	public static Map<String, String> getQueryParameters(URI uri) {
		return (parseQueryParameters(uri == null ? null : uri.getQuery()));
	}

	public static Map<String, String> getQueryParameters(URL url) {
		return (parseQueryParameters(url == null ? null : url.getQuery()));
	}

	private static Map<String, String> parseQueryParameters(String query) {
		if (!Strings.isEmpty(query)) {
			HashMap<String, String> map = new HashMap<String, String>();
			for (String pair : query.split("&")) {
				int idx = pair.indexOf("=");
				map.put((idx > 0) ? pair.substring(0, idx) : pair, (idx > 0) ? pair.substring(idx + 1) : "");
			}
			return (map);
		}
		return Collections.emptyMap();
	}

	public static String escapeSpecialCharacters(String input) {
		StringBuilder resultStr = new StringBuilder();
		for (char ch : input.toCharArray()) {
			if (isSafe(ch)) {
				resultStr.append(ch);
			} else {
				resultStr.append('%');
				resultStr.append(toHex(ch / 16));
				resultStr.append(toHex(ch % 16));
			}
		}

		return resultStr.toString();
	}

	private static char toHex(int ch) {
		return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
	}

	private static boolean isSafe(char ch) {
		return ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9')
				|| "-_.~".indexOf(ch) >= 0);
	}

	public static NetworkInterface getNetworkInterface() {
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface iface = interfaces.nextElement();
				// filters out 127.0.0.1 and inactive interfaces
				if (iface.isLoopback() || !iface.isUp())
					continue;

				return (iface);
			}
		} catch (SocketException e) {
		}
		return (null);
	}

	public static InetAddress getInetAddress() {
		NetworkInterface iface = getNetworkInterface();
		if (iface != null) {
			Enumeration<InetAddress> addresses = iface.getInetAddresses();
			while (addresses.hasMoreElements()) {
				return (addresses.nextElement());
			}
		}
		return (null);
	}
}
