package com.sm.javax.netx;

@SuppressWarnings ("serial")
public class NetException extends Exception
{

	public NetException ()
	{
	}

	public NetException (String arg0)
	{
		super (arg0);
	}

	public NetException (Throwable arg0)
	{
		super (arg0);
	}

	public NetException (String arg0, Throwable arg1)
	{
		super (arg0, arg1);
	}
}
