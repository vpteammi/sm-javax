package com.sm.javax.netx;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.Executors;

public class NetSimpleSocketServer implements Closeable
{
	private final ServerSocket serverSocket;
	private boolean exitSignaled = false;
	private NetException runException = null;
	private Thread worker = null;

	public NetSimpleSocketServer () throws NetException
	{
		try
		{
			serverSocket = new ServerSocket (0);
		}
		catch (IOException e)
		{
			throw new NetException (e);
		}
	}

	protected NetException catchException (Throwable e)
	{
		e.printStackTrace (System.err);
		return (runException = new NetException (e));
	}
	
	protected void onAccept (Socket requestSocket)
	{
		System.out.println ("accepted on port " + requestSocket.getLocalPort ());

		InputStream is = null;
		OutputStream os = null;
		try
		{
			is = requestSocket.getInputStream ();
			os = requestSocket.getOutputStream ();
			onAccept (is, os);
		}
		catch (IOException e)
		{
			catchException (e);
		}
		finally
		{
			if (is != null)
				try
				{
					is.close ();
				}
				catch (IOException e)
				{
					catchException (e);
				}
			if (os != null)
				try
				{
					os.close ();
				}
				catch (IOException e)
				{
					catchException (e);
				}
		}
	}

	protected void onAccept (InputStream is)
	{
	}

	protected void onAccept (OutputStream os)
	{
	}

	protected void onAccept (InputStream is, OutputStream os)
	{
		if (!isExitSignalled ()) onAccept (is);
		if (!isExitSignalled ()) onAccept (os);
	}

	protected void signalExit ()
	{
		exitSignaled = true;
	}

	protected boolean isExitSignalled ()
	{
		return (exitSignaled);
	}

	public int getLocalPort ()
	{
		return (serverSocket.getLocalPort ());
	}

	public void run () throws SocketException
	{
		run (0);
	}

	public void doRun ()
	{
		Socket requestSocket;
		exitSignaled = false;
		runException = null;

		try
		{
			System.out.println ("listening on port " + serverSocket.getLocalPort ());
			while (!isExitSignalled ()
					&& ((requestSocket = serverSocket.accept ()) != null))
			{
				onAccept (requestSocket);
				requestSocket.close ();
			}
		}
		catch (IOException e)
		{
			catchException (e);
		}
	}

	public void run (int timeoutInMSecs) throws SocketException
	{
		serverSocket.setSoTimeout (timeoutInMSecs);
		doRun ();
	}

	public void runInBackground () throws NetException
	{
		runInBackground (0);
	}

	public void runInBackground (int timeoutInMSecs) throws NetException
	{
		try
		{
			serverSocket.setSoTimeout (timeoutInMSecs);
		}
		catch (SocketException e)
		{
			throw catchException (e);
		}
		worker = Executors.defaultThreadFactory ().newThread (new Runnable ()
		{
			@Override
			public void run ()
			{
				doRun ();
				NetSimpleSocketServer.this.close ();
			}
		});
		worker.start ();
	}

	public void waitBackgroundRun () throws NetException
	{
		if (worker != null)
		{
			try
			{
				worker.join ();
			}
			catch (InterruptedException e)
			{
				catchException (e);
			}
		}
		
		if (runException != null)
			throw runException;
	}

	@Override
	public void close ()
	{
		try
		{
			serverSocket.close ();
		}
		catch (IOException e)
		{
			catchException (e);
		}
	}
}
