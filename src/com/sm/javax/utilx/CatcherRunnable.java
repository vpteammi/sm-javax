package com.sm.javax.utilx;

import java.util.function.Consumer;

public class CatcherRunnable<E extends Exception> extends ExceptionCatcher<E> implements Runnable{

	private final RunnableX<E> command;

	public CatcherRunnable(RunnableX<E> command) {
		super();
		this.command = command;
	}

	public CatcherRunnable(RunnableX<E> command, Consumer<E> excProcessor) {
		super(excProcessor);
		this.command = command;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			command.run();
		} catch (Exception e) {
			processException(null, (E) e);
		}
	}

}
