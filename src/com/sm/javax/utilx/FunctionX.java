package com.sm.javax.utilx;

@FunctionalInterface
public interface FunctionX<T, R, E extends Exception> {

	public R apply (T value) throws E;

}
