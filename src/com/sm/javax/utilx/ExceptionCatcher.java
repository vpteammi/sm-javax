package com.sm.javax.utilx;

import java.util.function.Consumer;

public class ExceptionCatcher<E extends Exception> extends ExceptionProcessor<Void, E> {

	public ExceptionCatcher() {
		super();
	}

	public ExceptionCatcher(Consumer<E> excProcessor) {
		super(excProcessor);
	}
}
