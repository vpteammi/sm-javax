package com.sm.javax.utilx;

public class Formats {

	static public String getHumanFormatForBytes (long bytes)
	{
		return getHumanFormatForBytes (bytes, false);
	}

	static private final long constantForKB = 1024;
	static private final long constantForMB = constantForKB * 1024;
	static private final long constantForGB = constantForMB * 1024;

	static public String getHumanFormatForBytes (long bytes, boolean shortFormat)
	{
		if (bytes > constantForGB)
		{
			double Gbytes = (double) bytes / constantForGB;
			return (shortFormat ? String.format ("%.1f GB", Gbytes) : String.format (
					"%.1f GB  (%,d bytes)", Gbytes, bytes));
		}
		else if (bytes > constantForMB)
		{
			double Mbytes = (double) bytes / constantForMB;
			return (shortFormat ? String.format ("%.1f MB", Mbytes) : String.format (
					"%.1f MB  (%,d bytes)", Mbytes, bytes));
		}
		else if (bytes > constantForKB)
		{
			double Kbytes = (double) bytes / constantForKB;
			return (shortFormat ? String.format ("%.1f KB", Kbytes) : String.format (
					"%.1f KB  (%,d bytes)", Kbytes, bytes));
		}
		else
		{
			return String.format ("%,d bytes", bytes);
		}
	}

}
