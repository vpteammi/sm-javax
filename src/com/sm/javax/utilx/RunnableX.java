package com.sm.javax.utilx;

@FunctionalInterface
public interface RunnableX<E extends Exception> {
	void run () throws E;
}
