package com.sm.javax.utilx;

import java.util.Iterator;
import java.util.function.Function;

public class ArrayIterator<E> implements Iterator<E>
{
	private final int size;
	private final Function<Integer, E> getElementByIndex;
	
	private int current = 0;
	
	public ArrayIterator (int size, Function<Integer, E> getElementByIndex)
	{
		this.size = size;
		this.getElementByIndex = getElementByIndex;
	}

	@Override
	public boolean hasNext ()
	{
		// TODO Auto-generated method stub
		return (current < size);
	}

	@Override
	public E next ()
	{
		return getElementByIndex.apply (current++);
	}
}
