package com.sm.javax.utilx;

import java.util.Comparator;
import java.util.Date;

public class Comparators
{
	public static class ComparatorWithNull <T> implements Comparator <T>
	{
		@Override
		public int compare (T o1, T o2)
		{
			if (o1 != o2)
			{
				if (o1 == null)
				{
					return (-1);
				}
				else if (o2 == null)
				{
					return (1);
				}
				else
				{
					return (compareNoNull (o1, o2));
				}
			}
			return 0;
		}
		
		protected int compareNoNull (T o1, T o2)
		{
			return (String.valueOf (o1).compareTo (String.valueOf (o2)));
		}
	}

	public final static class ComparatorForComparable <T extends Comparable<T>> extends ComparatorWithNull <T>
	{
		protected int compareNoNull (T o1, T o2)
		{
			return (o1.compareTo (o2));
		}
	}

	public final static Comparator<Object> OBJECT_COMPARATOR = new ComparatorWithNull<Object>();
	public final static Comparator<Boolean> BOOLEAN_COMPARATOR = new ComparatorForComparable<Boolean>();
	public final static Comparator<String> STRING_COMPARATOR = new ComparatorForComparable<String>();
	public final static Comparator<Integer> INTEGER_COMPARATOR = new ComparatorForComparable<Integer>();
	public final static Comparator<Long> LONG_COMPARATOR = new ComparatorForComparable<Long>();
	public final static Comparator<Short> SHORT_COMPARATOR = new ComparatorForComparable<Short>();
	public final static Comparator<Double> DOUBLE_COMPARATOR = new ComparatorForComparable<Double>();
	public final static Comparator<Float> FLOAT_COMPARATOR = new ComparatorForComparable<Float>();
	public final static Comparator<Character> CHARACTER_COMPARATOR = new ComparatorForComparable<Character>();
	public final static Comparator<Date> DATE_COMPARATOR = new ComparatorForComparable<Date>();
}
