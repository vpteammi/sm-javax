package com.sm.javax.utilx;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

public class Sets
{
	public static <T, V extends T> void addNoNull (Set<T> set, V item)
	{
		if ((set != null) && (item != null))
		{
			set.add (item);
		}
	}
	
	public static <T, V extends T> void addAllNoNull (Set<T> set, Iterable<V> items)
	{
		if ((set != null) && (items != null))
		{
			for (V item : items)
			{
				if (item != null)
					set.add (item);
			}
		}
	}
	
	public static <S, T> Set<T> toSet (Iterable<S> collection,
			Function<? super S, T> transform)
	{
		if (collection != null)
		{
			HashSet<T> set = new HashSet<T> ();

			for (S item : collection)
			{
				T value = transform.apply (item);
				if (value != null)
				{
					set.add (value);
				}
			}

			return (set);
		}

		return (null);
	}

	public static <T> Set<T> toSet (T[] array)
	{
		if (array != null)
		{
			HashSet<T> set = new HashSet<T> ();

			for (T item : array)
			{
				set.add (item);
			}
			
			return (set);
		}
		
		return (null);
	}

	public static <S, T> Set<T> toSet (S[] collection,
			Function<? super S, T> transform)
	{
		if (collection != null)
		{
			HashSet<T> set = new HashSet<T> ();

			for (S item : collection)
			{
				T value = transform.apply (item);
				if (value != null)
				{
					set.add (value);
				}
			}

			return (set);
		}

		return (null);
	}

	public static <T, S extends T, D extends T, V extends D> void collectClosure (
			Iterable<S> seeds, Set<D> closeureSet,
			Function<? super T, ? extends Iterable<V>> dependeciesGetter)
	{
		if (dependeciesGetter != null)
		{
			@SuppressWarnings ("unchecked")
			Iterable<T> unprocessed = (Iterable<T>) seeds;

			while (unprocessed != null)
			{
				HashSet<T> unprocessedSet = null;

				for (S seed : seeds)
				{
					Iterable<V> deps = dependeciesGetter.apply (seed);
					if (deps != null)
					{
						for (V dep : deps)
						{
							if (!closeureSet.contains (dep))
							{
								closeureSet.add (dep);

								if (unprocessedSet == null)
								{
									unprocessedSet = new HashSet<T> ();
								}

								unprocessedSet.add (dep);
							}
						}
					}
				}

				unprocessed = unprocessedSet;
			}
		}
	}
}
