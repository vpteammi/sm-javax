package com.sm.javax.utilx;

public interface BiFunctionX<T, U, R, E extends Exception> {

	public R apply (T left, U right) throws E;

}