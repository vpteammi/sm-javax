package com.sm.javax.utilx;

import java.util.EventObject;

@SuppressWarnings("serial")
public class EventX<SOURCE> extends EventObject {
	public EventX(SOURCE source) {
		super(source);
	}

	@SuppressWarnings("unchecked")
	@Override
	public SOURCE getSource() {
		// TODO Auto-generated method stub
		return (SOURCE) super.getSource();
	}
}
