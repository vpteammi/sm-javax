package com.sm.javax.utilx;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public abstract class LazyList<E> implements List<E> {

	private final int fromIndex;
	private final int toIndex;
	private final E[] array;

	@SuppressWarnings("unchecked")
	public LazyList(int size, Class<E> cls) {
		this.fromIndex = 0;
		this.toIndex = size;
		array = (E[]) Array.newInstance(cls, size);
		Arrays.fill(array, null);
	}

	public LazyList(E[] array) {
		this(array, 0, array.length);
	}

	public LazyList(E[] array, int fromIndex, int toIndex) {
		this.fromIndex = fromIndex;
		this.toIndex = toIndex;
		this.array = array;
	}

	@Override
	public int size() {
		return (toIndex - fromIndex);
	}

	@Override
	public boolean isEmpty() {
		return (toIndex == fromIndex);
	}

	@Override
	public boolean contains(Object o) {
		return indexOf(o) != -1;
	}

	private class LazyListIterator implements ListIterator<E> {

		private int idx;

		private LazyListIterator(int index) {
			idx = index;
		}

		private LazyListIterator() {
			this(0);
		}

		@Override
		public boolean hasNext() {
			return (idx < toIndex);
		}

		@Override
		public E next() {
			return hasNext() ? get(idx++) : null;
		}

		@Override
		public boolean hasPrevious() {
			return (idx >= fromIndex);
		}

		@Override
		public E previous() {
			return hasPrevious() ? get(idx--) : null;
		}

		@Override
		public int nextIndex() {
			return (idx + 1);
		}

		@Override
		public int previousIndex() {
			return (idx - 1);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Cannot modify list");
		}

		@Override
		public void set(E e) {
			LazyList.this.set(idx, e);
		}

		@Override
		public void add(E e) {
			throw new UnsupportedOperationException("Cannot modify list");
		}
	}

	@Override
	public Iterator<E> iterator() {
		return new LazyListIterator();
	}

	@Override
	public Object[] toArray() {
		return toArray (new Object [size()]);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a) {
		int size = size();
		if (a.length != size) {
			a = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
		}

		for (int i = 0; i < size; i++) {
			a[i] = (T) get(i);
		}

		return a;
	}

	@Override
	public boolean add(E e) {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		for (Object e : c) {
			if (!contains(e))
				return (false);
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public E get(int index) {
		E item = array [fromIndex + index];
		if (item == null)
		{
			set (index, item = load (index));
		}
		return item;
	}

	@Override
	public E set(int index, E element) {
		array[fromIndex + index] = element;
		return (element);
	}

	@Override
	public void add(int index, E element) {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public E remove(int index) {
		throw new UnsupportedOperationException("Cannot modify list");
	}

	@Override
	public int indexOf(Object o) {
		int size = size ();
		for (int i = 0; i < size; i++) {
			if (Objects.equals(o, get(i)))
				return (i-fromIndex);
		}
		return -1;
	}

	@Override
	public int lastIndexOf(Object o) {
		for (int i = size () - 1; i >= 0 ; i--) {
			if (Objects.equals(o, get(i)))
				return (i-fromIndex);
		}
		return -1;
	}

	@Override
	public ListIterator<E> listIterator() {
		return (new LazyListIterator());
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return (new LazyListIterator(index));
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		final int ofs = this.fromIndex + fromIndex;
		return (new LazyList<E>(array, ofs, this.fromIndex + toIndex) {

			@Override
			protected E load(int i) {
				return load (ofs, i);
			}});
	}
	
	protected E load (int ofs, int i)
	{
		return (load (ofs + i));
	}
	
	protected abstract E load (int i);
}
