package com.sm.javax.utilx;

public class StdoutLogger 
{
	public static boolean UseStdLogging = false;
	private static Boolean LogEnabled; 
	
	
	/*
	 * To enable STDIN logging execute JVM with
	 *  -Dcom.sm.javax.utilx.StdoutLogger.UseStdLogging=true
	 */
	private static boolean logEnabled ()
	{
		if (LogEnabled == null) {
			if (UseStdLogging) {
				return true;
			}
			LogEnabled = Boolean.getBoolean (StdoutLogger.class.getName () + ".UseStdLogging");
		}
		return LogEnabled;
	}
	
	public static void logInfo (String msg)
	{
		if (logEnabled ()) {
			System.out.println (msg);
		}
	}

	public static void logError (String msg, Exception e) 
	{
		if (logEnabled ()) {
			logInfo ("Error: " + msg + " - " + (e != null ? e.getMessage() : ""));
		}
	}
	
}
