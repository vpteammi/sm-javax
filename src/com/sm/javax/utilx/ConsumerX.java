package com.sm.javax.utilx;

@FunctionalInterface
public interface ConsumerX<T, E extends Exception> {

	public void accept (T t) throws E;
}
