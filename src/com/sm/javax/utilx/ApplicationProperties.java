package com.sm.javax.utilx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.sm.javax.langx.Strings;
import com.sm.javax.langx.annotation.SettingProperty;

@SuppressWarnings("serial")
public class ApplicationProperties extends Properties
{
	private static final String TRUE = "true";
	private static final String FALSE = "false";
	private static HashMap<String, ApplicationProperties> AppPropsMap = null;
	private static ApplicationProperties CurAppProps = null;
	
	protected File               mPropFile;
	protected boolean            mUpdated;
	private List<Field>          mFields;
	private CommandLineArguments mCmdArgs; 

	
	private static File getFileByName (String propFileName)
	{
		if (propFileName != null && ! propFileName.isEmpty ()) {
			return (new File (propFileName));
		}
		return null;
	}
	
	protected ApplicationProperties (File propFile, CommandLineArguments cmdArgs)
	{
		super ();
		mPropFile = propFile;
		mCmdArgs = cmdArgs;
		mFields = new ArrayList<Field> ();
		if (propFile != null) {
			try (FileInputStream inp = new FileInputStream (mPropFile)) {
				load (inp);
			} catch (FileNotFoundException e) {
				System.out.println ("Properties file " + propFile + " not found");
			} catch (IOException e) {
				System.out.println ("Failed to load properties file " + propFile);
			}
		}
		mUpdated = false;
	}
	
	protected ApplicationProperties (File propFile, String[] cmdArgs)
	{
		this (propFile, cmdArgs != null ? new CommandLineArguments (cmdArgs) : null);
	}
	
	protected ApplicationProperties (String propFileName, CommandLineArguments cmdArgs) throws IOException
	{
		this (getFileByName (propFileName), cmdArgs);
	}
	
	protected ApplicationProperties (String propFileName, String[] cmdArgs) throws IOException
	{
		this (getFileByName (propFileName), cmdArgs);
	}
	
	public static ApplicationProperties create (String propFileName, String[] cmdArgs) throws IOException
	{
		if (AppPropsMap == null) {
			AppPropsMap = new HashMap<String, ApplicationProperties> ();
		}
		if (propFileName == null) {
			propFileName = "";
		}
		String mapKey = buildKey (propFileName, cmdArgs);
		ApplicationProperties props = AppPropsMap.get (mapKey);
		if (props == null) {
			props = new ApplicationProperties (propFileName, cmdArgs);
			CurAppProps = props;
			AppPropsMap.put (mapKey, props);
		}
		return props;
	}
	
	public static ApplicationProperties create (String propFileName, CommandLineArguments cmdArgs) throws IOException
	{
		if (AppPropsMap == null) {
			AppPropsMap = new HashMap<String, ApplicationProperties> ();
		}
		if (propFileName == null) {
			propFileName = "";
		}
		String mapKey = buildKey (propFileName, cmdArgs != null ? cmdArgs.getArgs() : null);
		ApplicationProperties props = AppPropsMap.get (mapKey);
		if (props == null) {
			props = new ApplicationProperties (propFileName, cmdArgs);
			CurAppProps = props;
			AppPropsMap.put (mapKey, props);
		}
		return props;
	}
	
	public static ApplicationProperties create (String propFileName) throws IOException
	{
		return create (propFileName, (String[]) null);
	}
	
	private static String buildKey (String propFileName, String[] cmdArgs)
	{
		String key = propFileName + "-:-";
		if (cmdArgs == null) {
			key += Strings.concat (cmdArgs, " ");
		}
		return key;
	}
	
	public static ApplicationProperties loadProperties (String propFile, String[] args, Class<?> cls) 
	{
		ApplicationProperties props = null;
		String propFilePath = (propFile != null ? (new File (propFile)).getAbsolutePath () : null);
		try {
			StdoutLogger.logInfo ("Trying to read properties file: " + propFilePath);
			props = ApplicationProperties.create (propFilePath, args);
		}
		catch (IOException ex) {
			System.err.println ("Properties file " + propFilePath + " not found");
		}
		ApplicationProperties.processStaticProperties (cls);
		return props;
	}
	
	public static ApplicationProperties loadProperties (String propFile, CommandLineArguments args, Class<?> cls) 
	{
		ApplicationProperties props = null;
		String propFilePath = (propFile != null ? (new File (propFile)).getAbsolutePath () : null);
		try {
			StdoutLogger.logInfo ("Trying to read properties file: " + propFilePath);
			props = ApplicationProperties.create (propFilePath, args);
		}
		catch (IOException ex) {
			System.err.println ("Properties file " + propFilePath + " not found");
		}
		ApplicationProperties.processStaticProperties (cls);
		return props;
	}
	
	// -------------------------------------------------------------------------------------
	public File getPropertyFile ()
	{
		return mPropFile;
	}
	
	public static ApplicationProperties getCurrentInstance ()
	{
		return CurAppProps;
	}
	
	public CommandLineArguments getCommandLineArgs ()
	{
		return mCmdArgs;
	}

	// =============================================================================================
	public List<Field> getStaticFields ()
	{
		return mFields;
	}

	// ----------------------------------------------------------------------------------
	public static String getPropertyValue (String propName, String envVarName)
	{
		String val = System.getProperty (propName);
		if (val != null) {
			if (! Strings.isEmpty (val)) {
				StdoutLogger.logInfo ("    -D" + propName + "=" + val);
			}
		}
		if (val == null && ! Strings.isEmpty (envVarName)) {
			val = System.getenv (envVarName);
			StdoutLogger.logInfo ("    " + envVarName + " = " + val);
		}
		return val;
	}
	
	public String getProperty (String propName, String envVarName, String cmdName)
	{
		String val = null;
		if (mCmdArgs != null && !Strings.isEmpty(cmdName)) {
			val = mCmdArgs.getArgument (cmdName);
			StdoutLogger.logInfo ("    " + cmdName + " = " + val);
		}
		if (val == null) {
			val = getPropertyValue (propName, envVarName);
		}
		if (val == null) {
			val = getProperty (propName);
			StdoutLogger.logInfo ("    value = " + val);
		}
		return val;
	}
	
	// ---
	public String getStringProperty (String propName)
	{
		return getProperty (propName);
	}
	
	public String getStringProperty (String propName, String defaultValue)
	{
		return getProperty (propName, defaultValue);
	}
	
	public String getStringProperty (String propName, String envVarName, String cmdName, String defaultValue)
	{
		String val = getProperty (propName, envVarName, cmdName);
		return (val != null ? val : defaultValue);
	}
	
	// ---
	protected Integer getIntProperty (String propName, String envVarName, String cmdName)
	{
		String val = getProperty (propName, envVarName, cmdName);
		if (val != null) {
			try {
				Integer ival = Integer.parseInt (val);
				return ival;
			}
			catch (NumberFormatException ex) {}
		}
		return null;
	}

	public Integer getIntProperty (String propName)
	{
		String val = getProperty (propName);
		if (val != null) {
			try {
				Integer ival = Integer.parseInt (val);
				return ival;
			}
			catch (NumberFormatException ex) {}
		}
		return null;
	}
	
	public int getIntProperty (String propName, int defaultValue)
	{
		Integer ival = getIntProperty (propName);
		if (ival == null) {
			ival = defaultValue;
		}
		return ival;
	}
	
	private Boolean toBoolean (String val)
	{
		if (TRUE.equalsIgnoreCase (val) || "1".equals (val)) {
			return true;
		}
		if (FALSE.equalsIgnoreCase (val) || "0".equals (val)) {
			return false;
		}
		return null;
	}
	
	protected Boolean getBooleanProperty (String propName, String envVarName, String cmdName)
	{
		String val = null;
		if (mCmdArgs != null && !Strings.isEmpty (cmdName)) {
			if (mCmdArgs.containsKey (cmdName)) {
				Boolean rval = toBoolean (mCmdArgs.getArgument (cmdName));
				if (rval != null) {
					StdoutLogger.logInfo ("    " + cmdName + " = " + rval);
					return rval;
				}
				StdoutLogger.logInfo ("    " + cmdName);
				return true;
			}
		}
		String sysProp = System.getProperty (propName);
		if (sysProp != null) {
			val = sysProp;
			if (val.equals("")) {
				val = "true";
			}
			StdoutLogger.logInfo ("    -D" + propName + "=" + val);
		}
		if (val == null && ! Strings.isEmpty (envVarName)) {
			val = System.getenv (envVarName);
			StdoutLogger.logInfo ("    " + envVarName + " = " + val);
		}
		if (val == null) {
			val = getProperty (propName);
			StdoutLogger.logInfo ("    value = " + val);
		}
		return toBoolean (val);
	}
	
	public Boolean getBooleanProperty (String propName)
	{
		String val = getProperty (propName);
		if (val != null) {
			if (TRUE.equalsIgnoreCase (val) || "1".equals (val)) {
				return true;
			}
			if (FALSE.equalsIgnoreCase (val) || "0".equals (val)) {
				return false;
			}
		}
		return null;
	}
	
	public boolean getBooleanProperty (String propName, boolean defaultValue)
	{
		Boolean bval = getBooleanProperty (propName);
		if (bval == null) {
			bval = defaultValue;
		}
		return bval;
	}
	
	protected Long getLongProperty (String propName, String envVarName, String cmdName)
	{
		String val = getProperty (propName, envVarName, cmdName);
		if (val != null) {
			try {
				Long lval = Long.parseLong (val);
				return lval;
			}
			catch (NumberFormatException ex) {}
		}
		return null;
	}
	
	public Long getLongProperty (String propName)
	{
		String val = getProperty (propName);
		if (val != null) {
			try {
				Long lval = Long.parseLong (val);
				return lval;
			}
			catch (NumberFormatException ex) {}
		}
		return null;
	}
	
	protected Byte getByteProperty (String propName, String envVarName, String cmdName)
	{
		String val = getProperty (propName, envVarName, cmdName);
		if (val != null) {
			try {
				Byte bval = Byte.parseByte(val);
				return bval;
			}
			catch (NumberFormatException ex) {}
		}
		return null;
	}
	
	public Byte getByteProperty (String propName)
	{
		String val = getProperty (propName);
		if (val != null) {
			try {
				Byte bval = Byte.parseByte(val);
				return bval;
			}
			catch (NumberFormatException ex) {}
		}
		return null;
	}
	
	protected Character getCharProperty (String propName, String envVarName, String cmdName)
	{
		String val = getProperty (propName, envVarName, cmdName);
		if (val != null && ! val.isEmpty()) {
			Character cval = Character.valueOf (val.charAt (0));
			return cval;
		}
		return null;
	}
	
	public Character getCharProperty (String propName)
	{
		String val = getProperty (propName);
		if (val != null && ! val.isEmpty()) {
			Character cval = Character.valueOf (val.charAt (0));
			return cval;
		}
		return null;
	}
	
	public List<String> getPropertyNames ()
	{
		ArrayList<String> names = new ArrayList<String> ();
		for (Object key : keySet ()) {
			names.add ((String) key);
		}
		return names;
	}

	@Override
	public synchronized Object setProperty (String key, String value)
	{
		String existVal = getProperty (key);
		if (existVal == null) {
			if (value == null) {
				return existVal;
			}
		}
		else if (existVal.equals (value)) {
			return existVal;
		}
		mUpdated = true;
		return super.setProperty (key, value);
	}

	@Override
	public synchronized void clear ()
	{
		if (size () > 0) {
			mUpdated = true;
		}
		super.clear ();
	}

	@Override
	public synchronized Object put (Object key, Object value)
	{
		if (isEmptyOrComment (key)) {
			return null;
		}
		Object existVal = get (key);
		if (existVal == null) {
			if (value == null) {
				return existVal;
			}
		}
		else if (existVal.equals (value)) {
			return existVal;
		}
		mUpdated = true;
		// return super.put (key, value);
		super.put (key, value);
		return this.setProperty (key.toString (), value.toString ());
	}

	private boolean isEmptyOrComment(Object key) 
	{
		String skey = (String) key;
		if (skey == null || skey.isEmpty() || skey.matches ("^\\s*[;#].*")) {
			return true;
		}
		return false;
	}

	@Override
	public synchronized void putAll (Map<? extends Object, ? extends Object> objs)
	{
		objs.forEach ((key, value) -> put (key, value));
	}

	@Override
	public synchronized boolean remove (Object key, Object value)
	{
		boolean wasRemoved = super.remove (key, value);
		if (wasRemoved) {
			mUpdated = true;
		}
		return wasRemoved;
	}

	@Override
	public synchronized Object remove (Object key)
	{
		if (containsKey (key)) {
			mUpdated = true;
		}
		return super.remove (key);
	}

	@Override
	public synchronized boolean replace (Object key, Object oldValue, Object newValue)
	{
		boolean wasReplaced = super.replace (key, oldValue, newValue);
		if (wasReplaced) {
			mUpdated = true;
		}
		return wasReplaced;
	}

	@Override
	public synchronized Object replace (Object key, Object value)
	{
		if (containsKey (key)) {
			mUpdated = true;
		}
		return super.replace (key, value);
	}
	
	public void save ()
	{
		try {
			FileOutputStream out = new FileOutputStream (mPropFile);
			store (out, null);
			out.close ();
		}
		catch (IOException e) {
			e.printStackTrace ();
		}
	}

	@Override
	protected void finalize () throws Throwable
	{
		if (mUpdated && mPropFile != null) {
			save ();
		}
		super.finalize ();
	}

	// ------------------------------------------------------------------------------------------
	public static boolean isIntegerType (Class<?> type)
	{
		if (type.isPrimitive ()) {
			return type.getName ().equals ("int");
		}
		return (type == Integer.class);
	}
	
	public static boolean isBooleanType (Class<?> type)
	{
		if (type.isPrimitive ()) {
			return type.getName ().equals ("boolean");
		}
		return (type == Boolean.class);
	}
	
	public static boolean isByteType (Class<?> type)
	{
		if (type.isPrimitive ()) {
			return type.getName ().equals ("byte");
		}
		return (type == Byte.class);
	}
	
	public static boolean isCharType (Class<?> type)
	{
		if (type.isPrimitive ()) {
			return type.getName ().equals ("char");
		}
		return (type == Character.class);
	}
	
	public static boolean isLongType (Class<?> type)
	{
		if (type.isPrimitive ()) {
			return type.getName ().equals ("long");
		}
		return (type == Long.class);
	}
	
	public static boolean isStringType (Class<?> type)
	{
		if (type.isPrimitive ()) {
			return false;
		}
		return (type == String.class);
	}
	
	public static boolean isStringArrayType (Class<?> type)
	{
		return (type == String[].class);
	}
	
	public static boolean isIntArrayType (Class<?> type)
	{
		return (type == int[].class);
	}
	
	public static boolean isIntegerArrayType (Class<?> type)
	{
		return (type == Integer[].class);
	}
	
	public void processStaticSettingProperties (Class<?> cls)
	{
		if (cls != null && !processedClasses.contains (cls)) {
			processedClasses.add (cls);
			StdoutLogger.logInfo ("Processing static properties for " + cls.getName() + " {");
			Field[] fields = cls.getDeclaredFields();
			if (fields != null && fields.length > 0) {
				for (Field field : fields) {
					if (field.isAnnotationPresent (SettingProperty.class) && Modifier.isStatic (field.getModifiers ())) {
						processField (field);
					}
				}
			}
			StdoutLogger.logInfo ("}");
		}
	}
	
	public void initObjectAttributes (Object obj)
	{
		Class<? extends Object> cls = obj.getClass();
		if (cls != null) {
			Field[] fields = cls.getDeclaredFields();
			if (fields != null && fields.length > 0) {
				for (Field field : fields) {
					if (field.isAnnotationPresent (SettingProperty.class)) {
						processField (field, obj);
					}
				}
			}
		}
	}

	private static void setDefaultValues (Class<?> cls) 
	{
		if (cls != null && !defValuedClasses.contains (cls)) {
			defValuedClasses.add (cls);
			StdoutLogger.logInfo ("Processing default static properties for " + cls.getName() + " {");
			Field[] fields = cls.getDeclaredFields();
			if (fields != null && fields.length > 0) {
				for (Field field : fields) {
					if (field.isAnnotationPresent (SettingProperty.class) && Modifier.isStatic (field.getModifiers ())) {
						setDefaultValue  (field);
					}
				}
			}
			StdoutLogger.logInfo ("}");
		}
	}
	
	private static void setDefaultValue (Field field) 
	{
		SettingProperty ann = field.getAnnotation (SettingProperty.class);
		String envVarName = ann.envVariableName ();
		String defVal = getPropertyValue (field.getDeclaringClass ().getName () + "." + field.getName (), envVarName);
		if (defVal == null) {
			defVal = ann.defaultValue();;
		}
		Class<?> fieldType = field.getType ();
		try {
			field.setAccessible (true);
			if (isIntegerType (fieldType)) {
				if (defVal != null) {
					try {
						field.set (null, Integer.parseInt (defVal));
					}
					catch (NumberFormatException ex) {}
				}
			}
			else if (isStringType (fieldType)) {
				if (defVal != null) {
					defVal = unQuote (defVal);
					field.set (null, defVal);
				}
			}
			else if (isBooleanType (fieldType)) {
				if (defVal != null) {
					field.set (null, Boolean.valueOf (defVal));
				}
			}
			else if (isLongType (fieldType)) {
				if (defVal != null) {
					field.set (null, Long.valueOf (defVal));
				}
			}
			else if (isByteType (fieldType)) {
				if (defVal != null) {
					field.set (null, Byte.valueOf (defVal));
				}
			}
			else if (isCharType (fieldType)) {
				if (defVal != null) {
					field.set (null, Character.valueOf (defVal.charAt (0)));
				}
			}
		}
		catch (SecurityException e) {
			// e.printStackTrace ();
		}
		catch (IllegalArgumentException | IllegalAccessException e) {
			// e.printStackTrace ();
		}
	}
	
	// -------------------------------------------------------------------------
	private class FieldValue
	{
		private Field field;
		private CommandLineArguments cmdArgs;
		private SettingProperty ann;
		private String fullFieldName;
		
		public FieldValue (Field field, CommandLineArguments cmdArgs)
		{
			super ();
			this.field = field;
			this.cmdArgs = cmdArgs;
			ann = field.getAnnotation (SettingProperty.class);
			fullFieldName = field.getDeclaringClass ().getName () + "." + field.getName ();
		}
		
		public FieldValue (Field field)
		{
			this (field, null);
		}
		
		public String getStringValue ()
		{
			String val = null;
			if (cmdArgs != null) {
				String cmdName = ann.cmdLineName ();
				if (! Strings.isEmpty (cmdName)) {
					val = cmdArgs.getArgument (cmdName);
					StdoutLogger.logInfo ("    " + cmdName + " = " + val);
				}
			}
			if (val == null) {
				String propName = getFullName ();
				String envVarName = ann.envVariableName ();
				val = getPropertyValue (propName, envVarName);
				if (val == null) {
					val = ApplicationProperties.this.getProperty (propName);
					StdoutLogger.logInfo ("    value = " + val);
				}
				if (val == null) {
					val = ann.defaultValue ();
				}
			}
			if (!Strings.isEmpty (val) && needResolveValue  ()) {
				val = resolveValue (val);
				StdoutLogger.logInfo ("    resolved value = " + val);
			}
			return val;
		}
		
		public String[] getStringArrayValue ()
		{
			String[] val = null;
			if (cmdArgs != null) {
				String cmdName = ann.cmdLineName ();
				if (! Strings.isEmpty (cmdName)) {
					val = cmdArgs.getArguments (cmdName);
					if (val != null) {
						Stream<String> s = Arrays.stream (val)
											.map (arg -> arg.split (ann.listDelimiter ()))
											.flatMap (Arrays::stream);
						if (ann.distinct ()) {
							s = s.distinct ();
						}
						List<String> list = s.collect (Collectors.toList ());
						val = list.toArray (new String [list.size()]);
					}
					StdoutLogger.logInfo ("    " + cmdName + " = " + val);
				}
			}
			if (val == null) {
				String propName = getFullName ();
				String envVarName = ann.envVariableName ();
				String strval = getPropertyValue (propName, envVarName);
				if (strval == null) {
					strval = ApplicationProperties.this.getProperty (propName);
					StdoutLogger.logInfo ("    value = " + strval);
				}
				if (strval == null) {
					strval = ann.defaultValue ();
				}
				if (! Strings.isEmpty(strval)) {
					val = strval.split (ann.listDelimiter());
				}
			}
			if (val != null && needResolveValue  ()) {
				for (int i=0; i<val.length; i++) {
					val [i] = resolveValue (val [i]);
				}
				StdoutLogger.logInfo ("    resolved value = {" + Arrays.stream (val).collect(Collectors.joining(", ")) + "}");
			}
			return val;
		}
		
		public int[] getIntArrayValue ()
		{
			int [] val = null;
			String[] strArr = getStringArrayValue ();
			if (strArr != null) {
				int size = strArr.length;
				val = new int [size];
				for (int i=0; i<size; i++) {
					try {
						val [i] = Integer.parseInt (strArr [i]);
					}
					catch (NumberFormatException ex) {
						System.err.println ("Error: failed to convert '" + strArr [i] + "' to integer");
					} 
				}
			}
			return val;
		}
		
		public Integer[] getIntegerArrayValue ()
		{
			int[] data = getIntArrayValue ();
			Integer[] res = null;
			if (data != null) {
				res = Arrays.stream (data).boxed ().toArray (Integer[]::new);
			}
			return res;
		}
		
		public Boolean getBooleanValue ()
		{
			String val = null;
			if (cmdArgs != null) {
				String cmdName = ann.cmdLineName ();
				if (! Strings.isEmpty (cmdName)) {
					if (mCmdArgs.containsKey (cmdName)) {
						Boolean rval = toBoolean (cmdArgs.getArgument (cmdName));
						if (rval != null) {
							StdoutLogger.logInfo ("    " + cmdName + " = " + rval);
							return rval;
						}
						StdoutLogger.logInfo ("    " + cmdName);
						return true;
					}
				}
			}
			String propName = getFullName ();
			String sysProp = System.getProperty (propName);
			if (sysProp != null) {
				val = sysProp;
				if (val.equals("")) {
					val = "true";
				}
				StdoutLogger.logInfo ("    -D" + propName + "=" + val);
			}
			if (val == null) {
				String envVarName = ann.envVariableName ();
				if (! Strings.isEmpty (envVarName)) {
					val = System.getenv (envVarName);
					StdoutLogger.logInfo ("    " + envVarName + " = " + val);
				}
			}
			if (val == null) {
				val = getProperty (propName);
				StdoutLogger.logInfo ("    value = " + val);
			}
			if (val == null) {
				val = ann.defaultValue ();
			}
			if (!Strings.isEmpty (val) && needResolveValue  ()) {
				val = resolveValue (val);
				StdoutLogger.logInfo ("    resolved value = " + val);
			}
			return toBoolean (val);
		}
		
		public Object getValue ()
		{
			Object res = null;
			Class<?> fieldType = field.getType ();
			if (isBooleanType (fieldType)) {
				return getBooleanValue ();
			}
			if (fieldType.isArray()) {
				if (isStringArrayType (fieldType)) {
					return getStringArrayValue ();
				}
				else if (isIntArrayType (fieldType)) {
					return getIntArrayValue ();
				}
				else if (isIntegerArrayType (fieldType)) {
					return getIntegerArrayValue ();
				}
			}
			else {
				String val = getStringValue ();
				if (isStringType (fieldType)) {
					res = (val != null ? unQuote (val.trim ()) : val);
				}
				else if (isIntegerType (fieldType)) {
					if (val != null) {
						try {
							res = Integer.parseInt (val);
						}
						catch (NumberFormatException ex) {}
					}
				}
				else if (isLongType (fieldType)) {
					if (val != null) {
						try {
							res = Long.parseLong (val);
						}
						catch (NumberFormatException ex) {}
					}
				}
				else if (isCharType (fieldType)) {
					if (val != null && ! val.isEmpty()) {
						res = Character.valueOf (val.charAt (0));
					}
				}
				else if (isByteType (fieldType)) {
					if (val != null) {
						try {
							res = Byte.parseByte (val);
						}
						catch (NumberFormatException ex) {}
					}
				}
			}

			return res;
		}

		private final Pattern EnvVariable = Pattern.compile ("\\$\\{([^\\}\\\\s]+)\\}");
		private String resolveValue (String val) 
		{
			Matcher m = EnvVariable.matcher (val);
			boolean replace = m.find ();
			while (replace) {
				String varName = m.group (1);
				int nextStart = m.start() + 1;
				String varValue = System.getenv (varName);
				if (varValue != null) {
					val = val.replace ("${" + varName + "}", varValue);
				}
				m = EnvVariable.matcher (val);
				replace = m.find (nextStart);
			}
			return val;
		}

		private boolean needResolveValue () 
		{
			return ann.resolveEnvironmentVarName ();
		}

		public String getFullName () 
		{
			return fullFieldName;
		}
		
		
	}
	// -------------------------------------------------------------------------
	
	private void processField (Field field, Object obj) 
	{
		FieldValue fv = new FieldValue (field, mCmdArgs);
		StdoutLogger.logInfo ("   " + field.getName () + " { ");
		try {
			field.setAccessible (true);
			Object value = fv.getValue ();
			if (value != null) {
				field.set (obj, value);
			}
			StdoutLogger.logInfo ("  } = " + field.get (obj));
			mFields.add (field);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			StdoutLogger.logError ("failed to process field " + fv.getFullName (), e);
		}
	}
	
	private void processField_old (Field field, Object obj) 
	{
		SettingProperty ann = field.getAnnotation (SettingProperty.class);
		String defVal = ann.defaultValue();
		String envVarName = ann.envVariableName ();
		String cmdName = ann.cmdLineName();
		Class<?> fieldType = field.getType ();
		try {
			StdoutLogger.logInfo ("   " + field.getName () + " { ");
			field.setAccessible (true);
			String fullFieldName = field.getDeclaringClass ().getName () + "." + field.getName ();
			if (isIntegerType (fieldType)) {
				Integer value = getIntProperty (fullFieldName, envVarName, cmdName);
				if (value == null && defVal != null) {
					try {
						value = Integer.parseInt (defVal);
					}
					catch (NumberFormatException ex) {}
				}
				if (value != null) {
					field.set (null, value);
				}
			}
			else if (isStringType (fieldType)) {
				String value = getStringProperty (fullFieldName, envVarName, cmdName, defVal);
				if (value != null) {
					value = unQuote (value.trim ());
					field.set (null, value);
				}
			}
			else if (isBooleanType (fieldType)) {
				Boolean value = getBooleanProperty (fullFieldName, envVarName, cmdName);
				if (value == null && defVal != null) {
					value = Boolean.valueOf (defVal);
				}
				if (value != null) {
					field.set (null, value);
				}
			}
			else if (isLongType (fieldType)) {
				Long value = getLongProperty (fullFieldName, envVarName, cmdName);
				if (value == null && defVal != null) {
					value = Long.valueOf(defVal);
				}
				if (value != null) {
					field.set (null, value);
				}
			}
			else if (isByteType (fieldType)) {
				Byte value = getByteProperty (fullFieldName, envVarName, cmdName);
				if (value == null && defVal != null) {
					value = Byte.valueOf (defVal);
				}
				if (value != null) {
					field.set (null, value);
				}
			}
			else if (isCharType (fieldType)) {
				Character value = getCharProperty (fullFieldName, envVarName, cmdName);
				if (value == null && defVal != null) {
					value = Character.valueOf (defVal.charAt (0));
				}
				if (value != null) {
					field.set (null, value);
				}
			}
			StdoutLogger.logInfo ("  } = " + field.get (null));
			mFields.add(field);
		}
		catch (SecurityException e) {
			// e.printStackTrace ();
		}
		catch (IllegalArgumentException | IllegalAccessException e) {
			// e.printStackTrace ();
		}
	}
	
	private void processField (Field field) 
	{
		processField (field, null);
	}
	
	public static boolean isQuoted (String str)
	{
		if (str != null) {
			return (str.startsWith("\"") && str.endsWith("\"") && str.length() > 1);
		}
		return false;
	}
	
	
	public static String unQuote (String str)
	{
		if (isQuoted (str)) {
			str = str.substring (1, str.length () - 1);
		}
		return str;
	}
	
	private HashSet<Class<?>> processedClasses = new HashSet<> ();
	private static HashSet<Class<?>> defValuedClasses = new HashSet<> ();
	
	public static void processStaticProperties(Class<?> cls) 
	{
		ApplicationProperties props = ApplicationProperties.getCurrentInstance ();
		if (props != null) {
			props.processStaticSettingProperties (cls);
			Class<?> superClass = cls.getSuperclass ();
			while (superClass != null) {
				props.processStaticSettingProperties (superClass);
				superClass = superClass.getSuperclass ();
			}
		}
		else {
			ApplicationProperties.setDefaultValues (cls);
			Class<?> superClass = cls.getSuperclass ();
			while (superClass != null) {
				ApplicationProperties.setDefaultValues (superClass);
				superClass = superClass.getSuperclass ();
			}
		}
	}
	
	public static void initializeObjectAttributes (Object obj)
	{
		ApplicationProperties props = ApplicationProperties.getCurrentInstance ();
		if (props != null) {
			props.initObjectAttributes (obj);
		}
		else {
			ApplicationProperties.setDefaultValues (obj.getClass());
		}
	}
	
	public static List<Field> getAllStaticProperties ()
	{
		ApplicationProperties props = ApplicationProperties.getCurrentInstance ();
		if (props != null) {
			return props.getStaticFields ();
		}
		return null;
	}
	
	private static List<CommandLineArgument> getCommandLineArguments (List<Field> statProps)
	{
		if (statProps != null) {
			List<CommandLineArgument> cmdArgs = new ArrayList<CommandLineArgument> ();
			for (Field field : statProps) {
				SettingProperty ann = field.getAnnotation (SettingProperty.class);
				String defVal = ann.defaultValue();
				String cmdName = ann.cmdLineName();
				Class<?> fieldType = field.getType ();
				if (! Strings.isEmpty(cmdName)) {
					cmdArgs.add (new CommandLineArgument(cmdName, fieldType.getSimpleName(), defVal, ann.documentation()));
				}
			}
			return cmdArgs;
		}
		return null;
	}
	
	public static List<CommandLineArgument> getAllCommandLineArguments ()
	{
		List<Field> statProps = getAllStaticProperties ();
		return getCommandLineArguments (statProps);
	}
	
	public static List<CommandLineArgument> collectCommandLineArguments (Class<?> ... clss)
	{
		List<Field> statProps = collectStaticProperties (clss);
		return getCommandLineArguments (statProps);
	}
	
	public static List<Field> collectStaticProperties (Class<?> ... clss)
	{
		List<Field> statProps = new LinkedList<Field> ();
		for (Class<?> cls : clss) {
			if (cls != null) {
				Field[] fields = cls.getDeclaredFields();
				if (fields != null && fields.length > 0) {
					for (Field field : fields) {
						if (field.isAnnotationPresent (SettingProperty.class) && Modifier.isStatic (field.getModifiers ())) {
							statProps.add (field);
						}
					}
				}
			}
		}
		return statProps;
	}
	
	public static CommandLineArguments getCurrentCommandLineArgument ()
	{
		ApplicationProperties props = ApplicationProperties.getCurrentInstance ();
		if (props != null) {
			return props.getCommandLineArgs ();
		}
		return null;
	}
	
	public static File getCurrentPropertyFile ()
	{
		ApplicationProperties props = ApplicationProperties.getCurrentInstance ();
		if (props != null) {
			return props.getPropertyFile ();
		}
		return null;
	}
	
	// ===============================================================================
	public static class CommandLineArgument
	{
		String argumentName;
		String argumentType;
		String documentation;
		String defaultValue;
		
		public CommandLineArgument(String argumentName, String argumentType, String defaultValue, String documentation) {
			super();
			this.argumentName = argumentName;
			this.argumentType = argumentType;
			this.defaultValue = defaultValue;
			this.documentation = documentation;
		}

		public String getArgumentName() {
			return argumentName;
		}

		public String getArgumentType() {
			return argumentType;
		}

		public String getDefaultValue() {
			return defaultValue;
		}
		
		public String getDocumentation() {
			return documentation;
		}
		
		@Override
		public String toString ()
		{
			String str = String.format("-%s (%s)", argumentName, argumentType);
			if (! Strings.isEmpty (defaultValue)) {
				if ("String".equals(argumentType)) {
					str += " = \"" + defaultValue + "\"";
				}
				else {
					str += " = " + defaultValue;
				}
			}
			if (! Strings.isEmpty(documentation)) {
				str += " - " + documentation;
			}
			return str;
		}
	}
	
}

