package com.sm.javax.utilx;

import java.awt.Color;

import com.sm.javax.langx.Strings;

public class ColorUtils
{
	/**
	 * Creates color from HTML hex code. E.g. #FF0000 for Red, or 808080 for Gray
	 * 
	 * @param colorHexCode
	 * @return
	 */
	public static Color getByHexCode (String colorHexCode)
	{
		if (!Strings.isEmpty (colorHexCode))
		{
			int size = colorHexCode.length ();

			if ((size == 6) || ((size == 7) && (colorHexCode.charAt (0) == '#')))
			{
				if (size == 7)
					colorHexCode = colorHexCode.substring (1);
				try
				{
					int R = Integer.parseInt (colorHexCode.substring (0, 2), 16);
					int G = Integer.parseInt (colorHexCode.substring (2, 4), 16);
					int B = Integer.parseInt (colorHexCode.substring (4, 6), 16);
					return (new Color (R, G, B));
				}
				catch (NumberFormatException ex)
				{
				}
			}
		}

		throw new IllegalArgumentException ("Incorrect color hex code "
				+ colorHexCode);
	}

	public static String getHexCode (Color color)
	{
		return (String.format ("#%02X%02X%02X", color.getRed (), color.getGreen (), color.getBlue ()));
	}
}
