package com.sm.javax.utilx;

public interface BiConsumerX<T, U, E extends Exception> {
	public void accept(T t, U u) throws E;
}
