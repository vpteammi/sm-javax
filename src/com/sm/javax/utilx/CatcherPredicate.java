package com.sm.javax.utilx;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class CatcherPredicate<T, E extends Exception> extends
		ExceptionProcessor<T, E> implements Predicate<T> {

	private final PredicateX<T, E> predicate;

	public CatcherPredicate(PredicateX<T, E> predicate) {
		super();
		this.predicate = predicate;
	}

	public CatcherPredicate(PredicateX<T, E> predicate, Consumer<E> excProcessor) {
		super(excProcessor);
		this.predicate = predicate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean test(T arg0) {
		try {
			return predicate.test(arg0);
		} catch (Exception e) {
			processException(arg0, (E) e);
			return (false);
		}
	}
}
