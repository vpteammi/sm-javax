package com.sm.javax.utilx;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class CollectionUtils {
	public interface Filter<T> {
		boolean accept(T value);
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> filter(Iterable<T> collection, Filter<T> filter) {
		if ((filter == null) || (collection == null))
			return (Collections.EMPTY_LIST);
		ArrayList<T> list = new ArrayList<>();
		for (T item : collection) {
			if (filter.accept(item))
				list.add(item);
		}
		return (list);
	}

	public static <T> boolean has(Iterable<T> collection, Filter<T> filter) {
		if ((filter != null) && (collection != null)) {
			for (T item : collection) {
				if (filter.accept(item))
					return (true);
			}
		}
		return (false);
	}

	public static <T> T find(Collection<T> collection, Predicate<T> filter) {
		return (findOr(collection, filter, null));
	}

	public static <T> T findOr(Collection<T> collection, Predicate<T> filter,
			T notFoundValue) {
		return (collection == null ? null : collection.stream().filter(filter)
				.findAny().orElse(notFoundValue));
	}

	public static <T> Stream<T> stream(Collection<T> collection) {
		return (collection == null ? Stream.empty() : collection.stream());
	}

	public static <T> int size(Collection<T> collection) {
		return (collection == null ? 0 : collection.size());
	}

	public static <K, V> int size(Map<K, V> collection) {
		return (collection == null ? 0 : collection.size());
	}

	public static <T> boolean checkIndex(Collection<T> collection, int index) {
		return ((index >= 0) && (index < size(collection)));
	}

	public static <T> boolean contains(Collection<T> collection, T item) {
		return ((collection != null) && collection.contains(item));
	}

	public static <T> boolean swap(List<T> collection, int index1, int index2) {
		if (CollectionUtils.checkIndex(collection, index1)
				&& CollectionUtils.checkIndex(collection, index2)) {
			T aux = collection.get(index1);
			collection.set(index1, collection.get(index2));
			collection.set(index2, aux);
			return (true);
		}
		return (false);
	}

	public static <U, T extends List<U>> List<U> noNull(T list) {
		return (list == null ? Collections.emptyList() : list);
	}

	public static <U, T extends List<U>> T noEmptyOr(T list, T defaultList) {
		return (isEmpty(list) ? defaultList : list);
	}

	public static <U, T extends Set<U>> T noEmptyOr(T list, T defaultList) {
		return (isEmpty(list) ? defaultList : list);
	}

	public static <U, T extends Collection<U>> Collection<U> noNull(T collection) {
		return (collection == null ? Collections.emptyList() : collection);
	}

	@SuppressWarnings("unchecked")
	public static <U, T extends Iterable<U>> T noNull(T list) {
		return (list == null ? (T) Collections.emptyList() : list);
	}

	public static <T> HashSet<T> add(HashSet<T> set, T v) {
		if (set == null)
			set = new HashSet<>();
		set.add(v);
		return (set);
	}

	public static <T> ArrayList<T> add(ArrayList<T> list, T v) {
		if (list == null)
			list = new ArrayList<>();
		list.add(v);
		return (list);
	}

	public static <B, T extends B> ArrayList<B> addAll(ArrayList<B> list,
			Iterator<T> iterator) {
		if (iterator != null) {
			if (list == null)
				list = new ArrayList<>();
			while (iterator.hasNext()) {
				list.add(iterator.next());
			}
		}
		return (list);
	}

	public static <B, T extends B> List<B> addAll(List<B> list, @SuppressWarnings("unchecked") T... array) {
		if (array != null) {
			if (list == null)
				list = new ArrayList<>();
			for (T item : array) {
				list.add(item);
			}
		}
		return (list);
	}

	public static <B, T1 extends B, T2 extends B> Iterable<B> iterable(
			final Iterable<T1> i1, final Iterable<T2> i2) {
		return (new Iterable<B>() {

			@Override
			public Iterator<B> iterator() {
				return (CollectionUtils.iterator(i1, i2));
			}
		});
	}

	public static <B, T1 extends B, T2 extends B> Iterator<B> iterator(
			final Iterable<T1> i1, final Iterable<T2> i2) {
		return new Iterator<B>() {
			private Iterator<T1> iterator1 = CollectionUtils.iterator(i1);
			private Iterator<T2> iterator2 = CollectionUtils.iterator(i2);

			@Override
			public boolean hasNext() {
				return (iterator1 != null ? iterator1.hasNext() : iterator2
						.hasNext());
			}

			@Override
			public B next() {
				B next = null;
				if (iterator1 != null) {
					next = iterator1.next();
					if (!iterator1.hasNext())
						iterator1 = null;
				} else {
					next = iterator2.next();
				}
				return next;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public static <B, T extends B, C extends Collection<B>> C addAll(C c,
			Iterable<T> i) {
		if ((c != null) && (i != null)) {
			for (T item : i) {
				c.add(item);
			}
		}

		return (c);
	}

	@SafeVarargs
	public static <B, T extends B, C extends Collection<B>> C addAll(C c,
			T... array) {
		if ((c != null) && (array != null)) {
			for (T item : array) {
				c.add(item);
			}
		}

		return (c);
	}

	public static <B, T1 extends B, T2 extends B> Collection<B> combine(
			final Collection<T1> c1, final Collection<T2> c2) {
		return (new AbstractCollection<B>() {
			@Override
			public Iterator<B> iterator() {
				return CollectionUtils.iterator(c1, c2);
			}

			@Override
			public int size() {
				return CollectionUtils.size(c1) + CollectionUtils.size(c2);
			}
		});
	}

	@SuppressWarnings("unchecked")
	public static <T> Iterator<T> iterator(Iterable<T> iterable) {
		return (iterable == null ? Collections.EMPTY_LIST.iterator() : iterable
				.iterator());
	}

	public static <K, V> Map<K, V> noNull(Map<K, V> map) {
		return (isEmpty(map) ? Collections.emptyMap() : map);
	}

	public static <V> Set<V> noNull(Set<V> set) {
		return (isEmpty(set) ? Collections.emptySet() : set);
	}

	public static <T> boolean isEmpty(Collection<T> collection) {
		return (size(collection) == 0);
	}

	public static <K, V> boolean isEmpty(Map<K, V> collection) {
		return (size(collection) == 0);
	}

	public static <S, T> List<T> toList(S[] src,
			Function<? super S, T> transformation) {
		return toList(src, transformation, false);
	}

	public static <S, T> List<T> toList(S[] src,
			Function<? super S, T> transformation, boolean skipNulls) {
		if (src != null) {
			ArrayList<T> target = new ArrayList<>();

			for (S s : src) {
				T t = transformation.apply(s);
				if (!skipNulls || (t != null)) {
					target.add(t);
				}
			}

			return (target);
		}

		return (null);
	}

	public static <S, T> List<T> toList(Collection<S> src,
			Function<? super S, T> transformation, boolean skipNulls) {
		if (src != null) {
			ArrayList<T> target = new ArrayList<>();

			for (S s : src) {
				T t = transformation.apply(s);
				if (!skipNulls || (t != null)) {
					target.add(t);
				}
			}

			return (target);
		}

		return (null);
	}

	public static <S, T> List<T> toList(Collection<S> src,
			Function<? super S, T> transformation) {
		return (toList(src, transformation, false));
	}

	public static <T, C extends Collection<T>> C nullForEmpty(C c) {
		return (size(c) == 0) ? null : c;
	}

	public static <T> T findFirst(Collection<T> collection) {
		return (isEmpty(collection) ? null : collection.stream().findFirst()
				.get());
	}

	public static <T> T findAny(Collection<T> collection) {
		return (isEmpty(collection) ? null : collection.stream().findAny()
				.get());
	}
}
