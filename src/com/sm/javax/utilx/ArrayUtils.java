package com.sm.javax.utilx;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import com.sm.javax.langx.Objects;

public class ArrayUtils {
	public static final int[] EMPTY_INTEGER_ARRAY = {};
	public static final Object[] EMPTY_OBJECT_ARRAY = {};
	public static final String[] EMPTY_STRING_ARRAY = {};

	public static boolean isEmpty(char[] array) {
		return ((array == null) || array.length == 0);
	}

	public static Object[] noNull(Object[] arr) {
		return (isEmpty (arr) ? Objects.EMPTY_ARRAY : arr);
	}

	public static <T> boolean isEmpty(T[] array) {
		return (size(array) == 0);
	}

	public static <T> int size(T[] array) {
		return ((array == null) ? 0 : array.length);
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] newArray(Class<T> cls, int size) {
		return (T[]) (Array.newInstance(cls, size));
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] toSize(T[] array, int size) {
		if (array == null) {
			throw new IllegalArgumentException("array is null");
		}
		if (size < 0) {
			throw new IllegalArgumentException("size < 0");
		}
		if (array.length == size) {
			return (array);
		}
		return (T[]) (Array.newInstance(array.getClass().getComponentType(),
				size));
	}

	public static <T, S extends T> T[] concat(T[] array, S value) {
		if (value == null)
			return (array);

		if (array == null) {
			@SuppressWarnings("unchecked")
			Class<S> valueClass = (Class<S>) value.getClass();
			@SuppressWarnings("unchecked")
			T[] newArray = (S[]) Array.newInstance(valueClass, 1);
			newArray[0] = value;
			return (newArray);
		} else {
			T[] newArray = Arrays.copyOf(array, array.length + 1);
			newArray[array.length] = value;
			return (newArray);
		}
	}

	public static <T, S extends T> T[] concat(T[] array,
			@SuppressWarnings("unchecked") S... value) {
		if ((value == null) || (value.length == 0))
			return (array);

		if ((array == null) || (array.length == 0)) {
			return (value);
		} else {
			int arraySize = array.length;
			int valueSize = value.length;
			T[] newArray = Arrays.copyOf(array, arraySize + valueSize);
			System.arraycopy(value, 0, newArray, arraySize, valueSize);
			return (newArray);
		}
	}

	public static <T> T[] cuttail(T[] array, int newSize) {
		return (subarray(array, 0, newSize));
	}

	public static <T> T[] cuthead(T[] array, int fromIncl) {
		if (array == null) {
			throw new IllegalArgumentException("array is null");
		}
		return (subarray(array, fromIncl, array.length));
	}

	public static <T> T[] subarray(T[] array, int fromIncl, int toExcl) {
		if (array == null) {
			throw new IllegalArgumentException("array is null");
		} else if ((fromIncl < 0) || (toExcl > array.length)) {
			throw new IndexOutOfBoundsException();
		}

		return (Arrays.copyOfRange(array, fromIncl, toExcl));
	}

	public static <T> List<T> asList(T[] arrayObject) {
		if (arrayObject == null) {
			return (Collections.emptyList());
		} else {
			return (Arrays.asList(arrayObject));
		}
	}

	public static <T> ArrayList<T> toListNoNull(T[] arrayObject) {
		if (arrayObject == null) {
			return (new ArrayList<T>());
		} else {
			ArrayList<T> list = new ArrayList<T>(arrayObject.length);
			for (T obj : arrayObject) {
				list.add(obj);
			}
			return (list);
		}
	}

	public static <T> ArrayList<T> toList(
			@SuppressWarnings("unchecked") T... items) {
		ArrayList<T> list = new ArrayList<T>();

		for (T item : items) {
			list.add(item);
		}

		return (list);
	}

	public static <T> T[] sort(Collection<? extends T> collection,
			Comparator<? super T> comparator, T[] target) {
		T[] array = collection.toArray(target);
		Arrays.sort(array, comparator);
		return (array);
	}

	public static int[] toArray(Collection<Integer> collection) {
		int size = CollectionUtils.size(collection);
		int[] array;

		if (size == 0) {
			array = EMPTY_INTEGER_ARRAY;
		} else {
			array = new int[size];

			Iterator<Integer> iterator = collection.iterator();
			for (int i = 0; i < size; i++) {
				array[i] = iterator.next();
			}
		}
		return (array);
	}

	public static Object[] toObjectArray(Object arrayObject) {
		if (arrayObject != null) {
			Class<?> arrayClass = arrayObject.getClass();

			if (arrayClass.isArray()) {
				if (arrayClass.getComponentType().isPrimitive()) {
					if (arrayObject instanceof boolean[]) {
						boolean[] array = (boolean[]) arrayObject;
						Boolean[] objArray = new Boolean[array.length];
						for (int i = 0; i < array.length; i++) {
							objArray[i] = array[i];
						}
						return (objArray);
					}
					if (arrayObject instanceof int[]) {
						int[] array = (int[]) arrayObject;
						Integer[] objArray = new Integer[array.length];
						for (int i = 0; i < array.length; i++) {
							objArray[i] = array[i];
						}
						return (objArray);
					}
					if (arrayObject instanceof long[]) {
						long[] array = (long[]) arrayObject;
						Long[] objArray = new Long[array.length];
						for (int i = 0; i < array.length; i++) {
							objArray[i] = array[i];
						}
						return (objArray);
					}
					if (arrayObject instanceof short[]) {
						short[] array = (short[]) arrayObject;
						Short[] objArray = new Short[array.length];
						for (int i = 0; i < array.length; i++) {
							objArray[i] = array[i];
						}
						return (objArray);
					}
					if (arrayObject instanceof char[]) {
						char[] array = (char[]) arrayObject;
						Character[] objArray = new Character[array.length];
						for (int i = 0; i < array.length; i++) {
							objArray[i] = array[i];
						}
						return (objArray);
					}
					if (arrayObject instanceof byte[]) {
						byte[] array = (byte[]) arrayObject;
						Byte[] objArray = new Byte[array.length];
						for (int i = 0; i < array.length; i++) {
							objArray[i] = array[i];
						}
						return (objArray);
					}
					if (arrayObject instanceof double[]) {
						double[] array = (double[]) arrayObject;
						Double[] objArray = new Double[array.length];
						for (int i = 0; i < array.length; i++) {
							objArray[i] = array[i];
						}
						return (objArray);
					}
					if (arrayObject instanceof float[]) {
						float[] array = (float[]) arrayObject;
						Float[] objArray = new Float[array.length];
						for (int i = 0; i < array.length; i++) {
							objArray[i] = array[i];
						}
						return (objArray);
					}
				}
			}
		}

		return ((Object[]) arrayObject);
	}

	@SuppressWarnings("unchecked")
	public static <S, T> T[] transform(S[] src, T[] target,
			Function<? super S, T> transformation) {
		if (src != null) {
			if (target == null) {
				throw new IllegalArgumentException("target array is null");
			}

			if (src.length != target.length) {
				target = (T[]) Array.newInstance(target.getClass()
						.getComponentType(), src.length);
			}

			int i = 0;
			for (S s : src) {
				target[i++] = transformation.apply(s);
			}

			return (target);
		}

		return (null);
	}

	@SuppressWarnings("unchecked")
	public static <S, T> T[] toArray(Collection<S> src, T[] target,
			Function<? super S, T> transformation) {
		if (src != null) {
			if (target == null) {
				throw new IllegalArgumentException("target array is null");
			}

			int n = src.size();
			if (n != target.length) {
				target = (T[]) Array.newInstance(target.getClass()
						.getComponentType(), n);
			}

			int i = 0;
			for (S s : src) {
				target[i++] = transformation.apply(s);
			}

			return (target);
		}

		return (null);
	}

	public static <T> boolean checkIndex(T[] array, int index) {
		return (index >= 0) && (index < size(array));
	}

	public static boolean checkIndex(List<?> list, int index) {
		return (index >= 0) && (index < CollectionUtils.size(list));
	}

	public static String[] removeElement(String[] array, int ind) {
		if (checkIndex(array, ind)) {
			int size = array.length;
			String [] res = new String [size - 1];
			for (int i=0, k=0; i<size; i++) {
				if (i != ind) {
					res [k++] = array [i];
				}
			}
			return res;
		}
		return null;
	}
	
	public static String[] unshift (String[] array, String elem)
	{
		int size = size (array);
		String [] res = new String [size + 1];
		res [0] = elem;
		if (array != null) {
			for (int i=0; i<array.length; i++) {
				res [i+1] = array [i];
			} 
		}
		return res;
	}

}
