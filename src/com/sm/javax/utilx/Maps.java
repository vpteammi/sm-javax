package com.sm.javax.utilx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

public class Maps {

	public static <K, V, M extends Map<K, V>> M noEmptyOr(M map, M defaultMap) {
		return (isEmpty(map) ? defaultMap : map);
	}

	public static boolean isEmpty(Map<?, ?> map) {
		return ((map == null) || (map.size() == 0));
	}

	public static <K, V> V getOrNull(boolean doGet, Map<K, V> map, K key) {
		return ((doGet && (map != null)) ? map.get(key) : null);
	}

	public static <K, V> V getOrNull(Map<K, V> map, K key) {
		return (getOrNull(true, map, key));
	}

	public static <K, V> V getOrDefault(Map<K, V> map, K key, V defaulValue) {
		V value = getOrNull(true, map, key);
		return ((value == null) ? defaulValue : value);
	}

	@SuppressWarnings("unchecked")
	public static <K, V> Collection<K> keys(Map<K, V> map) {
		return (map == null ? Collections.EMPTY_SET : map.keySet());
	}

	public static <K, V> HashMap<K, V> put(HashMap<K, V> map, K key, V value) {
		if (map == null) {
			map = new HashMap<K, V>();
		}

		map.put(key, value);

		return (map);
	}

	public static <K, V> V remove(HashMap<K, V> map, K key) {
		if (map != null) {
			return (map.remove(key));
		}

		return (null);
	}

	@SuppressWarnings("unchecked")
	public static <K, V> Collection<V> values(Map<K, V> map) {
		return (map == null ? Collections.EMPTY_LIST : map.values());
	}

	public static <T, V> Map<T, V> mapKeys(T[] keyArray,
			Function<? super T, V> valueGetter) {
		HashMap<T, V> map = null;

		if (keyArray != null) {
			map = new HashMap<T, V>();

			for (T key : keyArray) {
				map.put(key, valueGetter.apply(key));
			}
		}

		return (map);
	}

	public static <T, K> Map<K, T> mapValues(T[] valueArray,
			Function<? super T, K> keyGetter) {
		HashMap<K, T> map = null;

		if (valueArray != null) {
			map = new HashMap<K, T>();

			for (T item : valueArray) {
				map.put(keyGetter.apply(item), item);
			}
		}

		return (map);
	}

	public static <T, K, V> Map<K, V> toMap(T[] array,
			Function<? super T, K> keyGetter, Function<? super T, V> valueGetter) {
		HashMap<K, V> map = null;

		if (array != null) {
			map = new HashMap<K, V>();

			for (T item : array) {
				map.put(keyGetter.apply(item), valueGetter.apply(item));
			}
		}

		return (map);
	}

	public static <T, V> Map<T, V> mapKeys(Iterable<T> keyCollection,
			Function<? super T, V> valueGetter) {
		HashMap<T, V> map = null;

		if (keyCollection != null) {
			map = new HashMap<T, V>();

			for (T key : keyCollection) {
				map.put(key, valueGetter.apply(key));
			}
		}

		return (map);
	}

	public static <T, K> Map<K, T> mapValues(Iterable<T> valueCollection,
			Function<? super T, K> keyGetter) {
		return ((valueCollection == null) ? null : addValues(
				new HashMap<K, T>(), valueCollection, keyGetter));
	}

	public static <V, T extends V, K> Map<K, V> addValues(Map<K, V> map,
			Iterable<T> valueCollection, Function<? super T, K> keyGetter) {
		if (valueCollection != null) {
			if (map == null)
				map = new HashMap<K, V>();

			for (T item : valueCollection) {
				map.put(keyGetter.apply(item), item);
			}
		}

		return (map);
	}

	public static <K, V, T> Map<K, V> putAll(Map<K, V> map, Map<K, T> addMap,
			Function<? super T, V> valueGetter) {
		if (addMap != null) {
			if (map == null)
				map = new HashMap<K, V>();
			for (Entry<K, T> entry : addMap.entrySet()) {
				map.put(entry.getKey(), valueGetter.apply(entry.getValue()));
			}
		}
		return (map);
	}

	public static <K, V> Map<K, V> filter(Map<K, V> map,
			Function<? super V, Boolean> filter) {
		if ((filter == null) || (map == null))
			return (map);

		HashMap<K, V> filtered = new HashMap<K, V>();

		for (Entry<K, V> entry : map.entrySet()) {
			if (filter.apply(entry.getValue())) {
				filtered.put(entry.getKey(), entry.getValue());
			}
		}

		return (filtered);
	}

	public static <K, V> Map<V, K> reverse(Map<K, V> map) {
		if (map != null) {
			HashMap<V, K> reversed = new HashMap<V, K>();

			for (Entry<K, V> entry : map.entrySet()) {
				reversed.put(entry.getValue(), entry.getKey());
			}

			return (reversed);
		}

		return (null);
	}

	public static <K, V> Map<V, List<K>> preciseReverse(Map<K, V> map) {
		if (map != null) {
			HashMap<V, List<K>> reversed = new HashMap<V, List<K>>();

			for (Entry<K, V> entry : map.entrySet()) {
				V reverseKey = entry.getValue();
				K reverseValue = entry.getKey();
				List<K> reverseValues = reversed.get(reverseKey);
				if (reverseValues == null) {
					reversed.put(reverseKey,
							Collections.singletonList(reverseValue));
				} else {
					if (reverseValues.size() == 1) {
						reversed.put(reverseKey,
								new ArrayList<K>(reverseValues));
					}
					reverseValues.add(reverseValue);
				}
			}

			return (reversed);
		}

		return (null);
	}

	public static <T, K, V> Map<K, V> toMap(Iterable<T> collection,
			Function<? super T, K> keyGetter, Function<? super T, V> valueGetter) {
		return (toMap(collection, keyGetter, valueGetter, false));
	}

	public static <T, K, V> Map<K, V> toMap(Iterable<T> collection,
			Function<? super T, K> keyGetter,
			Function<? super T, V> valueGetter, boolean skipNulls) {
		HashMap<K, V> map = null;

		if (collection != null) {
			map = new HashMap<K, V>();

			for (T item : collection) {
				V value = valueGetter.apply(item);
				if (!skipNulls || (value != null)) {
					map.put(keyGetter.apply(item), valueGetter.apply(item));
				}
			}
		}

		return (map);
	}

	public static <K, V> boolean containsKey(Map<K, V> map, K key) {
		return ((map != null) && map.containsKey(key));
	}

	public static <K, V> int size(Map<K, V> map) {
		return ((map == null) ? 0 : map.size());
	}
}
