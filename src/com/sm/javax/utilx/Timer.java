package com.sm.javax.utilx;

import java.io.PrintStream;
import java.util.Date;

public class Timer
{
	private Date startTime = TimeUtils.getNow ();
	private final String name;
	
	public Timer (String name)
	{
		this.name = name;
	}

	public long elapsedMSeconds ()
	{
		return (TimeUtils.getNow ().getTime () - startTime.getTime ());
	}
	
	public void printElapsed (PrintStream out)
	{
		out.println (name + ".elapsed=" + elapsedMSeconds () + " msecs");
	}
	
	public void printElapsed ()
	{
		printElapsed (System.out);
	}
}
