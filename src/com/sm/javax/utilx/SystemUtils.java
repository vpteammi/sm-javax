package com.sm.javax.utilx;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sm.javax.langx.Environment;
import com.sm.javax.langx.JavaCommand;

public class SystemUtils {
	private static ArrayList<String> jvmCommand = null;
	private static Class<?> mainClass = null;
	private static List<String> args = null;

	public static List<String> getJvmCommand() {
		if (jvmCommand == null) {
			jvmCommand = new ArrayList<String>();
			jvmCommand.add(Environment.getJavaExecutable());
			for (String jvmArg : ManagementFactory.getRuntimeMXBean()
					.getInputArguments()) {
				jvmCommand.add(jvmArg);
			}
			jvmCommand.add("-cp");
			jvmCommand.add(ManagementFactory.getRuntimeMXBean().getClassPath());
		}

		return (jvmCommand);
	}

	public static void saveApplicationCommand(Class<?> mainClass, String[] args) {
		SystemUtils.mainClass = mainClass;
		SystemUtils.args = Arrays.asList(args);
	}

	public static void restartApplication() throws IOException {
		if ((args == null) || (mainClass == null)) {
			throw new IllegalStateException("Application command was not saved");
		}

		new JavaCommand().inheritClassPath().inheritJvmArgs()
				.mainClass(mainClass).args(args).build().start();
		System.exit(0);
	}
}
