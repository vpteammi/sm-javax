package com.sm.javax.utilx;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.sm.javax.langx.Environment;
import com.sm.javax.langx.Objects;

public class ProcessUtils {

	public static final Pattern JVM_RUNNABLE_COMMAND_EXT = FileUtils.getExtensionMatchPatern("jar");

	public static boolean isRunnableOnJVM(String command) {
		return (FileUtils.nameMatches(command, JVM_RUNNABLE_COMMAND_EXT));
	}

	public static boolean isJavaExecutable(ProcessBuilder processBuider) {
		return (Objects.equals(processBuider.command().get(0), Environment.getJavaExecutable()));
	}

	public static ProcessBuilder processBuilder(List<String> commandLine) {
		if (!CollectionUtils.isEmpty(commandLine)) {
			String command = commandLine.get(0);
			if (isRunnableOnJVM(command)) {
				ArrayList<String> javaCommand = new ArrayList<String>();
				javaCommand.add(Environment.getJavaExecutable());
				javaCommand.add("-jar");
				javaCommand.addAll(commandLine);
				commandLine = javaCommand;
			}

			return new ProcessBuilder(commandLine);
		}

		throw new IllegalArgumentException("command is empty");
	}

	public static ProcessBuilder processBuilder(String... commandLine) {
		if (!ArrayUtils.isEmpty(commandLine)) {
			return (processBuilder(ArrayUtils.toList(commandLine)));
		}

		throw new IllegalArgumentException("command is empty");
	}
}
