package com.sm.javax.utilx;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 * A utility class to acquire Log instances.
 * Emulates the behavior of the org.apache.commons.logging.LogFactory.getLog(),
 * but simply delegates it to theLog.getLog() factory method.
 */
public class LogFactory {

    static Map<String, Log> logs = new TreeMap<String, Log>();

    //private static Log rootLog = getLog("");
    public static int DEFAULT_LEVEL = Level.INFO_INT;
    /** Include the instance name in the log message? */
    private static volatile boolean showLogName = false;
    /**
     * Include the short name (last component) of the logger in the log
     * message. Default to true - otherwise we'll be lost in a flood of
     * messages without knowing who sends them.
     */
    private static volatile boolean showShortName = false;
    /** Include the class/method name in the log message? */
    private static volatile boolean showMethodName = true;

    /** Static initialization*/
    static {
        try {
            String psaPackage = "com.lawson.psa";
            Logger psaLogger = Logger.getLogger(psaPackage);

            // MMV: Do not raise level if it is already set to DEBUG in log4j.properties
            if (psaLogger.getLevel() == null ||
                    DEFAULT_LEVEL < psaLogger.getLevel().toInt()) {
                psaLogger.setLevel(Level.toLevel(DEFAULT_LEVEL));
                LogFactory.getLog(psaPackage).setLevel(DEFAULT_LEVEL);
            }
            showLogName = false; //props.getProperty(SHOW_LOG_NAME_PROPERTY, false);
            showShortName = false; //props.getProperty(SHOW_SHORT_NAME_PROPERTY, false);
            showMethodName = false; //props.getProperty(SHOW_METHOD_NAME_PROPERTY, true);
        } catch (Exception ex) {
        } // ignore all
    }

    private LogFactory() // utility class private constructor
    {
    }

    /**
     * Factory method to return a named logger.
     *
     * @param clazz
     *            Class for which a log name will be derived
     */
    public static Log getLog(Class<?> clazz) {
        return getLog(clazz.getName());
    }

    /**
     * Factory method to return a named logger.
     *
     * @param name Logical name of the <code>Log</code> instance to be returned
     */
    public static Log getLog(String name) {
        return Log.getLog(name);
    }

    public static boolean isShowLogName() {
        return showLogName;
    }

    public static void setShowLogName(boolean showLogName) {
        LogFactory.showLogName = showLogName;
    }

    public static boolean isShowMethodName() {
        return showMethodName;
    }

    public static void setShowMethodName(boolean showMethodName) {
        LogFactory.showMethodName = showMethodName;
    }

    public static boolean isShowShortName() {
        return showShortName;
    }

    public static void setShowShortName(boolean showShortName) {
        LogFactory.showShortName = showShortName;
    }

    public static Map<String, Log> getCategoryMap() {
        return Collections.unmodifiableMap(logs);
    }
}
