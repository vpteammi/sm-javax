package com.sm.javax.utilx;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class CatcherConsumer<T, E extends Exception> extends
		ExceptionProcessor<T, E> implements Consumer<T> {

	private final ConsumerX<T, E> consumer;

	public CatcherConsumer(ConsumerX<T, E> consumer) {
		super();
		this.consumer = consumer;
	}

	public CatcherConsumer(ConsumerX<T, E> consumer, Consumer<E> excProcessor) {
		super(excProcessor);
		this.consumer = consumer;
	}

	public CatcherConsumer(ConsumerX<T, E> consumer,
			BiConsumer<T, E> excProcessor) {
		super(excProcessor);
		this.consumer = consumer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void accept(T t) {
		try {
			consumer.accept(t);
		} catch (Exception e) {
			processException(t, (E) e);
		}
	}
}
