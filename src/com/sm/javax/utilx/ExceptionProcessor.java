package com.sm.javax.utilx;

import java.lang.reflect.InvocationTargetException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.sm.javax.langx.ProgrammingError;

public class ExceptionProcessor<T,E extends Exception> {

	private E exc = null;
	private final BiConsumer<T, E> excProcessor;

	public ExceptionProcessor(BiConsumer<T, E> excProcessor) {
		this.excProcessor = excProcessor;
	}

	public ExceptionProcessor(Consumer<E> excProcessor) {
		this (new BiConsumer<T, E>() {
			@Override
			public void accept(T t, E u) {
				excProcessor.accept(u);
			}
		});
	}

	public ExceptionProcessor() {
		this (new BiConsumer<T, E>() {
			@Override
			public void accept(T t, E u) {
			}
		});
	}

	protected void processException(T culprit, E exc) {
		excProcessor.accept(culprit, exc);
		synchronized (this) {
			this.exc = exc;
		}
	}

	public synchronized void rethrow() throws E {
		if (exc != null)
			throw exc;
	}

	public synchronized <EXC extends Exception> void rethrowAs(
			Class<EXC> excClass) throws EXC {
		if (exc != null) {
			if (excClass.isInstance(exc)) {
				throw excClass.cast(exc);
			} else {
				try {
					throw excClass.getConstructor(Throwable.class).newInstance(
							exc);
				} catch (InstantiationException | IllegalAccessException
						| IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					throw ProgrammingError.shouldNotHappen(e);
				}
			}
		}
	}
}
