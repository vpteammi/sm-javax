package com.sm.javax.utilx;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.AsynchronousChannel;
import java.nio.channels.Channel;
import java.nio.file.Path;

public class SharedAccessDirectory implements Closeable {

	private final static String WRITE_LOCK = ".write";
	private final static String READ_LOCK = ".read";

	private final Path directoryPath;
	private AsynchronousChannel writeChannel;
	private AsynchronousChannel readChannel;

	public SharedAccessDirectory(Path path) throws IOException {
		this.directoryPath = path;
		PathUtils.mkdirs(path);
	}

	private synchronized void lockForRead() {

	}

	private void closeChannel(Channel channel) throws IOException {
		if (channel != null) {
			channel.close();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		close();
		super.finalize();
	}

	@Override
	public void close() throws IOException {
		IOException exception = null;
		try {
			closeChannel(readChannel);
		} catch (IOException e) {
			exception = e;
		}
		try {
			closeChannel(writeChannel);
		} catch (IOException e) {
			exception = e;
		}

		if (exception != null) {
			throw exception;
		}
	}

}
