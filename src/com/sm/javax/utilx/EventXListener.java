package com.sm.javax.utilx;

import java.util.EventListener;

public interface EventXListener<DATA> extends EventListener {
	public void onEvent (EventX<DATA> event);
}
