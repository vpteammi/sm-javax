package com.sm.javax.utilx;

import com.sm.javax.langx.annotation.SetterFor;
import com.sm.javax.xmlx.XMLInstantiator;
import com.sm.javax.xmlx.XMLSerializable;

@XMLSerializable
public class Tuple3 <A,B,C>
{
	public final A _1;
	public final B _2;
	public final C _3;
	
	@XMLInstantiator
	public Tuple3 (@SetterFor ("_1") A _1, @SetterFor ("_2") B _2, @SetterFor ("_3") C _3)
	{
		this._1 = _1;
		this._2 = _2;
		this._3 = _3;
	}

	@Override
	public int hashCode ()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_1 == null) ? 0 : _1.hashCode ());
		result = prime * result + ((_2 == null) ? 0 : _2.hashCode ());
		result = prime * result + ((_3 == null) ? 0 : _3.hashCode ());
		return result;
	}

	@Override
	public boolean equals (Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass () != obj.getClass ())
			return false;
		Tuple3<?,?,?> other = (Tuple3<?,?,?>) obj;
		if (_1 == null)
		{
			if (other._1 != null)
				return false;
		}
		else if (!_1.equals (other._1))
			return false;
		if (_2 == null)
		{
			if (other._2 != null)
				return false;
		}
		else if (!_2.equals (other._2))
			return false;
		if (_3 == null)
		{
			if (other._3 != null)
				return false;
		}
		else if (!_3.equals (other._3))
			return false;
		return true;
	}
	
	public A getFirst () { return _1; }
	public B getSecond () { return _2; }
	public C getThird () { return _3; }

}
