package com.sm.javax.utilx;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnumerationMatcher
{
	private ArrayList<ArrayList<String>> patterns = null;
	private Pattern pattern = null;
	private boolean patternsChanged = false;

	public int count ()
	{
		return (patterns == null ? 0 : patterns.size ());
	}

	public int match (String value)
	{
		Pattern pattern = getPattern ();
		
		if (pattern != null)
		{
			Matcher matcher = getPattern ().matcher (value);

			if (matcher.find ())
			{
				int enumCount = count ();
				int group = 1;

				for (int i = 0; i < enumCount; i++)
				{
					ArrayList<String> values = patterns.get (i);

					if (values != null)
					{
						for (int valueCount = values.size (); valueCount > 0; valueCount--)
						{
							if (matcher.start (group++) != -1)
							{
								return (i);
							}
						}
					}
				}
			}
		}

		return (-1);
	}

	private Pattern getPattern ()
	{
		if (patternsChanged)
		{
			pattern = null;
			
			if (patterns != null)
			{
				StringBuilder buffer = new StringBuilder ();
				boolean first = true;

				for (ArrayList<String> values : patterns)
				{
					if (values != null)
					{
						for (String value : values)
						{
							if (!first)
							{
								buffer.append ('|');
							}
							else
							{
								first = false;
							}
							buffer.append ('(').append (value).append (')');
						}
					}
				}

				if (buffer.length () > 0)
				{
					pattern = Pattern.compile (buffer.toString ());
				}
			}
			
			patternsChanged = false;
		}
		
		return (pattern);
	}
	
	private void add (int id)
	{
		if (id < 0)
			throw new IllegalArgumentException ("enum value id is negative: " + id);

		if (patterns == null)
		{
			patterns = new ArrayList<ArrayList<String>> ();
		}

		int enumCount = count ();

		if (id >= enumCount)
		{
			for (int i = enumCount; i < id; i++)
			{
				patterns.add (null);
			}
			patterns.add (new ArrayList<String> ());
		}
		else if (patterns.get (id) == null)
		{
			patterns.set (id, new ArrayList<String> ());
		}
	}

	public int add (String... patterns)
	{
		int id = count ();
		add (id, patterns);
		return (id);
	}

	public void add (int id, String... patterns)
	{
		add (id);
		ArrayList<String> values = this.patterns.get (id);
		for (String pattern : patterns)
		{
			if (pattern != null)
			{
				values.add (pattern);
				patternsChanged = true;
			}
		}
	}
	
	private static void find (EnumerationMatcher em, String value)
	{
		System.out.println ("find (\"" + value + "\") = " + em.match (value)); 
	}
	
	public static void main (String[] args)
	{
		System.out.println ("find = " + Pattern.compile ("(a.*a)(b.*b)").matcher ("aaa").find ()); 
		EnumerationMatcher em = new EnumerationMatcher ();
		em.add (12, "a.*a", "b.*b", "c.*c");
		em.add (4, "\\(.*\\)", "\\<.*\\>", "\\[.*\\]", "\\{.*\\}");
		find (em, "aaa");
		find (em, "cccccc");
		find (em, "(a.)");
		find (em, "{0}");
		find (em, "0}");
		find (em, "fffff0}");
		find (em, "()");	
		find (em, "[]");	
		find (em, "{}");	
		find (em, "<99999>");	
	}
}
