package com.sm.javax.utilx;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

@SuppressWarnings("serial")
public class Listeners<LISTENER_TYPE> extends
		CopyOnWriteArrayList<LISTENER_TYPE> {

	public static <L> Listeners<L> addListener(Listeners<L> listeners,
			L listener) {
		if (listeners == null) {
			listeners = new Listeners<L>();
		}

		listeners.add(listener);

		return (listeners);
	}

	public static <L> Listeners<L> removeListener(Listeners<L> listeners,
			L listener) {
		if (CollectionUtils.size(listeners) > 0) {
			if (listeners.remove(listener) && (listeners.size() == 0)) {
				listeners = null;
			}
		}
		return (listeners);
	}

	public static <L> void notifyListeners(Listeners<L> listeners,
			Consumer<L> event) {
		if (CollectionUtils.size(listeners) > 0) {
			listeners.forEach(event);
		}
	}
}
