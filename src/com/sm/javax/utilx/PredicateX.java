package com.sm.javax.utilx;

@FunctionalInterface
public interface PredicateX <T, E extends Exception> {
    boolean test(T t) throws E;
}