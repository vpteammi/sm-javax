package com.sm.javax.utilx;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.sm.javax.langx.ProgrammingError;

public class PathUtils {

	private static IOException directoryException(String type, Path directory) {
		return new IOException(String.format("Cannot create %s directory \"%s\"", type, directory));
	}

	public static void mkdirs(Path directory) throws IOException {
		mkdirs(directory.getFileName().toString(), directory);
	}

	public static void mkdirs(String type, Path directory) throws IOException {
		if (!Files.exists(directory)) {
			try {
				Files.createDirectories(directory);
			} catch (IOException e) {
				throw directoryException(type, directory);
			}
		}
	}

	public static String getName(Path path) {
		return (Objects.toString(path.getFileName()));
	}

	public static Path initializeDirectory(Path rootDirectory, String name) throws IOException {
		Path directory = rootDirectory.resolve(name);
		mkdirs(name, directory);
		return (directory);
	}

	public static Path initializeDirectory(String type, Path rootDirectory, String name) throws IOException {
		Path directory = rootDirectory.resolve(name);
		mkdirs(type, directory);
		return (directory);
	}

	private static FileChannel createLockChannel(Path directory, String locFileName) throws IOException {
		mkdirs(directory);
		Path lockFile = directory.resolve(locFileName);
		if (!Files.exists(lockFile)) {
			Files.createFile(lockFile);
		}
		return (FileChannel.open(lockFile, StandardOpenOption.WRITE));
	}

	public static FileLock tryLock(Path directory, String locFileName) throws IOException {
		FileChannel lockChannel = createLockChannel(directory, locFileName);
		return (lockChannel != null) ? lockChannel.tryLock() : null;
	}

	public static FileLock lock(Path directory, String locFileName) throws IOException {
		FileChannel lockChannel = createLockChannel(directory, locFileName);
		return (lockChannel != null) ? lockChannel.lock() : null;
	}

	public static void copyTo(Path source, Path destinationDir) throws IOException {
		mkdirs(destinationDir);
		copy(source, destinationDir.relativize(source.getFileName()));
	}

	public static class DataFileVisitor<DATA> extends SimpleFileVisitor<Path> {
		public DATA data = null;
	}

	public static long directoryLastModified(Path path) throws IOException {
		if (path != null) {
			FileTime lastModified = Files.getLastModifiedTime(path);
			if (Files.isDirectory(path)) {
				DataFileVisitor<FileTime> lastModifiedVisitor = new DataFileVisitor<FileTime>() {
					{
						data = lastModified;
					}

					private FileVisitResult adjustLastModified(Path path) throws IOException {
						FileTime lastModified = Files.getLastModifiedTime(path);

						if (lastModified.compareTo(data) > 0) {
							data = lastModified;
						}
						return FileVisitResult.CONTINUE;
					}

					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						return adjustLastModified(file);
					}

					@Override
					public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
						return adjustLastModified(dir);
					}
				};
				Files.walkFileTree(path, Collections.singleton(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,
						lastModifiedVisitor);

				return lastModifiedVisitor.data.toMillis();
			}

			return lastModified.toMillis();
		}

		return (0);
	}

	public static void copy(Path source, Path destination) throws IOException {
		Files.copy(source, destination, StandardCopyOption.COPY_ATTRIBUTES);
		if (Files.isDirectory(source)) {
			try {
				Files.walkFileTree(source, Collections.singleton(FileVisitOption.FOLLOW_LINKS), 1,
						new SimpleFileVisitor<Path>() {
							@Override
							public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
								copy(file, destination.resolve(file.getFileName()));
								return FileVisitResult.CONTINUE;
							}
						});
			} catch (IOException e) {
				deleteAllContent(destination);
				throw e;
			}
		}
	}

	/**
	 * Deletes all files in the directory. Returns list of files that were not
	 * deleted
	 * 
	 * @param directory
	 * @return
	 * @throws IOException
	 */
	public static Collection<Path> deleteAllContent(final Path directory) throws IOException {
		return (deleteContent(directory, d -> true));
	}

	public static List<Path> listSortedByLastModified(Path directory) throws IOException {
		List<Path> files = Files.list(directory).collect(Collectors.toList());
		Collections.sort(files, new Comparator<Path>() {
			@Override
			public int compare(Path path1, Path path2) {
				try {
					return Files.getLastModifiedTime(path1).compareTo(Files.getLastModifiedTime(path2));
				} catch (IOException e) {
					return (0);
				}
			}
		});

		return (files);
	}

	/**
	 * Deletes all files in the directory. Returns list of files that were not
	 * deleted
	 * 
	 * @param directory
	 * @return
	 * @throws IOException
	 */
	public static Collection<Path> deleteContent(final Path directory, final Predicate<Path> deleteCondition) {
		final ArrayList<Path> failed = new ArrayList<Path>();

		try {
			Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException e) throws IOException {
					if (Files.exists(file)) {
						// System.out.println("Failed " + file);
						failed.add(file);
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes arg1) throws IOException {
					try {
						// System.out.println("Deleting " + file);
						if (deleteCondition.test(file)) {
							Files.delete(file);
							// file.toFile().delete();
							// System.out.println("Deleted " + file);
						}
					} catch (IOException e) {
						// System.out.println("Failed " + e.getMessage());
						visitFileFailed(file, e);
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException arg1) throws IOException {
					// System.out.println("Post visit directory " + dir);
					if (!directory.equals(dir)) {
						try {
							if (!Files.list(dir).findAny().isPresent()) {
								visitFile(dir, null);
							} else {
								// System.out.println("Directory " + dir
								// + " has files");
							}
						} catch (IOException e) {
							// System.out.println("Failed list directory " + dir
							// + ": " + e.getMessage());
							visitFileFailed(dir, e);
						}
					}
					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			throw ProgrammingError.shouldNotHappen(e);
		}

		return (CollectionUtils.nullForEmpty(failed));
	}

	public static void touch(Path file) throws IOException {
		Files.setLastModifiedTime(file, FileUtils.now());
	}
}
