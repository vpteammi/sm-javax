package com.sm.javax.utilx;

import java.util.Iterator;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public interface TaskExecutor extends Executor {

	public Executor getExecutor();

	@Override
	public default void execute(Runnable command) {
		if (command != null) {
			Executor executor = getExecutor();
			if (executor == null) {
				runAsDebug(command);
			} else {
				executor.execute(() -> runAsDebug(command));
			}
		}
	}

	public default void runAsDebug(Runnable command) {
		try {
			command.run();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public default <CE extends Exception, E extends CE> void execute(RunnableX<E> command, Consumer<CE> catcher) {
		execute(() -> run(command, catcher));
	}

	public default <T> void execute(T item, Consumer<T> command) {
		if (item != null && command != null) {
			execute(() -> command.accept(item));
		}
	}

	public default <T, E extends Exception> void execute(T item, ConsumerX<T, E> command, Consumer<E> catcher) {
		if (item != null && command != null) {
			execute(item, new CatcherConsumer<T, E>(command, catcher));
		}
	}

	public default void run(Runnable command) {
		if (command != null) {
			command.run();
		}
	}

	@SuppressWarnings("unchecked")
	public default <CE extends Exception, E extends CE> void run(RunnableX<E> command, Consumer<CE> catcher) {
		if (command != null) {
			try {
				command.run();
			} catch (Exception e) {
				if (catcher != null) {
					catcher.accept((E) e);
				}
			}
		}
	}

	public default <T> void run(T item, Consumer<T> command) {
		if (item != null && command != null) {
			run(() -> command.accept(item));
		}
	}

	public default <T, E extends Exception> void run(T item, ConsumerX<T, E> command, Consumer<E> catcher) {
		if (item != null && command != null) {
			run(() -> new CatcherConsumer<T, E>(command, catcher).accept(item));
		}
	}

	public default <T> void execute(Iterable<T> collection, Consumer<T> command) {
		if (collection != null) {
			collection.forEach(item -> execute(() -> command.accept(item)));
		}
	}

	public default <T, E extends Exception> void execute(Iterable<T> collection, ConsumerX<T, E> command,
			Consumer<E> catcher) {
		if (collection != null) {
			CatcherConsumer<T, E> commandX = new CatcherConsumer<T, E>(command, catcher);
			collection.forEach(item -> execute(() -> commandX.accept(item)));
		}
	}

	public default <T> T executeButOne(Iterable<T> collection, Consumer<T> command) {
		if (collection != null) {
			Iterator<T> iterator = collection.iterator();
			if (iterator.hasNext()) {
				T first = iterator.next();
				iterator.forEachRemaining(item -> execute(() -> command.accept(item)));
				return (first);
			}
		}
		return (null);
	}

	public default <T> void executeOrRun(Iterable<T> collection, Consumer<T> command) {
		run(executeButOne(collection, command), command);
	}

	public default <T, E extends Exception> T executeButOne(Iterable<T> collection, ConsumerX<T, E> command,
			Consumer<E> catcher) {
		if (collection != null) {
			Iterator<T> iterator = collection.iterator();
			if (iterator.hasNext()) {
				CatcherConsumer<T, E> commandX = new CatcherConsumer<T, E>(command, catcher);
				T first = iterator.next();
				iterator.forEachRemaining(item -> execute(() -> commandX.accept(item)));
				return (first);
			}
		}

		return (null);
	}

	public default <T, E extends Exception> void executeOrRun(Iterable<T> collection, ConsumerX<T, E> command,
			Consumer<E> catcher) {
		run(executeButOne(collection, command, catcher), command, catcher);
	}
}
