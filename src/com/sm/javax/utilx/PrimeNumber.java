package com.sm.javax.utilx;

import java.math.BigInteger;
import java.util.BitSet;

public class PrimeNumber
{
	private static BigInteger[] BELL_PRIMES;

	private static BigInteger getBigInteger (BigInteger[] values, int idx)
	{
		if ((idx < 0) || (idx >= values.length)) {
			throw new IllegalArgumentException ("Exepected index in 0.." + (values.length - 1) + " range");
		}
		return values[idx];
	}

	public static BigInteger[] getBellPrimes ()
	{
		if (BELL_PRIMES == null) {
			BELL_PRIMES = new BigInteger [] { new BigInteger ("2"), new BigInteger ("5"), new BigInteger ("877"),
					new BigInteger ("27644437"), new BigInteger ("35742549198872617291353508656626642567"),
					new BigInteger ("359334085968622831041960188598043661065388726959079837") };
		}
		return BELL_PRIMES;
	}

	public static BigInteger getBellPrime (int idx)
	{
		return getBigInteger (getBellPrimes (), idx);
	}

	private static BigInteger[] CAROL_PRIMES;

	public static BigInteger[] getCarolPrimes ()
	{
		if (CAROL_PRIMES == null) {
			CAROL_PRIMES = new BigInteger [] { new BigInteger ("7"), new BigInteger ("47"), new BigInteger ("223"),
					new BigInteger ("3967"), new BigInteger ("16127"), new BigInteger ("1046527"),
					new BigInteger ("16769023"), new BigInteger ("1046527"), new BigInteger ("1073676287"),
					new BigInteger ("68718952447"), new BigInteger ("274876858367"), new BigInteger ("4398042316799"),
					new BigInteger ("1125899839733759"), new BigInteger ("18014398241046527"),
					new BigInteger ("1298074214633706835075030044377087") };
		}
		return CAROL_PRIMES;
	}

	public static BigInteger getCarolPrime (int idx)
	{
		return getBigInteger (getCarolPrimes (), idx);
	}

	private static int getInteger (int[] values, int idx)
	{
		if ((idx < 0) || (idx >= values.length)) {
			throw new IllegalArgumentException ("Exepected index in 0.." + (values.length - 1) + " range");
		}
		return values[idx];
	}

	private static int[] CENTER_DECAGONAL_PRIMES;

	public static int[] getCenterDecagonalPrimes ()
	{
		if (CENTER_DECAGONAL_PRIMES == null) {
			CENTER_DECAGONAL_PRIMES = new int [] { 11, 31, 61, 101, 151, 211, 281, 661, 911, 1051, 1201, 1361, 1531,
					1901, 2311, 2531, 3001, 3251, 3511, 4651, 5281, 6301, 6661, 7411, 9461, 9901, 12251, 13781, 14851,
					15401, 18301, 18911, 19531, 20161, 22111, 24151, 24851, 25561, 27011, 27751 };
		}
		return CENTER_DECAGONAL_PRIMES;
	}

	public static int getCenterDecagonalPrime (int idx)
	{
		return getInteger (getCenterDecagonalPrimes (), idx);
	}

	private static int[] CENTER_HEPTAGONAL_PRIMES;

	public static int[] getCenterHeptagonalPrimes ()
	{
		if (CENTER_HEPTAGONAL_PRIMES == null) {
			CENTER_HEPTAGONAL_PRIMES = new int [] { 43, 71, 197, 463, 547, 953, 1471, 1933, 2647, 2843, 3697, 4663,
					5741, 8233, 9283, 10781, 11173, 12391, 14561, 18397, 20483, 29303, 29947, 34651, 37493, 41203,
					46691, 50821, 54251, 56897, 57793, 65213, 68111, 72073, 76147, 84631, 89041 };
		}
		return CENTER_HEPTAGONAL_PRIMES;
	}

	public static int getCenterHeptagonalPrime (int idx)
	{
		return getInteger (getCenterHeptagonalPrimes (), idx);
	}

	private static int[] CENTER_SQUARE_PRIMES;

	public static int[] getCenterSquarePrimes ()
	{
		if (CENTER_SQUARE_PRIMES == null) {
			CENTER_SQUARE_PRIMES = new int [] { 5, 13, 41, 61, 113, 181, 313, 421, 613, 761, 1013, 1201, 1301, 1741,
					1861, 2113, 2381, 2521, 3121, 3613, 4513, 5101, 7321, 8581, 9661, 9941, 10513, 12641, 13613, 14281,
					14621, 15313, 16381, 19013, 19801, 20201, 21013, 21841, 23981, 24421, 26681 };
		}
		return CENTER_SQUARE_PRIMES;
	}

	public static int getCenterSquarePrime (int idx)
	{
		return getInteger (getCenterSquarePrimes (), idx);
	}

	private static int[] CENTER_TRIANGULAR_PRIMES;

	public static int[] getCenterTriangularPrimes ()
	{
		if (CENTER_TRIANGULAR_PRIMES == null) {
			CENTER_TRIANGULAR_PRIMES = new int [] { 19, 31, 109, 199, 409, 571, 631, 829, 1489, 1999, 2341, 2971, 3529,
					4621, 4789, 7039, 7669, 8779, 9721, 10459, 10711, 13681, 14851, 16069, 16381, 17659, 20011, 20359,
					23251, 25939, 27541, 29191, 29611, 31321, 34429, 36739, 40099, 40591, 42589 };
		}
		return CENTER_TRIANGULAR_PRIMES;
	}

	public static int getCenterTriangularPrime (int idx)
	{
		return getInteger (getCenterTriangularPrimes (), idx);
	}

	public static int[] getByEratosthenesSieve (int upperBound)
	{
		int upperBoundSquareRoot = (int) Math.sqrt (upperBound);

		BitSet isComposite = new BitSet (upperBound + 1);

		for (int m = 2; m <= upperBoundSquareRoot; m++) {

			if (!isComposite.get (m)) {

				for (int k = m * m; k <= upperBound; k += m) {
					isComposite.set (k, true);
				}
			}
		}

		int primeCount = 0;

		for (int m = upperBoundSquareRoot; m <= upperBound; m++)
			if (!isComposite.get (m)) {
				primeCount++;
			}

		int[] primes = new int [primeCount];
		int idx = 0;

		for (int m = upperBoundSquareRoot; m <= upperBound; m++)
			if (!isComposite.get (m)) {
				primes[idx++] = m;
			}

		return primes;
	}

}
