package com.sm.javax.utilx;

import java.util.ArrayList;

public class ValueByKeySelector<INIT_KEY, MAP_KEY, SELECT_KEY, VALUE> {

	private final ArrayList<KeyValue<MAP_KEY, VALUE>> entries = new ArrayList<KeyValue<MAP_KEY, VALUE>>();
	private final MapKeyExtractor<INIT_KEY, MAP_KEY> mapKeyExtrator;
	private final Selector<MAP_KEY, SELECT_KEY, VALUE> selector;

	public interface MapKeyExtractor<IK, MK> {
		public MK extract(IK init_key);
	}

	public interface Selector<MK, SK, V> {
		public boolean match(SK selectKey, MK mapKey);
	}

	public ValueByKeySelector(
			MapKeyExtractor<INIT_KEY, MAP_KEY> mapKeyExtrator,
			Selector<MAP_KEY, SELECT_KEY, VALUE> selector) {
		this.mapKeyExtrator = mapKeyExtrator;
		this.selector = selector;
	}

	public ValueByKeySelector<INIT_KEY, MAP_KEY, SELECT_KEY, VALUE> add(
			VALUE value, INIT_KEY key) {
		entries.add(new KeyValue<MAP_KEY, VALUE>(mapKeyExtrator.extract(key),
				value));
		return (this);
	}

	public VALUE select(SELECT_KEY key) {
		return (select(key, null));
	}

	public <DV extends VALUE> VALUE select(SELECT_KEY key, DV defaultValue) {
		for (KeyValue<MAP_KEY, VALUE> entry : entries) {
			if (selector.match(key, entry.key))
				return (entry.value);
		}

		return (defaultValue);
	}
}
