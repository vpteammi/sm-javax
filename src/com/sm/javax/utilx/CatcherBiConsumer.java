package com.sm.javax.utilx;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class CatcherBiConsumer<T, U, E extends Exception> extends
		ExceptionProcessor<T, E> implements BiConsumer<T, U> {

	private final BiConsumerX<T, U, E> consumer;

	public CatcherBiConsumer(BiConsumerX<T, U, E> consumer) {
		super();
		this.consumer = consumer;
	}

	public CatcherBiConsumer(BiConsumerX<T, U, E> consumer,
			Consumer<E> excProcessor) {
		super(excProcessor);
		this.consumer = consumer;
	}

	public CatcherBiConsumer(BiConsumerX<T, U, E> consumer,
			BiConsumer<T, E> excProcessor) {
		super(excProcessor);
		this.consumer = consumer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void accept(T t, U u) {
		try {
			consumer.accept(t, u);
		} catch (Exception e) {
			processException(t, (E) e);
		}
	}
}