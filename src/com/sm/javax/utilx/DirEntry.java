package com.sm.javax.utilx;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class DirEntry<EntryData>
{
	private EntryData data;
	private DirEntry<EntryData> parent;
	private final String name;
	private ArrayList<DirEntry<EntryData>> subentries = null;

	private class EmptyIterator implements Iterator<DirEntry<EntryData>>
	{
		@Override
		public boolean hasNext ()
		{
			return false;
		}

		@Override
		public DirEntry<EntryData> next ()
		{
			throw new NoSuchElementException ();
		}

		@Override
		public void remove ()
		{
			throw new UnsupportedOperationException ();
		}
	}

	public DirEntry (String name)
	{
		this.parent = null;
		this.name = name;
		this.data = null;
	}

	protected void setEntryData (EntryData data)
	{
		this.data = data;
	}

	protected void setParent (DirEntry<EntryData> parent)
	{
		this.parent = parent;
	}
	
	private ArrayList<DirEntry<EntryData>> getSubentriesArray ()
	{
		if (subentries == null)
		{
			subentries = new ArrayList<DirEntry<EntryData>> ();
		}
		return (subentries);
	}

	protected void addSubentry (DirEntry<EntryData> subentry)
	{
		if (subentries == null)
		{
			subentries = new ArrayList<DirEntry<EntryData>> ();
		}
		getSubentriesArray ().add (subentry);
		subentry.setParent (this);
	}

	private DirEntry<EntryData> getEntry (HashMap<String, DirEntry<EntryData> > map, String name)
	{
		File file = new File (name);
		name = file.getPath ();
		DirEntry<EntryData> entry = map.get (name);
		if (entry == null)
		{
			String fileName = file.getName ();
			String parentName = file.getParent ();
			DirEntry<EntryData> parent = (parentName == null ? this : getEntry (map, parentName));
			entry = parent.getSubentry (fileName);
			if (entry == null)
			{
				entry = new DirEntry<EntryData> (fileName);
				parent.addSubentry (entry);
				map.put (name, entry);
			}
		}
		return (entry);
	}
	
	public void addSubentries (NamedIterator<EntryData> subentries)
	{
		if ((subentries != null) && subentries.hasNext ())
		{
			HashMap<String, DirEntry<EntryData> > entryMap =
				new HashMap<String, DirEntry<EntryData> > ();
			while (subentries.hasNext ())
			{
				EntryData data = subentries.next ();
				String name = subentries.getName (data);
				DirEntry<EntryData> entry = getEntry (entryMap, name);
				entry.setEntryData (data);
			}
		}
	}

	public DirEntry<EntryData> getParent ()
	{
		return (parent);
	}

	public EntryData getEntryData ()
	{
		return (data);
	}

	public Iterator<DirEntry<EntryData>> getSubentries ()
	{
		return (subentries == null ? new EmptyIterator () : subentries.iterator ());
	}

	public DirEntry<EntryData> getSubentry (String name)
	{
		if (subentries != null)
		{
			for (DirEntry<EntryData> entry : subentries)
			{
				if (entry.getName ().equals (name))
					return (entry);
			}
		}
		return (null);
	}

	public boolean hasSubentries ()
	{
		return ((subentries == null) || subentries.size () == 0);
	}

	public String getName ()
	{
		return (name);
	}

	@Override
	public String toString ()
	{
		return (name);
	}
}
