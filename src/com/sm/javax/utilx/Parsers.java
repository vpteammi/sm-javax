package com.sm.javax.utilx;

import java.util.HashMap;

import com.sm.javax.langx.Strings;
import com.sm.javax.langx.ValueParseException;

public class Parsers {
	private final static HashMap<Class<?>, StringParser<?>> parsers = new HashMap<Class<?>, StringParser<?>>();

	public static void registerParser(Class<?> clazz, StringParser<?> parser) {
		parsers.put(clazz, parser);
	}

	public static StringParser<?> getParser(Class<?> clazz) {
		return (parsers.get(clazz));
	}

	public final static StringParser<String> STRING_PARSER = new StringParser<String>() {
		@Override
		public String parse(String stringRepr) {
			return (stringRepr);
		}
	};

	public final static StringParser<Character> CHARACTER_PARSER = new StringParser<Character>() {
		@Override
		public Character parse(String stringRepr) throws ValueParseException {
			if (!Strings.isEmpty(stringRepr) && (stringRepr.length() == 1)) {
				return (stringRepr.charAt(0));
			}

			throw new ValueParseException("Cannot parse \"" + stringRepr
					+ "\" as character");
		}
	};

	public final static StringParser<Boolean> BOOLEAN_PARSER = new StringParser<Boolean>() {
		@Override
		public Boolean parse(String stringRepr) {
			return (Boolean.parseBoolean(stringRepr));
		}

		@Override
		public Boolean tryParse(String stringRepr) {
			try {
				if ("true".equals(stringRepr.trim().toLowerCase())) {
					return (Boolean.TRUE);
				} else if ("false".equals(stringRepr.trim().toLowerCase())) {
					return (Boolean.FALSE);
				}
			} catch (Exception e) {
			}
			return null;
		}
	};

	public final static StringParser<Integer> INTEGER_PARSER = new StringParser<Integer>() {
		@Override
		public Integer parse(String stringRepr) throws ValueParseException {
			try {
				return (Integer.parseInt(stringRepr));
			} catch (NumberFormatException e) {
				throw new ValueParseException(e);
			}
		}
	};

	public final static StringParser<Long> LONG_PARSER = new StringParser<Long>() {
		@Override
		public Long parse(String stringRepr) throws ValueParseException {
			try {
				return (Long.parseLong(stringRepr));
			} catch (NumberFormatException e) {
				throw new ValueParseException(e);
			}
		}
	};

	public final static StringParser<Short> SHORT_PARSER = new StringParser<Short>() {
		@Override
		public Short parse(String stringRepr) throws ValueParseException {
			try {
				return (Short.parseShort(stringRepr));
			} catch (NumberFormatException e) {
				throw new ValueParseException(e);
			}
		}
	};

	public final static StringParser<Double> DOUBLE_PARSER = new StringParser<Double>() {
		@Override
		public Double parse(String stringRepr) throws ValueParseException {
			try {
				return (Double.parseDouble(stringRepr));
			} catch (NumberFormatException e) {
				throw new ValueParseException(e);
			}
		}
	};

	public final static StringParser<Float> FLOAT_PARSER = new StringParser<Float>() {
		@Override
		public Float parse(String stringRepr) throws ValueParseException {
			try {
				return (Float.parseFloat(stringRepr));
			} catch (NumberFormatException e) {
				throw new ValueParseException(e);
			}
		}
	};

	static {
		registerParser(char.class, CHARACTER_PARSER);
		registerParser(Character.class, CHARACTER_PARSER);
		registerParser(int.class, INTEGER_PARSER);
		registerParser(Integer.class, INTEGER_PARSER);
		registerParser(short.class, SHORT_PARSER);
		registerParser(Short.class, SHORT_PARSER);
		registerParser(long.class, LONG_PARSER);
		registerParser(Long.class, LONG_PARSER);
		registerParser(double.class, DOUBLE_PARSER);
		registerParser(Double.class, DOUBLE_PARSER);
		registerParser(float.class, FLOAT_PARSER);
		registerParser(Float.class, FLOAT_PARSER);
		registerParser(boolean.class, BOOLEAN_PARSER);
		registerParser(Boolean.class, BOOLEAN_PARSER);
		registerParser(String.class, STRING_PARSER);
	}

	public final static StringParser<?>[] BASIC_PARSERS = { BOOLEAN_PARSER,
			INTEGER_PARSER, LONG_PARSER, DOUBLE_PARSER };
	public final static StringParser<?>[] MAIN_PARSERS = { BOOLEAN_PARSER,
			SHORT_PARSER, INTEGER_PARSER, LONG_PARSER, FLOAT_PARSER,
			DOUBLE_PARSER };

	public static Object parseValue(String text, StringParser<?>... parsers) {
		if (!Strings.isEmpty(text)) {
			Object value;
			for (StringParser<?> parser : parsers) {
				if ((value = parser.tryParse(text)) != null)
					return (value);
			}
		}
		return (text);
	}

	public static Object parseBasicValue(String text) {
		return (parseValue(text, BASIC_PARSERS));
	}

	public static Object parseMainValue(String text) {
		return (parseValue(text, MAIN_PARSERS));
	}
}
