package com.sm.javax.utilx;

import java.util.Iterator;

public interface NamedIterator<T> extends Iterator<T>
{
	String getName (T item);
}
