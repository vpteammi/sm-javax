package com.sm.javax.utilx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sm.javax.langx.Objects;
import com.sm.javax.langx.Strings;

public class CommandLineArguments extends HashMap<String, List<String>>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2377363350804023296L;

	private static final Pattern ArgFormat = Pattern.compile ("^\\-([^=]+)=(.*)");

	private ArrayList<String> arguments;
	private String[] nativeArguments;

	public CommandLineArguments ()
	{
		super ();
		arguments = new ArrayList<> ();
	}

	public CommandLineArguments (String[] args)
	{
		this ();
		nativeArguments = args;
		parseArguments (args);
	}

	public static CommandLineArguments parse (String[] args)
	{
		return new CommandLineArguments (args);
	}

	public void parseArguments (String[] args)
	{
		if (args != null && args.length > 0) {
			StdoutLogger.logInfo ("Arguments {");
			for (String arg : args) {
				StdoutLogger.logInfo ("  arg: " + arg);
				arguments.add (arg);
				if (arg.startsWith ("-")) {
					Matcher m = ArgFormat.matcher (arg);
					if (m.find ()) {
						String key = m.group (1);
						String value = m.group (2);
						List<String> curVal = get (key);
						if (curVal == null) {
							curVal = new LinkedList<> ();
							put (key, curVal);
						}
						curVal.add (value);
						StdoutLogger.logInfo ("    " + key + ": " + value);
					}
					else {
						String key = arg.substring (1);
						put (key, null);
						StdoutLogger.logInfo ("    " + key);
					}
				}
			}
			StdoutLogger.logInfo ("}");
		}
	}

	public boolean hasArgument (String argName)
	{
		return containsKey (argName);
	}

	public String getArgument (String argName)
	{
		List<String> list = get (argName);
		if (list != null) {
			return list.get (list.size() - 1); // we take the last provided argument
		}
		return null;
	}
	
	public String[] getArguments (String argName)
	{
		List<String> list = get (argName);
		if (list != null) {
			return list.toArray (new String [list.size ()]);
		}
		return null;
	}

	public String getArgument (String argName, String defaultValue)
	{
		return Strings.noNullOr (get (argName), defaultValue);
	}

	public int getArgumentCount ()
	{
		return arguments.size ();
	}

	public String getArgument (int i)
	{
		return arguments.get (i);
	}

	public Integer getIntArgument (String argName)
	{
		String value = getArgument (argName);
		return Strings.isEmpty (value) ? null : Integer.parseInt (value);
	}

	public int getIntArgument (String argName, int defaultValue)
	{
		return Objects.noNull (getIntArgument (argName), defaultValue);
	}

	public String[] getArgs ()
	{
		return nativeArguments;
	}

}
