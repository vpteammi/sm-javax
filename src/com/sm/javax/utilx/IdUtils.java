package com.sm.javax.utilx;

import java.text.SimpleDateFormat;

public class IdUtils
{
	private static long uniqueIdCounter = 0;
	
	private static synchronized String newUniqueCounter ()
	{
		if (uniqueIdCounter == Long.MAX_VALUE)
			uniqueIdCounter = 0;
		return String.format ("%d", uniqueIdCounter++);
	}

	private static final SimpleDateFormat UNIQUE_ID_FORMAT = new SimpleDateFormat ("yyyy_MM_dd_hh_mm_ss_S_");
	
	public static String newUniqueId ()
	{
		return (UNIQUE_ID_FORMAT.format (TimeUtils.getNow ()) + newUniqueCounter ());
	}
}
