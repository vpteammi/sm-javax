package com.sm.javax.utilx;

public class MathUtils
{
	public static int sign (long l)
	{
		return (l == 0) ? 0 : (l > 0) ? 1 : -1;
	}
}
