package com.sm.javax.utilx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import javax.swing.Icon;
import javax.swing.filechooser.FileSystemView;

import com.sm.javax.langx.Strings;

public class FileUtils
{
	public static Tuple2<File, String[]> parse (File f)
	{
		ArrayList<String> names = new ArrayList<String> ();

		while (f != null) {
			String name = f.getName ();
			if (name.length () > 0) {
				names.add (0, name);
				f = f.getParentFile ();
			}
			else {
				break;
			}
		}
		return (new Tuple2<File, String[]> (f, names.toArray (new String [names.size ()])));
	}

	public static String toString (File f, int width)
	{
		if (f != null) {
			String label = f.toString ();
			if (label.length () > width) {
				Tuple2<File, String[]> parsed = parse (f);
				File root = parsed._1;
				String[] names = parsed._2;
				label = (root == null) ? "" : root.toString ();
				if (names.length > 0) {
					int nnames = names.length;
					String suffix = (nnames > 0) ? names[nnames - 1] : "";
					String prefix = "";
					int length = label.length () + 4 + suffix.length ();

					if (length < width + 2) {
						int hnames = nnames / 2;

						for (int i = 0; i < hnames; i++) {
							String p = names[i];
							String s = names[nnames - i - 2];
							if ((s.length () + length) < width) {
								suffix = s + File.separator + suffix;
								length += s.length () + 1;
							}
							if ((p.length () + length) < width) {
								prefix = prefix + p + File.separator;
								length += p.length () + 1;
							}
						}
					}

					label = label + prefix + "..." + File.separator + suffix;
				}
			}

			return (label);
		}

		return (null);
	}

	public static void touch (File file)
	{
		file.setLastModified (new Date ().getTime ());
	}

	public static File changeExtension (File file, String newExtension)
	{
		String name = file.getName ();
		File path = file.getParentFile ();
		String[] names = name.split ("\\.");
		if (names.length > 1) {
			names[names.length - 1] = newExtension;
			return (new File (path, Strings.concat (names, ".")));
		}
		else {
			return (new File (path, name + "." + newExtension));
		}
	}

	public static File trimExtension (File file)
	{
		return (new File (file.getParentFile (), trimExtension (file.getName ())));
	}

	public static String trimExtension (String name)
	{
		String[] names = name.split ("\\.");
		return (names.length < 2 ? name : Strings.concat (names, ".", 0, names.length - 1));
	}

	public static File getUniqueFile (File file, String suffixFormat)
	{
		File parent = file.getParentFile ();
		String name = trimExtension (file.getName ());
		String ext = getExt (file);
		ext = Strings.isEmpty (ext) ? "" : ("." + ext);
		int count = 0;
		while (file.exists ()) {
			file = new File (parent, name + String.format (suffixFormat, ++count) + ext);
		}
		return (file);
	}

	public static File getUniqueFile (File file)
	{
		return (getUniqueFile (file, ".[%d]"));
	}

	public static File appendDefaultIfNoExtension (File file, String defaultExtension)
	{
		String name = file.getName ();
		String[] names = name.split ("\\.");

		if (names.length == 1) {
			file = new File (file.getParentFile (), name + "." + defaultExtension);
		}

		return (file);
	}

	private final static FileSystemView fileSystemView = FileSystemView.getFileSystemView ();

	public static Icon getSystemIcon (File file)
	{
		return (fileSystemView.getSystemIcon (file));
	}

	public static Pattern getExtensionMatchPatern (String extPatern)
	{
		return (Pattern.compile (".*\\." + extPatern + "\\z", Pattern.CASE_INSENSITIVE));
	}

	public static boolean hasExt (File file, String extPattern)
	{
		return (nameMatches (file, getExtensionMatchPatern (extPattern)));
	}

	public static boolean hasExt (String filename, String extPattern)
	{
		return (nameMatches (filename, getExtensionMatchPatern (extPattern)));
	}

	public static String getExt (File file)
	{
		String name = file.getName ();
		int idx = name.lastIndexOf ('.');
		return (idx < 0) ? null : name.substring (idx + 1);
	}

	public static boolean nameMatches (String filename, Pattern pattern)
	{
		return (!Strings.isEmpty (filename) && pattern.matcher (filename).matches ());
	}

	public static boolean nameMatches (File file, String pattern)
	{
		return ((file != null) && file.getName ().matches (pattern));
	}

	public static boolean nameMatches (File file, Pattern pattern)
	{
		return ((file != null) && nameMatches (file.getName (), pattern));
	}

	private static Pattern RAR_ARCHIVE_PATTERN = getExtensionMatchPatern ("rar");

	public static boolean isRarArchive (File file)
	{
		return ((file != null) && file.isFile () && nameMatches (file, RAR_ARCHIVE_PATTERN));
	}

	private static Pattern ZIP_ARCHIVE_PATTERN = getExtensionMatchPatern ("(zip|jar)");

	public static boolean isZipArchive (File file)
	{
		return ((file != null) && file.isFile () && nameMatches (file, ZIP_ARCHIVE_PATTERN));
	}

	private static Pattern ARCHIVE_PATTERN = getExtensionMatchPatern ("(zip|jar|rar)");

	public static boolean isArchive (File file)
	{
		return ((file != null) && file.isFile () && nameMatches (file, ARCHIVE_PATTERN));
	}

	public static class Comparator extends Comparators.ComparatorWithNull<File>
	{
		@Override
		protected int compareNoNull (File file0, File file1)
		{
			try {
				int value = file0.getCanonicalPath ().compareTo (file1.getCanonicalPath ());
				if (value == 0) {
					long diff = file0.length () - file1.length ();
					if (diff == 0) {
						diff = file0.lastModified () - file1.lastModified ();
						if (diff == 0)
							return (0);
					}
					return (diff > 0 ? 1 : -1);
				}
				return (value);
			}
			catch (IOException e) {
				return (1);
			}
		}
	}

	public final static Comparator FILE_COMPARATOR = new Comparator ();

	public static FileTime now ()
	{
		return (FileTime.from (Instant.now ()));
	}
	
	public static String getTextFileContent (File textFile)
	{
		try (BufferedReader inp = new BufferedReader (new FileReader (textFile))) {
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = inp.readLine ()) != null) {
				sb.append(line + "\n");
			}
			return sb.toString ();
		}
		catch (IOException e) {
		}
		return null;
	}

	public static String getTextFileLine (File textFile, int lineNum)
	{
		if (lineNum > 0) {
			try (BufferedReader inp = new BufferedReader (new FileReader (textFile))) {
				String line;
				while ((line = inp.readLine ()) != null) {
					if (--lineNum == 0) {
						return line;
					}
				}
			}
			catch (IOException e) {
			}
		}
		return null;
	}

	public static void copyFolder (File folderFrom, File folderTo) throws IOException
	{
		if (!folderTo.exists ()) {
			folderTo.mkdirs ();
		}
		if (folderTo.exists () && folderFrom.exists ()) {
			File[] files = folderFrom.listFiles ();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory ()) {
						copyFolder (file, new File (folderTo, file.getName ()));
					}
					else if (file.canRead ()) {
						copyFile (file, new File (folderTo, file.getName ()));
					}
				}
			}
		}
	}

	private static final int BufferSize = 4096;

	public static void copyFile (File fileFrom, File fileTo) throws IOException
	{
		try (FileInputStream inp = new FileInputStream (fileFrom)) {
			byte buf[] = new byte [BufferSize];
			try (FileOutputStream out = new FileOutputStream (fileTo)) {

				int bytesRead = 0;
				while ((bytesRead = inp.read (buf)) > -1) {
					out.write (buf, 0, bytesRead);
				}
			}
			catch (IOException ex) {
				throw ex;
			}
		}
		catch (IOException e) {
			throw e;
		}
	}

	public static boolean deleteDirectory (File dir)
	{
		if (!dir.exists ()) {
			return true;
		}
		if (!dir.isDirectory ()) {
			return false;
		}
		File[] files = dir.listFiles ();
		if (files != null && files.length > 0) {
			for (File entry : files) {
				if (entry.isDirectory ()) {
					if (!deleteDirectory (entry)) {
						return false;
					}
				}
				else {
					if (!entry.delete ()) {
						return false;
					}
				}
			}
		}
		return dir.delete ();
	}

}
