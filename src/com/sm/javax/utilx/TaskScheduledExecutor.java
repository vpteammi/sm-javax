package com.sm.javax.utilx;

import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public interface TaskScheduledExecutor extends TaskExecutor {
	public default Future<?> schedule(Runnable command, long delay,
			TimeUnit unit) {
		return (getExecutor().schedule(command, delay, unit));
	}

	@SuppressWarnings("unchecked")
	public default <E extends Exception> Future<?> schedule(
			RunnableX<E> command, long delay, TimeUnit unit, Consumer<E> catcher) {
		return (getExecutor().schedule(() -> {
			try {
				command.run();
			} catch (Exception e) {
				if (catcher != null) {
					catcher.accept((E) e);
				}
			}
		}, delay, unit));
	}

	public ScheduledExecutorService getExecutor();
}
