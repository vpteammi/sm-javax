package com.sm.javax.utilx;

import java.util.Random;

public class Hash
{
	public static int of (Object obj)
	{
		return (obj == null) ? 0 : obj.hashCode ();
	}

	public static int of (Object obj, int range)
	{
		return of (of (obj), range);
	}

	private final static int HASH_RANDOMIZER = (int) PrimeNumber.getCenterHeptagonalPrime (25);

	public static int of (long value, int range)
	{
		return new Random (value).nextInt (HASH_RANDOMIZER) % range;
	}
}
