package com.sm.javax.utilx;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

public class CatcherFunction<T, R, E extends Exception> extends
		ExceptionProcessor<T, E> implements Function<T, R> {

	private final FunctionX<T, R, E> func;

	public CatcherFunction(FunctionX<T, R, E> func) {
		super();
		this.func = func;
	}

	public CatcherFunction(FunctionX<T, R, E> func, Consumer<E> excProcessor) {
		super(excProcessor);
		this.func = func;
	}

	public CatcherFunction(FunctionX<T, R, E> func,
			BiConsumer<T, E> excProcessor) {
		super(excProcessor);
		this.func = func;
	}

	@SuppressWarnings("unchecked")
	@Override
	public R apply(T arg0) {
		try {
			return func.apply(arg0);
		} catch (Exception e) {
			processException(arg0, (E) e);
			return (null);
		}
	}
}
