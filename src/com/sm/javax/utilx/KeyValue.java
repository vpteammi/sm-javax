package com.sm.javax.utilx;

public class KeyValue<K, V>
{
	public final K key;
	public final V value;
	
	public KeyValue (K key, V value)
	{
		super ();
		this.key = key;
		this.value = value;
	}
}
