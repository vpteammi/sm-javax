package com.sm.javax.utilx;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DomUtilities
{
	public static boolean hasElementWithTageName (Element elem, String tag)
	{
		NodeList nodes = elem.getElementsByTagName (tag);
		
		return ((nodes != null) && (nodes.getLength () > 0));
	}
	
	public static Element getElementByTagName (Element elem, String tag)
	{
		NodeList nodes = elem.getElementsByTagName (tag);
	
		if ((nodes != null) && (nodes.getLength () > 0))
		{
			Node node = nodes.item (0);
	
			if (node instanceof Element)
			{
				return ((Element) node);
			}
		}
	
		return (null);
	}

}
