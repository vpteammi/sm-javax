package com.sm.javax.utilx;

import com.sm.javax.langx.ValueParseException;

public interface StringParser<T> {
	T parse(String stringRepr) throws ValueParseException;

	default T tryParse(String stringRepr) {
		try {
			return (parse(stringRepr));
		} catch (ValueParseException e) {
			return (null);
		}
	}
}