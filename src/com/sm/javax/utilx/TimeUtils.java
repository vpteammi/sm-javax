package com.sm.javax.utilx;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils
{
	public static Date getNow ()
	{
		return (new Date ());
	}

	public final static long SECONDS_AS_MSECS = 1000;
	public final static long MINUTE_AS_MSECS = 60 * SECONDS_AS_MSECS;
	public final static long HOUR_AS_MSECS = 60 * MINUTE_AS_MSECS;
	public final static long DAY_AS_MSECS = 24 * HOUR_AS_MSECS;

	public static Date addMilliseconds (Date date, long milliseconds)
	{
		Calendar calendar = Calendar.getInstance ();
		calendar.setTime (new Date (date.getTime () + milliseconds));
		return (calendar.getTime ());
	}

	public static Date getToday ()
	{
		Calendar calendar = Calendar.getInstance ();
		return (getDate (calendar.get (Calendar.DAY_OF_MONTH), calendar.get (Calendar.MONTH),
				calendar.get (Calendar.YEAR)));
	}

	public static Date getDate (int date, int month, int year)
	{
		Calendar calendar = Calendar.getInstance ();
		calendar.set (year, month, date);
		return (calendar.getTime ());
	}

	public static final DateFormat dd_MMM_yyyy_hh_mm = new SimpleDateFormat ("dd-MMM-yyyy hh:mm");

	public static String toSimpleDayHourMinFormat (Date date)
	{
		return (date == null ? "" : dd_MMM_yyyy_hh_mm.format (date));
	}

	public static final DateFormat dd_MMM_yyyy_hh_mm_ss = new SimpleDateFormat ("dd-MMM-yyyy hh:mm:ss");

	public static String toSimpleDayHourMinSecFormat (Date date)
	{
		return (date == null ? "" : dd_MMM_yyyy_hh_mm_ss.format (date));
	}

	public static final DateFormat dd_MMM_yyyy = new SimpleDateFormat ("dd-MMM-yyyy");

	public static String toSimpleDayFormat (Date date)
	{
		return (date == null ? "" : dd_MMM_yyyy.format (date));
	}

	public static final DateFormat yyyy_MM_dd_HH_mm_ss_SSS = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss.SSS");

	public static String toLogTimeFormat (Date date)
	{
		return (date == null ? "" : yyyy_MM_dd_HH_mm_ss_SSS.format (date));
	}

	public static final DateFormat MM_dd_HH_mm_ss_SSS = new SimpleDateFormat ("MM-dd HH:mm:ss.SSS");

	public static String toLogShortTimeFormat (Date date)
	{
		return (date == null ? "" : MM_dd_HH_mm_ss_SSS.format (date));
	}

	private static final String LOG_LINE_FORMAT = "%s [T:%04x]: %s";

	public static String logLine (Object message)
	{
		return String.format (LOG_LINE_FORMAT, toLogTimeFormat (getNow ()), Thread.currentThread ().getId (),
				message);
	}

	public static boolean between (Date date, Date beforeIncl, Date afterExcl)
	{
		return ((date != null) && ((beforeIncl == null) || beforeIncl.before (date))
				&& ((afterExcl == null) || afterExcl.after (date)));
	}

	public static Date earlier (Date d1, Date d2)
	{
		if (d1 == null)
			return d2;
		if (d2 == null)
			return d1;
		return (d1.after (d2) ? d2 : d1);
	}

	public static Date later (Date d1, Date d2)
	{
		if (d1 == null)
			return d2;
		if (d2 == null)
			return d1;
		return (d1.before (d2) ? d2 : d1);
	}
}
