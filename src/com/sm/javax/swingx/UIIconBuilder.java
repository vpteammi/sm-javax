/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   UIIconBuilder.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Created.
 */

package com.sm.javax.swingx;

/*
 * %W% %E%
 * 
 * Copyright 2006, 2007 Software Marathon, Inc. All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *  - Redistribution in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * Neither the name of Software Marathon, Inc. or the names of contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. AND ITS LICENSORS SHALL NOT BE LIABLE
 * FOR ANY DAMAGES OR LIABILITIES SUFFERED BY LICENSEE AS A RESULT OF OR
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SOFTWARE MARATHON OR ITS LICENSORS BE LIABLE
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL,
 * CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS
 * OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE
 * THIS SOFTWARE, EVEN IF SOFTWARE MARATHON HAS BEEN ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGES.
 * 
 * You acknowledge that this software is not designed, licensed or intended for
 * use in the design, construction, operation or maintenance of any nuclear
 * facility.
 */

/*
 * Donated to PTC by Software Marathon, Inc. Can be used as-is without any
 * liability. All rights for the initial implementation belongs to Software
 * Marathon, Inc.
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class UIIconBuilder
{
	public static Icon buildIcon (int iconSize, Color backgroundColor, char letter)
	{
		return new ImageIcon (buildImage (iconSize, backgroundColor, letter));
	}

	public static BufferedImage buildImage (int iconSize, Color backgroundColor,
			char letter)
	{
		BufferedImage image = new BufferedImage (iconSize, iconSize,
				BufferedImage.TYPE_INT_ARGB);
		// set completely transparent
		for (int col = 0; col < iconSize; col++)
		{
			for (int row = 0; row < iconSize; row++)
			{
				image.setRGB (col, row, 0x0);
			}
		}
		Graphics2D graphics = (Graphics2D) image.getGraphics ();
		graphics.setRenderingHint (RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graphics.setRenderingHint (RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.setColor (backgroundColor);
		graphics.fillOval (0, 0, iconSize - 1, iconSize - 1);
		// create a whitish spot in the left-top corner of the icon
		double id4 = iconSize / 4.0;
		double spotX = id4;
		double spotY = id4;
		for (int col = 0; col < iconSize; col++)
		{
			for (int row = 0; row < iconSize; row++)
			{
				// distance to spot
				double dx = col - spotX;
				double dy = row - spotY;
				double dist = Math.sqrt (dx * dx + dy * dy);

				// distance of 0.0 - comes 90% to Color.white
				// distance of ICON_DIMENSION - stays the same

				if (dist > iconSize)
				{
					dist = iconSize;
				}

				int currColor = image.getRGB (col, row);
				int transp = (currColor >>> 24) & 0xFF;
				int oldR = (currColor >>> 16) & 0xFF;
				int oldG = (currColor >>> 8) & 0xFF;
				int oldB = (currColor >>> 0) & 0xFF;

				double coef = 0.9 - 0.9 * dist / iconSize;
				int dr = 255 - oldR;
				int dg = 255 - oldG;
				int db = 255 - oldB;

				int newR = (int) (oldR + coef * dr);
				int newG = (int) (oldG + coef * dg);
				int newB = (int) (oldB + coef * db);

				int newColor = (transp << 24) | (newR << 16) | (newG << 8) | newB;
				image.setRGB (col, row, newColor);
			}
		}
		// draw outline of the icon
		graphics.setColor (Color.black);
		graphics.drawOval (0, 0, iconSize - 1, iconSize - 1);
		letter = Character.toUpperCase (letter);
		graphics.setFont (new Font ("Arial", Font.BOLD, iconSize - 5));
		FontRenderContext frc = graphics.getFontRenderContext ();
		String letterString = Character.toString (letter);
		TextLayout mLayout = new TextLayout (letterString, graphics.getFont (), frc);

		float x = (float) (-.5 + (iconSize - mLayout.getBounds ().getWidth ()) / 2);
		float y = iconSize
				- (float) ((iconSize - mLayout.getBounds ().getHeight ()) / 2);
		// draw the letter
		graphics.drawString (letterString, x, y);

		return (image);
	}
	
	public static void saveAsPng (BufferedImage image, File file) throws IOException
	{
		ImageIO.write (image, "png", file);
	}
	
	public static void saveAsJpg (BufferedImage image, File file) throws IOException
	{
		ImageIO.write (image, "jpg", file);
	}
}
