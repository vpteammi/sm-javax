/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   UIIcons.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Created.
 */

package com.sm.javax.swingx;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.sm.javax.utilx.FileUtils;
import com.sm.javax.utilx.Tuple2;

public class UIIcons {
	private final static ImageIcon NO_ICON = new ImageIcon(
			UIIcons.class.getResource("images/empty.gif"));
	private final static Image NO_IMAGE = Toolkit.getDefaultToolkit().getImage(
			UIIcons.class.getResource("images/empty.gif"));

	private final static ConcurrentHashMap<String, Icon> iconCache = new ConcurrentHashMap<String, Icon>();
	private final static ConcurrentHashMap<Tuple2<Icon, Icon>, Icon> overlayedIconCache = new ConcurrentHashMap<Tuple2<Icon, Icon>, Icon>();
	private final static ConcurrentHashMap<String, Image> imageCache = new ConcurrentHashMap<String, Image>();

	private static String getImageId(Class<?> resourceClass, String imageName) {
		if ((resourceClass == null) || (imageName == null)) {
			return (null);
		} else {
			return (resourceClass.getCanonicalName() + ":" + imageName);
		}
	}

	public static synchronized Icon getIcon(Class<?> resourceClass,
			String iconName) {
		String imageId = getImageId(resourceClass, iconName);
		if (imageId == null) {
			return (null);
		} else if (iconCache.containsKey(imageId)) {
			Icon icon = iconCache.get(imageId);
			return (icon == NO_ICON ? null : iconCache.get(imageId));
		} else {
			Image image = getImage(resourceClass, iconName);
			Icon icon = null;
			if (image != null)
				icon = new ImageIcon(image);
			iconCache.put(imageId, (icon == null) ? NO_ICON : icon);
			return (icon);
		}
	}

	public static synchronized Image getImage(Class<?> resourceClass,
			String imageName) {
		String imageId = getImageId(resourceClass, imageName);
		if (imageId == null) {
			return (null);
		} else {
			if (imageCache.containsKey(imageId)) {
				Image image = imageCache.get(imageId);
				return ((image == NO_IMAGE) ? null : image);
			} else {
				String iconFile = "images/" + imageName;
				URL url = resourceClass.getResource(iconFile);
				if (url == null)
					url = resourceClass.getResource(iconFile + ".png");
				if (url == null)
					url = resourceClass.getResource(iconFile + ".gif");
				Image image = (url != null) ? Toolkit.getDefaultToolkit()
						.getImage(url) : null;
				imageCache.put(imageId, (image == null) ? NO_IMAGE : image);
				return (image);
			}
		}
	}

	public static Image getImage(Class<?> resourceClass, String subdir,
			String imageName) {
		Image image = null;
		if ((subdir != null) && (imageName != null)) {
			image = getImage(resourceClass, subdir + "/" + imageName);
		}
		if (image == null) {
			image = getImage(resourceClass, imageName);
		}

		return (image);
	}

	public static Icon getIcon(Class<?> resourceClass, String subdir,
			String iconName) {
		Icon icon = null;
		if ((subdir != null) && (iconName != null)) {
			icon = getIcon(resourceClass, subdir + "/" + iconName);
		}
		if (icon == null) {
			icon = getIcon(resourceClass, iconName);
		}

		return (icon);
	}

	public static Icon initIcon(Icon icon, String iconName) {
		return (initIcon(UIIcons.class, icon, iconName));
	}

	public static Icon initIcon(Class<?> resourceClass, Icon icon,
			String iconName) {
		return (icon == null) ? getIcon(resourceClass, iconName) : icon;
	}

	private static Icon errorIcon = null;

	private static Icon warningIcon = null;

	private static Icon infoIcon = null;

	public static final Icon getErrorIcon() {
		if (errorIcon == null) {
			errorIcon = getIcon(UIIcons.class, "message-error");
		}
		return (errorIcon);
	}

	public static final Icon getWarningIcon() {
		if (warningIcon == null) {
			warningIcon = getIcon(UIIcons.class, "message-warning");
		}
		return (warningIcon);
	}

	public static final Icon getInfoIcon() {
		if (infoIcon == null) {
			infoIcon = getIcon(UIIcons.class, "message-info");
		}
		return (infoIcon);
	}

	public static final Icon getMessageIcon(UIMessageType type) {
		switch (type) {
		case ERROR:
			return (getErrorIcon());
		case WARNING:
			return (getWarningIcon());
		case INFO:
			return (getInfoIcon());
		case OTHER:
			break;
		default:
			break;
		}
		return (null);
	}

	public static final Icon getOverlayIcon(Icon background, Icon overlayIcon) {
		Icon icon = background;

		if ((overlayIcon != null) && (background != null)) {
			Tuple2<Icon, Icon> key = new Tuple2<Icon, Icon>(background,
					overlayIcon);

			icon = overlayedIconCache.get(key);

			if (icon == NO_ICON)
				icon = null;

			if (icon != null)
				return (icon);

			icon = UIIcons.overlayIcons(background, overlayIcon, -1, -1);
			overlayedIconCache.put(key, (icon == null) ? NO_ICON : icon);
		}

		return (icon);
	}

	public static final Icon getSystemIcon(File file) {
		return (FileUtils.getSystemIcon(file));
	}

	public static Image getImage(Icon icon) {
		if (icon instanceof ImageIcon) {
			return ((ImageIcon) icon).getImage();
		}

		return (null);
	}

	public static Icon overlayIcons(Image bgImage, Image fgImage, int ofsw,
			int ofsh) {
		if ((bgImage != null) && (fgImage != null)) {
			return (new ImageIcon(overlayImages(bgImage, fgImage, ofsw, ofsh)));
		}

		return (null);
	}

	public static Icon overlayIcons(Icon bgIcon, Icon fgIcon, int ofsw, int ofsh) {
		return (overlayIcons(bgIcon, getImage(fgIcon), ofsw, ofsh));
	}

	public static Icon overlayIcons(Image bgImage, Icon fgIcon, int ofsw,
			int ofsh) {
		Icon finalIcon = overlayIcons(bgImage, getImage(fgIcon), ofsw, ofsh);

		if (finalIcon == null) {
			finalIcon = new ImageIcon(bgImage);
		}

		return (finalIcon);
	}

	public static Icon overlayIcons(Icon bgIcon, Image fgImage, int ofsw,
			int ofsh) {
		Icon finalIcon = overlayIcons(getImage(bgIcon), fgImage, ofsw, ofsh);

		if (finalIcon == null) {
			finalIcon = bgIcon;
		}

		return (finalIcon);
	}

	public static Image overlayImages(Image bgImage, Image fgImage, int ofsw,
			int ofsh) {
		int bgw = bgImage.getWidth(null);
		int bgh = bgImage.getHeight(null);
		int fgw = fgImage.getWidth(null);
		int fgh = fgImage.getHeight(null);
		int w = Math.max(fgw, bgw);
		int h = Math.max(bgh, fgh);
		BufferedImage finalImage = new BufferedImage(w, h,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = finalImage.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(bgImage, 0, 0, null);
		int x = (ofsw < 0) ? (w - fgw - (ofsw + 1)) : ofsw;
		int y = (ofsh < 0) ? (h - fgh - (ofsh + 1)) : ofsh;
		g.drawImage(fgImage, x, y, null);
		g.dispose();
		return (finalImage);
	}
}
