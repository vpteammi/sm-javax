/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   UIMessageType.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31		prf    $$1     Created.
 */

package com.sm.javax.swingx;

public enum UIMessageType {
	ERROR, INFO, WARNING, TODO, OTHER
}
