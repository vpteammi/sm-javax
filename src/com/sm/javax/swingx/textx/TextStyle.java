package com.sm.javax.swingx.textx;

import java.awt.Color;

import com.sm.javax.langx.annotation.SetterFor;
import com.sm.javax.utilx.ColorUtils;
import com.sm.javax.xmlx.XMLInstantiator;
import com.sm.javax.xmlx.XMLSerializable;

@XMLSerializable
public class TextStyle
{
	public static final TextStyle EMPTY = new TextStyle ();
	public static final TextStyle BOLD = new TextStyle (null, true);

	public final Color foregroundColor;
	public final Color backgroundColor;
	public final boolean italic;
	public final boolean bold;
	public final boolean underline;

	public TextStyle ()
	{
		this ((Color) null, (Color) null, false, false, false);
	}

	public TextStyle (String foregroundColor, String backgroundColor,
			boolean bold, boolean italic, boolean underline)
	{
		this.foregroundColor = foregroundColor == null ? null
				: ColorUtils.getByHexCode (foregroundColor);
		this.backgroundColor = backgroundColor == null ? null
				: ColorUtils.getByHexCode (backgroundColor);
		this.italic = italic;
		this.bold = bold;
		this.underline = underline;
	}

	public TextStyle (Color foregroundColor, Color backgroundColor)
	{
		this (foregroundColor, backgroundColor, false, false, false);
	}

	public TextStyle (Color foregroundColor)
	{
		this (foregroundColor, null);
	}

	public TextStyle (Color foregroundColor, boolean bold)
	{
		this (foregroundColor, (Color) null, bold, false, false);
	}

	public TextStyle (boolean bold, boolean italic, boolean underline)
	{
		this ((Color) null, null, bold, italic, underline);
	}

	public TextStyle (Color foregroundColor, boolean bold, boolean italic,
			boolean underline)
	{
		this (foregroundColor, (Color) null, bold, italic, underline);
	}

	public TextStyle (TextStyle baseStyle, Color foregroundColor,
			Color backgroundColor, Boolean bold, Boolean italic,
			Boolean underline)
	{
		this (
				foregroundColor == null ? baseStyle.foregroundColor : foregroundColor,
				backgroundColor == null ? baseStyle.foregroundColor : foregroundColor,
				bold == null ? baseStyle.bold : bold,
				italic == null ? baseStyle.italic : italic,
				underline == null ? baseStyle.underline : underline);
	}

	@XMLInstantiator
	public TextStyle (@SetterFor ("foregroundColor") Color foregroundColor,
			@SetterFor ("backgroundColor") Color backgroundColor,
			@SetterFor ("bold") boolean bold, @SetterFor ("italic") boolean italic,
			@SetterFor ("underline") boolean underline)
	{
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
		this.italic = italic;
		this.bold = bold;
		this.underline = underline;
	}
}
