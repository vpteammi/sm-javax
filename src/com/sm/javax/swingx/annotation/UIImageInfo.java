package com.sm.javax.swingx.annotation;

import java.awt.Image;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import javax.swing.Icon;

import com.sm.javax.langx.reflect.ReflectionException;
import com.sm.javax.swingx.UIIcons;

public class UIImageInfo {
	private final Field field;
	private final Object value;

	private UIImageInfo(Field field, Object value) {
		super();
		this.field = field;
		this.value = value;
		field.setAccessible(true);
	}

	public static UIImageInfo get(Field field, UIImage image)
			throws ReflectionException {
		Class<?> fieldClass = field.getType();
		boolean isIcon = Icon.class.equals(fieldClass);
		boolean isImage = Image.class.equals(fieldClass);
		if (isIcon || isImage) {
			int modifiers = field.getModifiers();
			boolean isStatic = Modifier.isStatic(modifiers);
			if (isStatic) {
				if (!Modifier.isPrivate(modifiers)) {
					throw new ReflectionException(
							"@UIImage is not applicable to", field,
							": static field must be private");
				}
			}

			Class<?> resourceClass = image.root();

			if ((resourceClass == null) || UIImage.class.equals(resourceClass)) {
				resourceClass = field.getDeclaringClass();
			}

			String subdir = image.group();

			if (UIImage.SKIPPED.equals(subdir)) {
				subdir = null;
			}

			Object imageValue = null;

			String backgroundFile = image.background();
			Image backgroundImage = null;

			if (!UIImage.SKIPPED.equals(backgroundFile)) {
				backgroundImage = UIIcons.getImage(resourceClass, subdir,
						backgroundFile);

				if (backgroundImage == null) {
					throw new ReflectionException("@UIImage specified for",
							field, ": background image file \""
									+ backgroundFile + "\" is not found");
				}
			}

			String imageFile = image.name();

			if (isIcon) {
				Icon icon = UIIcons.getIcon(resourceClass, subdir, imageFile);

				if ((icon != null) && (backgroundImage != null)) {
					icon = UIIcons.overlayIcons(backgroundImage, icon, 0, 0);
				}

				imageValue = icon;
			} else {
				Image img = UIIcons.getImage(resourceClass, subdir, imageFile);

				if ((img != null) && (backgroundImage != null)) {
					img = UIIcons.overlayImages(backgroundImage, img, 0, 0);
				}

				imageValue = img;
			}

			if (imageValue == null) {
				throw new ReflectionException("@UIImage specified for", field,
						": image file \"" + imageFile + "\" is not found");
			}

			if (isStatic) {
				field.setAccessible(true);
				initialize(null, field, imageValue);
				return (null);
			} else {
				return (new UIImageInfo(field, imageValue));
			}
		} else {
			throw new ReflectionException("@UIImage is not applicable to",
					field, ": field must be either Image or Icon");
		}
	}

	public void initialize(Object obj) throws ReflectionException {
		initialize(obj, field, value);
	}

	private static void initialize(Object obj, Field field, Object imageValue)
			throws ReflectionException {
		try {
			field.set(obj, imageValue);
		} catch (IllegalArgumentException e) {
			throw new ReflectionException("Failed to initialize image field",
					field, null);
		} catch (IllegalAccessException e) {
			throw new ReflectionException("Failed to initialize image field",
					field, null);
		}
	}

	public static void initializeImages(Class<?> annotatedClass) {
		for (Field fld : annotatedClass.getDeclaredFields()) {
			if (Modifier.isStatic(fld.getModifiers())) {
				UIImage imageSpec = fld.getAnnotation(UIImage.class);
				if (imageSpec != null) {
					try {
						UIImageInfo.get(fld, imageSpec);
					} catch (ReflectionException e) {
						ReflectionException.reportException(annotatedClass, e,
								"Cannot initialize static images of "
										+ annotatedClass.getCanonicalName());
					}
				}
			}
		}
	}
}
