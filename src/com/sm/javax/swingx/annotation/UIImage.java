package com.sm.javax.swingx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.sm.javax.langx.annotation.Annotations;

/**
 * 
 * Specifies initialization of Image or Icon fields. The image will be searched
 * in "image" subdirectory of root class file.
 * UIAnnotations.initialize() method should be called to initialize annotated fields 
 * 
 * @author Olga
 */

@Retention (value = RetentionPolicy.RUNTIME)
@Target (ElementType.FIELD)
public @interface UIImage
{
	final static String SKIPPED = Annotations.OMITTED_STRING_VALUE;

	/**
	 * File name of image file. If file extension is omitted then ".png" and
	 * ".gif" will be tried
	 * 
	 * @return
	 */
	String name ();

	/**
	 * File name of background image file. If file extension is omitted then ".png" and
	 * ".gif" will be tried
	 * 
	 * @return
	 */
	String background () default SKIPPED;

	/**
	 * Name of subdirectory relative to "images" subdirectory of root class file
	 * 
	 * @return
	 */
	String group () default SKIPPED;

	/**
	 * Root class. The image will be searched in "images" subdirectory of root
	 * class file. If root is omitted than field containing class is used
	 * 
	 * @return
	 */
	Class<?> root () default UIImage.class;
}
