package com.sm.javax.swingx;

public interface UIActivatable
{
	public void onActivate ();
	
	public void onDeactivate ();
}
