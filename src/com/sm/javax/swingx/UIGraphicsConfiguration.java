package com.sm.javax.swingx;

import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;

public class UIGraphicsConfiguration
{
	public static Rectangle getPhysicalScreenSize() {
		final Dimension s = Toolkit.getDefaultToolkit().getScreenSize();
		return new Rectangle(0, 0, s.width, s.height);
	}

	public static Rectangle getVirtualScreenSize() {
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment ();
		Rectangle virtualBounds = new Rectangle ();
		GraphicsDevice[] gs = env.getScreenDevices ();
		for (int j = 0; j < gs.length; j++)
		{
			GraphicsDevice gd = gs[j];
			GraphicsConfiguration[] gc =
					gd.getConfigurations ();
			for (int i = 0; i < gc.length; i++)
			{
				virtualBounds = virtualBounds.union (gc[i].getBounds ());
			}
		}
		return new Rectangle (0, 0, virtualBounds.width, virtualBounds.height);
	}

}
