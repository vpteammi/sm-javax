/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   UIUtilities.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Created.
 */

package com.sm.javax.swingx;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.sm.javax.langx.Environment;
import com.sm.javax.langx.Strings;
import com.sm.javax.swingx.textx.TextStyle;

public class UIUtilities {
	@SuppressWarnings("serial")
	public static class NonEventDispatchThread extends Error {
		protected NonEventDispatchThread() {
		}
	}

	public static final void ensureEventDispatchThread() {
		if (!SwingUtilities.isEventDispatchThread()) {
			throw new NonEventDispatchThread();
		}
	}

	public static String getMessage(Object message) {
		if (message == null)
			return (null);
		else if (message instanceof Throwable)
			return getExceptionMessage(null, (Throwable) message, false);
		else
			return message.toString();
	}

	public static String getExceptionMessage(String title, Throwable ex, boolean multiLine) {
		StringBuilder message = new StringBuilder();

		if (title != null) {
			message.append(title);
		}

		while (ex != null) {
			String msg = ex.getMessage();
			ex = ex.getCause();
			if ((msg != null) && ((ex == null) || !msg.equals(ex.toString()))) {
				if (message.length() > 0) {
					if (multiLine) {
						message.append('\n');
					} else {
						message.append(": ");
					}
				}

				message.append(msg);
			}
		}

		return (message.toString());
	}

	public static void setLightWeightPopupEnabled(boolean enabled) {
		JPopupMenu.setDefaultLightWeightPopupEnabled(enabled);
		ToolTipManager.sharedInstance().setLightWeightPopupEnabled(enabled);
	}

	public static int[] getIndices(int size) {
		if (size < 0)
			return (null);

		int[] indices = new int[size];

		for (int i = 0; i < size; i++) {
			indices[i] = i;
		}

		return (indices);
	}

	private static Boolean isWindowsLAF = null;

	public static boolean isWindowsLookAndFeel() {
		if (isWindowsLAF == null) {
			isWindowsLAF = UIManager.getLookAndFeel().getName().equalsIgnoreCase(WINDOWS_LAF.name);
		}
		return (isWindowsLAF);
	}

	public static class LAF {
		public final String name;

		LAF(String name) {
			this.name = name;
		}
	}

	public final static LAF WINDOWS_LAF = new LAF("windows");
	public final static LAF NIMBUS_LAF = new LAF("nimbus");
	public final static LAF NATIVE_LAF;

	static {
		NATIVE_LAF = (Environment.isWindowsOS() ? WINDOWS_LAF : null);
	}

	public static void setNativeLookAndFeel() {
		if (NATIVE_LAF != null) {
			setLookAndFeel(NATIVE_LAF);
		}
	}

	public static void setLookAndFeel(LAF lookandfeel) {
		for (LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
			if (laf.getName().equalsIgnoreCase(lookandfeel.name)) {
				try {
					UIManager.setLookAndFeel(laf.getClassName());
					break;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static boolean isHidingTreeNodesSupported() {
		return (!isWindowsLookAndFeel());
	}

	public static void setBackground(Component comp, Color color) {
		if (!(comp instanceof JButton)) {
			comp.setBackground(color);
			if (comp instanceof Container)
				for (Component component : ((Container) comp).getComponents()) {
					setBackground(component, color);
				}
		}
	}

	public static Font deriveFont(Font font, TextStyle style) {
		int fontStyle = Font.PLAIN;
		if (style.italic)
			fontStyle += Font.ITALIC;
		if (style.bold)
			fontStyle += Font.BOLD;
		font = font.deriveFont(fontStyle);
		Map<TextAttribute, Integer> fontAttributes = new HashMap<TextAttribute, Integer>();
		if (style.underline) {
			fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		}
		return (font.deriveFont(fontAttributes));
	}

	public static void setTextStyle(Component comp, TextStyle style) {
		comp.setFont(deriveFont(comp.getFont(), style));
		comp.setForeground(style.foregroundColor);
		comp.setBackground(style.backgroundColor);
	}

	public static Rectangle getMaximumWindowBounds() {
		final Dimension s = Toolkit.getDefaultToolkit().getScreenSize();
		return new Rectangle(0, 0, s.width, s.height);
	}

	public static void positionComponentRelativeToParent(final Component dialog, final double horizontalPercent,
			final double verticalPercent) {
		final Dimension d = dialog.getSize();
		final Container parent = dialog.getParent();
		final Dimension p = parent.getSize();

		final int baseX = parent.getX() - d.width;
		final int baseY = parent.getY() - d.height;
		final int w = d.width + p.width;
		final int h = d.height + p.height;
		int x = baseX + (int) (horizontalPercent * w);
		int y = baseY + (int) (verticalPercent * h);

		// make sure the dialog fits completely on the screen...
		final Rectangle s = getMaximumWindowBounds();
		x = Math.min(x, (s.width - d.width));
		x = Math.max(x, 0);
		y = Math.min(y, (s.height - d.height));
		y = Math.max(y, 0);

		dialog.setBounds(x + s.x, y + s.y, d.width, d.height);
	}

	public static int getStringWidth(Component component, String str) {
		return (getStringWidth(component, component.getFont(), str));
	}

	public static int getStringWidth(Component component, Font font, String str) {
		return ((int) getStringBounds(component, font, str).getWidth());
	}

	public static Rectangle2D getStringBounds(Component component, String str) {
		return (getStringBounds(component, component.getFont(), str));
	}

	public static Rectangle2D getStringBounds(Component component, Font font, String str) {
		return (font.getStringBounds(str, ((Graphics2D) (component.getGraphics())).getFontRenderContext()));
	}

	public static FontMetrics getFontMetrics(Component component, Font font) {
		return component.getFontMetrics(font);
	}

	static void activateComponentsTree(Component root, boolean activate) {
		if (root instanceof UIActivatable) {
			if (activate) {
				((UIActivatable) root).onActivate();
			} else {
				((UIActivatable) root).onDeactivate();
			}
		}
		if (root instanceof Container) {
			for (Component component : ((Container) root).getComponents()) {
				activateComponentsTree(component, activate);
			}
		}
	}

	public static String buildComponentName(String name, Object parent) {
		if (parent instanceof Component) {
			return buildComponentName(name, (Component) parent);
		}

		return (name);
	}

	public static String buildComponentName(String name, Component parent) {
		String parentName = parent.getName();
		if (!Strings.isEmpty(parentName)) {
			name = parentName + "." + name;
		}

		return (name);
	}

	public static void setComponentName(Component comp, String name, Component parent) {
		comp.setName(buildComponentName(name, parent));
	}

	public static void setComponentName(Component comp, String name, Object parent) {
		comp.setName(buildComponentName(name, parent));
	}
	
	static public String getHumanFormatForBytes (long bytes)
	{
		return getHumanFormatForBytes (bytes, false);
	}

	static private final long CONST_FOR_KB = 1024;
	static private final long CONST_FOR_MB = CONST_FOR_KB * 1024;
	static private final long CONST_FOR_GB = CONST_FOR_MB * 1024;

	static public String getHumanFormatForBytes (long bytes, boolean shortFormat)
	{
		if (bytes > CONST_FOR_GB)
		{
			double Gbytes = (double) bytes / CONST_FOR_GB;
			return (shortFormat ? String.format ("%.1f GB", Gbytes) : String.format (
					"%.1f GB  (%d bytes)", Gbytes, bytes));
		}
		else if (bytes > CONST_FOR_MB)
		{
			double Mbytes = (double) bytes / CONST_FOR_MB;
			return (shortFormat ? String.format ("%.1f MB", Mbytes) : String.format (
					"%.1f MB  (%d bytes)", Mbytes, bytes));
		}
		else if (bytes > CONST_FOR_KB)
		{
			double Kbytes = (double) bytes / CONST_FOR_KB;
			return (shortFormat ? String.format ("%.1f KB", Kbytes) : String.format (
					"%.1f KB  (%d bytes)", Kbytes, bytes));
		}
		else
		{
			return String.format ("%d bytes", bytes);
		}
	}
}
