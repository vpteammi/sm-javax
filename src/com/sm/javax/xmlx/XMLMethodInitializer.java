package com.sm.javax.xmlx;

import java.beans.Encoder;
import java.beans.Expression;
import java.beans.Statement;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;

import com.sm.javax.langx.Strings;
import com.sm.javax.langx.annotation.SetterFor;
import com.sm.javax.langx.reflect.Classes;
import com.sm.javax.langx.reflect.MethodAmbiguityException;
import com.sm.javax.langx.reflect.ValueAccessor;

class XMLMethodInitializer {
	private final ArrayList<ValueAccessor<Object>> initValues = new ArrayList<ValueAccessor<Object>>();
	private final Class<?> declaringClass;
	private final String name;

	private static SetterFor getSetterAnnotation(Annotation[] annotations) {
		if (annotations != null) {
			for (Annotation annotation : annotations) {
				if (annotation instanceof SetterFor) {
					return ((SetterFor) annotation);
				}
			}
		}

		return (null);
	}

	private ValueAccessor<Object> findAccessor(Class<?> des11nType, String name)
			throws MethodAmbiguityException, XmlSerializationException {
		ValueAccessor<Object> accessor = ValueAccessor.getAccessor(declaringClass, true, name);
		if ((accessor == null) && !name.startsWith("get")) {
			accessor = ValueAccessor.getAccessor(declaringClass, true, "get" + Strings.toFirstCharUpperCase(name));
		}

		if (accessor != null) {
			if (!Classes.isAssignableToFrom(des11nType, accessor.getType())) {
				throw new XmlSerializationException(null, accessor, "cannot be used to serialize value");
			}
		}

		return (accessor);
	}

	private void buildInitValues(Class<?>[] parmTypes, Annotation[][] parmAnnotations)
			throws MethodAmbiguityException, XmlSerializationException {
		if ((name != null) && (parmTypes.length == 1) && (getSetterAnnotation(parmAnnotations[0]) == null)
				&& (name.length() > 3) && (name.startsWith("set"))) {
			String property = Strings.toFirstCharLowerCase(name.substring(3));
			ValueAccessor<Object> accessor = findAccessor(parmTypes[0], property);
			if (accessor != null) {
				initValues.add(accessor);
			} else {
				throw new XmlSerializationException(
						"Cannot find field or getter for property '" + property + "' to serialize value");
			}
		} else {
			int size = parmTypes.length;

			for (int i = 0; i < size; i++) {
				SetterFor setter = getSetterAnnotation(parmAnnotations[i]);
				if (setter == null) {
					throw new XmlSerializationException("@SetterFor is missing for parameter #" + i);
				}
				String property = setter.value();
				if (Strings.isEmpty(property)) {
					throw new XmlSerializationException("@SetterFor has empty value for parameter #" + i);
				}
				ValueAccessor<Object> accessor = findAccessor(parmTypes[i], setter.value());
				if (accessor != null) {
					initValues.add(accessor);
				} else {
					throw new XmlSerializationException("cannot find field or getter for parameter #" + i
							+ " @SetterFor ('" + property + "') to serialize value");
				}
			}
		}
	}

	private XMLMethodInitializer(Class<?> declaringClass, String name) {
		this.declaringClass = declaringClass;
		this.name = name;
	}

	public XMLMethodInitializer(Constructor<?> constructor) throws XmlSerializationException {
		this(constructor.getDeclaringClass(), null);

		try {
			buildInitValues(constructor.getParameterTypes(), constructor.getParameterAnnotations());
		} catch (MethodAmbiguityException e) {
			throw new XmlSerializationException(null, constructor, e.getMessage());
		} catch (XmlSerializationException e) {
			throw new XmlSerializationException(null, constructor, e.getMessage());
		}
	}

	public XMLMethodInitializer(Method method) throws XmlSerializationException {
		this(method.getDeclaringClass(), method.getName());

		try {
			buildInitValues(method.getParameterTypes(), method.getParameterAnnotations());
		} catch (MethodAmbiguityException e) {
			throw new XmlSerializationException(null, method, e.getMessage());
		} catch (XmlSerializationException e) {
			throw new XmlSerializationException(null, method, e.getMessage());
		}
	}

	public Object[] getValues(Object obj) {
		int size = initValues.size();
		Object[] values = new Object[size];

		for (int i = 0; i < size; i++) {
			values[i] = initValues.get(i).getValue(obj);
		}
		return (values);
	}

	public Expression instantiate(Object obj) {
		return instantiate(obj, getValues(obj));
	}

	public Expression instantiate(Object obj, Object[] values) {
		return new Expression(obj, obj.getClass(), "new", values);
	}

	public void initialize(Object obj, Encoder out) {
		out.writeStatement(new Statement(obj, name, getValues(obj)));
	}
}
