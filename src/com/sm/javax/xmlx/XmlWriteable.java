package com.sm.javax.xmlx;

public interface XmlWriteable
{
	public void writeToXml (XmlWriter writer);
}
