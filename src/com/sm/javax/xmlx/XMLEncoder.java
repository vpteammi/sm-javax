package com.sm.javax.xmlx;

import java.beans.PersistenceDelegate;
import java.io.OutputStream;

class XMLEncoder extends java.beans.XMLEncoder
{
	public XMLEncoder (OutputStream out)
	{
		super (out);
	}

	@Override
	public PersistenceDelegate getPersistenceDelegate (Class<?> cls)
	{
		try
		{
			XMLSerializableClass serializer = XMLSerializableClass.get (cls);
			
			if (serializer != null)
			{
				return (serializer);
			}
		}
		catch (XmlSerializationException e)
		{
			e.reportException (cls);
		}
		return super.getPersistenceDelegate (cls);
	}
}
