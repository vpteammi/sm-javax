/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   UwgmExBeanAttributeAccessor.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Renamed as UwgmExBeanAttributeAccessor.java for uwgmexplorer
 */

package com.sm.javax.xmlx;

/*
 * %W% %E%
 * 
 * Copyright 2006, 2007 Software Marathon, Inc. All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *  - Redistribution in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * Neither the name of Software Marathon, Inc. or the names of contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. AND ITS LICENSORS SHALL NOT BE LIABLE
 * FOR ANY DAMAGES OR LIABILITIES SUFFERED BY LICENSEE AS A RESULT OF OR
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SOFTWARE MARATHON OR ITS LICENSORS BE LIABLE
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL,
 * CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS
 * OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE
 * THIS SOFTWARE, EVEN IF SOFTWARE MARATHON HAS BEEN ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGES.
 * 
 * You acknowledge that this software is not designed, licensed or intended for
 * use in the design, construction, operation or maintenance of any nuclear
 * facility.
 */

/*
 * Donated to PTC by Software Marathon, Inc. Can be used as-is without any
 * liability. All rights for the initaial implementation belongs to Software
 * Marathon, Inc.
 */

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.sm.javax.utilx.Log;
import com.sm.javax.utilx.LogFactory;

class XmlAttributeAccessor
{
	private static interface Converter
	{
		public Object convert (String value) throws ParseException;
	};

	public final static HashMap<Class<?>, Converter> converters = new HashMap<Class<?>, Converter> ();
	private static final Log logger = LogFactory.getLog (XmlAttributeAccessor.class.getName ());

	private static final class DateConverter implements Converter
	{
		private final DateFormat format;

		public DateConverter (DateFormat format)
		{
			this.format = format;
		}

		public DateConverter (String pattern)
		{
			this (new SimpleDateFormat (pattern));
		}

		public DateConverter (int style)
		{
			this (DateFormat.getDateInstance (style));
		}

		public Object convert (String value) throws ParseException
		{
			return (format.parse (value));
		}
	}

	public final static DateConverter defaultDateConverter = new DateConverter (
			DateFormat.getDateInstance (DateFormat.SHORT));

	static
	{
		converters.put (String.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (value);
			}
		});
		converters.put (Integer.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (Integer.parseInt (value));
			}
		});
		converters.put (int.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (Integer.parseInt (value));
			}
		});
		converters.put (long.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (Long.parseLong (value));
			}
		});
		converters.put (Long.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (Long.parseLong (value));
			}
		});
		converters.put (double.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (Double.parseDouble (value));
			}
		});
		converters.put (Double.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (Double.parseDouble (value));
			}
		});
		converters.put (Character.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (value.charAt (0));
			}
		});
		converters.put (char.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (value.charAt (0));
			}
		});
		converters.put (boolean.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (Boolean.valueOf (value));
			}
		});
		converters.put (Boolean.class, new Converter ()
		{
			public Object convert (String value)
			{
				return (Boolean.valueOf (value));
			}
		});
	}

	private Class<?> beanType = null;

	private String name = null;

	private Class<?> type = null;

	private Field field = null;

	private Method method = null;

	private Converter converter = null;

	private boolean useAlternativeSetter = false;

	public XmlAttributeAccessor (Class<?> beanType, String name)
			throws NoSuchMethodException, SecurityException
	{
		this.name = name;
		this.beanType = beanType;
		AnnotatedElement annotated = null;
		if (beanType.isArray ())
		{
			type = beanType.getComponentType ();
		}
		else
		{
			Method defaultSetter = null;
			Method alternativeSetter = null;
			String setterName = "set" + name.substring (0, 1).toUpperCase ()
					+ name.substring (1);
			Method[] methods = beanType.getMethods ();
			if (methods != null)
			{
				for (Method setter : methods)
				{
					if (setter.getName ().equals (setterName))
					{
						Class<?>[] parameters = setter.getParameterTypes ();

						if (parameters.length == 1)
						{
							if (defaultSetter != null)
							{
								throw new NoSuchMethodException (
										"More than one setter '" + setterName
												+ "' found in class "
												+ beanType.getName ());
							}
							else
							{
								defaultSetter = setter;
							}
						}
						else if ((parameters.length > 1)
								&& isAlternativeSetter (setter))
						{
							if (alternativeSetter != null)
							{
								throw new NoSuchMethodException (
										"More than one setter '" + setterName
												+ "' found in class "
												+ beanType.getName ());
							}
							else
							{
								alternativeSetter = setter;
							}
						}
					}

					if (alternativeSetter != null)
					{
						method = alternativeSetter;
						useAlternativeSetter = true;
					}
					else if (defaultSetter != null)
					{
						method = defaultSetter;
					}
					if (method != null)
					{
						type = method.getParameterTypes ()[0];
						annotated = method;
					}
				}
			}

			if (method == null)
			{
				try
				{
					field = beanType.getDeclaredField (name);
					field.setAccessible (true);
					type = field.getType ();
					annotated = field;
				}
				catch (NoSuchFieldException ex)
				{
					logger.error ("exception occured", ex);
				}
			}
		}

		if ((type != null) && !type.isArray ())
		{
			converter = converters.get (type);

			if (converter == null)
			{
				if (type.isAssignableFrom (java.util.Date.class))
				{
					XmlValueFormat format = (XmlValueFormat) annotated
							.getAnnotation (XmlValueFormat.class);
					if (format != null)
					{
						String formatString = format.format ();
						if ((formatString != null)
								&& (formatString.length () > 0))
						{
							converter = new DateConverter (formatString);
						}
						else
						{
							converter = new DateConverter (format.style ());
						}
					}
					else
					{
						converter = defaultDateConverter;
					}
				}
			}
		}
	}

	protected boolean isAlternativeSetter (Method method)
	{
		return (false);
	}

	public String getName ()
	{
		return (name);
	}

	public Class<?> getType ()
	{
		return (type);
	}

	public boolean exists ()
	{
		return (type != null);
	}

	public boolean isPrimitive ()
	{
		return ((type != null) && (converter != null));
	}

	public boolean isArray ()
	{
		return ((type != null) && type.isArray ());
	}

	public Object newInstance () throws IllegalAccessException,
			InstantiationException
	{
		if (type == null)
			throw new InstantiationException ();
		if (isArray ())
		{
			return new ArrayList<Object> ();
		}
		else
		{
			return type.newInstance ();
		}
	}

	public Object parseValue (String value) throws ParseException
	{
		return (((value == null) || (converter == null)) ? value : converter
				.convert (value));
	}

	public void setStringValue (Object obj, String value)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, ParseException
	{
		setValue (obj, parseValue (value));
	}

	protected Object completeValue (Object value)
	{
		if ((value != null) && isArray () && (value instanceof Collection))
		{
			Collection<?> list = (Collection<?>) value;
			Object[] array = (Object[]) java.lang.reflect.Array.newInstance (
					type.getComponentType (), list.size ());
			return (list.toArray (array));
		}
		else
		{
			return (value);
		}
	}

	protected void setCompleteValue (Object obj, Object value)
			throws InvocationTargetException, IllegalArgumentException,
			IllegalAccessException
	{
		if (method != null)
		{
			if (useAlternativeSetter)
			{
				throw new IllegalArgumentException (
						"Alternative setter should be called to set value '"
								+ name + "' in class " + beanType.getName ());
			}
			method.invoke (obj, new Object[] {value});
		}
		else if (field != null)
		{
			field.set (obj, value);
		}
	}

	public void setValue (Object obj, Object value)
			throws InvocationTargetException, IllegalArgumentException,
			IllegalAccessException
	{
		setCompleteValue (obj, completeValue (value));
	}
}
