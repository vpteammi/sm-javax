package com.sm.javax.xmlx;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks class constructor used by XML decoder for instantiation of serialized object
 * Types and names of method parameters must match types and names of corresponding serialized fields or getters
 * @author Olga
 *
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(ElementType.CONSTRUCTOR)
public @interface XMLInstantiator {

}
