package com.sm.javax.xmlx;

import java.beans.Encoder;
import java.beans.Expression;
import java.beans.PersistenceDelegate;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

class XMLSerializableClass extends PersistenceDelegate {
	public XMLSerializableClass() {
		superCls = null;
		instantiator = null;
		initializers = null;
	}

	public XMLSerializableClass(Class<?> cls) throws XmlSerializationException {
		Class<?> parentCls = cls.getSuperclass();

		while ((parentCls != null) && !isXMLSerializable(parentCls))
			parentCls = parentCls.getSuperclass();

		superCls = get(parentCls);

		Constructor<?> defaultConstructor = null;
		Constructor<?> des11nConstructor = null;

		for (Constructor<?> constructor : cls.getConstructors()) {
			if (constructor.getParameterTypes().length == 0) {
				defaultConstructor = constructor;
			}
			if (constructor.getAnnotation(XMLInstantiator.class) != null) {
				if (!Modifier.isPublic(constructor.getModifiers())) {
					throw new XmlSerializationException("Non-public constructor", constructor,
							"marked with @XMLInstantiator");
				}
				if (des11nConstructor == null) {
					des11nConstructor = constructor;
				} else {
					throw new XmlSerializationException(
							"More than one @XMLInstantiator constructor Class:" + cls.getCanonicalName());
				}
			}
		}

		initializers = new ArrayList<XMLMethodInitializer>();

		Method des11nMethod = null;

		for (Method method : cls.getDeclaredMethods()) {
			if (method.getAnnotation(XMLInstantiator.class) != null) {
				if (!Modifier.isPublic(method.getModifiers())) {
					throw new XmlSerializationException("Non-public factory method", method,
							"marked with @XMLInstantiator");
				}
				if (!Modifier.isStatic(method.getModifiers())) {
					throw new XmlSerializationException("Non-static factory method", method,
							"marked with @XMLInstantiator");
				}

				if (des11nMethod == null) {
					des11nMethod = method;
				} else {
					throw new XmlSerializationException("More than one @XMLInstantiator static method");
				}
			}
			if (method.getAnnotation(XMLInitializer.class) != null) {
				if (!Modifier.isPublic(method.getModifiers())) {
					throw new XmlSerializationException("Non-public method", method, "marked with @XMLInitializer");
				}
				initializers.add(new XMLMethodInitializer(method));
			}
		}

		if ((des11nConstructor != null) && (des11nMethod != null)) {
			throw new XmlSerializationException("Both @XMLInstantiator constructor and factory method are declared");
		} else if (des11nConstructor != null) {
			instantiator = new XMLMethodInitializer(des11nConstructor);
		} else if (des11nMethod != null) {
			instantiator = new XMLMethodInitializer(des11nMethod);
		} else if (defaultConstructor != null) {
			instantiator = new XMLMethodInitializer(defaultConstructor);
		} else {
			throw new XmlSerializationException(
					"Neither default constructor nor @XMLInstantiator constructor is defined");
		}
	}

	private final XMLSerializableClass superCls;
	private final XMLMethodInitializer instantiator;
	private final ArrayList<XMLMethodInitializer> initializers;

	private static boolean isXMLSerializable(Class<?> cls) {
		return (cls.getAnnotation(XMLSerializable.class) != null);
	}

	public static XMLSerializableClass get(Class<?> cls) throws XmlSerializationException {
		if (cls == null)
			return (null);

		XMLSerializableClass s11nCls = classes.get(cls);

		if (s11nCls == null) {
			if (isXMLSerializable(cls)) {
				try {
					s11nCls = new XMLSerializableClass(cls);
				} catch (XmlSerializationException e) {
					classes.put(cls, undefinedXMLS11n);
					throw e;
				}
			} else {
				s11nCls = undefinedXMLS11n;
			}

			classes.put(cls, s11nCls);
		}

		return (s11nCls == undefinedXMLS11n ? null : s11nCls);
	}

	public Expression instantiate(Object obj) {
		return (instantiator.instantiate(obj));
	}

	public void initialize(Object obj, Encoder out) {
		if (superCls != null) {
			superCls.initialize(obj, out);
		}
		for (XMLMethodInitializer initializer : initializers) {
			initializer.initialize(obj, out);
		}
	}

	private final static XMLSerializableClass undefinedXMLS11n = new XMLSerializableClass();

	private final static ConcurrentHashMap<Class<?>, XMLSerializableClass> classes = new ConcurrentHashMap<Class<?>, XMLSerializableClass>();

	@Override
	protected Expression instantiate(Object obj, Encoder out) {
		return instantiate(obj);
	}

	@Override
	protected void initialize(Class<?> type, Object oldInstance, Object newInstance, Encoder out) {
		initialize(oldInstance, out);
	}
}
