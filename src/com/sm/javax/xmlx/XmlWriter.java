package com.sm.javax.xmlx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class XmlWriter extends PrintWriter
{
	public XmlWriter (Writer out)
	{
		super (out);
		printHeader ();
	}

	public XmlWriter (OutputStream out)
	{
		super (out);
		printHeader ();
	}

	public XmlWriter (String fileName) throws FileNotFoundException
	{
		super (fileName);
		printHeader ();
	}

	public XmlWriter (File file) throws FileNotFoundException
	{
		super (file);
		printHeader ();
	}

	public XmlWriter (Writer out, boolean autoFlush)
	{
		super (out, autoFlush);
		printHeader ();
	}

	public XmlWriter (OutputStream out, boolean autoFlush)
	{
		super (out, autoFlush);
		printHeader ();
	}

	public XmlWriter (String fileName, String csn) throws FileNotFoundException,
			UnsupportedEncodingException
	{
		super (fileName, csn);
		printHeader ();
	}

	public XmlWriter (File file, String csn) throws FileNotFoundException,
			UnsupportedEncodingException
	{
		super (file, csn);
		printHeader ();
	}

	private void printHeader ()
	{
		println ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	}

	public void openTag (String tag)
	{
		println ("<" + tag + ">");
	}

	public void closeTag (String tag)
	{
		println ("</" + tag + ">");
	}

	public void printTag (String tag, Object value)
	{
		printTag (tag, value, false);
	}

	public void printGroup (String groupTag, String itemTag, Object[] values)
	{
		printGroup (groupTag, itemTag, values, false);
	}

	public <T> void printGroup (String groupTag, String itemTag,
			Iterable<T> values)
	{
		printGroup (groupTag, itemTag, values, false);
	}

	public <T> void printGroup (String groupTag, String itemTag,
			Iterable<T> values, boolean isCDATA)
	{
		openTag (groupTag);
		for (Object value : values)
		{
			if (value instanceof XmlWriteable)
			{
				openTag (itemTag);
				((XmlWriteable) value).writeToXml (this);
				closeTag (itemTag);
			}
			else
			{
				printTag (itemTag, value, isCDATA);
			}
		}
		closeTag (groupTag);
	}

	public void printGroup (String groupTag, String itemTag, Object[] values,
			boolean isCDATA)
	{
		openTag (groupTag);
		for (Object value : values)
		{
			if (value instanceof XmlWriteable)
			{
				openTag (itemTag);
				((XmlWriteable) value).writeToXml (this);
				closeTag (itemTag);
			}
			else
			{
				printTag (itemTag, value, isCDATA);
			}
		}
		closeTag (groupTag);
	}

	public void printTag (String tag, Object value, boolean isCDATA)
	{
		String strValue = (value == null ? "" : String.valueOf (value));

		if (isCDATA)
		{
			strValue.replace ("]]>", "]]]]><![CDATA[>");
			printf ("<%s><![CDATA[%s]]></%s>", tag, value, tag);
		}
		else
		{
			if ((strValue.indexOf ("&") > -1) || (strValue.indexOf ("<") > -1)
					|| (strValue.indexOf (">") > -1))
			{
				throw new IllegalArgumentException ("Text cannot contain <, > or &");
			}
			printf ("<%s>%s</%s>", tag, value, tag);
		}
	}
}
