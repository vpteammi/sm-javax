/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:    UwgmExXmlParser.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Renamed as  UwgmExXmlParser.java for uwgmexplorer
 */

package com.sm.javax.xmlx;

/*
 * %W% %E%
 * 
 * Copyright 2006, 2007 Software Marathon, Inc. All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *  - Redistribution in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * Neither the name of Software Marathon, Inc. or the names of contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. AND ITS LICENSORS SHALL NOT BE LIABLE
 * FOR ANY DAMAGES OR LIABILITIES SUFFERED BY LICENSEE AS A RESULT OF OR
 * RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SOFTWARE MARATHON OR ITS LICENSORS BE LIABLE
 * FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL,
 * CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS
 * OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE
 * THIS SOFTWARE, EVEN IF SOFTWARE MARATHON HAS BEEN ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGES.
 * 
 * You acknowledge that this software is not designed, licensed or intended for
 * use in the design, construction, operation or maintenance of any nuclear
 * facility.
 */

/*
 * Donated to PTC by Software Marathon, Inc. Can be used as-is without any
 * liability. All rights for the initaial implementation belongs to Software
 * Marathon, Inc.
 */

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sm.javax.utilx.Log;
import com.sm.javax.utilx.LogFactory;

public class XmlParser {
	private static SAXParserFactory saxFactory = null;
	private static final Log logger = LogFactory.getLog(XmlParser.class
			.getName());

	private final static synchronized SAXParserFactory getSaxFactory() {
		if (saxFactory == null) {
			saxFactory = SAXParserFactory.newInstance();
		}

		return (saxFactory);
	}

	public final static Object parseXml(InputStream input, Class<?> topType)
			throws SAXException, IOException {
		return (parseXml(input, new Handler(topType)));
	}

	public final static Object parseXml(InputStream input, Object topObject)
			throws SAXException, IOException {
		return (parseXml(input, new Handler(topObject)));
	}

	private final static Object parseXml(InputStream input, Handler handler)
			throws SAXException, IOException {
		SAXParser sp = null;
		try {
			sp = getSaxFactory().newSAXParser();
		} catch (ParserConfigurationException ex) {
			throw new SAXException(ex);
		}
		sp.parse(input, handler);
		return (handler.getObject());
	}

	private final static Constructor<?> getObjectConstructor() {
		try {
			return (Object.class.getConstructor(new Class[0]));
		} catch (SecurityException ex) {
			logger.error("exception occured", ex);
		} catch (NoSuchMethodException ex) {
			logger.error("exception occured", ex);
		}
		logger.error("Cannot get default public constructor for class Object");
		// System.err
		// .println("Cannot get default public constructor for class Object");
		return (null);
	}

	private final static Constructor<?> noConstructor = getObjectConstructor();

	private final static Class<?>[] saxConstructorArgs = new Class[] { Attributes.class };

	private final static class Handler extends DefaultHandler {
		private final HashMap<Class<?>, HashMap<String, AttributeAccessor>> classCache = new HashMap<Class<?>, HashMap<String, AttributeAccessor>>();

		private Constructor<?> getSaxConstructor(Class<?> type) {
			Constructor<?> c = constructorCache.get(type);
			if (c == null) {
				try {
					c = type.getConstructor(saxConstructorArgs);
				} catch (SecurityException ex) {
					logger.error("exception occured", ex);
					c = noConstructor;
				} catch (NoSuchMethodException ex) {
					logger.error("exception occured", ex);
					c = noConstructor;
				}
				constructorCache.put(type, c);
			}

			return ((c == noConstructor) ? null : c);
		}

		private final HashMap<Class<?>, Constructor<?>> constructorCache = new HashMap<Class<?>, Constructor<?>>();

		private final ArrayList<String> tagStack = new ArrayList<String>();

		private final ArrayList<Object> objectStack = new ArrayList<Object>();

		private final ArrayList<Class<?>> typeStack = new ArrayList<Class<?>>();

		private final ArrayList<Attributes> attributesStack = new ArrayList<Attributes>();

		private final ArrayList<AttributeAccessor> accessorStack = new ArrayList<AttributeAccessor>();

		private final ArrayList<HashMap<String, AttributeAccessor>> accessorsStack = new ArrayList<HashMap<String, AttributeAccessor>>();

		private Object topObject = null;

		private final Class<?> topType;

		private int topIndex = 0;

		private String stringValue;

		public Handler(Class<?> topType) {
			this.topType = topType;
		}

		public Handler(Object topObject) throws SAXException {
			this(topObject.getClass());
			try {
				pushValue("", topObject);
			} catch (IllegalAccessException ex) {
				logger.error("exception occured", ex);
				throw makeSAXException(ex);
			}
		}

		private void pushValue(String tagName, Object value)
				throws IllegalAccessException {
			pushValue(tagName, value.getClass(), value, null, null);
		}

		private void pushValue(String tagName, Class<?> type,
				AttributeAccessor accessor, Attributes attributes)
				throws IllegalAccessException, InstantiationException,
				IllegalArgumentException, InvocationTargetException {
			pushValue(tagName, type, newValue(type, attributes), accessor,
					attributes);
		}

		private void pushValue(String tagName, Class<?> type, Object value,
				AttributeAccessor accessor, Attributes attributes)
				throws IllegalAccessException {
			HashMap<String, AttributeAccessor> accessors = classCache.get(type);

			if (accessors == null) {
				accessors = new HashMap<String, AttributeAccessor>();
				classCache.put(type, accessors);
			}

			if (topIndex == objectStack.size()) {
				tagStack.add(tagName);
				objectStack.add(value);
				typeStack.add(type);
				accessorStack.add(accessor);
				accessorsStack.add(accessors);
				attributesStack.add(attributes);
			} else {
				tagStack.set(topIndex, tagName);
				objectStack.set(topIndex, value);
				typeStack.set(topIndex, type);
				accessorStack.set(topIndex, accessor);
				accessorsStack.set(topIndex, accessors);
				attributesStack.set(topIndex, attributes);
			}
			topIndex++;
		}

		private Object newValue(Class<?> type, Attributes attributes)
				throws IllegalAccessException, InstantiationException,
				InvocationTargetException, IllegalArgumentException {
			if (type == null) {
				return (null);
			} else if (type.isArray()) {
				return (new ArrayList<Object>());
			} else if (!type.isPrimitive()) {
				Constructor<?> saxConstructor = getSaxConstructor(type);
				if (saxConstructor != null) {
					return saxConstructor.newInstance(attributes);
				} else {
					return (type.newInstance());
				}
			} else {
				return (null);
			}
		}

		private void pushValue(String tagName, Class<?> type,
				Attributes attributes) throws IllegalAccessException,
				InstantiationException, InvocationTargetException,
				IllegalArgumentException {
			pushValue(tagName, type, null, attributes);
		}

		private void pushValue(String tagName, AttributeAccessor accessor,
				Attributes attributes) throws InstantiationException,
				IllegalAccessException, InvocationTargetException,
				IllegalArgumentException {
			if (!accessor.exists()) {
				pushValue(tagName, null, accessor, attributes);
			} else {
				pushValue(tagName, accessor.getType(), accessor, attributes);
			}
		}

		private SAXException makeSAXException(Exception ex) {
			return makeSAXException(ex, tagStack.get(topIndex - 1));
		}

		private SAXException makeSAXException(Exception ex, String tagName) {
			return new SAXException("Error while processing tag <" + tagName
					+ ">", ex);
		}

		public Object getObject() {
			return (topObject);
		}

		private final static boolean debugOut = false;

		private void debugTag(String qName, boolean open) {
			if (debugOut) {
				for (int i = 0; i < topIndex; i++)
					System.out.print(' ');
				System.out.print('<');
				if (!open)
					System.out.print('/');
				System.out.print(qName);
				System.out.println('>');
			}
		}

		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			debugTag(qName, true);
			stringValue = "";
			try {
				if (topIndex == 0) {
					pushValue(qName, topType, attributes);
				} else {
					int curIndex = topIndex - 1;
					Class<?> beanType = typeStack.get(curIndex);
					if (beanType == null) {
						pushValue(qName, null, null, attributes);
					} else {
						HashMap<String, AttributeAccessor> accessors = accessorsStack
								.get(curIndex);
						AttributeAccessor accessor = accessors.get(qName);
						if (accessor == null) {
							String name = qName.toLowerCase()
									.replaceAll("-", "_").replace(":", "_");
							accessor = new AttributeAccessor(beanType, name);
							accessors.put(qName, accessor);
						}
						pushValue(qName, accessor, attributes);
					}
				}
			} catch (InvocationTargetException ex) {
				logger.error("exception occured", ex);
				throw makeSAXException(ex, qName);
			} catch (IllegalArgumentException ex) {
				logger.error("exception occured", ex);
				throw makeSAXException(ex, qName);
			} catch (InstantiationException ex) {
				logger.error("exception occured", ex);
				throw makeSAXException(ex, qName);
			} catch (IllegalAccessException ex) {
				logger.error("exception occured", ex);
				throw makeSAXException(ex, qName);
			} catch (SecurityException ex) {
				logger.error("exception occured", ex);
				throw makeSAXException(ex, qName);
			} catch (NoSuchMethodException ex) {
				logger.error("exception occured", ex);
				throw makeSAXException(ex, qName);
			}
		}

		public void characters(char[] ch, int start, int length)
				throws SAXException {
			stringValue = stringValue + new String(ch, start, length);
		}

		@SuppressWarnings("unchecked")
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			int curIndex = topIndex - 1;
			if (curIndex != 0) {
				AttributeAccessor accessor = accessorStack.get(curIndex);
				if (accessor != null) {
					int parentIndex = curIndex - 1;
					Object parent = objectStack.get(parentIndex);
					try {
						Class<?> parentType = typeStack.get(parentIndex);
						if (accessor.isPrimitive()) {
							if (parentType.isArray()) {
								((ArrayList<Object>) parent).add(accessor
										.parseValue(stringValue));
							} else {
								accessor.setStringValue(parent, stringValue);
							}
						} else {
							Object value = objectStack.get(curIndex);
							if (parentType.isArray()) {
								((ArrayList<Object>) parent).add(value);
							} else {
								accessor.setValue(parent, value,
										attributesStack.get(curIndex));
							}
						}
					} catch (IllegalAccessException ex) {
						logger.error("exception occured", ex);
						throw makeSAXException(ex);
					} catch (IllegalArgumentException ex) {
						logger.error("exception occured", ex);
						throw makeSAXException(ex);
					} catch (InvocationTargetException ex) {
						logger.error("exception occured", ex);
						throw makeSAXException(ex);
					} catch (ParseException ex) {
						logger.error("exception occured", ex);
						throw makeSAXException(ex);
					}
				}
			}
			topIndex--;
			debugTag(qName, false);
		}

		public void endDocument() {
			Object topValue = objectStack.get(topIndex);
			if ((topValue != null) && topType.isArray()) {
				ArrayList<?> list = (ArrayList<?>) topValue;
				Object[] array = (Object[]) java.lang.reflect.Array
						.newInstance(topType.getComponentType(), list.size());
				topValue = list.toArray(array);
			}
			topObject = topValue;
		}
	}

	private final static class AttributeAccessor extends XmlAttributeAccessor {
		private Method setter = null;

		public AttributeAccessor(Class<?> beanType, String name)
				throws NoSuchMethodException, SecurityException {
			super(beanType, name);
		}

		protected boolean isAlternativeSetter(Method method) {
			Class<?>[] parameters = method.getParameterTypes();
			if (parameters.length == 2) {
				if (parameters[1].isAssignableFrom(Attributes.class)) {
					setter = method;
					return (true);
				}
			}
			return (false);
		}

		public void setValue(Object obj, Object value, Attributes attributes)
				throws InvocationTargetException, IllegalArgumentException,
				IllegalAccessException {
			value = completeValue(value);
			if (setter != null) {
				setter.invoke(obj, new Object[] { value, attributes });
			} else {
				setCompleteValue(obj, value);
			}
		}
	}
}
