package com.sm.javax.xmlx;

import java.beans.Encoder;
import java.beans.Expression;
import java.beans.PersistenceDelegate;
import java.beans.Statement;
import java.io.File;
import java.net.URI;
import java.net.URL;
import java.sql.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.sm.javax.langx.reflect.Classes;
import com.sm.javax.utilx.CollectionUtils;

public class Cloner extends Encoder {

	public Cloner() {
	}

	private final static HashSet<Class<?>> immutableClasses = new HashSet<>();

	static {
		CollectionUtils.addAll(immutableClasses, String.class, Class.class, Date.class, File.class, URL.class,
				URI.class);
	}

	private HashMap<Object, Object> copiedObjects = new HashMap<>();

	public boolean isImmutable(Object obj) {
		if (obj != null) {
			Class<?> cls = obj.getClass();
			return (Classes.isPrimitive(cls) || cls.isEnum() || immutableClasses.contains(cls));
		}

		return (true);
	}

	public boolean isImmutable(Object[] args) {
		if (args != null) {
			for (Object arg : args) {
				if (!isImmutable(arg)) {
					return (false);
				}
			}
		}

		return (true);
	}

	public Object copy(Object obj) {
		return (isImmutable(obj) ? obj : getCopied(obj));
	}

	public Object[] copy(Object[] args) {
		if (isImmutable(args)) {
			return (args);
		}
		int length = args.length;
		Object[] copied = new Object[length];
		for (int i = 0; i < length; i++) {
			copied[i] = copy(args[i]);
		}
		return (copied);
	}

	private Object getCopied(Object obj) {
		Object copied = copiedObjects.get(obj);
		if (copied == null) {
			throw new IllegalArgumentException("Object " + obj + " was not copied");
		}
		return copied;
	}

	public boolean isReady(Object obj) {
		return (isImmutable(obj) || copiedObjects.containsKey(obj));
	}

	public boolean isReady(Object[] args) {
		if (args != null) {
			for (Object arg : args) {
				if (!isReady(arg)) {
					return (false);
				}
			}
		}

		return (true);
	}

	public boolean isReady(Object target, Object[] args) {
		return (isReady(target) && isReady(args));
	}

	public boolean isReady(Expression expr) {
		return (isReady(expr.getTarget(), expr.getArguments()));
	}

	public void execute(Expression expr) throws Exception {
//		System.out.println(expr);
		copiedObjects.put(expr.getValue(),
				new Expression(copy(expr.getTarget()), expr.getMethodName(), copy(expr.getArguments())).getValue());
	}

	public boolean isReady(Statement stat) {
		return (isReady(stat.getTarget(), stat.getArguments()));
	}

	public void execute(Statement stat) throws Exception {
//		System.out.println(stat);
		new Statement(copy(stat.getTarget()), stat.getMethodName(), copy(stat.getArguments())).execute();
	}

	@Override
	public PersistenceDelegate getPersistenceDelegate(Class<?> cls) {
		try {
			XMLSerializableClass serializer = XMLSerializableClass.get(cls);

			if (serializer != null) {
				return (serializer);
			}
		} catch (XmlSerializationException e) {
			e.reportException(cls);
		}
		return super.getPersistenceDelegate(cls);
	}

	private class Item {
		Item prev;
		Item next;

		Item(Item prev) {
			this.prev = prev;
			if (prev != null) {
				prev.next = this;
			}
		}

		boolean isReady() {
			return (false);
		}

		void execute() {
		}

		final void process() {
			if (isReady()) {
				execute();
				if (prev != null) {
					prev.next = next;
				}
				if (next != null) {
					next.prev = prev;
				}
			}
		}
	}

	private class ExpressionItem extends Item {
		final Expression expr;

		ExpressionItem(Item prev, Expression expr) {
			super(prev);
			this.expr = expr;
		}

		@Override
		boolean isReady() {
			return Cloner.this.isReady(expr);
		}

		@Override
		void execute() {
			try {
				Cloner.this.execute(expr);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public String toString() {
			return expr.toString();
		}
	}

	private class StatementItem extends Item {
		final Statement stat;

		StatementItem(Item prev, Statement stat) {
			super(prev);
			this.stat = stat;
		}

		@Override
		boolean isReady() {
			return Cloner.this.isReady(stat);
		}

		@Override
		void execute() {
			try {
				Cloner.this.execute(stat);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public String toString() {
			return stat.toString();
		}
	}

	Item toDoHead = new Item(null);
	Item toDoTail = toDoHead;

	@Override
	public void writeExpression(Expression expr) {
		if (isReady(expr)) {
			try {
				execute(expr);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			toDoTail = new ExpressionItem(toDoTail, expr);
		}
		super.writeExpression(expr);
	}

	@Override
	public void writeStatement(Statement stat) {
		if (isReady(stat)) {
			try {
				execute(stat);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			toDoTail = new StatementItem(toDoTail, stat);
		}
		super.writeStatement(stat);
	}

	public Object deepClone(Object obj) throws CloneNotSupportedException {
		if (XmlUtils.isSerializable(obj)) {
			if (isImmutable(obj)) {
				return (obj);
			}
			copiedObjects.clear();
			writeObject(obj);
			while (toDoHead.next != null) {
				Item cur = toDoHead;
				while ((cur = cur.next) != null) {
					cur.process();
				}
			}
			return (copiedObjects.get(obj));
		}

		throw new CloneNotSupportedException(obj.toString());
	}

	public static Object clone(Object obj) throws CloneNotSupportedException {
		return (new Cloner().deepClone(obj));
	}
}
