package com.sm.javax.xmlx;

import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;

public class XmlUtils {
	public static void printTag(PrintWriter writer, String tag, String value) throws XmlWrongDataException {
		printTag(writer, tag, value, false);
	}

	public static void printTag(PrintWriter writer, String tag, String value, boolean cdata)
			throws XmlWrongDataException {
		if (value == null)
			value = "";
		if (cdata) {
			value.replace("]]>", "]]]]><![CDATA[>");
			writer.printf("<%s><![CDATA[%s]]></%s>", tag, value, tag);
		} else {
			if ((value.indexOf("&") > -1) || (value.indexOf("<") > -1) || (value.indexOf(">") > -1)) {
				throw new XmlWrongDataException("Text cannot contain <, > or &");
			}
			writer.printf("<%s>%s</%s>", tag, value, tag);
		}
	}

	public static void saveToXML(OutputStream out, Object obj) {
		XMLEncoder enc = new XMLEncoder(new BufferedOutputStream(out));
		enc.writeObject(obj);
		enc.close();
	}

	public static Object loadFromXML(InputStream in) {
		XMLDecoder dec = new XMLDecoder(new BufferedInputStream(in));
		Object obj = dec.readObject();
		dec.close();
		return (obj);
	}

	public static boolean isSerializable(Object obj) {
		return ((obj == null) || (obj.getClass().getAnnotation(XMLSerializable.class) != null)
				|| (obj instanceof Serializable));
	}

	public static Object clone(Object obj) throws CloneNotSupportedException {
		return (Cloner.clone(obj));
	}
}
