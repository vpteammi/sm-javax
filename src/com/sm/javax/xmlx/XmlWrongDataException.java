/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   UwgmExWrongXMLDataException.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Renamed as UwgmExWrongXMLDataException.java for uwgmexplorer
 */

package com.sm.javax.xmlx;

import java.io.IOException;

@SuppressWarnings ("serial")
public class XmlWrongDataException extends IOException
{
	public XmlWrongDataException (String message)
	{
		super (message);
	}
}
