package com.sm.javax.xmlx;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks set method used by XML decoder for to complete initialization of serialized object.
 * Types and names of method parameters must match types and names of corresponding serialized fields or getters
 * @author Olga
 *
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface XMLInitializer {
}
