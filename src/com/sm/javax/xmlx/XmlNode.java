package com.sm.javax.xmlx;

import org.w3c.dom.Element;

public class XmlNode
{

	public static boolean isNodeOfTag (Element xmlNode, String type)
	{
		return (xmlNode.getNodeName ().equalsIgnoreCase (type));
	}
}
