package com.sm.javax.xmlx;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.sm.javax.langx.reflect.ReflectionException;
import com.sm.javax.langx.reflect.ValueAccessor;

@SuppressWarnings ("serial")
class XmlSerializationException extends ReflectionException
{
	protected XmlSerializationException (Exception exc)
	{
		super (exc);
	}

	protected XmlSerializationException (String messageStart, Class<?> clazz,
			String messageEnd)
	{
		super (messageStart, clazz, messageEnd);
	}

	protected XmlSerializationException (String message, Exception exc)
	{
		super (message, exc);
	}

	protected XmlSerializationException (String messageStart, Field field,
			String messageEnd)
	{
		super (messageStart, field, messageEnd);
	}

	protected XmlSerializationException(String messageStart, ValueAccessor<?> accessor,
			String messageEnd)
	{
		super (messageStart, accessor, messageEnd);
	}

	public XmlSerializationException (String messageStart,
			Constructor<?> constructor, String messageEnd)
	{
		super (messageStart, constructor, messageEnd);
		// TODO Auto-generated constructor stub
	}

	public XmlSerializationException (String messageStart, Method method,
			String messageEnd)
	{
		super (messageStart, method, messageEnd);
		// TODO Auto-generated constructor stub
	}

	protected XmlSerializationException (String message)
	{
		super (message);
	}
}
