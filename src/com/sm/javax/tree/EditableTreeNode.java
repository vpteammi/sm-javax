package com.sm.javax.tree;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.Icon;

import com.sm.javax.langx.ValueCastException;
import com.sm.javax.xmlx.XMLInstantiator;
import com.sm.javax.xmlx.XMLSerializable;
import com.sm.javax.xmlx.XmlUtils;

@XMLSerializable
public class EditableTreeNode<ParentType extends EditableTreeNode<?>> {

	private ParentType parent = null;

	@XMLInstantiator
	public EditableTreeNode() {
	}

	public ParentType getParent() {
		return parent;
	}

	public void setParent(ParentType parent) {
		this.parent = parent;
	}

	protected void setParentsDeep() {
		getChildren().stream().forEach(child -> {
			child.setParentUnsafe(this);
			child.setParentsDeep();
		});
	}

	@SuppressWarnings("unchecked") void setParentUnsafe(EditableTreeNode<?> parent) {
		this.parent = (ParentType) parent;
	}

	@SuppressWarnings("unchecked")
	public final static List<? extends EditableTreeNode<?>> EMPTY_CHILDREN = Collections.EMPTY_LIST;

	public List<? extends EditableTreeNode<?>> getChildren() {
		return (EMPTY_CHILDREN);
	}

	@SuppressWarnings({ "rawtypes" })
	protected static List<? extends EditableTreeNode> asList(EditableTreeNode<?> item) {
		return (item == null) ? EMPTY_CHILDREN : Arrays.<EditableTreeNode<?>> asList(item);
	}

	@SuppressWarnings({ "rawtypes" })
	protected static List<? extends EditableTreeNode> asList(EditableTreeNode<?>... items) {
		return (items.length == 0) ? EMPTY_CHILDREN : Arrays.<EditableTreeNode<?>> asList(items);
	}

	public Icon getIcon() {
		return (null);
	}

	public String toLabel() {
		return (toString());
	}

	/**
	 * Index in parent.getChildernItems ()
	 * 
	 * @return -1 if parent is null or child is not in the parent list anymore
	 */
	public int getParentIndex() {
		if (parent != null) {
			int index = 0;
			for (EditableTreeNode<?> item : parent.getChildren()) {
				if (item == this)
					return (index);
				index++;
			}
		}

		return (-1);
	}

	public boolean isLastInParentList() {
		int index = getParentIndex();
		return ((index > -1) && parent.getChildren().size() == (index + 1));
	}

	public boolean isFirstInParentList() {
		return (getParentIndex() == 0);
	}

	public boolean isSwapableInParent() {
		return ((parent != null) && parent.isChildrenSwapSupported());
	}

	public boolean isChildrenSwapSupported() {
		return (false);
	}

	public boolean swapChildren(int index1, int index2) {
		return (false);
	}

	public EditableTreeNode<?>[] pathToRoot() {
		int size = getPathToRootSize();
		return (size == 0 ? null : fillPathToRootSize(new EditableTreeNode[size]));
	}

	public EditableTreeNode<?>[] pathToRootForChild() {
		int size = getPathToRootSize();
		EditableTreeNode<?>[] path = new EditableTreeNode[size + 1];
		fillPathToRootSize(path, size)[size] = this;
		return (path);
	}

	private EditableTreeNode<?>[] fillPathToRootSize(EditableTreeNode<?>[] pathToRoot, int size) {
		EditableTreeNode<?> parent = this.parent;
		int index = size;
		while (parent != null) {
			pathToRoot[--index] = parent;
			parent = parent.getParent();
		}
		return (pathToRoot);
	}

	private EditableTreeNode<?>[] fillPathToRootSize(EditableTreeNode<?>[] pathToRoot) {
		return (fillPathToRootSize(pathToRoot, pathToRoot.length));
	}

	private int getPathToRootSize() {
		EditableTreeNode<?> parent = this.parent;
		int size = 0;
		while (parent != null) {
			size++;
			parent = parent.getParent();
		}
		return (size);
	}

	public void save(OutputStream out) {
		XmlUtils.saveToXML(out, this);
	}

	public void save(File file) throws FileNotFoundException {
		save(new FileOutputStream(file));
	}

	public void save(String filename) throws FileNotFoundException {
		save(new FileOutputStream(filename));
	}

	public static <DEF_TYPE extends EditableTreeNode<?>> DEF_TYPE load(InputStream in, Class<DEF_TYPE> definitionClass)
			throws ValueCastException {
		try {
			DEF_TYPE def = definitionClass.cast(XmlUtils.loadFromXML(in));
			def.setParentsDeep();
			return (def);
		} catch (ClassCastException e) {
			throw new ValueCastException(e);
		}
	}

	public static <DEF_TYPE extends EditableTreeNode<?>> DEF_TYPE load(File file, Class<DEF_TYPE> definitionClass)
			throws FileNotFoundException, ValueCastException {
		return (load(new FileInputStream(file), definitionClass));
	}

	public static <DEF_TYPE extends EditableTreeNode<?>> DEF_TYPE load(String filename, Class<DEF_TYPE> definitionClass)
			throws FileNotFoundException, ValueCastException {
		return (load(new FileInputStream(filename), definitionClass));
	}
}