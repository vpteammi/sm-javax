package com.sm.javax.iox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.attribute.FileTime;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.sm.javax.langx.Strings;

public class IOZipUtils {

	private final static int STREAM_COPY_BUF_SIZE = 16 * 1024;

	private final int bufferSize;

	public IOZipUtils(int bufferSize) {
		super();
		this.bufferSize = bufferSize;
	}

	public IOZipUtils() {
		this(STREAM_COPY_BUF_SIZE);
	}

	public void unzip(File zipFile, File directory) throws IOException {
		try (FileInputStream fileInput = new FileInputStream(zipFile)) {
			unzip(fileInput, directory);
		}
	}

	public void unzip(InputStream input, File directory) throws IOException {
		try (ZipInputStream stream = new ZipInputStream(input)) {
			unzip(stream, directory);
		}
	}

	public void unzip(ZipInputStream zip, File directory) throws IOException {
		ZipEntry entry;

		while ((entry = zip.getNextEntry()) != null) {
			File destFile = new File(directory, entry.getName());
			destFile.getParentFile().mkdirs();
			try (FileOutputStream output = new FileOutputStream(destFile)) {
				StreamUtils.copy(zip, output, bufferSize);
			}
			destFile.setLastModified(entry.getLastModifiedTime().toMillis());
		}

	}

	public void zip(File zipFile, File file) throws IOException {
		zip(zipFile, file.getName(), file);
	}

	public void zip(File zipFile, String entryName, File file)
			throws IOException {
		try (FileOutputStream stream = new FileOutputStream(zipFile)) {
			zip(stream, entryName, file);
		}
	}

	public void zip(OutputStream output, File file) throws IOException {
		zip(output, file.getName(), file);
	}

	public void zip(OutputStream output, String entryName, File file)
			throws IOException {
		try (ZipOutputStream stream = new ZipOutputStream(output)) {
			zip(stream, entryName, file);
		}
	}

	public void zip(ZipOutputStream zip, String entryName, File file)
			throws IOException {
		if (file.isDirectory()) {
			for (String name : file.list()) {
				zip(zip, Strings.concat(entryName, File.separator, name),
						new File(file, name));
			}
		} else {
			ZipEntry entry = new ZipEntry(Strings.isEmpty(entryName) ? file.getName() : entryName);
			entry.setLastModifiedTime(FileTime.fromMillis(file.lastModified()));
			zip.putNextEntry(entry);
			try (FileInputStream input = new FileInputStream(file)) {
				StreamUtils.copy(input, zip, bufferSize);
			}
			zip.closeEntry();
		}
	}
}
