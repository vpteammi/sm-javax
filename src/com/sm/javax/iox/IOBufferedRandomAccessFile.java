package com.sm.javax.iox;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class IOBufferedRandomAccessFile extends RandomAccessFile
{
	private static final int DefaultBufferSize = 32 * 1024;
	
	private byte [] mBuffer = null;
	private int mBufferSize = DefaultBufferSize;
	private int mBufferBytes;  // number of bytes actually read into the buffer
	private int mBufferPos;    // offset in the buffer
	private long mBufferOffset = 0; // offset of the buffer in the file 

	/* --------------------------------------------------------------------------------------------
	 * public constructors
	 */
	public IOBufferedRandomAccessFile (File file, String mode, int bufferSize) throws IOException
	{
		super (file, mode);
		initBuffer (bufferSize);
	}

	public IOBufferedRandomAccessFile (File file, String mode) throws IOException
	{
		this (file, mode, DefaultBufferSize);
	}
	
	public IOBufferedRandomAccessFile (String name, String mode, int bufferSize) throws IOException
	{
		this (new File (name), mode, bufferSize);
	}
	
	public IOBufferedRandomAccessFile (String name, String mode) throws IOException
	{
		this (new File (name), mode, DefaultBufferSize);
	}
	
	/* --------------------------------------------------------------------------------------------
	 * overridden methods
	 */
	@Override
	public long getFilePointer ()
	{
		return (mBufferOffset + mBufferPos);
	}
	
	@Override
	public void seek (long pos) throws IOException
	{
		if (mBufferOffset<=pos && pos<mBufferOffset+mBufferBytes) {
			mBufferPos = (int) (pos - mBufferOffset);
		}
		else {
			super.seek (pos);
			invalidate ();
		}
	}
	
	@Override
	public final int read () throws IOException
	{
		if (mBufferPos >= mBufferBytes) {
			if (fillBuffer () < 0) {
				return -1;
			}
		}
		if (mBufferBytes > 0) {
			return mBuffer [mBufferPos++];
		}
		return -1;
	}
	
	@Override
	public int read (byte b [], int off, int len) throws IOException
	{
		int bytesLeft = mBufferBytes - mBufferPos;
		if (bytesLeft >= len) {
			System.arraycopy (mBuffer, mBufferPos, b, off, len);
			mBufferPos += len;
			return len;
		}
		/*
		 * we have fewer bytes in the buffer than we need to read
		 */
		System.arraycopy (mBuffer, mBufferPos, b, off, bytesLeft);
		int bytesRead = bytesLeft;
		if (fillBuffer () > 0) {
			return (bytesRead + read (b, off+bytesRead, len-bytesRead));
		}
		return bytesRead;
	}
	
	@Override
	public int read (byte b []) throws IOException
	{
		return read (b, 0, b.length);
	}

	/*
	 * private methods
	 */
	private void initBuffer (int bufferSize) throws IOException
	{
		if (bufferSize > 0) {
			mBufferSize = bufferSize;
		}
		mBuffer = new byte [mBufferSize];
		invalidate ();
	}
	
	private void invalidate () throws IOException
	{
		mBufferBytes = 0;
		mBufferPos = 0;
		mBufferOffset = super.getFilePointer ();
	}
	
	private int fillBuffer () throws IOException
	{
		mBufferOffset = super.getFilePointer ();
		int bytesRead = super.read (mBuffer);
		if (bytesRead > 0) {
			mBufferBytes = bytesRead;
			mBufferPos = 0;
		}
		return bytesRead;
	}

}
