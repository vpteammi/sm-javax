package com.sm.javax.iox;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

import com.sm.javax.unrar.Archive;
import com.sm.javax.unrar.exception.RarException;
import com.sm.javax.unrar.rarfile.FileHeader;
import com.sm.javax.utilx.NamedIterator;

public class IORarArchiveFile extends IOArchiveFile
{
	private final Archive rarArchive;

	public IORarArchiveFile (File file) throws IOException
	{
		super (file);
		try
		{
			rarArchive = new Archive (file);
		}
		catch (RarException e)
		{
			throw new IOException (e);
		}
	}

	private static class Entry implements IOArchiveEntry
	{
		final FileHeader rarEntry;

		public Entry (FileHeader entry)
		{
			this.rarEntry = entry;
		}

		@Override
		public String getName ()
		{
			return (rarEntry.getFileNameString ().trim ());
		}

		@Override
		public long getSize ()
		{
			return (rarEntry.getFullUnpackSize ());
		}

		@Override
		public long getTime ()
		{
			Date time = rarEntry.getMTime ();
			
			if (time == null)
			{
				time = new Date ();
			}
			
			return (time.getTime ());
		}

		@Override
		public boolean isDirectory ()
		{
			return (rarEntry.isDirectory ());
		}

		@Override
		public long getCompressedSize ()
		{
			return (rarEntry.getFullPackSize ());
		}
	}

	private class EntryIterator implements NamedIterator<IOArchiveEntry>
	{
		private final Iterator<FileHeader> entries;

		public EntryIterator ()
		{
			entries = rarArchive.getFileHeaders ().iterator ();
		}

		@Override
		public String getName (IOArchiveEntry entry)
		{
			return entry.getName ();
		}

		@Override
		public boolean hasNext ()
		{
			return (entries.hasNext ());
		}

		@Override
		public IOArchiveEntry next ()
		{
			return (new Entry (entries.next ()));
		}

		@Override
		public void remove ()
		{
		}
	}

	@Override
	protected NamedIterator<IOArchiveEntry> entries ()
	{
		return (new EntryIterator ());
	}

	private interface OutputCreator
	{
		public void createOutput (RarEntryOutputStream outputStream) throws Throwable;
	}
	
	private static class RarEntryOutputStream extends PipedOutputStream
	{
		public final CountDownLatch readCompleteSignal = new CountDownLatch(1);
		public Throwable problem = null;
		
		public void waitForReadCompletion ()
		{
			try
			{
				readCompleteSignal.await ();
			}
			catch (InterruptedException e)
			{
			}
		}
		
		public void setProblem (Throwable problem)
		{
			this.problem = problem;
		}
		
		@SuppressWarnings("resource")
		public static RarEntryInputStream startInputStream (final OutputCreator creator) throws IOException
		{
			return (new RarEntryOutputStream().getInputStream (creator));
		}
		
		public RarEntryInputStream getInputStream (final OutputCreator creator) throws IOException
		{
			RarEntryInputStream inputStream = new RarEntryInputStream (this);
			
			new Thread ()
			{
				@Override
				public void run ()
				{
					try
					{
						creator.createOutput (RarEntryOutputStream.this);
					}
					catch (Throwable e)
					{
						setProblem (e);
					}
					finally
					{
						try
						{
							close ();
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
						waitForReadCompletion ();
					}
				}			
			}.start ();
			
			return (inputStream);
		}
	}

	private static class RarEntryInputStream extends PipedInputStream
	{
		private final RarEntryOutputStream outputStream;
		
		public RarEntryInputStream (RarEntryOutputStream outputStream) throws IOException
		{
			super (outputStream, 4 * 1024);
			this.outputStream = outputStream;
		}
		
		@Override
		public void close () throws IOException
		{
			super.close ();
			outputStream.readCompleteSignal.countDown ();
			if (outputStream.problem != null)
			{
				throw new IOException (outputStream.problem);
			}
		}
		
	}
	
	@Override
	protected InputStream getInputStream (IOArchiveEntry entry)
			throws IOException
	{
		if (entry instanceof Entry)
		{
			final Entry rarEntry = (Entry) entry;
/*
			File tempFile = new File ("c:\\Temp\\unrar.tmp");
			FileOutputStream os = new FileOutputStream (tempFile);
			try
			{
				rarArchive.extractFile (rarEntry.rarEntry, os);
			}
			catch (RarException e)
			{
				throw new IOException (e);
			}
			finally
			{
				os.close ();
			}

			return (new FileInputStream (tempFile));
*/
			return RarEntryOutputStream.startInputStream (new OutputCreator () {
				@Override
				public void createOutput (RarEntryOutputStream outputStream)
						throws Throwable
				{
					rarArchive.extractFile (rarEntry.rarEntry, outputStream);
				}});
		}

		throw new IllegalArgumentException ("Argument " + entry + "is not RarEntry");
	}

}
