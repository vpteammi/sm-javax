package com.sm.javax.iox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class StreamUtils
{
	public final static void copy (InputStream from, OutputStream to) throws IOException
	{
		copy (from, to, 4096);
	}
	
	public final static void copy (InputStream from, OutputStream to, int bufSize) throws IOException
	{
		byte[] buffer = new byte[bufSize];
		int nbytes;
		while ((nbytes = from.read (buffer)) > -1)
		{
			to.write (buffer, 0, nbytes);
		}
	}

	public final static char LF = '\n';

	public static String readStreamAsString (InputStream stream)
			throws IOException
	{
		BufferedReader reader = new BufferedReader (new InputStreamReader (stream));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder ();
	
		while ((line = reader.readLine ()) != null)
		{
			stringBuilder.append (line);
			stringBuilder.append (LF);
		}
	
		return stringBuilder.toString ();
	}
}
