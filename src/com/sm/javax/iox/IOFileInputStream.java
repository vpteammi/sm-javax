/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   IOFileInputStream.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Created.

 */

package com.sm.javax.iox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class IOFileInputStream extends IOInputStream
{
	private File mFile;
	public IOFileInputStream (String filename) throws FileNotFoundException
	{
		this (new File (filename));
	}

	public IOFileInputStream (File file) throws FileNotFoundException
	{
		super (new FileInputStream (file), file.length (), true);
		mFile = file;
	}
	
	public File getFile ()
	{
		return mFile;
	}
}
