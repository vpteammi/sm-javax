package com.sm.javax.iox;

import com.sm.javax.progress.ProgressControllable;

public interface ProgressControllableContent extends
		ProgressControllable, IODataSource
{

}
