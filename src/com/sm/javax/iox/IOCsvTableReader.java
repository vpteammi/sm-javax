package com.sm.javax.iox;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sm.javax.langx.Strings;
import com.sm.javax.utilx.CollectionUtils;
import com.sm.javax.utilx.Parsers;

public class IOCsvTableReader extends IOCsvReader {

	private List<String> propertyNames;

	public IOCsvTableReader(InputStream input) throws IOException {
		reader = new LineNumberReader(new InputStreamReader(input));
		propertyNames = parseCsvLine(reader.readLine());
	}

	public IOCsvTableReader(InputStream input, List<String> propertyNames)
			throws IOException {
		reader = new LineNumberReader(new InputStreamReader(input));
		this.propertyNames = propertyNames;
	}

	private void parseValue(Map<String, Object> values, String name,
			String value) {
		values.put(name, Parsers.parseBasicValue(value));
	}

	public Map<String, Object> nextTableRecord() throws IOException {
		String line = reader.readLine();

		if (!Strings.isEmpty(line)) {
			List<String> values = parseCsvLine(line);

			if (CollectionUtils.size(propertyNames) > 0) {
				HashMap<String, Object> record = new HashMap<String, Object>();
				int index = 0;
				int size = CollectionUtils.size(values);
				for (String name : propertyNames) {
					if (index < size) {
						String value = values.get(index++);
						parseValue(record, name, value);
					} else {
						break;
					}
				}
				return record;
			}
		}

		reader.close();
		reader = null;

		return (null);
	}
}