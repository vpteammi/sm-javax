package com.sm.javax.iox;

import java.io.IOException;
import java.net.URL;

public class IOHttpInputStream extends IOInputStream
{
	public IOHttpInputStream (URL url) throws IOException
	{
		super (url.openStream (), true);
	}
}
