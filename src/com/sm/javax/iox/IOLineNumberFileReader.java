package com.sm.javax.iox;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * Please note that for this class to work properly the size of the buffer should be
 * larger than any string length.
 */
public class IOLineNumberFileReader
{
	public static int DEFAULT_BUFFER_SIZE = 32 * 1024;
	public static String DEFAULT_CHAR_SET = "UTF-8";
	
	/* --------------------------------------------------------------------------------------------
	 * private data members
	 */
	private byte [] mBuffer = null;
	private int mBufferOffset = 0;
	private int mBytesInBuffer = 0;
	private int mBufferSize = DEFAULT_BUFFER_SIZE;
	private InputStream mInput = null;
	private long mLastLineOffset = 0;
	private int mLastLineLength = 0;
	private Long mEndOffset = null; // if this variable is set, the reader won't read after this offset
	private String mCharSet = DEFAULT_CHAR_SET;
	private ArrayList<Integer> mByteOffsets = null;
	private ArrayList<Integer> mCharOffsets = null;
	private int mCurCharOffset = 0;
	private boolean mSaveOffsets = false;
	
	/* --------------------------------------------------------------------------------------------
	 * constructors 
	 */
	public IOLineNumberFileReader (InputStream input, int bufferSize, String charSet, long startOffset) throws IOException
	{
		mInput = input;
		if (bufferSize > 0) {
			mBufferSize = bufferSize;
		}
		mCharSet = charSet;
		if (startOffset > 0) {
			mLastLineOffset = startOffset;
			mInput.skip (startOffset);
		}
		initBuffer (mBufferSize);
	}

	public IOLineNumberFileReader (InputStream input, int bufferSize, String charSet, long startOffset, long endOffset) throws IOException
	{
		this (input, bufferSize, charSet, startOffset);
		mEndOffset = (endOffset >=0 ? endOffset : null);
	}

	public IOLineNumberFileReader (File file, int bufferSize, String charSet, long startOffset) throws IOException
	{
		this (new FileInputStream (file), bufferSize, charSet, startOffset);
	}

	public IOLineNumberFileReader (File file, int bufferSize, String charSet, long startOffset, long endOffset) throws IOException
	{
		this (file, bufferSize, charSet, startOffset);
		mEndOffset = (endOffset >=0 ? endOffset : null);
	}

	public IOLineNumberFileReader (File file) throws IOException
	{
		this (file, DEFAULT_BUFFER_SIZE, DEFAULT_CHAR_SET, 0L);
	}
	
	public IOLineNumberFileReader (File file, long startOffset) throws IOException
	{
		this (file, DEFAULT_BUFFER_SIZE, DEFAULT_CHAR_SET, startOffset);
	}
	
	public IOLineNumberFileReader (File file, int bufferSize) throws IOException
	{
		this (file, bufferSize, DEFAULT_CHAR_SET, 0L);
	}
	
	public IOLineNumberFileReader (File file, int bufferSize, long startOffset) throws IOException
	{
		this (file, bufferSize, DEFAULT_CHAR_SET, startOffset);
	}
	
	public IOLineNumberFileReader (File file, int bufferSize, long startOffset, long endOffset) throws IOException
	{
		this (file, bufferSize, DEFAULT_CHAR_SET, startOffset, endOffset);
	}
	
	public IOLineNumberFileReader (File file, int bufferSize, String charSet) throws IOException
	{
		this (file, bufferSize, charSet, 0L);
	}
	
	public IOLineNumberFileReader (File file, String charSet, long startOffset) throws IOException
	{
		this (file, DEFAULT_BUFFER_SIZE, charSet, startOffset);
	}
	public IOLineNumberFileReader (File file, String charSet, long startOffset, long endOffset) throws IOException
	{
		this (file, DEFAULT_BUFFER_SIZE, charSet, startOffset, endOffset);
	}
	
	public IOLineNumberFileReader (String fileName) throws IOException
	{
		this (new File (fileName), DEFAULT_BUFFER_SIZE, DEFAULT_CHAR_SET, 0L);
	}
	
	public IOLineNumberFileReader (String fileName, long startOffset) throws IOException
	{
		this (new File (fileName), DEFAULT_BUFFER_SIZE, DEFAULT_CHAR_SET, startOffset);
	}
	
	public IOLineNumberFileReader (String fileName, long startOffset, long endOffset) throws IOException
	{
		this (new File (fileName), DEFAULT_BUFFER_SIZE, DEFAULT_CHAR_SET, startOffset, endOffset);
	}
	
	public IOLineNumberFileReader (String fileName, int bufferSize) throws IOException
	{
		this (new File (fileName), bufferSize, DEFAULT_CHAR_SET, 0L);
	}
	
	public IOLineNumberFileReader (String fileName, int bufferSize, long startOffset) throws IOException
	{
		this (new File (fileName), bufferSize, DEFAULT_CHAR_SET, startOffset);
	}
	
	public IOLineNumberFileReader (String fileName, int bufferSize, long startOffset, long endOffset) throws IOException
	{
		this (new File (fileName), bufferSize, DEFAULT_CHAR_SET, startOffset, endOffset);
	}
	
	public IOLineNumberFileReader (String fileName, int bufferSize, String charSet) throws IOException
	{
		this (new File (fileName), bufferSize, charSet, 0L);
	}
	
	public IOLineNumberFileReader (String fileName, int bufferSize, String charSet, long startOffset) throws IOException
	{
		this (new File (fileName), bufferSize, charSet, startOffset);
	}
	
	public IOLineNumberFileReader (String fileName, int bufferSize, String charSet, long startOffset, long endOffset) throws IOException
	{
		this (new File (fileName), bufferSize, charSet, startOffset, endOffset);
	}
	
	/* --------------------------------------------------------------------------------------------
	 * public methods
	 */
	public String readLine () throws IOException
	{
		mLastLineOffset += mLastLineLength;
		if (mEndOffset != null && mLastLineOffset >= mEndOffset) {
			return null;
		}
		if (mSaveOffsets) {
			mByteOffsets.add ((int) mLastLineOffset);
			mCharOffsets.add (mCurCharOffset);
		}
		int start = mBufferOffset;
		boolean endOfLineFound = false;
		for (mLastLineLength=0; mBufferOffset<mBytesInBuffer; mBufferOffset++) {
			mLastLineLength ++;
			if (mBuffer [mBufferOffset] == 0x0a) {
				endOfLineFound = true;
				mBufferOffset ++;
				break;
			}
		}
		if (endOfLineFound) {
			String line = new String (mBuffer, start, mLastLineLength, mCharSet);
			mCurCharOffset += line.length ();
			return line;
		}
		
		if (readBuffer (start)) {
			while (! endOfLineFound) {
				for (; mBufferOffset<mBytesInBuffer; mBufferOffset++) {
					mLastLineLength ++;
					if (mBuffer [mBufferOffset] == 0x0a) {
						endOfLineFound = true;
						mBufferOffset ++;
						break;
					}
				}
				if (endOfLineFound) {
					break;
				}
				storeBuffer (mBytesInBuffer);
				if (! readBuffer (mBufferOffset)) {
					break;
				}
			}
		}
		start = 0;
		
		if (mLastLineLength == 0) {
			return null;
		}
		byte[] buffer = combineStoredBuffers ();
		if (buffer == null) {
			buffer = mBuffer;
		}
		String line = new String (buffer, start, mLastLineLength, mCharSet);
		mCurCharOffset += line.length ();
		return line;
	}
	
	private ArrayList<byte[]> mStoredBuffers;
	private void storeBuffer (int bytesInBuffer)
	{
		if (mStoredBuffers == null) {
			mStoredBuffers = new ArrayList<byte[]> ();
		} 
		byte[] buffer = Arrays.copyOf (mBuffer, bytesInBuffer);
		mStoredBuffers.add (buffer);
	}
	
	private byte[] combineStoredBuffers ()
	{
		if (mStoredBuffers == null) {
			return null;
		}
		int size = mStoredBuffers.size ();
		int bufSize = mBufferOffset;
		for (int i=0; i<size; i++) {
			bufSize += mStoredBuffers.get (i).length;
		}
		byte [] buffer = null;
		buffer = new byte [bufSize];
		int offset = 0;
		for (int i=0; i<size; i++) {
			byte [] bufferToCopy = mStoredBuffers.get (i); 
			int len = bufferToCopy.length;
			System.arraycopy (bufferToCopy, 0, buffer, offset, len);
			offset += len;
		}
		System.arraycopy (mBuffer, 0, buffer, offset, mBufferOffset);
		
		mStoredBuffers = null;
		return buffer;
	}
	
	public String readChoppedLine () throws IOException
	{
		String line = readLine ();
		if (line != null) {
			line = line.replace("\n", "");
			line = line.replace("\r", "");
		}
		return line;
	}
	
	public long getLastLineOffset ()
	{
		return mLastLineOffset;
	}

	public long getNextCharOffset ()
    {
          return mCurCharOffset;
    }
	
	public void close () throws IOException
	{
		mInput.close ();
		mBufferOffset = 0;
		mBytesInBuffer = 0;
	}
	
	public void setCharSet (String charSet)
	{
		mCharSet = charSet;
	}
	
	public String getCharSet ()
	{
		return mCharSet;
	}
	
	public void setSaveOffsets (boolean val)
	{
		mSaveOffsets = val;
		if (mByteOffsets == null) {
			mByteOffsets = new ArrayList<Integer> ();
			mCharOffsets = new ArrayList<Integer> ();
		}
	}
	
	public Integer [] getByteOffsets ()
	{
		if (mByteOffsets != null) {
			return mByteOffsets.toArray (new Integer [mByteOffsets.size ()]);
		}
		return null;
	}
	
	public Integer [] getByteOffsets (int startInd, int endInd)
	{
		if (mByteOffsets == null) {
			return null;
		}
		Integer [] res = new Integer [endInd-startInd+1];
		for (int i=startInd; i<=endInd; i++) {
			res [i-startInd] = mByteOffsets.get (i);
		}
		return res;
	}

	public Integer [] getByteOffsetsRange (int startOffset, int endOffset)
	{
		if (mByteOffsets == null) {
			return null;
		}
		int startInd = mByteOffsets.indexOf (startOffset);
		int endInd = mByteOffsets.indexOf (endOffset);
		return getByteOffsets (startInd, endInd);
	}

	public Integer [] getCharOffsets ()
	{
		if (mCharOffsets == null) {
			return null;
		}
		return mCharOffsets.toArray (new Integer [mCharOffsets.size ()]);
	}
	
	public Integer [] getCharOffsets (int startInd, int endInd)
	{
		if (mCharOffsets == null) {
			return null;
		}
		int startCharOffset = mCharOffsets.get (startInd);
		Integer [] res = new Integer [endInd-startInd+1];
		for (int i=startInd; i<=endInd; i++) {
			res [i-startInd] = mCharOffsets.get (i) - startCharOffset;
		}
		return res;
	}
	
	public Integer [] getCharOffsetsRange (int startOffset, int endOffset)
	{
		if (mCharOffsets == null) {
			return null;
		}
		int startInd = mByteOffsets.indexOf (startOffset);
		int endInd = mByteOffsets.indexOf (endOffset);
		return getCharOffsets (startInd, endInd);
	}
	
	/* --------------------------------------------------------------------------------------------
	 * private methods
	 */
	private void initBuffer (int size) throws IOException
	{
		mBuffer = new byte [size];
		mBytesInBuffer = mInput.read (mBuffer);
		mBufferOffset = 0;
	}

	private boolean readBuffer (int offset) throws IOException
	{
		int len = mBytesInBuffer - offset;
		for (int i=0; i<len; i++) {
			mBuffer [i] = mBuffer [offset+i];
		}
		int bytesRead = mInput.read (mBuffer, len, mBufferSize-len);
		if (bytesRead == -1) {
			return false;
		}
		mBytesInBuffer = bytesRead + len;
		mBufferOffset = len;
		return true;
	}

	public long getOffest ()
	{
		return (mLastLineOffset + mLastLineLength);
	}
	
}
