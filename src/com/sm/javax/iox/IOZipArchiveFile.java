package com.sm.javax.iox;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import com.sm.javax.utilx.NamedIterator;

public class IOZipArchiveFile extends IOArchiveFile {
	private final ZipFile zipFile;

	private static class Entry implements IOArchiveEntry {
		final ZipEntry zipEntry;

		public Entry(ZipEntry entry) {
			this.zipEntry = entry;
		}

		@Override
		public String getName() {
			return (zipEntry.getName());
		}

		@Override
		public long getSize() {
			return (zipEntry.getSize());
		}

		@Override
		public long getTime() {
			return (zipEntry.getTime());
		}

		@Override
		public boolean isDirectory() {
			return (zipEntry.isDirectory());
		}

		@Override
		public long getCompressedSize() {
			return (zipEntry.getCompressedSize());
		}
	}

	private class EntryIterator implements NamedIterator<IOArchiveEntry> {
		private final Enumeration<? extends ZipEntry> entries;

		public EntryIterator() {
			this.entries = zipFile.entries();
		}

		@Override
		public String getName(IOArchiveEntry entry) {
			return entry.getName();
		}

		@Override
		public boolean hasNext() {
			return (entries.hasMoreElements());
		}

		@Override
		public IOArchiveEntry next() {
			return (new Entry(entries.nextElement()));
		}

		@Override
		public void remove() {
		}
	}

	public IOZipArchiveFile(File file) throws ZipException, IOException {
		super(file);
		zipFile = new ZipFile(file);
	}

	@Override
	protected NamedIterator<IOArchiveEntry> entries() {
		return (new EntryIterator());
	}

	@Override
	protected InputStream getInputStream(IOArchiveEntry entry)
			throws IOException {
		if (entry instanceof Entry) {
			return (zipFile.getInputStream(((Entry) entry).zipEntry));
		}

		throw new IllegalArgumentException("Argument " + entry
				+ "is not ZipEntry");
	}
}
