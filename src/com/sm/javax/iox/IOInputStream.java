package com.sm.javax.iox;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import com.sm.javax.progress.ProgressIndicator;

public class IOInputStream extends InputStream implements ProgressControllableContent
{
	private final InputStream inputStream;
	
	private final boolean closeSourceStream;

	private final long inputSize;

	final private long onePercentIncrement;

	private boolean cancelled = false;

	private boolean closed = false;

	private long currentPosition = 0;

	private long nextReportPosition = 0;

	private ProgressIndicator progressBar = null;

//	private static final Log logger = LogFactory.getLog (IOInputStream.class
//			.getName ());

	public IOInputStream (InputStream inputStream, boolean closeSourceStream)
	{
		this (inputStream, Long.MAX_VALUE, closeSourceStream);
	}

	public IOInputStream (InputStream inputStream, long inputSize, boolean closeSourceStream)
	{
		this.inputStream = inputStream;
		this.inputSize = inputSize;
		this.closeSourceStream = closeSourceStream;
		onePercentIncrement = (inputSize / 100) + 1;
		nextReportPosition = onePercentIncrement;
	}

	@Override
	public boolean stop ()
	{
		cancelled = true;
		return (true);
	}

	@Override
	public void addProgress (ProgressIndicator progressBar)
	{
		this.progressBar = progressBar;
		if (progressBar != null)
		{
			progressBar.setListener (this);
			progressBar.setBounds (0, 100);
		}
	}

	private void reportCancelled () throws IOCancelException, IOException
	{
		closed = true;
		super.close ();
		throw new IOCancelException ("File access was cancelled");
	}

	private void reportProgress ()
	{
		if (progressBar != null)
		{
			if (currentPosition < (inputSize - 1))
			{
				progressBar
						.setValue ((int) (((double) currentPosition / (double) inputSize) * 100));
			}
			else
			{
				progressBar.setValue (100);
			}
		}
		if (currentPosition >= nextReportPosition)
		{
			nextReportPosition += onePercentIncrement;
			if (nextReportPosition >= inputSize)
			{
				nextReportPosition = inputSize - 1;
			}
		}
	}

	private void stopProgressBar ()
	{
		if (!cancelled && (progressBar != null))
		{
			progressBar.done ();
		}
	}

	@Override
	public void close () throws IOException
	{
		try
		{
			stopProgressBar ();
		}
		finally
		{
			if (!closed)
			{
				onClose ();
			}
		}
	}

	protected void onClose () throws IOException
	{
		super.close ();
		if (closeSourceStream)
		{
			inputStream.close ();
		}
	}

	@Override
	public int available () throws IOException
	{
		int avail = inputStream.available ();
		if (readFromBuf)
			avail += (bufElemCount - buffer.position ());
		return (avail);
	}

	@Override
	public long skip (long n) throws IOException
	{
		if (cancelled)
		{
			reportCancelled ();
		}

		long skipped = 0;

		if (n <= 0) return (0);
		
		if (readFromBuf)
		{
			int skipInBuf = (int) Math.min (n, bufElemCount - buffer.position ());
			buffer.position (buffer.position () + skipInBuf);
			skipped += skipInBuf;
			n -= skipInBuf;
		}
		else if (writeToBuf)
		{
			int skipInBuf = (int) Math.min (n, buffer.capacity () - buffer.position ());
			if (skipInBuf > 0)
			{
				byte[] tmpBuf = new byte [skipInBuf];
				while (skipInBuf > 0)
				{
					int nbytes = inputStream.read (tmpBuf, (int)skipped, skipInBuf);
					if (nbytes > 0)
					{
						skipped += nbytes;
						skipInBuf -= nbytes;
						n -= nbytes;
					}
					else
					{
						skipInBuf = 0;
					}
				}
				if (skipped > 0)
				{
					buffer.put (tmpBuf, 0, (int)skipped);
				}
			}
		}

		checkBufferPosition ();

		if (n > 0)
		{
			skipped += inputStream.skip (n);
		}

		if ((currentPosition += skipped) >= nextReportPosition)
		{
			reportProgress ();
		}

		return (skipped);
	}

	@Override
	public int read () throws IOException
	{
		if (cancelled)
		{
			reportCancelled ();
		}

		int nbytes;
		if (readFromBuf)
		{
			nbytes = buffer.get ();
			checkBufferPosition ();
		}
		else
		{
			nbytes = inputStream.read ();
			if (writeToBuf)
			{
				buffer.put ((byte) nbytes);
				checkBufferPosition ();
			}
		}

		if ((currentPosition += 1) >= nextReportPosition)
		{
			reportProgress ();
		}

		return (nbytes);
	}

	@Override
	public int read (byte b[]) throws IOException
	{
		if (cancelled)
		{
			reportCancelled ();
		}

		int nbytes;
		if (readFromBuf || writeToBuf)
		{
			nbytes = read (b, 0, b.length);
		}
		else
		{
			nbytes = inputStream.read (b);
		}

		if ((currentPosition += nbytes) >= nextReportPosition)
		{
			reportProgress ();
		}

		return (nbytes);
	}

	@Override
	public int read (byte b[], int off, int len) throws IOException
	{
		if (cancelled)
		{
			reportCancelled ();
		}

		int nbytes;
		if (readFromBuf)
		{
			int pos = buffer.position ();
			if (bufElemCount - pos >= len)
			{
				nbytes = len;
			}
			else
			{
				nbytes = bufElemCount - pos;
			}
			buffer.get (b, off, nbytes);
			if (nbytes < len)
			{
				int readBytes = inputStream.read (b, off + nbytes, len - nbytes);
				nbytes += readBytes;
			}
		}
		else
		{
			nbytes = inputStream.read (b, off, len);
			if (writeToBuf)
			{
				int pos = buffer.position ();
				buffer.put (b, off, Math.min (nbytes, buffer.capacity () - pos));
				checkBufferPosition ();
			}
		}

		if ((currentPosition += nbytes) >= nextReportPosition)
		{
			reportProgress ();
		}

		return (nbytes);
	}

	/**
	 * ======================================================================= The
	 * following methods implements reseting the stream
	 */
	private boolean readFromBuf = false;
	private boolean writeToBuf = false;
	private int bufElemCount = 0;
	private int bufSize = 0;
	private ByteBuffer buffer = null;

	@Override
	public boolean markSupported ()
	{
		return true;
	}

	@Override
	public void mark (int readLimit)
	{
		if (readFromBuf)
		{
			if (buffer == null || bufSize < readLimit)
			{
				ByteBuffer tmpBuf = ByteBuffer.allocate (readLimit);
				int pos = 0;
				if (buffer != null)
				{
					pos = buffer.position ();
					tmpBuf.put (buffer.array ());
				}
				buffer = tmpBuf;
				buffer.position (pos);
			}
		}
		else
		{
			buffer = ByteBuffer.allocate (readLimit);
			bufElemCount = 0;
		}
		buffer.mark ();
		writeToBuf = true;
		bufSize = readLimit;
	}

	@Override
	public void reset () throws IOException
	{
		if (!writeToBuf)
			throw new IOException ("mark position is invalid");
		readFromBuf = true;
		writeToBuf = false;
		bufElemCount = buffer.position ();
		buffer.reset ();
		currentPosition -= bufElemCount;
		reportProgress ();
	}

	private void checkBufferPosition ()
	{
		if (buffer.position () >= bufElemCount)
		{
			readFromBuf = false;
		}
		if (buffer.position () >= buffer.capacity ())
		{
			writeToBuf = false;
		}
	}
	
	public long getCurrentPosition ()
	{
	    return currentPosition;
	}
}
