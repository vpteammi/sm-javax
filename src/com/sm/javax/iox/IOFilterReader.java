package com.sm.javax.iox;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Writer;

public abstract class IOFilterReader extends PipedReader
{
	private IOException exception = null;
	
	public IOFilterReader ()
	{
		super ();
	}

	public void startContentWrite ()
	{
		exception = null;
		
		final PipedWriter writer = new PipedWriter ();
		
		try
		{
			this.connect (writer);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		Thread backgroundReader = new Thread ()
		{
			@Override
			public void run ()
			{
				try
				{
					doWrite (writer);
				}
				catch (IOException e)
				{
					exception = e;
				}
				finally
				{
					try
					{
						writer.close ();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		};
		backgroundReader.start ();
	}
	
	public void close ()  throws IOException
	{
		super.close ();
		if (exception != null)
		{
			throw exception;
		}
	}
	
	protected abstract void doWrite (Writer writer) throws IOException;
}
