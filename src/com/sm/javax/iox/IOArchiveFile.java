package com.sm.javax.iox;

import java.io.File;
import java.io.IOException;

import com.sm.javax.utilx.FileUtils;

public abstract class IOArchiveFile extends IOArchive
{
	private final File file;
	
	protected IOArchiveFile (File file)
	{
		this.file = file;
	}

	public File getFile ()
	{
		return (file);
	}
	
	@Override
	public String getName ()
	{
		return file.getAbsolutePath ();
	}
	
	public static IOArchive getArchive (String path) throws IOException
	{
		return getArchive (new File (path));
	}

	public static IOArchive getArchive (File file) throws IOException
	{
		if (FileUtils.isZipArchive (file))
		{
			return (new IOZipArchiveFile (file));
		}
		else if (FileUtils.isRarArchive (file))
		{
			return (new IORarArchiveFile (file));
		}
		
		throw new IOException ("Unknown archive file extension " + file);
	}
}
