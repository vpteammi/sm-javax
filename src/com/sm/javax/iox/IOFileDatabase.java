package com.sm.javax.iox;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import com.sm.javax.langx.Objects;
import com.sm.javax.langx.Strings;
import com.sm.javax.utilx.TimeUtils;
import com.sm.javax.xmlx.XmlUtils;

public abstract class IOFileDatabase<DbType, DbListener> implements Closeable
{

	private File dbFile = null;
	private boolean saved = true;
	private final ArrayList<DbListener> listeners = new ArrayList<DbListener> ();

	@SuppressWarnings ("unchecked")
	public synchronized boolean open (File filename) throws IOException
	{
		if (filename != null)
		{
			if (!Objects.equals (filename, this.dbFile))
			{
				if (this.dbFile != null)
				{
					close ();
				}
	
				if (filename.exists ())
				{
					DbType db = (DbType) XmlUtils
							.loadFromXML (new FileInputStream (filename));
					if (db == null)
					{
						return (false);
					}
					onDatabaseOpen (db);
				}
	
				dbFile = filename;
				saved = true;
	
				return (true);
			}
		}
	
		return (false);
	}

	public File getDbFile ()
	{
		return dbFile;
	}

	protected abstract void onDatabaseOpen (DbType db);
	
	protected void notifyListeners (Consumer<DbListener> notifier)
	{
		synchronized (listeners)
		{
			Date start = TimeUtils.getNow ();
	
			int count = 0;
	
			for (DbListener listener : listeners)
			{
				notifier.accept (listener);
				count++;
			}
	
			System.out.println ("" + count + " notifications sent in "
					+ (TimeUtils.getNow ().getTime () - start.getTime ()) + " msecs");
		}
	}

	protected void startSave ()
	{
		Executors.defaultThreadFactory ().newThread (new Runnable ()
		{
			@Override
			public void run ()
			{
				saveOnBackground ();
			}
		}).start ();
	}

	private synchronized void saveOnBackground ()
	{
		try
		{
			save ();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace ();
		}
	}

	private void save () throws FileNotFoundException
	{
		if ((dbFile != null) && !saved)
		{
			File tmpFile = new File (dbFile.getAbsolutePath () + ".tmp");
			Date start = TimeUtils.getNow ();
			XmlUtils.saveToXML (new FileOutputStream (tmpFile), this);
			Date end = TimeUtils.getNow ();
			System.out.println ("Saved to "
					+ Strings.quote (tmpFile.getAbsolutePath ()) + ": "
					+ (end.getTime () - start.getTime ()) + " msecs");
			if (dbFile.exists ())
			{
				File bakFile = new File (dbFile.getAbsolutePath () + ".bak");
				if (bakFile.exists ())
				{
					bakFile.delete ();
				}
				if (dbFile.renameTo (bakFile))
					;
			}
			tmpFile.renameTo (dbFile);
			saved = true;
		}
	}

	@Override
	public synchronized void close () throws IOException
	{
		save ();
		onDatabaseClosed ();
	}

	protected abstract void onDatabaseClosed ();
	
	public IOFileDatabase ()
	{
		super ();
	}

	public void addListener (DbListener l)
	{
		synchronized (listeners)
		{
			listeners.add (l);
		}
	}

	public void removeListener (DbListener l)
	{
		synchronized (listeners)
		{
			listeners.remove (l);
		}
	}

	public void markChanged ()
	{
		markChanged (true);
	}

	public synchronized void markChanged (boolean changed)
	{
		saved = !changed;
	}
}