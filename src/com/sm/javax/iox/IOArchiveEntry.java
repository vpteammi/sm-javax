package com.sm.javax.iox;

public interface IOArchiveEntry
{
	String getName ();
	long getSize ();
	long getTime ();
	boolean isDirectory ();
	long getCompressedSize ();
}
