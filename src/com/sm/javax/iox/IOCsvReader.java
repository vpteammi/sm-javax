package com.sm.javax.iox;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.sm.javax.langx.Strings;
import com.sm.javax.utilx.Parsers;

public class IOCsvReader {

	protected LineNumberReader reader = null;

	public IOCsvReader(InputStream input) throws IOException {
		reader = new LineNumberReader(new InputStreamReader(input));
	}

	public List<Object> nextRecord() throws IOException {
		String line = reader.readLine();

		if (!Strings.isEmpty(line)) {
			return (parseCsvLine(line).stream().map(
					s -> Parsers.parseBasicValue(s))
					.collect(Collectors.toList()));
		}

		reader.close();
		reader = null;

		return (null);
	}

	protected enum ParserState {
		LOOKING_FOR_COMMA, LOOKING_FOR_CLOSING_COMMA
	}

	public static List<String> parseCsvLine(String line) {
		if (Strings.isEmpty(line)) {
			return null;
		}
		ArrayList<String> fields = new ArrayList<String>();
		int size = line.length();
		ParserState state = ParserState.LOOKING_FOR_COMMA;
		String field = "";
		int i = 0;
		while (i < size) {
			char ch = line.charAt(i++);
			if (isSpecialSymbol(ch)) {
				switch (state) {
				case LOOKING_FOR_COMMA:
					if (ch == ',') {
						fields.add(field);
						field = "";
					} else if (ch == '"') {
						state = ParserState.LOOKING_FOR_CLOSING_COMMA;
					}
					continue;
				case LOOKING_FOR_CLOSING_COMMA:
					if (ch == '"') {
						if (i >= size)
							continue;
						ch = line.charAt(i++);
						if (ch != '"') {
							if (ch == ',') {
								fields.add(field);
								field = "";
							}
							state = ParserState.LOOKING_FOR_COMMA;
							continue;
						}
					}
					break;
				}
			}
			field += ch;
		}
		fields.add(field);
		return (fields);
	}

	private static boolean isSpecialSymbol(char ch) {
		return (ch == ',' || ch == '"');
	}

	public IOCsvReader() {
		super();
	}

}