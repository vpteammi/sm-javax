/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   IOCancelException.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Created.

 */

package com.sm.javax.iox;

import java.io.IOException;

@SuppressWarnings("serial")
public class IOCancelException extends IOException {
	public IOCancelException(String message) {
		super(message);
	}
}
