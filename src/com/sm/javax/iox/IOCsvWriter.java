package com.sm.javax.iox;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

public class IOCsvWriter
{
	private final PrintWriter writer;

	public IOCsvWriter (PrintStream stream)
	{
		this (new PrintWriter (stream));
	}

	public IOCsvWriter (PrintWriter writer)
	{
		this.writer = writer;
	}

	private void printValue (Object value, boolean last)
	{
		writer.print ('"');
		writer.print (Objects.toString (value, "").replace ("\"", "\"\""));
		writer.print ('"');
		if (!last)
		{
			writer.print (",");
		}
	}

	public void printLine (Iterator<?> cells)
	{
		if (cells != null)
		{
			while (cells.hasNext ())
			{
				Object next = cells.next ();
				printValue (next, !cells.hasNext ());
			}
		}
	}

	public void printLine (Stream<?> cells)
	{
		if (cells != null)
		{
			printLine (cells.iterator ());
		}
	}

	public void printLine (Iterable<?> cells)
	{
		if (cells != null)
		{
			printLine (cells.iterator ());
		}
	}

	public void printLine (Object[] cells)
	{
		int ncols = cells.length;
		for (int col = 0; col < ncols; col++)
		{
			printValue (cells[col], col == (ncols - 1));
		}
		writer.println ();
	}

	public void printLine (Object firstCell, Object... moreCells)
	{
		printValue (firstCell, moreCells.length == 0);
		printLine (moreCells);
	}
}
