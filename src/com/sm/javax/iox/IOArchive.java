package com.sm.javax.iox;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;

import com.sm.javax.progress.ProgressIndicator;
import com.sm.javax.utilx.DirEntry;
import com.sm.javax.utilx.NamedIterator;

public abstract class IOArchive
{
	private DirEntry<IOArchiveEntry> topEntries = null;
	
	public String getName () {return (null);}

	public Iterator<DirEntry<IOArchiveEntry>> getTopEntries ()
	{
		if (topEntries == null)
		{
			topEntries = new DirEntry<IOArchiveEntry> ("");
			topEntries.addSubentries (entries ());
		}
		return (topEntries.getSubentries ());
	}

	protected abstract NamedIterator<IOArchiveEntry> entries ();

	public int getEntriesCount ()
	{
		return (getEntriesCount (getTopEntries ()));
	}

	private int getEntriesCount (Iterator<DirEntry<IOArchiveEntry>> entries)
	{
		int count = 0;
		while (entries.hasNext ())
		{
			count += (1 + getEntriesCount (entries.next ().getSubentries ()));
		}
		return count;
	}

	public void unpack (File directory) throws IOException
	{
		unpack (directory, getTopEntries ());
	}

	public void unpack (File targetDirectory,
			Iterator<DirEntry<IOArchiveEntry>> entries) throws IOException
	{
		while (entries.hasNext ())
		{
			unpack (targetDirectory, entries.next ());
		}
	}

	public void unpack (File targetDirectory, DirEntry<IOArchiveEntry> entry)
			throws IOException
	{
		IOArchiveEntry edata = entry.getEntryData ();
		boolean isdir = true;
		long time = 0;
		File entryFile = new File (targetDirectory, entry.getName ());
		if (edata == null)
		{
			time = new Date ().getTime ();
		}
		else
		{
			time = edata.getTime ();
			isdir = edata.isDirectory ();
		}
		if (isdir)
		{
			entryFile.mkdir ();
			unpack (entryFile, entry.getSubentries ());
		}
		else
		{
			unpack (new FileOutputStream (entryFile), entry.getEntryData ());
		}
		entryFile.setLastModified (time);
	}

	public void unpack (OutputStream os, IOArchiveEntry entry)
			throws IOException
	{
		unpack (os, entry, null);
	}

	public void unpack (OutputStream os, IOArchiveEntry entry, ProgressIndicator progressBar)
			throws IOException
	{
		InputStream is = getInputStream (entry);
		if (progressBar != null)
		{
			@SuppressWarnings("resource")
			IOInputStream stream = new IOInputStream (is, entry.getSize (), true);
			progressBar.setValue (0);
			stream.addProgress (progressBar);
			is = stream;
		}
		try
		{
			StreamUtils.copy (is, os, 16 * 1024);
		}
		finally
		{
			is.close ();
		}
	}

	protected abstract InputStream getInputStream (IOArchiveEntry entry) throws IOException;

	public void printEntry (IOArchive zip, DirEntry<IOArchiveEntry> entry,
			int level)
	{
		for (int i = 0; i < level; i++)
			System.out.print (" ");
		System.out.println (entry.getName () + "; entry=" + entry.getEntryData ());
		IOArchiveEntry edata = entry.getEntryData ();
		if (edata != null)
		{
			System.out.println ("time=" + new Date (edata.getTime ()) + "; size="
					+ edata.getSize () + "; compressed=" + edata.getCompressedSize ());

			if (!edata.isDirectory ())
			{
				byte[] b = new byte[16 * 1024];
				try
				{
					InputStream is = zip.getInputStream (edata);
					long size = 0;
					while (is.available () > 0)
					{
						size += is.read (b);
					}
					is.close ();
					System.out.println (entry.getName () + ": read=" + size);
				}
				catch (IOException e)
				{
					e.printStackTrace ();
				}
			}
		}
		for (Iterator<DirEntry<IOArchiveEntry>> e = entry.getSubentries (); e.hasNext ();)
		{
			printEntry (zip, e.next (), level + 1);
		}
	}
}
