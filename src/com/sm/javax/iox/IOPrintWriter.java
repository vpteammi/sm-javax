package com.sm.javax.iox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Arrays;

public class IOPrintWriter extends PrintWriter {

	private final static int MAX_SHIFT = 50;
	private final static char[] SHIFTS = new char[MAX_SHIFT];

	static {
		Arrays.fill(SHIFTS, ' ');
	}

	private int currentIndent = 0;
	private boolean newline = true;
	private int step = 2;

	public IOPrintWriter(Writer out) {
		super(out);
		// TODO Auto-generated constructor stub
	}

	public IOPrintWriter(OutputStream out) {
		super(out);
		// TODO Auto-generated constructor stub
	}

	public IOPrintWriter(String fileName) throws FileNotFoundException {
		super(fileName);
		// TODO Auto-generated constructor stub
	}

	public IOPrintWriter(File file) throws FileNotFoundException {
		super(file);
		// TODO Auto-generated constructor stub
	}

	public IOPrintWriter(Writer out, boolean autoFlush) {
		super(out, autoFlush);
		// TODO Auto-generated constructor stub
	}

	public IOPrintWriter(OutputStream out, boolean autoFlush) {
		super(out, autoFlush);
		// TODO Auto-generated constructor stub
	}

	public IOPrintWriter(String fileName, String csn)
			throws FileNotFoundException, UnsupportedEncodingException {
		super(fileName, csn);
		// TODO Auto-generated constructor stub
	}

	public IOPrintWriter(File file, String csn) throws FileNotFoundException,
			UnsupportedEncodingException {
		super(file, csn);
		// TODO Auto-generated constructor stub
	}

	public void indent(int step) {
		currentIndent += step * this.step;
	}

	public void step (int step)
	{
		this.step  = step;
	}

	public void left ()
	{
		indent (-1);
	}
	
	public void right ()
	{
		indent (1);
	}
	
	private void printIndent() {
		if (newline) {
			newline = false;
			if (currentIndent > 0) {
				super.write(SHIFTS, 0,
						currentIndent <= MAX_SHIFT ? currentIndent : MAX_SHIFT);
			}
		}
	}

	@Override
	public void write(char[] buf, int off, int len) {
		printIndent();
		// TODO: find \n and shift
		super.write(buf, off, len);
	}

	@Override
	public void write(String s, int off, int len) {
		printIndent();
		// TODO: find \n and shift
		super.write(s, off, len);
	}

	@Override
	public void write(int c) {
		if (c == '\n')
		{
			newline = true;
		}
		else
		{
			printIndent();
		}
		super.write(c);
	}

	@Override
	public void println() {
		super.println();
		newline = true;
	}
}
