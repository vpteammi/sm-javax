package com.sm.javax.query;

class BeginExpressionAdaptor<CONTINUE_EXPR_TYPE extends QueryBuilderHolder> implements BeginExpression<CONTINUE_EXPR_TYPE> {
	
	final CONTINUE_EXPR_TYPE endExpr;
	
	public BeginExpressionAdaptor(CONTINUE_EXPR_TYPE endExpr) {
		this.endExpr = endExpr;
	}

	@Override
	public CONTINUE_EXPR_TYPE continueExperession ()
	{
		return endExpr;
	}
}
