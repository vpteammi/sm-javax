package com.sm.javax.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

// -------------------------------------------
public class PropertyArray extends PropertyValue implements Iterable<PropertyValue>
{
	public PropertyArray (PropertyValue[] value) {
		super (Arrays.asList (value));
	}
	
	public PropertyArray (List<PropertyValue> value) {
		super (new ArrayList<PropertyValue> (value));
	}
	
	public boolean isArray () { return true; }
	
	public PropertyValue[] getValue () { 
		return list ().toArray(new PropertyValue []{}); 
	}
	
	@SuppressWarnings("unchecked")
	private List<PropertyValue> list () {
		return (List<PropertyValue>) value;
	}
	
	public int size () { return (value != null ? list ().size () : 0); }
	
	public PropertyValue get (int ind) {
		return (value == null ? null : getValue () [ind]);
	}
	
	public PropertyArray set (int ind, PropertyValue val) {
		if (value != null) {
			list ().set (ind, val);
		}
		return this;
	}
	
	@Override
	public String toString ()
	{
		if (value == null) {
			return "null";
		}
		String buf = "[" + 
				list().stream().map(v -> v.toString()).collect(Collectors.joining (", ")) + 
				"]";
		return buf;
	}

	@Override
	public Iterator<PropertyValue> iterator () {
		if (value != null) {
			return list ().iterator ();
		}
		return Collections.<PropertyValue>emptyList ().iterator ();
	}
	
}