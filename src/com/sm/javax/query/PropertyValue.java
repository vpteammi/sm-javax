package com.sm.javax.query;

import java.util.List;
import java.util.regex.Pattern;

public abstract class PropertyValue {
	
	protected final Object value;
	
	protected PropertyValue (Object value)
	{
		super ();
		this.value = value;
	}
	
	// -------------------------------------
	public abstract Object getValue ();
	
	@Override
	public String toString () {
		if (value == null) {
			return "null";
		}
		return value.toString ();
	}
	
	// -------------------------------------
	public static PropertyValue create (String value) {
		return new StringPropertyValue (value);
	}
	
	public static PropertyValue create (int value) {
		return new IntegerPropertyValue (value);
	}
	
	public static PropertyValue create (boolean value) {
		return new BooleanPropertyValue (value);
	}
	
	public static PropertyValue create (double value) {
		return new DoublePropertyValue (value);
	}
	
	public static PropertyValue create (long value) {
		return new LongPropertyValue (value);
	}
	
	public static PropertyValue create (Pattern value) {
		return new RegExpPropertyValue (value);
	}
	
	public static PropertyValue create (PropertyValue[] value) {
		return new PropertyArray (value);
	}
	
	public static PropertyValue create (List<PropertyValue> value) {
		return new PropertyArray (value);
	}
	
	public static PropertyValue create (Object value) throws IllegalArgumentException {
		if (value instanceof String) {
			return create ((String) value);
		}
		if (value instanceof Integer) {
			return create ((Integer) value);
		}
		if (value instanceof Boolean) {
			return create ((Boolean) value);
		}
		if (value instanceof Double) {
			return create ((Double) value);
		}
		if (value instanceof Long) {
			return create ((Long) value);
		}
		if (value instanceof Pattern) {
			return create ((Pattern) value);
		}
		//
		throw (new IllegalArgumentException ());
	}
	
	// -------------------------------------------
	public String getStringValue () {
		return (String) value;
	}

	public int getIntValue () {
		return (int) value;
	}

	public long getLongValue () {
		return (long) value;
	}

	public boolean getBooleanValue () {
		return (boolean) value;
	}

	public double getDoubleValue () {
		return (double) value;
	}

	public PropertyValue[] getArrayValue () {
		return (PropertyValue[]) value;
	}

	// -------------------------------------------
	public boolean isString () { return false; }
	public boolean isInteger () {return false; }
	public boolean isLong () {return false; }
	public boolean isBoolean () {return false; }
	public boolean isDouble () {return false; }
	public boolean isArray () {return false; }
	public boolean isObject () {return false; }
	public boolean isRegExp () {return false; }

	// -------------------------------------------
	private static class StringPropertyValue extends PropertyValue
	{
		public StringPropertyValue (String value) {
			super (value);
		}
		
		public boolean isString () { return true; }
		
		public String getValue () { return (String) value; }
		
		@Override
		public String toString () {
			if (value == null) {
				return "null";
			}
			return "\"" + value + "\"";
		}
	}
	
	// -------------------------------------------
	private static class IntegerPropertyValue extends PropertyValue
	{
		public IntegerPropertyValue (int value) {
			super (value);
		}
		
		public boolean isInteger () { return true; }
		
		public Integer getValue () { return (Integer) value; }
	}
	
	// -------------------------------------------
	private static class BooleanPropertyValue extends PropertyValue
	{
		public BooleanPropertyValue (boolean value) {
			super (value);
		}
		
		public boolean isBoolean () { return true; }
		
		public Boolean getValue () { return (Boolean) value; }
	}

	// -------------------------------------------
	private static class DoublePropertyValue extends PropertyValue
	{
		public DoublePropertyValue (Double value) {
			super (value);
		}
		
		public boolean isDouble () { return true; }
		
		public Double getValue () { return (Double) value; }
	}

	// -------------------------------------------
	private static class LongPropertyValue extends PropertyValue
	{
		public LongPropertyValue (Long value) {
			super (value);
		}
		
		public boolean isLong () { return true; }
		
		public Long getValue () { return (Long) value; }
	}

}
