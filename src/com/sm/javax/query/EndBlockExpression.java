package com.sm.javax.query;

public interface EndBlockExpression<END_EXPR_TYPE extends QueryBuilderHolder> extends ContinueExpression<EndBlockExpression<END_EXPR_TYPE>>
{
	public default END_EXPR_TYPE bc ()
	{
		getQueryBuilder ().consumeBc ();
		return endExpression ();
	}
	
	public END_EXPR_TYPE endExpression ();
}
