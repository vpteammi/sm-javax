package com.sm.javax.query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertyCondition <CONTINUE_EXPR_TYPE extends QueryBuilderHolder>{
	private final CONTINUE_EXPR_TYPE endExpr;
	private final PropertyPath path;
	
	PropertyCondition(PropertyPath path, CONTINUE_EXPR_TYPE endExpr) {
		super();
		this.endExpr = endExpr;
		this.path = path;
	}

	public CONTINUE_EXPR_TYPE exists ()
	{
		endExpr.getQueryBuilder ().consumeExists (path);
		return endExpr;
	}
	
	public CONTINUE_EXPR_TYPE notexists ()
	{
		endExpr.getQueryBuilder ().consumeNotExists (path);
		return endExpr;
	}
	
	public CONTINUE_EXPR_TYPE type (IPropertyType type)
	{
		endExpr.getQueryBuilder ().consumeType (path, type);
		return endExpr;
	}
	
	public CONTINUE_EXPR_TYPE gt (Object value)
	{
		endExpr.getQueryBuilder ().consumeGt (path, value);
		return endExpr;
	}
	public CONTINUE_EXPR_TYPE ge (Object value)
	{
		endExpr.getQueryBuilder ().consumeGe (path, value);
		return endExpr;
	}
	public CONTINUE_EXPR_TYPE lt (Object value)
	{
		endExpr.getQueryBuilder ().consumeLt (path, value);
		return endExpr;
	}
	public CONTINUE_EXPR_TYPE le (Object value)
	{
		endExpr.getQueryBuilder ().consumeLe (path, value);
		return endExpr;
	}
	public CONTINUE_EXPR_TYPE eq (Object value)
	{
		endExpr.getQueryBuilder ().consumeEq (path, value);
		return endExpr;
	}
	public CONTINUE_EXPR_TYPE ne (Object value)
	{
		endExpr.getQueryBuilder ().consumeNe (path, value);
		return endExpr;
	}
	
	public CONTINUE_EXPR_TYPE in (Object ... values)
	{
		endExpr.getQueryBuilder ().consumeIn (path, values);
		return endExpr;
	}
	
	public CONTINUE_EXPR_TYPE notIn (Object ... values)
	{
		endExpr.getQueryBuilder ().consumeNotIn (path, values);
		return endExpr;
	}
	
	private static final Pattern RegExprFormat = Pattern.compile ("^/([^\\/]*)/([imsx]*)$"); 
	
	public CONTINUE_EXPR_TYPE match (String regexpr)
	{
		Matcher m = RegExprFormat.matcher(regexpr);
		Pattern pattern = null;
		if (m.find()) {
			String expr = m.group (1);
			String opts = m.group (2);
			int flags = 0;
			if (opts.indexOf ('i') != -1) { flags |= Pattern.CASE_INSENSITIVE; }
			if (opts.indexOf ('x') != -1) { flags |= Pattern.COMMENTS; }
			if (opts.indexOf ('m') != -1) { flags |= Pattern.MULTILINE; }
			if (opts.indexOf ('s') != -1) { flags |= Pattern.DOTALL; }
			pattern = Pattern.compile (expr, flags);
		}
		else {
			pattern = Pattern.compile (regexpr);
		}
		endExpr.getQueryBuilder ().consumeMatch (path, pattern);
		return endExpr;
	}
}
