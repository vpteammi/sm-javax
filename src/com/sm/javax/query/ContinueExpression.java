package com.sm.javax.query;

public interface ContinueExpression<CONTINUE_EXPR_TYPE extends QueryBuilderHolder> extends QueryBuilderHolder {
	public default BeginExpression<CONTINUE_EXPR_TYPE> and ()
	{
		getQueryBuilder ().consumeAnd ();
		return new BeginExpressionAdaptor<CONTINUE_EXPR_TYPE> (continueExpression ());
	}
	public default BeginExpression<CONTINUE_EXPR_TYPE> or ()
	{
		getQueryBuilder ().consumeOr ();
		return new BeginExpressionAdaptor<CONTINUE_EXPR_TYPE> (continueExpression ());
	}
	public CONTINUE_EXPR_TYPE continueExpression ();
	
	@Override
	public default QueryBuilder getQueryBuilder ()
	{
		return continueExpression ().getQueryBuilder ();
	}
}
