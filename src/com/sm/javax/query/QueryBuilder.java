package com.sm.javax.query;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;

public interface QueryBuilder
{
	public void startFinder ();

	// Find builder
	public void consumeBo ();
	public void consumeBc ();
	public void consumeOr ();
	public void consumeAnd ();
	public void consumeNot ();
	public void consumeLt (PropertyPath path, Object value);
	public void consumeLe (PropertyPath path, Object value);
	public void consumeGt (PropertyPath path, Object value);
	public void consumeGe (PropertyPath path, Object value);
	public void consumeEq (PropertyPath path, Object value);
	public void consumeNe (PropertyPath path, Object value);
	public void consumeExists (PropertyPath path);
	public void consumeNotExists(PropertyPath path);
	public void consumeMatch (PropertyPath path, Pattern pattern);
	public void consumeType (PropertyPath path, IPropertyType type);
	
	public void startUpdater ();

	// Update Builder
	public void editProperty (PropertyPath path);
	public void consumeUnset ();
	public void consumeSet (Object value);
	public void consumeInc (Object value);
	public void consumeDec (Object value);
	public void consumeMult (Object value);
	public void consumeMax (Object value);
	public void consumeMin (Object value);
	public void consumeIn (PropertyPath path, Object... values);
	public void consumeNotIn (PropertyPath path, Object... values);
	
	public QueryBuilder clear ();
	public QueryBuilder complete ();
	
	// ------------------------------------------------------------------
	public FindResult find (List<String> fields);
	public default FindResult find (String[] fields) {
		return find (fields == null ? null : Arrays.asList (fields));
	}
	public default FindResult find () {
		return find ((List<String>)null);
	}

	public FindResult findOne (List<String> fields);
	public default FindResult findOne (String[] fields) {
		return findOne (fields == null ? null : Arrays.asList (fields));
	}
	public default FindResult findOne () {
		return findOne ((List<String>)null);
	}

	public FindResult count ();
	
	public DeleteResult remove ();

	public UpdateResult update ();

	// ------------------------------------------------------------------
	public void find (List<String> fields, Consumer<FindResult> handler);

	public default void find (String[] fields, Consumer<FindResult> handler) {
		find (fields == null ? null : Arrays.asList (fields), handler);
	}
	public default void find (Consumer<FindResult> handler) {
		find ((List<String>) null, handler);
	}

	public void findOne (List<String> fields, Consumer<FindResult> handler);
	public default void findOne (String[] fields, Consumer<FindResult> handler) {
		findOne (fields == null ? null : Arrays.asList (fields), handler);
	}
	public default void findOne (Consumer<FindResult> handler) {
		findOne ((List<String>) null, handler);
	}

	public void count (Consumer<Result<Long>> handler);
	
	public void remove (Consumer<Result<Long>> handler);

	public void update (Consumer<Result<UpdateResult>> handler);

}
