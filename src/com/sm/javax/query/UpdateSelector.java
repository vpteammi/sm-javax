package com.sm.javax.query;

public abstract class UpdateSelector implements QueryBuilderHolder
{

	public final QueryBuilder queryBuilder;

	public UpdateSelector (QueryBuilder queryBuilder)
	{
		this.queryBuilder = queryBuilder;
	}

	public PropertyEditor property (String name)
	{
		return property (PropertyPath.toPropertyPath (name));
	}

	public PropertyEditor property (PropertyPath path)
	{
		return new PropertyEditor (this, path);
	}

	@Override
	public QueryBuilder getQueryBuilder ()
	{
		return queryBuilder;
	}

	abstract Updater getUpdater ();
}