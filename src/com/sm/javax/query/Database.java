package com.sm.javax.query;

public class Database {

	private final QueryBuilder queryBuilder;
	
	public Database(QueryBuilder queryBuilder) {
		this.queryBuilder = queryBuilder;
	}

	public FindExpression query () {
		return new FindExpression (queryBuilder);
	}
}
