package com.sm.javax.query;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.sm.javax.langx.Strings;

public class PropertyObject extends PropertyValue implements Iterable<Entry<String, PropertyValue>>
{
	
	public PropertyObject () {
		super (new HashMap<String, PropertyValue> ());
	}
	
	public PropertyObject (Map<String, PropertyValue> attrs) {
		super (new HashMap<String, PropertyValue> (attrs));
	} 

	@SuppressWarnings("unchecked")
	protected Map<String, PropertyValue> attributes ()
	{
		return (Map<String, PropertyValue>) value;
	}

	// ------------------------------------------------------------------
	@Override
	public boolean isObject () { return true; }
	
	public boolean hasAttribute (String attrName) {
		return attributes ().containsKey (attrName);
	}
	
	public PropertyValue getAttribute (String attrName) {
		PropertyValue val = attributes ().get (attrName);
		return val;
	}
	
	@Override
	public String toString ()
	{
		if (value == null) {
			return "null";
		}
		String buf = "{" +
			attributes().entrySet().stream()
				.map(entry -> entry.getKey() + ": " + entry.getValue().toString())
				.collect(Collectors.joining(", ")) +
		 	"}";
		return buf;
	}
	
	// ------------------------------------------------------------------
	private void put (String[] pathElems, PropertyValue value) {
		if (pathElems != null && pathElems.length > 0) {
			PropertyObject target = this;
			int lastInd = pathElems.length - 1; 
			for (int i=0; i<lastInd; i++) {
				String attrName = pathElems [i];
				if (! target.hasAttribute(attrName)) {
					PropertyObject obj = new PropertyObject ();
					target.addAttribute (attrName, obj);
					target = obj;
				}
				else {
					PropertyValue attrVal = target.getAttribute (attrName);
					if (attrVal.isObject ()) {
						target = (PropertyObject) attrVal;
					}
					else {
						PropertyObject obj = new PropertyObject ();
						target.addAttribute (attrName, obj);
						target = obj;
					}
				}
			}
			target.addAttribute(pathElems [lastInd], value);
		}
	}
	
	public PropertyObject put (PropertyPath path, PropertyValue value) {
		if (path != null) {
			put (path.getPathParts(), value);
		}
		return this;
	}
	
	public PropertyObject put (String path, PropertyValue value) {
		if (! Strings.isEmpty (path)) {
			String[] pathElems = path.split ("\\.");
			put (pathElems, value);
		}
		return this;
	}
	
	public PropertyObject put (PropertyPath path, String value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (PropertyPath path, int value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (PropertyPath path, long value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (PropertyPath path, double value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (PropertyPath path, boolean value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (PropertyPath path, Pattern value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (PropertyPath path, PropertyValue[] value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (PropertyPath path, List<PropertyValue> value) {
		return put (path, PropertyValue.create (value));
	} 
	
	public PropertyObject put (String path, String value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (String path, int value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (String path, long value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (String path, double value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (String path, boolean value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (String path, Pattern value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (String path, PropertyValue[] value) {
		return put (path, PropertyValue.create (value));
	} 
	public PropertyObject put (String path, List<PropertyValue> value) {
		return put (path, PropertyValue.create (value));
	} 
	
	// ------------------------------------------------------------------
	public PropertyValue get (String[] pathElems) {
		if (pathElems != null && pathElems.length > 0) {
			PropertyObject target = this;
			int lastInd = pathElems.length - 1; 
			for (int i=0; i<lastInd; i++) {
				String attrName = pathElems [i];
				if (target.hasAttribute (attrName)) {
					PropertyValue attrVal = target.getAttribute (attrName);
					if (attrVal.isObject ()) {
						target = (PropertyObject) attrVal;
					}
					else {
						return null;
					}
				}
				else {
					return null;
				}
			}
			return target.getAttribute(pathElems [lastInd]);
		}
		return null;
	}
	
	public PropertyValue get (String path) {
		if (! Strings.isEmpty (path)) {
			String[] pathElems = path.split ("\\.");
			return get (pathElems);
		}
		return null;
	}
	
	public PropertyValue get (PropertyPath path) {
		if (path != null) {
			return get (path.getPathParts ());
		}
		return null;
	}
	
	// ------------------------------------------------------------------
	private void addAttribute (String attrName, PropertyValue value) {
		attributes ().put(attrName, value);
	}

	@Override
	public PropertyObject getValue() {
		return this;
	}

	// ------------------------------------------------------------------
	public PropertyObject add (PropertyObject obj) {
		if (obj != this && obj != null) {
			for (Entry<String, PropertyValue> entry : obj.attributes ().entrySet ()) {
				this.addAttribute(entry.getKey (), entry.getValue ());
			}
		}
		return this;
	}
	
	public Set<String> keys () {
		if (value != null) {
			return attributes ().keySet ();
		}
		return null;
	}
	
	public Set<String> allKeys () {
		HashSet<String> keys = new HashSet<> ();
		for (Entry<String, PropertyValue> entry : attributes ().entrySet ()) {
			String key = entry.getKey ();
			PropertyValue value = entry.getValue ();
			keys.add (key);
			if (value != null && value.isObject ()) {
				PropertyObject obj = (PropertyObject) value;
				obj.allKeys ().stream ().map (k -> key + "." + k).forEach (k -> keys.add (k)); 
			}
		}
		return keys;
	}
	
	@Override
	public Iterator<Entry<String, PropertyValue>> iterator () {
		return attributes ().entrySet ().iterator ();
	}

}
