package com.sm.javax.query;

public interface IPropertyType {
	
	public int getTypeId ();
	
	public String getTypeName ();
	
	public default String getTypeAlias () {
		return getTypeName ();
	}

}
