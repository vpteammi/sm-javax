package com.sm.javax.query;

public class DeleteResult extends Result<Long> {

	public DeleteResult(Throwable err) {
		super (err);
	}

	public DeleteResult (long docsDeleted) {
		super (docsDeleted);
	}
	
	public long getRemovedCount () {
		return (long) value ();
	}

}
