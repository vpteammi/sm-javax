package com.sm.javax.query;

import java.util.List;

public class FindContinueExpression implements ContinueExpression<FindContinueExpression>
{
	private final FindExpression findExpression;

	public FindContinueExpression (FindExpression findExpression)
	{
		this.findExpression = findExpression;
	}

	public FindResult find (List<String> fields)
	{
		return findExpression.find (fields);
	}
	public FindResult find (String[] fields)
	{
		return find (fields);
	}
	public FindResult find ()
	{
		return findExpression.find ();
	}

	public FindResult findOne (List<String> fields)
	{
		return findExpression.findOne (fields);
	}
	public FindResult findOne (String[] fields)
	{
		return findExpression.findOne (fields);
	}
	public FindResult findOne ()
	{
		return findExpression.findOne ();
	}
	
	public FindResult count ()
	{
		return findExpression.count ();
	}
	
	public DeleteResult remove ()
	{
		return findExpression.remove ();
	}

	public Updater update ()
	{
		return findExpression.update ();
	}

	@Override
	public QueryBuilder getQueryBuilder ()
	{
		return findExpression.getQueryBuilder ();
	}

	@Override
	public FindContinueExpression continueExpression ()
	{
		return this;
	}
}
