package com.sm.javax.query;

interface QueryBuilderHolder
{
	public QueryBuilder getQueryBuilder ();
}
