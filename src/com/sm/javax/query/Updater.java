package com.sm.javax.query;

import java.util.function.Consumer;

public class Updater extends UpdateSelector {
	public Updater (QueryBuilder queryBuilder)
	{
		super (queryBuilder);
	}

	public UpdateResult execute ()
	{
		return getQueryBuilder ().update ();
	}

	public void execute (Consumer<Result<UpdateResult>> handler)
	{
		getQueryBuilder ().update (handler);
	}

	@Override
	Updater getUpdater ()
	{
		return this;
	}
}
