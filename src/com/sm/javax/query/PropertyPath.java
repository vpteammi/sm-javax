package com.sm.javax.query;

import java.util.Arrays;
import java.util.stream.Collectors;

public class PropertyPath
{
	public static final String JsonPathSeparator = ".";
	private static final String RegExprJsonPathSeparator = "\\.";
	public static final String CanonicalPathSeparator = "/";
	
	private String[] mPath;
	
	public PropertyPath (String path)
	{
		mPath = path.split (RegExprJsonPathSeparator);
	}

	public PropertyPath (String path, String pathSep)
	{
		mPath = path.split (pathSep);
	}
	
	public PropertyPath (PropertyPath path, String subpath)
	{
		String[] subPath = (subpath != null ? subpath.split (RegExprJsonPathSeparator) : null);
		mPath = buildPath (path, subPath);
	}
	
	public PropertyPath (PropertyPath path, String subpath, String pathSep)
	{
		String[] subPath = (subpath != null ? subpath.split (pathSep) : null);
		mPath = buildPath (path, subPath);
	}
	
	protected PropertyPath (String[] path)
	{
		mPath = path;
	}
	
	protected PropertyPath (String[] path, int length)
	{
		mPath = Arrays.copyOf (path, length);
	}
	
	public static PropertyPath toPropertyPath (String path)
	{
		return (new PropertyPath (path));
	}
	
	// --------------------------------------------------------------
	private String[] buildPath (PropertyPath path, String[] subPath)
	{
		if (subPath == null) {
			subPath = new String [0];
		} 
		String[] resPath = subPath;
		if (path != null) {
			int size1 = path.mPath.length;
			int size2 = subPath.length;
			resPath = Arrays.copyOf (path.mPath, size1+size2);
			for (int i=0; i<size2; i++) {
				resPath [size1+i] = subPath [i];
			}
		}
		return resPath;
	}
	
	// --------------------------------------------------------------
	public boolean isSimpleName ()
	{
		return (mPath.length == 1);
	}
	
	public String[] getPathParts ()
	{
		return mPath;
	}
	
	public String getPathPart (int ind)
	{
		return mPath [ind];
	}
	
	public int getPathPartsSize ()
	{
		return mPath.length;
	}
	
	public PropertyPath getParent ()
	{
		if (mPath.length > 1) {
			return (new PropertyPath (mPath, mPath.length-1));
		}
		return null;
	}
	
	public String getName ()
	{
		return mPath [mPath.length - 1];
	}

	// --------------------------------------------------------------
	public String getCanonicalPath ()
	{
		return Arrays.stream (mPath).collect (Collectors.joining (CanonicalPathSeparator));
	}
	
	public String getJsonPath ()
	{
		return Arrays.stream (mPath).collect (Collectors.joining (JsonPathSeparator));
	}
	
	@Override
	public String toString ()
	{
		return getJsonPath ();
	}

}
