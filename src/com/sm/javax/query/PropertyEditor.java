package com.sm.javax.query;

public class PropertyEditor extends UpdateSelector {
	private final Updater updater;
	//private final PropertyPath path;
	
	public PropertyEditor (UpdateSelector updater, PropertyPath propertyPath)
	{
		super (updater.queryBuilder);
		// this.path = propertyPath;
		this.updater = updater.getUpdater ();
		getQueryBuilder ().editProperty (propertyPath);
	}
	
	public Updater unset ()
	{
		getQueryBuilder ().consumeUnset ();
		return getUpdater ();
	}
	public Updater set (Object value)
	{
		getQueryBuilder ().consumeSet (value);
		return getUpdater ();
	}
	public Updater inc (Object value)
	{
		getQueryBuilder ().consumeInc (value);
		return getUpdater ();
	}
	public Updater dec (Object value)
	{
		getQueryBuilder ().consumeDec (value);
		return getUpdater ();
	}
	public Updater mult (Object value)
	{
		getQueryBuilder ().consumeMult (value);
		return getUpdater ();
	}
	public Updater max (Object value)
	{
		getQueryBuilder ().consumeMax (value);
		return getUpdater ();
	}
	public Updater min (Object value)
	{
		getQueryBuilder ().consumeMin (value);
		return getUpdater ();
	}

	@Override
	Updater getUpdater ()
	{
		return updater;
	}
}
