package com.sm.javax.query;

// --------------------------------------------------------------------------------
public class Result<T> 
{
	private Throwable err;
	private boolean   succeeded;
	private T         value;
	
	protected Result ()
	{}
	
	public Result (Throwable err)
	{
		super ();
		setFailure(err);
	}
	
	public Result (T value)
	{
		super ();
		setValue (value);
	}
	
	public Result (T value, boolean succeeded)
	{
		super ();
		this.succeeded = succeeded;
		setValue (value);
	}
	
	public Result (String errMsg)
	{
		this (new Exception (errMsg));
	}
	
	public Result<T> setValue (T value)
	{
		this.succeeded = true;
		this.value = value;
		return this;
	}

	public Throwable cause ()
	{
		return err;
	}

	protected Result<T>  setFailure (Throwable err)
	{
		this.succeeded = false;
		this.err = err;
		return this;
	}
	
	public boolean succeeded ()
	{
		return succeeded;
	}

	public boolean failed ()
	{
		return (! succeeded ());
	}
	
	public T value ()
	{
		return value;
	}
	
}