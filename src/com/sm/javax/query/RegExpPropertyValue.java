package com.sm.javax.query;

import java.util.regex.Pattern;

public class RegExpPropertyValue extends PropertyValue {

	public RegExpPropertyValue (Pattern value) {
		super (value);
	}
	
	public RegExpPropertyValue (String strValue) {
		this (Patterns.parseRegularExpression (strValue));
	}

	@Override
	public Pattern getValue () {
		return (Pattern) value;
	}
	
	@Override
	public boolean isRegExp() { return true; }
	
	@Override
	public String toString () {
		return Patterns.ConvertToString (getValue ());
	}

}
