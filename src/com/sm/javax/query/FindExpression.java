package com.sm.javax.query;

import java.util.Arrays;
import java.util.List;

public class FindExpression implements BeginExpression<FindContinueExpression>
{
	protected final QueryBuilder queryBuilder;

	public FindExpression (QueryBuilder queryBuilder)
	{
		this.queryBuilder = queryBuilder;
		getQueryBuilder ().startFinder ();
	}
	
	public FindResult find (List<String> fields)
	{
		return getQueryBuilder ().find (fields);
	}

	public FindResult find (String[] fields)
	{
		return find (fields == null ? null : Arrays.asList (fields));
	}

	public FindResult find ()
	{
		return find ((List<String>)null);
	}

	public FindResult findOne (List<String> fields)
	{
		return getQueryBuilder ().findOne (fields);
	}
	
	public FindResult findOne (String[] fields)
	{
		return findOne (fields == null ? null : Arrays.asList (fields));
	}

	public FindResult findOne ()
	{
		return findOne ((List<String>)null);
	}

	public FindResult count ()
	{
		return getQueryBuilder ().count ();
	}
	
	public DeleteResult remove ()
	{
		return getQueryBuilder ().remove ();
	}

	public Updater update ()
	{
		getQueryBuilder ().startUpdater ();
		return new Updater (getQueryBuilder ());
	}

	@Override
	public FindContinueExpression continueExperession ()
	{
		return new FindContinueExpression (this);
	}

	@Override
	public QueryBuilder getQueryBuilder ()
	{
		return queryBuilder;
	}

}