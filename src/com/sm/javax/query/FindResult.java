package com.sm.javax.query;

public class FindResult extends Result<Object> {
	
	public FindResult (Throwable err) {
		super (err);
	}
	
	public FindResult (Object value) {
		super (value);
	}

}
