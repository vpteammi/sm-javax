package com.sm.javax.query;

public class UpdateResult extends Result<Long> {

	protected long docsFound;
	
	public UpdateResult (Throwable errorCause) {
		super(errorCause);
	}

	public UpdateResult (long docsModified) {
		super (docsModified);
	}

	public UpdateResult (long docsModified, long docsFound) {
		super (docsModified);
		this.docsFound = docsFound;
	}
	
	public long getDocumentsFound () {
		return (long) value ();
	}

	public long getDocumentsModified () {
		return docsFound;
	}

}
