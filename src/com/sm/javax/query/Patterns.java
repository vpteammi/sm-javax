package com.sm.javax.query;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sm.javax.langx.Strings;

public class Patterns
{
	private static final Pattern StringRegExpr = Pattern.compile ("^/(.*)/([imxs]*)$");

	public static Pattern parseRegularExpression (String value)
	{
		if (value == null || "".equals (value) || !value.startsWith ("/")) {
			return null;
		}
		Matcher m = StringRegExpr.matcher (value);
		if (m.find ()) {
			String regex = m.group (1);
			String options = m.group (2);
			return ConvertToPattern (regex, options);
		}
		return null;
	}

	public static Pattern ConvertToPattern (String regex, String options)
	{
		int flags = 0;
		if (! Strings.isEmpty (options)) {
			if (options.indexOf ('i') != -1) { flags = Pattern.CASE_INSENSITIVE; }
			if (options.indexOf ('m') != -1) { flags |= Pattern.MULTILINE; }
			if (options.indexOf ('x') != -1) { flags |= Pattern.COMMENTS; }
			if (options.indexOf ('s') != -1) { flags |= Pattern.DOTALL; }
		}
		return Pattern.compile (regex, flags);
	}
	
	public static String RegExOptions (Pattern regex)
	{
		int flags = regex.flags ();
		String options = "";
		if ((flags & Pattern.CASE_INSENSITIVE) != 0) { options += "i"; }
		if ((flags & Pattern.MULTILINE) != 0) { options += "m"; }
		if ((flags & Pattern.COMMENTS) != 0) { options += "x"; }
		if ((flags & Pattern.DOTALL) != 0) { options += "s"; }
		return options;
	}
	
	public static String ConvertToString (Pattern regex) 
	{
		return ("/" + regex.pattern () + "/" + RegExOptions (regex));
	}

}
