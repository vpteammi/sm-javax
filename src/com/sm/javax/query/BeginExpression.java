package com.sm.javax.query;

public interface BeginExpression<CONTINUE_EXPR_TYPE extends QueryBuilderHolder> extends QueryBuilderHolder
{
	public default BeginExpression<EndBlockExpression<CONTINUE_EXPR_TYPE>> bo ()
	{
		continueExperession ().getQueryBuilder ().consumeBo ();
		EndBlockExpression<CONTINUE_EXPR_TYPE> ebe = new EndBlockExpression<CONTINUE_EXPR_TYPE> ()
		{
			@Override
			public CONTINUE_EXPR_TYPE endExpression ()
			{
				return BeginExpression.this.continueExperession ();
			}

			@Override
			public QueryBuilder getQueryBuilder ()
			{
				return BeginExpression.this.getQueryBuilder ();
			}

			@Override
			public EndBlockExpression<CONTINUE_EXPR_TYPE> continueExpression ()
			{
				return this;
			}
		};
		return new BeginExpressionAdaptor<EndBlockExpression<CONTINUE_EXPR_TYPE>> (ebe);
	}

	public default BeginExpression<CONTINUE_EXPR_TYPE> not ()
	{
		continueExperession ().getQueryBuilder ().consumeNot ();
		return new BeginExpressionAdaptor<CONTINUE_EXPR_TYPE> (continueExperession ());
	}

	public default PropertyCondition<CONTINUE_EXPR_TYPE> property (String name)
	{
		return property (new PropertyPath (name));
	}

	public default PropertyCondition<CONTINUE_EXPR_TYPE> property (PropertyPath path)
	{
		return new PropertyCondition<CONTINUE_EXPR_TYPE> (path, continueExperession ());
	}

	@Override
	public default QueryBuilder getQueryBuilder ()
	{
		return continueExperession ().getQueryBuilder ();
	}

	CONTINUE_EXPR_TYPE continueExperession ();
}