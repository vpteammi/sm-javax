package com.sm.javax.emailx;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

public class EMailBuilder
{
	private InternetAddress from = null;
	private ArrayList<InternetAddress> to = null;
	private ArrayList<InternetAddress> replyTo = null;
	private ArrayList<InternetAddress> cc = null;
	private ArrayList<InternetAddress> bcc = null;
	private String subject = null;
	private StringBuilder body = null;
	private boolean asHtml = false;
	private final Session session;

	private ArrayList<InternetAddress> allocReplyTo ()
	{
		if (replyTo == null)
			replyTo = new ArrayList<InternetAddress> ();
		return (replyTo);
	}

	private ArrayList<InternetAddress> allocTo ()
	{
		if (to == null)
			to = new ArrayList<InternetAddress> ();
		return (to);
	}

	private ArrayList<InternetAddress> allocCc ()
	{
		if (cc == null)
			cc = new ArrayList<InternetAddress> ();
		return (cc);
	}

	private ArrayList<InternetAddress> allocBcc ()
	{
		if (bcc == null)
			bcc = new ArrayList<InternetAddress> ();
		return (bcc);
	}

	private StringBuilder allocBody ()
	{
		if (body == null)
			body = new StringBuilder ();
		return (body);
	}

	public EMailBuilder (Session session)
	{
		this.session = session;
	}

	public static EMailBuilder smtp (String server)
	{
		Properties props = System.getProperties ();
		props.put ("mail.smtp.host", server);
		return (new EMailBuilder (Session.getInstance (props, null)));
	}

	private EMailBuilder add (ArrayList<InternetAddress> list,
			InternetAddress... addresses)
	{
		for (InternetAddress addr : addresses)
		{
			list.add (addr);
		}

		return (this);
	}

	private EMailBuilder add (ArrayList<InternetAddress> list,
			Iterable<InternetAddress> addresses)
	{
		for (InternetAddress addr : addresses)
		{
			list.add (addr);
		}

		return (this);
	}

	public EMailBuilder from (InternetAddress from) throws AddressException
	{
		this.from = from;
		return (this);
	}

	public EMailBuilder from (String emailAddr) throws AddressException
	{
		return (from (new InternetAddress (emailAddr)));
	}

	public EMailBuilder from (String emailAddr, String name)
			throws AddressException, UnsupportedEncodingException
	{
		return (from (new InternetAddress (emailAddr, name)));
	}

	public EMailBuilder replyTo (InternetAddress... addresses)
	{
		return (add (allocReplyTo (), addresses));
	}

	public EMailBuilder replyTo (Iterable<InternetAddress> addresses)
	{
		return (add (allocReplyTo (), addresses));
	}

	public EMailBuilder replyTo (String toList) throws AddressException
	{
		return (replyTo (InternetAddress.parse (toList, false)));
	}

	public EMailBuilder to (InternetAddress... addresses)
	{
		return (add (allocTo (), addresses));
	}

	public EMailBuilder to (Iterable<InternetAddress> addresses)
	{
		return (add (allocTo (), addresses));
	}

	public EMailBuilder to (String toList) throws AddressException
	{
		return (to (InternetAddress.parse (toList, false)));
	}

	public EMailBuilder cc (InternetAddress... addresses)
	{
		return (add (allocCc (), addresses));
	}

	public EMailBuilder cc (Iterable<InternetAddress> addresses)
	{
		return (add (allocCc (), addresses));
	}

	public EMailBuilder cc (String toList) throws AddressException
	{
		return (cc (InternetAddress.parse (toList, false)));
	}

	public EMailBuilder bcc (InternetAddress... addresses)
	{
		return (add (allocBcc (), addresses));
	}

	public EMailBuilder bcc (Iterable<InternetAddress> addresses)
	{
		return (add (allocBcc (), addresses));
	}

	public EMailBuilder bcc (String toList) throws AddressException
	{
		return (bcc (InternetAddress.parse (toList, false)));
	}

	public EMailBuilder subject (Object subj)
	{
		if (subj != null)
			subject = subj.toString ();
		return (this);
	}

	public EMailBuilder body (Object chunk)
	{
		if (chunk != null)
			allocBody ().append (chunk.toString ());
		return (this);
	}

	public EMailBuilder asHtml ()
	{
		asHtml = true;
		return (this);
	}

	private void setRecepients (Message msg, List<InternetAddress> list,
			RecipientType type) throws MessagingException
	{
		if ((list != null) && (list.size () > 0))
		{
			msg.setRecipients (type, list.toArray (new InternetAddress[list.size ()]));
		}
	}

	public void send () throws MessagingException, IOException
	{
		Message msg = new MimeMessage (session);

		if (from == null)
		{
			throw new IllegalArgumentException ("missing sender");
		}

		msg.setFrom (from);

		if ((to == null) && (cc == null) && (bcc == null))
		{
			throw new IllegalArgumentException ("missing recipient");
		}

		setRecepients (msg, to, Message.RecipientType.TO);
		setRecepients (msg, cc, Message.RecipientType.CC);
		setRecepients (msg, bcc, Message.RecipientType.BCC);
		
		if ((replyTo != null) && (replyTo.size () > 0))
		{
			msg.setReplyTo (replyTo.toArray (new InternetAddress[replyTo.size ()]));
		}

		String subj = subject;

		if (subject != null)
		{
			msg.setSubject (subject);
		}
		else
		{
			subj = "";
		}

		if (body != null)
		{
			String mimeType = asHtml ? "text/html" : "text/plain";
			String text = body.toString ();
			if (asHtml)
			{
				text = String.format (
						"<html><head><title>%s</title></head><body>%s</body></html>", subj,
						text);
			}
			msg.setDataHandler (new DataHandler (new ByteArrayDataSource (text,
					mimeType)));
			msg.setSentDate (new Date ());
			Transport.send (msg);
		}
	}
}
