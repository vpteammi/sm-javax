/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   UIProgressReporter.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Created.
 */

package com.sm.javax.progress;


public interface ProgressReporter {
	public void addProgress(ProgressIndicator progressBar);
}
