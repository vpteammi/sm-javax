package com.sm.javax.progress;

public interface ProgressListener {
	public void reportProgress(int current, int total);
}
