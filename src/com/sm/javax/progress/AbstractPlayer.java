package com.sm.javax.progress;

public abstract class AbstractPlayer implements Player {
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sm.javax.progress.Player#resumePlaying()
	 */
	@Override
	public boolean resumePlaying() {
		return (false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sm.javax.progress.Player#pause()
	 */
	@Override
	public boolean pause() {
		return (false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sm.javax.progress.Player#step()
	 */
	@Override
	public boolean step() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sm.javax.progress.Player#skipToStart()
	 */
	@Override
	public boolean skipToStart() {
		return (false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sm.javax.progress.Player#skipToEnd()
	 */
	@Override
	public boolean skipToEnd() {
		return (false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sm.javax.progress.Player#fastForward()
	 */
	@Override
	public boolean fastForward() {
		return (false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sm.javax.progress.Player#rewind()
	 */
	@Override
	public boolean rewind() {
		return (false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sm.javax.progress.Player#repeat()
	 */
	@Override
	public boolean repeat(boolean on) {
		return false;
	}
}
