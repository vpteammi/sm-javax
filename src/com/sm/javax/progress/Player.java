package com.sm.javax.progress;

public interface Player {

	public abstract boolean startPlaying();

	public abstract boolean resumePlaying();

	public abstract boolean pause();

	public abstract boolean step();

	public abstract boolean stop();

	public abstract boolean skipToStart();

	public abstract boolean skipToEnd();

	public abstract boolean fastForward();

	public abstract boolean rewind();

	public abstract boolean eject();

	public abstract boolean repeat(boolean on);

}