/*** Set Tab Size to 4 to edit this file. Check that the two following lines **
 **** appear the same:                                                       ***
 **                      #   #   #   #   #   #   #   #   #   #
 **                      #   #   #   #   #   #   #   #   #   #
 *******************************************************************************

 FILE:   UIProgressBar.java

 ******************************************************************************/
/*
 HISTORY:

 13-Apr-09   L-03-31     prf     $$1     Created.
 */

package com.sm.javax.progress;


public interface ProgressIndicator {
	public void setBounds(int min, int max);

	public void setValue(int value);

	public void setMessage(String message);

	public void done();

	public void enableStop(boolean enable);

	public void setListener(StopListener l);
}
