package com.sm.javax.cryptox;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


public class DesEncrypter {
	Cipher ecipher;
	Cipher dcipher;

	public DesEncrypter(SecretKey key) {
		try {
			ecipher = Cipher.getInstance(ENCRYPTION);
			dcipher = Cipher.getInstance(ENCRYPTION);
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher.init(Cipher.DECRYPT_MODE, key);

		} catch (javax.crypto.NoSuchPaddingException e) {
		} catch (java.security.NoSuchAlgorithmException e) {
		} catch (java.security.InvalidKeyException e) {
		}
	}

	public DesEncrypter(String key) {
		this(getDesKey(key));
	}

	public byte[] encryptToBytes(String str) {
		try {
			// Encode the string into bytes using utf-8
			byte[] utf8 = str.getBytes(ENCODING);

			return ecipher.doFinal(utf8);
		} catch (javax.crypto.BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

	public String encrypt(String str) {
		return Base64.encode(encryptToBytes(str));
	}

	public String decrypt(String str) {
		try {
			// Decode base64 to get bytes
			byte[] dec = Base64.decode(str);

			// Decrypt
			byte[] utf8 = dcipher.doFinal(dec);

			// Decode using utf-8
			return new String(utf8, ENCODING);
		} catch (javax.crypto.BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		}
		return null;
	}

	private static byte[] secretTo8Bytes(String secret) {
		if ((secret == null) || (secret.length() == 0)) {
			throw new IllegalArgumentException("secret cannot be empty");
		}
		try {
			secret = Base64.encode(secret.getBytes(ENCODING));
			int pos = secret.length() - 1;
			while (secret.length() < 8) {
				secret += secret.charAt(pos--);
				if (pos < 0) {
					pos = secret.length() - 1;
				}
			}
			return (secret.substring(0, 8).getBytes(ENCODING));
		} catch (UnsupportedEncodingException e) {
			System.err.println("This should not happen");
			e.printStackTrace();
			return (null);
		}
	}

	private final static int KEY_SIZE = 8;
	private final static String ENCRYPTION = "DES";
	private final static String ENCODING = "UTF8";

	private static SecretKey getDesKey(String secret) {
		SecretKey key = new SecretKeySpec(secretTo8Bytes(secret), ENCRYPTION);
		byte[] secretBytes = new DesEncrypter(key).encryptToBytes(secret);
		byte[] keyBytes;
		if (secretBytes.length == 8) {
			keyBytes = secretBytes;
		} else {
			keyBytes = new byte[KEY_SIZE];
			for (int i = 0; i < KEY_SIZE; i++) {
				keyBytes[i] = secretBytes[i % secretBytes.length];
			}
		}
		return (new SecretKeySpec(keyBytes, ENCRYPTION));
	}
}
