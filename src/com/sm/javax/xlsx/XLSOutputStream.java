package com.sm.javax.xlsx;

import java.io.Closeable;
import java.io.IOException;
import java.io.PrintWriter;

import com.sm.javax.iox.StreamUtils;
import com.sm.javax.langx.Strings;

public class XLSOutputStream implements Closeable
{
	private final PrintWriter writer;
	private boolean row = false;
	private boolean cell = false;

	public XLSOutputStream (String title, PrintWriter writer) throws IOException
	{
		this.writer = writer;
		String text = StreamUtils.readStreamAsString (XLSOutputStream.class
				.getResourceAsStream ("xlshead.template"));
		if (title != null)
		{
			text.replaceAll ("XLS_SHEET_TITLE", title);
		}
		writer.println (text);
	}

	public final static String DATA_PLAIN = "data-plain";
	public final static String DATA_LONGTEXT = "data-longtext";
	public final static String DATA_CURRENCY = "data-currency";
	public final static String DATA_AMOUNT = "data-amount";
	public final static String DATA_PERCENT = "data-percent";
	public final static String DATA_DATE = "data-date";
	public final static String CONTEXT_HEADER = "context-header";
	
	public XLSOutputStream row ()
	{
		return (row (null));
	}
	
	private void printClass (String tagClass)
	{
		if (!Strings.isEmpty (tagClass))
		{
			writer.print (" class=");
			writer.print (Strings.quote (tagClass));
		}
	}
	
	public XLSOutputStream row (String rowClass)
	{
		if (cell)
		{
			writer.print ("</td>");
			cell = false;
		}
		writer.println();
		if (row)
		{
			writer.print ("</tr>");
		}
		row = true;
		writer.print ("<tr");
		printClass (rowClass);
		writer.println (">");
		
		return (this);
	}
	
	public XLSOutputStream cell (String cellClass, int colspan)
	{
		if (cell)
		{
			writer.print ("</td>");
		}
		writer.print ("<td");
		printClass (cellClass);
		if (colspan > 1)
		{
			writer.print (" colspan=");
			writer.print (Strings.quote (colspan));
		}
		writer.print (">");
		
		return (this);
	}
	
	public XLSOutputStream cell (int colspan)
	{
		return (cell (null, colspan));
	}
	
	public XLSOutputStream cell (String cellClass)
	{
		return (cell (cellClass, 1));
	}
	
	public XLSOutputStream cell ()
	{
		return (cell (null, 1));
	}
	
	public XLSOutputStream data (int value)
	{
		writer.print (value);
		return (this);
	}
	
	public XLSOutputStream data (double value)
	{
		writer.print (value);
		return (this);
	}
	
	public XLSOutputStream data (String value)
	{
		writer.print (value);
		return (this);
	}
	
	@Override
	public void close () throws IOException
	{
		writer.println ("</HTML>");
	}
}
