package com.sm.javax.langx;

public interface ObjectValueHolder extends Casters {
	public Object getValue();

	public default String getStringValue() throws ValueCastException {
		return (Casters.toString(getValue()));
	}

	public default String getStringrValueOr(String defaultValue)
			throws ValueCastException {
		return (Casters.toStringOr(getValue(), defaultValue));
	}

	public default Character getCharacterValue() throws ValueCastException {
		return (Casters.toCharacter(getValue()));
	}

	public default Character getCharacterValueOr(Character defaultValue)
			throws ValueCastException {
		return (Casters.toCharacterOr(getValue(), defaultValue));
	}

	public default Boolean getBooleanValue() throws ValueCastException {
		return (Casters.toBoolean(getValue()));
	}

	public default Boolean getBooleanValueOr(Boolean defaultValue)
			throws ValueCastException {
		return (Casters.toBooleanOr(getValue(), defaultValue));
	}

	public default Short getShortValue() throws ValueCastException {
		return (Casters.toShort(getValue()));
	}

	public default Short getShortValueOr(Short defaultValue)
			throws ValueCastException {
		return (Casters.toShortOr(getValue(), defaultValue));
	}

	public default Integer getIntegerValue() throws ValueCastException {
		return (Casters.toInteger(getValue()));
	}

	public default Integer getIntegerValueOr(Integer defaultValue)
			throws ValueCastException {
		return (Casters.toIntegerOr(getValue(), defaultValue));
	}

	public default Long getLongValue() throws ValueCastException {
		return (Casters.toLong(getValue()));
	}

	public default Long getLongValueOr(Long defaultValue)
			throws ValueCastException {
		return (Casters.toLongOr(getValue(), defaultValue));
	}

	public default Double getDoubleValue() throws ValueCastException {
		return (Casters.toDouble(getValue()));
	}

	public default Double getDoubleValueOr(Double defaultValue)
			throws ValueCastException {
		return (Casters.toDoubleOr(getValue(), defaultValue));
	}

	public default Float getFloatValue() throws ValueCastException {
		return (Casters.toFloat(getValue()));
	}

	public default Float getFloatValueOr(Float defaultValue)
			throws ValueCastException {
		return (Casters.toFloatOr(getValue(), defaultValue));
	}
}
