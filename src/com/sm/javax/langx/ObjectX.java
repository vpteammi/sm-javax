package com.sm.javax.langx;

public interface ObjectX {

	public default String toDebugString() {
		return (toString());
	}

	public default String getDebugTypeName() {
		return (getClass().getCanonicalName());
	}
}
