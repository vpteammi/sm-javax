package com.sm.javax.langx;

public interface UIIdentifiable {
	String getUIId();
}
