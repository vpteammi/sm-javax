package com.sm.javax.langx;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.sm.javax.utilx.Parsers;

public class Cast<CLS> {

	public final Class<CLS> targetClass;

	public Cast(Class<CLS> targetClass) {
		super();
		this.targetClass = targetClass;
	}

	public CLS apply(Object source) throws ValueCastException {
		try {
			return targetClass.cast(source);
		} catch (ClassCastException | NullPointerException e) {
			throw new ValueCastException(e);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}

	protected boolean isAssignable(Object source) {
		return (source == null || targetClass.isInstance(source));
	}

	public final static Cast<Object> Object = new Cast<Object>(Object.class) {
		@Override
		public java.lang.Object apply(Object source) throws ValueCastException {
			return (source);
		}
	};

	public final static Cast<String> String = new Cast<String>(String.class) {
		@Override
		public java.lang.String apply(Object source) throws ClassCastException {
			return (Strings.toString(source));
		}
	};

	public final static Cast<Character> Character = new Cast<Character>(
			Character.class) {
		@Override
		public java.lang.Character apply(Object source)
				throws ValueCastException {
			if (isAssignable(source))
				return (super.apply(source));
			try {
				return (Parsers.CHARACTER_PARSER
						.parse(Strings.toString(source)));
			} catch (ValueParseException e) {
				throw new ValueCastException(e);
			}
		}
	};

	public final static Cast<Boolean> Boolean = new Cast<Boolean>(Boolean.class) {
		@Override
		public java.lang.Boolean apply(Object source) throws ValueCastException {
			if (isAssignable(source))
				return (super.apply(source));
			try {
				return (Parsers.BOOLEAN_PARSER.parse(Strings.toString(source)));
			} catch (ValueParseException e) {
				throw new ValueCastException(e);
			}
		}
	};

	public final static Cast<Short> Short = new Cast<Short>(Short.class) {
		@Override
		public java.lang.Short apply(Object source) throws ValueCastException {
			if (isAssignable(source))
				return (super.apply(source));
			try {
				return (Parsers.SHORT_PARSER.parse(Strings.toString(source)));
			} catch (ValueParseException e) {
				throw new ValueCastException(e);
			}
		}
	};

	public final static Cast<Integer> Integer = new Cast<Integer>(Integer.class) {
		@Override
		public java.lang.Integer apply(Object source) throws ValueCastException {
			if (isAssignable(source))
				return (super.apply(source));
			try {
				return (Parsers.INTEGER_PARSER.parse(Strings.toString(source)));
			} catch (ValueParseException e) {
				throw new ValueCastException(e);
			}
		}
	};

	public final static Cast<Long> Long = new Cast<Long>(Long.class) {
		@Override
		public java.lang.Long apply(Object source) throws ValueCastException {
			if (isAssignable(source))
				return (super.apply(source));
			try {
				return (Parsers.LONG_PARSER.parse(Strings.toString(source)));
			} catch (ValueParseException e) {
				throw new ValueCastException(e);
			}
		}
	};

	public final static Cast<Double> Double = new Cast<Double>(Double.class) {
		@Override
		public java.lang.Double apply(Object source) throws ValueCastException {
			if (isAssignable(source))
				return (super.apply(source));
			try {
				return (Parsers.DOUBLE_PARSER.parse(Strings.toString(source)));
			} catch (ValueParseException e) {
				throw new ValueCastException(e);
			}
		}
	};

	public final static Cast<Float> Float = new Cast<Float>(Float.class) {
		@Override
		public java.lang.Float apply(Object source) throws ValueCastException {
			if (isAssignable(source))
				return (super.apply(source));
			try {
				return (Parsers.FLOAT_PARSER.parse(Strings.toString(source)));
			} catch (ValueParseException e) {
				throw new ValueCastException(e);
			}
		}
	};

	public final static Map<Class<?>, Cast<?>> STANDARD_CONVERTERS;

	static {
		HashMap<Class<?>, Cast<?>> converters = new HashMap<Class<?>, Cast<?>>();
		converters.put(String.targetClass, String);
		converters.put(Character.targetClass, Character);
		converters.put(Short.targetClass, Short);
		converters.put(Integer.targetClass, Integer);
		converters.put(Long.targetClass, Long);
		converters.put(Double.targetClass, Double);
		converters.put(Float.targetClass, Float);
		STANDARD_CONVERTERS = Collections.unmodifiableMap(converters);
	}
}
