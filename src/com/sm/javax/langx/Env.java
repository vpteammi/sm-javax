package com.sm.javax.langx;

import com.sm.javax.utilx.Parsers;

public class Env extends Vars {

	@Override
	public boolean has(java.lang.String name) {
		return (System.getenv(name) != null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(java.lang.String name) {
		return (T) Parsers.parseMainValue(System.getenv(name));
	}

}
