package com.sm.javax.langx;

public class Enums {

	public static <T extends Enum<T>> T valueByOrdinal (Class<T> enumType, int ordinal)
	{
		T[] values = enumType.getEnumConstants();
		if ((ordinal >= 0) && (ordinal < values.length))
		{
			return (values [ordinal]);
		}
		return (null);
	}

}
