package com.sm.javax.langx;

public interface Casters {
	public static <T> T to(Object value, Class<T> cls)
			throws ValueCastException {
		try {
			return (cls.cast(value));
		} catch (ClassCastException | NullPointerException e) {
			throw new ValueCastException(e);
		}
	}

	public static <T> T toOr(Object value, Class<T> cls, T defaultValue) {
		try {
			return (to(value, cls));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

	public static String toString(Object value) throws ValueCastException {
		return (Cast.String.apply(value));
	}

	public static String toStringOr(Object value, String defaultValue) {
		try {
			return (toString(value));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

	public static Boolean toBoolean(Object value) throws ValueCastException {
		return (Cast.Boolean.apply(value));
	}

	public static Boolean toBooleanOr(Object value, Boolean defaultValue) {
		try {
			return (Objects.noNull(toBoolean(value), defaultValue));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

	public static Character toCharacter(Object value) throws ValueCastException {
		return (Cast.Character.apply(value));
	}

	public static Character toCharacterOr(Object value, Character defaultValue) {
		try {
			return (Objects.noNull(toCharacter(value), defaultValue));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

	public static Short toShort(Object value) throws ValueCastException {
		return (Cast.Short.apply(value));
	}

	public static Short toShortOr(Object value, Short defaultValue) {
		try {
			return (Objects.noNull(toShort(value), defaultValue));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

	public static Integer toInteger(Object value) throws ValueCastException {
		return (Cast.Integer.apply(value));
	}

	public static Integer toIntegerOr(Object value, Integer defaultValue) {
		try {
			return (Objects.noNull(toInteger(value), defaultValue));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

	public static Long toLong(Object value) throws ValueCastException {
		return (Cast.Long.apply(value));
	}

	public static Long toLongOr(Object value, Long defaultValue) {
		try {
			return (Objects.noNull(toLong(value), defaultValue));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

	public static Double toDouble(Object value) throws ValueCastException {
		return (Cast.Double.apply(value));
	}

	public static Double toDoubleOr(Object value, Double defaultValue) {
		try {
			return (Objects.noNull(toDouble(value), defaultValue));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

	public static Float toFloat(Object value) throws ValueCastException {
		return (Cast.Float.apply(value));
	}

	public static Float toFloatOr(Object value, Float defaultValue) {
		try {
			return (Objects.noNull(toFloat(value), defaultValue));
		} catch (ValueCastException e) {
			return (defaultValue);
		}
	}

}
