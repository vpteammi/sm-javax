package com.sm.javax.langx;

import java.io.IOException;
import java.net.ServerSocket;

public class ProcessUUID {
	static private ServerSocket procSocket = null;
	static private int procId = -1;

	public static int getId() {
		if (procSocket == null) {
			try {
				procSocket = new ServerSocket(0);
				procId = procSocket.getLocalPort();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return (procId);
	}

	private static long idCount = 0;

	public static synchronized String generateSystemUniqueString() {
		return (String.format("%d.%x", getId(), idCount++));
	}
}
