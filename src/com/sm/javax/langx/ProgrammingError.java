package com.sm.javax.langx;

@SuppressWarnings("serial")
public class ProgrammingError extends Error {

	public ProgrammingError() {
	}

	public ProgrammingError(String message) {
		super(message);
	}

	public ProgrammingError(String format, Object...args) {
		this(String.format(format, args));
	}
	
	public ProgrammingError(Throwable cause) {
		super("This should not happen", cause);
	}

	public ProgrammingError(String message, Throwable cause) {
		super(message, cause);
	}

	public ProgrammingError(Throwable arg0, String format, Object...args) {
		this(String.format(format, args), arg0);
	}

	public ProgrammingError(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public static ProgrammingError shouldNotHappen (Throwable cause)
	{
		return new ProgrammingError("This should not happen", cause);
	}

	public static ProgrammingError shouldNotHappen (Throwable arg0, String format, Object...args)
	{
		return shouldNotHappen(arg0, String.format(format, args));
	}

	public static ProgrammingError shouldNotHappen (String cause)
	{
		return new ProgrammingError("This should not happen: " + cause);
	}

	public static ProgrammingError shouldNotHappen (String format, Object...args)
	{
		return shouldNotHappen(String.format(format, args));
	}
}
