package com.sm.javax.langx;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.PrintStream;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.sm.javax.swingx.UIComponentMover;
import com.sm.javax.swingx.UIGraphicsConfiguration;

public class Debug {
	public static void showToDo(String message) {
		JOptionPane.showMessageDialog(null, "Not implemented\n" + message,
				"TODO", JOptionPane.WARNING_MESSAGE);
	}

	private final static ImageIcon STOP_ICON;
	private final static ImageIcon ANCHOR_ICON;
	private final static ImageIcon TARGET_ICON;
	private final static ImageIcon HAND_ICON;

	static {
		STOP_ICON = new ImageIcon(Debug.class.getResource("images/stop.png"));
		ANCHOR_ICON = new ImageIcon(Debug.class.getResource("images/anchor.png"));
		TARGET_ICON = new ImageIcon(Debug.class.getResource("images/target.png"));
		HAND_ICON = new ImageIcon(Debug.class.getResource("images/hand.png"));
	}

	private static void setComponentTransparent(JComponent comp) {
		comp.setOpaque(true);
		comp.setBackground(TRANSPARENT);
	}

	private final static Color TRANSPARENT = new Color(0, 0, 0, 0);

	@SuppressWarnings("serial")
	public static Window showStopButton(String title, String tooltip) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// ignore
		}
		JFrame frame = new JFrame(title);
		frame.setIconImage(STOP_ICON.getImage());
		JButton stopButton = new JButton(new AbstractAction(null, STOP_ICON) {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		stopButton.setToolTipText(tooltip);
		JToolBar toolbar = new JToolBar();
		toolbar.add(stopButton);
		toolbar.setFloatable(false);
		JPanel panel = new JPanel(new BorderLayout());
		Color background = panel.getBackground();
		setComponentTransparent(panel);
		setComponentTransparent(toolbar);
		setComponentTransparent(stopButton);
		panel.add(toolbar, BorderLayout.CENTER);
		JComponent mover;
		if (!Strings.isEmpty(title)) {
			JLabel topbar = new JLabel("<html><b>" + title + "</b></html>",
					HAND_ICON, SwingConstants.CENTER);
			topbar.setBackground(background);
			panel.add(mover = topbar, BorderLayout.NORTH);
		} else {
			JLabel anchor = new JLabel(TARGET_ICON);
			panel.add(mover = anchor, BorderLayout.SOUTH);

		}
		frame.add(panel);
		frame.setUndecorated(true);
		frame.setBackground(TRANSPARENT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		new UIComponentMover(frame, mover);
		Rectangle rect = UIGraphicsConfiguration.getPhysicalScreenSize();
		int x = (int) rect.getMaxX() - frame.getWidth();
		int y = (int) rect.getMaxY() / 2;
		frame.setLocation(x, y);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		return (frame);
	}

	public static <T> PrintStream prettyPrint(PrintStream out, Stream<T> stream) {
		return (prettyPrint(out, stream, Strings::toString));
	}

	public static <T> PrintStream prettyPrint(PrintStream out,
			Stream<T> stream, Function<T, String> stringify) {
		return (prettyPrint(out, stream, stringify, "  "));
	}

	public static <T> PrintStream prettyPrint(PrintStream out,
			Stream<T> stream, String indent) {
		return (prettyPrint(out, stream, Strings::toString, indent, null, null));
	}

	public static <T> PrintStream prettyPrint(PrintStream out,
			Stream<T> stream, Function<T, String> stringify, String indent) {
		return (prettyPrint(out, stream, stringify, indent, null, null));
	}

	public static <T> PrintStream prettyPrint(PrintStream out,
			Stream<T> stream, String indent, String preLine, String postLine) {
		return (prettyPrint(out, stream, Strings::toString, indent, preLine,
				postLine));
	}

	public static <T> PrintStream prettyPrint(PrintStream out,
			Stream<T> stream, Function<T, String> stringify, String indent,
			String preLine, String postLine) {
		if (out != null) {
			indent = Strings.noNullOr(indent, "");
			if (preLine != null) {
				out.println(preLine);
			}
			if (stream != null) {
				stream.forEach(v -> out.println(stringify.apply(v)));
			}
			if (postLine != null) {
				out.println(postLine);
			}
		}
		return (out);
	}
}
