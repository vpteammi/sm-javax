package com.sm.javax.langx;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.sm.javax.utilx.ArrayUtils;
import com.sm.javax.utilx.CollectionUtils;
import com.sm.javax.utilx.Parsers;

public class Args extends Vars {

	final Map<String, Object> argMap;
	private final Object[] args;
	private final String command;

	private Args(Map<String, Object> argMap, Object[] args) {
		this(null, argMap, args);
	}

	private Args(String command, Map<String, Object> argMap, Object[] args) {
		super();
		this.argMap = argMap;
		this.args = args;
		this.command = command;
	}

	public String getCommand() {
		return command;
	}

	public Set<String> names() {
		return (argMap.keySet());
	}

	public int size() {
		return (args.length);
	}

	@Override
	public boolean has(String name) {
		return (argMap.containsKey(name));
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(String name) {
		return (T) (argMap.get(name));
	}

	@SuppressWarnings("unchecked")
	public <T> T get(int index) {
		return (T) (args[index]);
	}

	@SuppressWarnings("unchecked")
	public <T> T must(int index) throws MissingException {
		if ((index < 0) || (index >= args.length)) {
			throw new MissingException("Missing argument #" + index);
		}
		return (T) (args[index]);
	}

	public String getString(int index) {
		return (Strings.toString(get(index)));
	}

	public String mustString(int index) throws MissingException {
		return (Strings.toString(must(index)));
	}

	public String argsToString() {
		return (argsToString(" "));
	}

	private String argsToString(String sep) {
		return (Arrays.stream(args).map(v -> v.toString()).collect(Collectors
				.joining(sep)));
	}

	@SuppressWarnings("serial")
	private static void showDialog(String title, String label, String button,
			Runnable command) {
		JFrame frame = new JFrame(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel(new BorderLayout());
		if (!Strings.isEmpty(label)) {
			panel.add(new JLabel(label), BorderLayout.NORTH);
		}
		JPanel buttons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		buttons.add(new JButton(new AbstractAction(button) {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Thread (command).start();
			}
		}));
		buttons.add(new JButton(new AbstractAction("Close") {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		}));
		panel.add(buttons, BorderLayout.SOUTH);
		frame.add(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public void showCopyDialog(String title) {
		String args = argsToString("`");
		showDialog(title, args, "Copy",
				() -> Toolkit.getDefaultToolkit().getSystemClipboard()
						.setContents(new StringSelection(args), null));
	}

	public static void showPasteAndRunDialog(String title,
			Consumer<String[]> mainFunction) {
		showDialog(title, null, "Paste and Run", () -> {
			try {
				mainFunction.accept(splitCommandLine((String) Toolkit.getDefaultToolkit()
						.getSystemClipboard().getData(DataFlavor.stringFlavor), "`"));
			} catch (HeadlessException | UnsupportedFlavorException
					| IOException e) {
				e.printStackTrace();
			}
		});
	}

	public static String[] splitCommandLine(String argsLine) {
		return (splitCommandLine(argsLine, "(\\s)+"));
	}

	public static String[] splitCommandLine(String argsLine, String sepRegExpr) {
		return (Strings.split(Strings.trim(argsLine), sepRegExpr));
	}

	public static Args parseCommandLine(String commandLine) {
		String[] splitCommand = splitCommandLine(commandLine);
		return (parse(splitCommand[0],
				Arrays.asList(splitCommand).subList(1, splitCommand.length)));
	}

	public static Args parseArgs(String argsLine) {
		return (parse(null, splitCommandLine(argsLine)));
	}

	public static Args parse(String[] args) {
		return (parse(null, args));
	}

	public static Args parse(String command, String[] args) {
		return (parse(command, ArrayUtils.asList(args)));
	}

	public static Args parse(String command, String args) {
		return (parse(command, splitCommandLine(args)));
	}

	private final static Args EMPTY_ARGS = new Args(null,
			Collections.emptyMap(), Objects.EMPTY_ARRAY);

	public static Args create(String command, Map<String, Object> argMap,
			Object[] args) {
		argMap = CollectionUtils.noNull(argMap);
		args = ArrayUtils.noNull(args);
		if (Strings.isEmpty(command) && CollectionUtils.isEmpty(argMap)
				&& ArrayUtils.isEmpty(args)) {
			return (EMPTY_ARGS);
		}
		return (new Args(command, argMap, args));
	}

	public static Args parse(String command, Collection<String> args) {
		int size = CollectionUtils.size(args);
		if (size > 0) {
			HashMap<String, Object> argMap = new HashMap<String, Object>();
			Object[] argArray = new Object[size];
			int index = 0;
			for (String arg : args) {
				argArray[index++] = Parsers.parseMainValue(arg);
				if (!Strings.isEmpty(arg)) {
					int idx = arg.indexOf("=");
					if (idx > 0) {
						argMap.put(arg.substring(0, idx),
								Parsers.parseMainValue(arg.substring(idx + 1)));
					} else {
						argMap.put(arg, Boolean.TRUE);
					}
				}
			}
			return (create(command, argMap, argArray));
		} else {
			return (create(command, null, null));
		}
	}
}
