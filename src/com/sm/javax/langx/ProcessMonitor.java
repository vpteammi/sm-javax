package com.sm.javax.langx;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ProcessMonitor {

	private final ScheduledExecutorService executor;
	private long checkIntervalMSecs = 3000;

	public ProcessMonitor(ScheduledExecutorService executor) {
		this.executor = executor;
	}

	public ProcessMonitor checkInterval(long interval, TimeUnit unit) {
		return (checkInterval(unit.toMillis(interval)));
	}

	public ProcessMonitor checkInterval(long intervalInMillis) {
		checkIntervalMSecs = intervalInMillis;
		return (this);
	}

	public interface ProcessOwner {
		void processTerminated(Process process);
	}

	private final HashMap<Process, ProcessOwner> activeProcesses = new HashMap<Process, ProcessOwner>();
	private boolean running = false;
	private ScheduledFuture<?> nextCheck;

	public synchronized void addProcess(ProcessOwner owner, Process process) {
		activeProcesses.put(process, owner);
		reschedule();
	}

	public synchronized void removeProcess(Process process) {
		activeProcesses.remove(process);
		reschedule();
	}

	public synchronized void shutdown() {
		activeProcesses.keySet().stream().collect(Collectors.toList())
				.forEach(process -> activeProcesses.remove(process));
		reschedule();
	}

	private synchronized void checkProcesses() {
		List<Entry<Process, ProcessOwner>> terminated = activeProcesses
				.entrySet().stream().filter(entry -> !entry.getKey().isAlive())
				.collect(Collectors.toList());
		terminated.forEach(entry -> activeProcesses.remove(entry.getKey()));
		executor.execute(() -> notify(terminated));
		reschedule();
	}

	private static void notify(List<Entry<Process, ProcessOwner>> terminated) {
		terminated.parallelStream().forEach(
				entry -> entry.getValue().processTerminated(entry.getKey()));
	}

	private synchronized void reschedule() {
		int size = activeProcesses.size();
		if (!running) {
			if (size > 0) {
				nextCheck = executor.scheduleAtFixedRate(this::checkProcesses,
						checkIntervalMSecs, checkIntervalMSecs,
						TimeUnit.MILLISECONDS);
			}
		} else if ((nextCheck != null) && (size == 0)) {
			nextCheck.cancel(false);
		}
		running = size > 0;
	}
}
