package com.sm.javax.langx;

public abstract class Vars {

	@SuppressWarnings("serial")
	public static class MissingException extends Exception {
		protected MissingException(String message) {
			super(message);
		}
	}

	public static class Id<T> {
		public final String name;
		private final Cast<T> converter;

		public Id(String name, Cast<T> converter) {
			super();
			this.name = name;
			this.converter = converter;
		}
	}

	public static Id<String> String(String name) {
		return (new Id<String>(name, Cast.String));
	}

	public static Id<Character> Character(String name) {
		return (new Id<Character>(name, Cast.Character));
	}

	public static Id<Boolean> Boolean(String name) {
		return (new Id<Boolean>(name, Cast.Boolean));
	}

	public static Id<Short> Short(String name) {
		return (new Id<Short>(name, Cast.Short));
	}

	public static Id<Integer> Integer(String name) {
		return (new Id<Integer>(name, Cast.Integer));
	}

	public static Id<Long> Long(String name) {
		return (new Id<Long>(name, Cast.Long));
	}

	public static Id<Double> Double(String name) {
		return (new Id<Double>(name, Cast.Double));
	}

	public static Id<Float> Float(String name) {
		return (new Id<Float>(name, Cast.Float));
	}

	public Vars() {
		super();
	}

	public abstract boolean has(String name);

	public boolean has(Id<?> id) {
		return (has(id.name));
	}

	@SuppressWarnings("unchecked")
	private <T, U> T checkMust(U value, String name) throws MissingException {
		if (value == null) {
			throw new MissingException("Missing argument " + name);
		}
		return (T) (value);
	}

	public <T> T must(String name) throws MissingException {
		return checkMust(get(name), name);
	}

	public abstract <T> T get(String name);

	public String getString(String name) {
		return (Strings.toString(get(name)));
	}

	public String mustString(String name) throws MissingException {
		return (Strings.toString(must(name)));
	}

	public <T> T must(Id<T> id) throws ValueCastException, MissingException {
		return (checkMust(get(id), id.name));
	}

	public <T> T get(Id<T> id) throws ValueCastException {
		return (id.converter.apply(get(id.name)));
	}

}