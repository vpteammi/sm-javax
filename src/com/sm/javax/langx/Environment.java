package com.sm.javax.langx;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.sm.javax.utilx.CollectionUtils;

public class Environment {
	public final static String JAVA_HOME_DIRECTORY_PROPERTY = "java.home";

	public static File getJavaHomeDirectory() {
		return (new File(System.getProperty(JAVA_HOME_DIRECTORY_PROPERTY)));
	}

	private static String JAVA_EXECUTABLE = null;

	public static String getJavaExecutable() {
		if (JAVA_EXECUTABLE == null) {
			File executable = new File(new File(getJavaHomeDirectory(), "bin"),
					"java");
			JAVA_EXECUTABLE = executable.getAbsolutePath().toString();
		}
		return (JAVA_EXECUTABLE);
	}

	public static String getJavaClassPath() {
		return (ManagementFactory.getRuntimeMXBean().getClassPath());
	}

	public static List<String> getJavaInputArguments() {
		return (ManagementFactory.getRuntimeMXBean().getInputArguments());
	}

	public static List<String> splitPath(String path) {
		return (Strings.isEmpty(path) ? Collections.emptyList() : Arrays
				.asList(path.split(File.pathSeparator)));
	}

	public static String toPath(Collection<String> dirs) {
		return (CollectionUtils.isEmpty(dirs) ? "" : dirs.stream().collect(
				Collectors.joining(File.pathSeparator)));
	}

	private final static Pattern PROC_NAME_PATTERN = Pattern.compile("(\\d+)@");

	public static long getOsProcessId() {
		Matcher procIdMatcher = PROC_NAME_PATTERN.matcher(ManagementFactory
				.getRuntimeMXBean().getName());
		return procIdMatcher.find() ? Integer.parseInt(procIdMatcher.group(1))
				: -1;
	}

	public final static String USER_NAME_PROPERTY = "user.name";

	public static String getUserName() {
		return (System.getProperty(USER_NAME_PROPERTY));
	}

	public final static String CURRENT_DIRECTORY_PROPERTY = "user.dir";

	public static File getCurrentDirectory() {
		return (new File(System.getProperty(CURRENT_DIRECTORY_PROPERTY)));
	}

	public final static String HOME_DIRECTORY_PROPERTY = "user.home";

	public static File getHomeDirectory() {
		return (new File(System.getProperty(HOME_DIRECTORY_PROPERTY)));
	}

	public final static String TEMPORARY_DIRECTORY_PROPERTY = "java.io.tmpdir";

	public static File getTempDirectory() {
		return (new File(System.getProperty(TEMPORARY_DIRECTORY_PROPERTY)));
	}

	public static File getTempDirectory(String subdir) {
		return (new File(getTempDirectory(), subdir));
	}

	public final static String JAVA_LIBRARY_PATH_PROPERTY = "java.library.path";

	public static String getJavaLibraryPath() {
		return (System.getProperty(JAVA_LIBRARY_PATH_PROPERTY));
	}

	public static void addToJavaLibraryPath(String libraryPath) {
		setJavaLibraryPath(libraryPath + ";" + getJavaLibraryPath());
	}

	public static void setJavaLibraryPath(String javaLibraryPath) {
		System.setProperty(JAVA_LIBRARY_PATH_PROPERTY, javaLibraryPath);

		Field fieldSysPath;
		try {
			fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
			fieldSysPath.setAccessible(true);
			fieldSysPath.set(null, null);
		} catch (NoSuchFieldException | SecurityException
				| IllegalArgumentException | IllegalAccessException e1) {
			throw new Error("Failed to set java.library.path", e1);
		}
	}

	public final static String OS_NAME_PROPERTY = "os.name";

	public static String getOSName() {
		return (System.getProperty(OS_NAME_PROPERTY));
	}

	public final static String WINDOWS_PLATFORM = "windows";

	/**
	 * Returns the canonical name of the platform. This value is derived from
	 * the System property os.name.
	 * 
	 * @return The platform string.
	 */
	public static String getPlatform() {
		// See list of os names at: http://lopica.sourceforge.net/os.html
		// or at: http://www.tolstoy.com/samizdat/sysprops.html
		String osname = getOSName();
		if (osname.startsWith("Windows")) {
			return WINDOWS_PLATFORM;
		}
		return canonical(osname);
	}

	/**
	 * Returns the default DLL extension for platform.
	 * 
	 * @return The default DLL extension.
	 */
	public static String getPlatformDLLext() {
		if (getPlatform().equals(WINDOWS_PLATFORM)) {
			return ".dll";
		}
		return ".so";
	}

	public final static String OS_ARCHITECTURE_PROPERTY = "os.arch";

	public static String getOSArchitecture() {
		return (System.getProperty(OS_ARCHITECTURE_PROPERTY));
	}

	public final static String AMD64_ARCHITECTURE = "amd64";
	public final static String X86_ARCHITECTURE = "x86";

	/**
	 * Return the name of the architecture. This value is determined by the
	 * System property os.arch.
	 * 
	 * @return The architecture string.
	 */
	public static String getArchitecture() {
		String arch = getOSArchitecture();
		if (arch.endsWith("86")) {
			return X86_ARCHITECTURE;
		}
		return canonical(arch);
	}

	public static int getAvailableProcessors() {
		return (Runtime.getRuntime().availableProcessors());
	}

	public static int getActiveThreadsCount() {
		return (ManagementFactory.getThreadMXBean().getThreadCount());
	}

	public static long getMaxMemory() {
		return (getMaxJavaMemory());
	}

	public static long getMaxJavaMemory() {
		return (Runtime.getRuntime().maxMemory());
	}

	public static long getFreeJavaMemory() {
		return (Runtime.getRuntime().freeMemory());
	}

	public static long getTotalJavaMemory() {
		return (Runtime.getRuntime().totalMemory());
	}

	/**
	 * @param value
	 *            The value to be canonicalized.
	 * @return The value with all '/', '\' and ' ' replaced with '_', and all
	 *         uppercase characters replaced with lower case equivalents.
	 */
	private static String canonical(String value) {
		return value.toLowerCase().replaceAll("[\\\\/ ]", "_");
	}

	private static Boolean isWindowsOS = null;

	public static boolean isWindowsOS() {
		if (isWindowsOS == null) {
			isWindowsOS = getPlatform().equals(WINDOWS_PLATFORM);
		}
		return (isWindowsOS);
	}
}
