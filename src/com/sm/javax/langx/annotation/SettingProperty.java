package com.sm.javax.langx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention (RetentionPolicy.RUNTIME)
@Target (ElementType.FIELD)
public @interface SettingProperty {
	String defaultValue ();
	public String documentation () default "";
	public String envVariableName () default "";
	public String cmdLineName () default "";
	public boolean resolveEnvironmentVarName () default true;
	public String listDelimiter () default "\\s*,\\s*";
	public boolean distinct () default false;
}
