package com.sm.javax.langx.annotation;

import com.sm.javax.langx.Strings;

public class Annotations
{
	public static final String OMITTED_STRING_VALUE = "$OMITTED$";
	
	public static boolean isOmitted (String value)
	{
		return (OMITTED_STRING_VALUE.equals (value));
	}
	public static boolean isOmittedOrEmpty (String value)
	{
		return (Strings.isEmpty(value) || isOmitted (value));
	}
}
