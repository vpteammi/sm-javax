package com.sm.javax.langx;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.sm.javax.utilx.CollectionUtils;

public class JavaCommand {

	private final String executable;
	private final ArrayList<String> jvmArgs = new ArrayList<String>();
	private final ArrayList<String> classPath = new ArrayList<String>();
	private String mainClass;
	private String jarFile;
	private final ArrayList<String> args = new ArrayList<String>();

	public JavaCommand(String executable) {
		this.executable = executable;
	}

	public JavaCommand(File executable) {
		this(executable.getAbsolutePath());
	}

	public JavaCommand() {
		this(Environment.getJavaExecutable());
	}

	public ProcessBuilder build() {
		ArrayList<String> command = new ArrayList<String>();
		command.add(executable);
		if (!CollectionUtils.isEmpty(jvmArgs)) {
			command.addAll(jvmArgs);
		}
		if (!CollectionUtils.isEmpty(classPath)) {
			command.add("-cp");
			command.add(Environment.toPath(classPath));
		}
		if (!Strings.isEmpty(jarFile)) {
			command.add("-jar");
			command.add(jarFile);
		}
		if (!Strings.isEmpty(mainClass)) {
			command.add(mainClass);
		}
		if (!CollectionUtils.isEmpty(args)) {
			command.addAll(args);
		}
		return (new ProcessBuilder(command));
	}

	public JavaCommand mainClass(String mainClass) {
		this.mainClass = mainClass;
		return (this);
	}

	public JavaCommand mainClass(Class<?> mainClass) {
		return (mainClass(mainClass.getName()));
	}

	public JavaCommand jar(String jar) {
		this.jarFile = jar;
		return (this);
	}

	public JavaCommand jar(File jar) {
		return (jar(jar.getAbsolutePath()));
	}

	public JavaCommand inheritClassPath() {
		return (classPath(Environment.splitPath(Environment.getJavaClassPath())));
	}

	public JavaCommand classPath(Collection<String> paths) {
		classPath.addAll(paths);
		return (this);
	}

	public JavaCommand classPath(String path) {
		return (classPath(Environment.splitPath(path)));
	}

	public JavaCommand classPath(String... paths) {
		return (classPath(Arrays.asList(paths)));
	}

	public JavaCommand inheritJvmArgs() {
		return (jvmArgs(Environment.getJavaInputArguments()));
	}

	public JavaCommand jvmArg(String arg) {
		jvmArgs.add(arg);
		return (this);
	}

	public JavaCommand jvmArgs(Collection<String> args) {
		jvmArgs.addAll(args);
		return (this);
	}

	public JavaCommand jvmArgs(String... args) {
		return (jvmArgs(Arrays.asList(args)));
	}

	public JavaCommand arg(String arg) {
		args.add(arg);
		return (this);
	}

	public JavaCommand args(Collection<String> args) {
		this.args.addAll(args);
		return (this);
	}

	public JavaCommand args(String... args) {
		return (args(Arrays.asList(args)));
	}
}
