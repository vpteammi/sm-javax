package com.sm.javax.langx;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.function.Supplier;

import com.sm.javax.langx.reflect.Classes;
import com.sm.javax.utilx.TimeUtils;

public class Objects {

	public final static Object[] EMPTY_ARRAY = new Objects[0];

	public static <T, D extends T> T noNull(T obj, D defaultObj) {
		return (obj == null) ? defaultObj : obj;
	}

	public static boolean isInstanceOfAny(Object obj, Class<?>... classes) {
		if (obj != null) {
			for (Class<?> cls : classes) {
				if (cls.isInstance(obj))
					return (true);
			}
		}

		return (false);
	}

	public static boolean isInstanceOfSameClass(Object obj, Object obj2) {
		if (obj == obj2)
			return (true);

		if ((obj == null) || (obj2 == null))
			return (false);

		return (obj.getClass() == obj2.getClass());
	}

	public static boolean equals(Object obj, Object obj2) {
		return ((obj == obj2) || (obj != null) && obj.equals(obj2));
	}

	public static boolean equalsToAny(Object obj, Object... objects) {
		for (Object o : objects) {
			if (equals(obj, o))
				return (true);
		}

		return (false);
	}

	@FunctionalInterface
	public interface Convert<T, V> {
		public V to(T value);
	}

	@SuppressWarnings("unchecked")
	public static <T, U extends T, V> V[] toArray(Collection<U> collection,
			Convert<T, V> convert, V[] array) {
		if (array == null) {
			throw new IllegalArgumentException("null array");
		}

		int size = (collection == null) ? 0 : collection.size();

		if (array.length != size) {
			array = (V[]) Array.newInstance(
					array.getClass().getComponentType(), size);
		}

		if (size > 0) {
			int i = 0;
			for (T obj : collection) {
				array[i++] = convert.to(obj);
			}
		}

		return (array);
	}

	public static boolean isBoolean(Object obj) {
		return ((obj != null) && Classes.isBoolean(obj.getClass()));
	}

	public static void wait(Object target, Supplier<Boolean> condition,
			long timeoutMillseconds) throws InterruptedException 
	{
		long startTime =  TimeUtils.getNow().getTime();
		long endTime = startTime + timeoutMillseconds;
		while (!condition.get()) {
			target.wait(timeoutMillseconds);
			long now = TimeUtils.getNow().getTime();
			timeoutMillseconds = (endTime - now);
			if (timeoutMillseconds <= 0) {
				throw new InterruptedException("Timeout expired (waited from " + startTime + " till " + now + ")");
			}
		}
	}
}
