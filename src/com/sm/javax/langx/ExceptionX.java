package com.sm.javax.langx;

public class ExceptionX extends Exception {

	private static final long serialVersionUID = 1L;

	public ExceptionX() {
	}

	public ExceptionX(String message) {
		super(message);
	}

	public ExceptionX(String format, Object... args) {
		this(String.format(format, args));
	}

	public ExceptionX(Throwable cause) {
		super(cause);
	}

	public ExceptionX(Throwable cause, String format, Object... args) {
		this(String.format(format, args), cause);
	}

	public ExceptionX(String message, Throwable cause) {
		super(message, cause);
	}

	public ExceptionX(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public String getLocalizedMessage(String reason) {
		return (getLocalizedMessage(reason, this));
	}

	public static String getLocalizedMessage(String reason, Throwable e) {
		return (Strings.concat(reason, ": ",
				(e == null) ? null : e.getLocalizedMessage()));
	}
}
