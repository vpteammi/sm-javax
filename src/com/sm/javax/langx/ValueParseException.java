package com.sm.javax.langx;

@SuppressWarnings("serial")
public class ValueParseException extends ExceptionX {

	public ValueParseException() {
	}

	public ValueParseException(String message) {
		super(message);
	}

	public ValueParseException(Throwable cause) {
		super(cause);
	}

	public ValueParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public ValueParseException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ValueParseException(String format, Object... args) {
		super(format, args);
	}

	public ValueParseException(Throwable cause, String format, Object... args) {
		super(cause, format, args);
	}
}
