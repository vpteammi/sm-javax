package com.sm.javax.langx;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.sm.javax.utilx.ArrayUtils;

public class Strings {
	public final static String[] EMPTY_ARRAY = new String[0];

	public static final String[] noNull(String[] array) {
		return (array == null ? EMPTY_ARRAY : array);
	}

	public static String toString(Object obj) {
		return (obj == null ? null : obj.toString());
	}

	public static String toDebugString(Object obj) {
		return (obj == null ? "null" : obj.getClass().getName() + "{"
				+ obj.toString() + "}");
	}

	public static String toStringOrNull(Object obj) {
		return (obj == null ? "null" : obj.toString());
	}

	public static boolean isEmpty(String str) {
		return ((str == null) || (str.length() == 0));
	}

	public static final String noNull(Object s) {
		return (s == null ? "" : s.toString());
	}

	public static final String noNullOr(Object s, Object defaultString) {
		return (s == null ? toStringOrNull(defaultString) : s.toString());
	}

	public static final String noEmptyOr(Object s, Object defaultString) {
		String str = noNull(s);
		return (isEmpty(str) ? toStringOrNull(defaultString) : str);
	}

	public static final String noNull(Object... objects) {
		for (Object o : objects) {
			if (o != null) {
				return (o.toString());
			}
		}

		return ("");
	}

	public static final boolean equal(String s1, String s2) {
		return ((s1 == s2) || ((s1 != null) && s1.equals(s2)));
	}

	public static final boolean equalIgnoreCase(String s1, String s2) {
		return ((s1 == s2) || ((s1 != null) && s1.equalsIgnoreCase(s2)));
	}

	public static final String trim(String str) {
		return (str == null ? null : str.trim());
	}

	public static final String[] split(String str, String regExpr) {
		return (str == null ? null : str.split(regExpr));
	}

	public static final StringBuilder append(StringBuilder builder,
			String... strings) {
		if (builder == null) {
			builder = new StringBuilder();
		}

		for (String s : strings) {
			builder.append(s);
		}

		return (builder);
	}

	private static <T, V extends T> void append(StringBuilder builder, V item,
			String sep, Function<T, ?> transform) {
		if (item != null) {
			Object value = transform.apply(item);
			if (value != null) {
				if (!isEmpty(sep) && builder.length() > 0)
					builder.append(sep);
				builder.append(value.toString());
			}
		}
	}

	public static final <T> StringBuilder concat(StringBuilder builder,
			Iterable<? extends T> collection, String sep,
			Function<T, ?> transform) {
		if (builder == null)
			builder = new StringBuilder();

		if (collection != null) {
			for (T item : collection) {
				append(builder, item, sep, transform);
			}
		}
		return (builder);
	}

	public static final <T> StringBuilder concat(StringBuilder builder,
			Collection<? extends T> collection, Function<T, ?> transform) {
		return (concat(builder, collection, null, transform));
	}

	public static final <T> StringBuilder concat(
			Collection<? extends T> collection, Function<T, ?> transform) {
		return (concat(null, collection, null, transform));
	}

	public static final <T> StringBuilder concat(StringBuilder builder,
			Collection<? extends T> collection, String sep,
			Function<T, ?> transform) {
		if (builder == null)
			builder = new StringBuilder();

		if (collection != null) {
			for (T item : collection) {
				append(builder, item, sep, transform);
			}
		}
		return (builder);
	}

	public static final <T, V extends T> StringBuilder concat(
			StringBuilder builder, V[] array, Function<T, ?> transform) {
		return (concat(builder, array, null, transform));
	}

	public static final <T, V extends T> StringBuilder concat(V[] array,
			Function<T, ?> transform) {
		return (concat(null, array, null, transform));
	}

	public static final <T, V extends T> StringBuilder concat(
			StringBuilder builder, V[] array, String sep,
			Function<T, ?> transform) {
		if (builder == null)
			builder = new StringBuilder();

		if (array != null) {
			for (T item : array) {
				append(builder, item, sep, transform);
			}
		}
		return (builder);
	}

	public static final StringBuilder concat(StringBuilder builder, String sep,
			String str) {
		if (builder == null) {
			return (isEmpty(str) ? null : new StringBuilder(str));
		}
		if (!isEmpty(str)) {
			if (!isEmpty(sep) && (builder.length() > 0))
				builder.append(sep);
			builder.append(str);
		}

		return (builder);
	}

	public static final StringBuilder concat(StringBuilder leftBuilder,
			String sep, StringBuilder rightBuilder) {
		if (leftBuilder == null) {
			return (rightBuilder);
		}

		if (rightBuilder != null) {
			if (!isEmpty(sep)) {
				leftBuilder.append(sep);
			}

			leftBuilder.append(rightBuilder);
		}

		return (leftBuilder);
	}

	public static final StringBuilder concat(String str, String sep,
			StringBuilder builder) {
		if (builder == null) {
			return (isEmpty(str) ? null : new StringBuilder(str));
		}
		if (!isEmpty(str)) {
			if (!isEmpty(sep))
				builder.insert(0, sep);
			builder.insert(0, str);
		}

		return (builder);
	}

	public static final String concat(String s1, String sep, String s2) {
		if (isEmpty(s1))
			return (s2);
		if (isEmpty(s2))
			return (s1);

		return (String.join(sep, s1, s2));
	}

	public static final <T> String concat(Iterable<T> collection, String sep) {
		if (collection == null)
			return null;

		StringBuilder builder = new StringBuilder();
		String csep = "";

		for (T obj : collection) {
			builder.append(csep).append(obj);
			csep = sep;
		}

		return (builder.toString());
	}

	public static final <T> String concat(T[] array, String sep) {
		return (concat(array, sep, 0));
	}

	public static final <T> String concat(T[] array, String sep, int fromIncl) {
		if (array == null)
			return null;

		return (concat(array, sep, 0, array.length));
	}

	public static final <T> String concat(T[] array, String sep, int fromIncl,
			int toExcl) {
		if (array == null)
			return null;

		StringBuilder builder = new StringBuilder();
		String csep = "";

		if ((fromIncl < 0) || (toExcl > array.length)) {
			throw new IndexOutOfBoundsException();
		}

		for (int i = fromIncl; i < toExcl; i++) {
			builder.append(csep).append(array[i]);
			csep = sep;
		}

		return (builder.toString());
	}

	public static String separateWords(String source, String wordSep,
			String wordStartRegexpr) {
		Pattern pattern = Pattern.compile(wordStartRegexpr);
		return (Arrays
				.stream(source.split(wordSep))
				.map(word -> {
					Matcher matcher = pattern.matcher(word);
					if (matcher.find()) {
						StringBuffer builder = new StringBuffer();
						while (matcher.find()) {
							String replacement = (matcher.start() == 0) ? matcher
									.group() : (wordSep + matcher.group());
							matcher.appendReplacement(builder, replacement);
						}
						matcher.appendTail(builder);
						return (builder.toString());
					} else {
						return (word);
					}
				}).collect(Collectors.joining(wordSep)));
	}

	public static final <T> String quote(T value) {
		return ("\"" + value.toString() + "\"");
	}

	public static String substring(String str, int beginIndex, int length,
			boolean cutWithEllipsis) {
		if (str != null) {
			int strlen = str.length();
			if (beginIndex < strlen) {
				int endIndex = beginIndex + length;
				if (endIndex < strlen) {
					String cutStr = str.substring(beginIndex, endIndex);
					return (cutWithEllipsis ? (cutStr + "...") : cutStr);
				} else {
					return (str.substring(beginIndex, strlen));
				}
			}

			return ("");
		}

		return (null);
	}

	public static String substring(String str, int beginIndex, int length) {
		return (substring(str, beginIndex, length, false));
	}

	public static String toFirstCharUpperCase(String str) {
		if (isEmpty(str))
			return (str);
		if (str.length() > 1) {
			return (str.substring(0, 1).toUpperCase() + str.substring(1));
		} else {
			return (str.toUpperCase());
		}
	}

	public static String removeWhitespaces(String str) {
		return (replaceWhitespaces(str, ""));
	}

	public static String replaceWhitespaces(String str, String replacement) {
		return (str == null) ? str : str.replaceAll("\\s+", replacement);
	}

	public static String toFirstCharLowerCase(String str) {
		if (isEmpty(str))
			return (str);
		if (str.length() > 1) {
			return (str.substring(0, 1).toLowerCase() + str.substring(1));
		} else {
			return (str.toLowerCase());
		}
	}

	public static boolean parseBoolean(String strRepr, boolean defaultValue) {
		return (isEmpty(strRepr) ? defaultValue : Boolean.parseBoolean(strRepr));
	}

	public static int parseInteger(String strRepr, int defaultValue) {
		return (isEmpty(strRepr) ? defaultValue : Integer.parseInt(strRepr));
	}

	public static double parseDouble(String strRepr, double defaultValue) {
		return (isEmpty(strRepr) ? defaultValue : Double.parseDouble(strRepr));
	}

	public static final String[] parseList(String list) {
		return (parseList(list, "[({,.:})]"));
	}

	public static final String[] parseList(String list, String separators) {
		if (list == null)
			return (null);

		StringTokenizer tokens = new StringTokenizer(list, separators);
		HashSet<String> set = new HashSet<String>();
		while (tokens.hasMoreTokens()) {
			String token = tokens.nextToken();
			set.add(token);
		}
		String[] array = new String[set.size()];
		return ((String[]) set.toArray(array));
	}

	public interface Convert<T> extends Objects.Convert<T, String> {
	}

	public final static Convert<Object> TO_STRING = new Convert<Object>() {
		@Override
		public String to(Object value) {
			return Strings.toString(value);
		}
	};

	public static <T> String[] toArray(Collection<T> collection) {
		return (toArray(collection, TO_STRING));
	}

	public static <T, U extends T> String[] toArray(Collection<U> collection,
			Convert<T> convert) {
		return (Objects.toArray(collection, convert, EMPTY_ARRAY));
	}

	public static String commonPrefix(String s1, String s2) {
		if (!isEmpty(s1) && !isEmpty(s2)) {
			int l1 = s1.length();
			int l2 = s2.length();
			if (l1 > l2) {
				String st = s1;
				s1 = s2;
				s2 = st;
			}
			int size = Math.min(l1, l2);
			for (int i = 1; i < size; i++) {
				if (s1.charAt(i) != s2.charAt(i))
					return s1.substring(0, i);
			}

			return (s1);
		}

		return "";
	}

	public static String commonPrefix(String[] strings) {
		if (ArrayUtils.isEmpty(strings)) {
			return "";
		}

		int size = strings.length;
		String prefix = strings[0];

		for (int i = 1; !isEmpty(prefix) && (i < size); i++) {
			prefix = commonPrefix(prefix, strings[i]);
		}

		return (prefix);
	}

	public static String cutTails(String s, String tail) {
		if (!isEmpty(s) && !isEmpty(s)) {
			int tlength = tail.length();
			while (!isEmpty(s) && s.endsWith(tail)) {
				s = s.substring(0, s.length() - tlength);
			}
		}

		return (s);
	}
	
	public final static Function<Object, String> TO_STRING_FUNCTION = new Function<Object, String>() {
		@Override
		public String apply(Object value) {
			return value.toString();
		}
	};

	public final static Function<Object, String> TO_QUOTED_STRING_FUNCTION = new Function<Object, String>() {
		@Override
		public String apply(Object value) {
			return Strings.quote(value);
		}
	};

	public static final String NULL_STRING = null;

	public static final String NewLine = "\n";

	// -----------------------------------------------------------
	public static boolean isQuoted (String str, String quoteSymbol)
	{
		if (str != null) {
			return (str.startsWith(quoteSymbol) && str.endsWith(quoteSymbol) && str.length() > 1);
		}
		return false;
	}
	
	public static boolean isQuoted (String str)
	{
		return isQuoted (str, "\"");
	}
	
	
	public static String unQuote (String str, boolean verifyFirst)
	{
		if (!verifyFirst || isQuoted (str)) {
			str = str.substring (1, str.length () - 1);
		}
		return str;
	}
	
	public static String unQuote (String str)
	{
		return unQuote (str, true);
	}
	// -----------------------------------------------------------
}
