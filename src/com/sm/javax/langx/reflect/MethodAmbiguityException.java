package com.sm.javax.langx.reflect;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.sm.javax.utilx.Tuple2;

@SuppressWarnings ("serial")
public class MethodAmbiguityException extends ReflectionException
{
	@SuppressWarnings ("rawtypes")
	private final List <Tuple2<MethodSignature, Method>> methods;
	
	@SuppressWarnings ("rawtypes")
	public MethodAmbiguityException (String message, List<Tuple2<MethodSignature,Method>> method)
	{
		super (message);
		this.methods = method;
	}
	
	@SuppressWarnings ("rawtypes")
	@Override
	public void printStackTrace (PrintStream stream)
	{
		stream.println ("Ambiguous method matching");
		for (Tuple2<MethodSignature, Method> method : methods)
		{
			stream.println ("Pattern: " + method._1 + ", method: " + method._2);
		}
		super.printStackTrace (stream);
	}

	@SuppressWarnings ("rawtypes")
	@Override
	public void printStackTrace (PrintWriter stream)
	{
		stream.println ("Ambiguous method matching");
		for (Tuple2<MethodSignature, Method> method : methods)
		{
			stream.println ("Pattern: " + method._1 + ", method: " + method._2);
		}
		super.printStackTrace (stream);
	}

	@SuppressWarnings ("rawtypes")
	public static <T extends MethodSignature> List <Tuple2<MethodSignature, Method>> toArray (List <Tuple2<T, Method>> list)
	{
		ArrayList<Tuple2<MethodSignature, Method>> array = new ArrayList <Tuple2<MethodSignature, Method>> ();
		for (Tuple2<T, Method> item : list)
		{
			array.add (new Tuple2<MethodSignature, Method> (item._1, item._2));
		}
		return (array);
	}	
}
