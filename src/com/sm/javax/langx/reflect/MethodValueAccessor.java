package com.sm.javax.langx.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodValueAccessor<T> extends ValueAccessor<T>
{
	public MethodValueAccessor(String accessSpec, Method valueGetter,
			MethodVariant<T> methodVariant)
	{
		super(accessSpec);
		this.methodVariant = methodVariant;
		this.valueGetter = valueGetter;
	}

	private final MethodVariant<T> methodVariant;
	private final Method valueGetter;

	@SuppressWarnings("unchecked")
	@Override
	public T getValue(Object obj, Object...args)
	{
			try
			{
				return (T) (invoke(obj, methodVariant.getArgs (args)));
			}
			catch (ReflectionException e)
			{
				reportAccessException(obj, e);
			}
			
			return (null);
	}

	public Object invoke(Object obj, Object[] args)
	{
		try
		{
			return (valueGetter.invoke(obj, args));
		}
		catch (IllegalArgumentException e)
		{
			reportAccessException(obj, e);
		}
		catch (IllegalAccessException e)
		{
			reportAccessException(obj, e);
		}
		catch (InvocationTargetException e)
		{
			reportAccessException(obj, e);
		}
		return (null);
	}

	@Override
	public Class<?> getType()
	{
		return (Class<?>) (valueGetter.getReturnType());
	}

	@Override
	public String toString ()
	{
		return (super.toString() + "=" + valueGetter.toString());
	}
}
