package com.sm.javax.langx.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.swing.JOptionPane;

@SuppressWarnings ("serial")
public class ReflectionException extends Exception
{
	protected ReflectionException (String message)
	{
		super (message);
	}

	protected ReflectionException (Exception exc)
	{
		super (exc);
	}

	protected ReflectionException (String message, Exception exc)
	{
		super (message, exc);
	}

	public ReflectionException (String messageStart, Field field,
			String messageEnd)
	{
		this (makeFieldMessage (messageStart, field, messageEnd));
	}

	public ReflectionException (String messageStart, Class<?> clazz,
			String messageEnd)
	{
		this (makeClassMessage (messageStart, clazz, messageEnd));
	}

	public ReflectionException (String messageStart, Constructor<?> constructor,
			String messageEnd)
	{
		this (makeConstructorMessage (messageStart, constructor, messageEnd));
	}

	public ReflectionException (String messageStart, Method method,
			String messageEnd)
	{
		this (makeMethodMessage (messageStart, method, messageEnd));
	}

	public ReflectionException (String messageStart,
			ValueAccessor<?> accessor,
			String messageEnd)
	{
		this (makeAccessorMessage (messageStart, accessor, messageEnd));
	}

	public static String makeAccessorMessage (String messageStart,
			ValueAccessor<?> accessor,
			String messageEnd)
	{
		messageStart = (messageStart == null) ? "" : (messageStart + " ");
		messageEnd = (messageEnd == null) ? "" : (" " + messageEnd);
		return (messageStart + accessor + messageEnd);
	}

	public static String makeFieldMessage (String messageStart, Field field,
			String messageEnd)
	{
		messageStart = (messageStart == null) ? "" : (messageStart + " ");
		messageEnd = (messageEnd == null) ? "" : (" " + messageEnd);
		return (messageStart + field.getDeclaringClass ().getCanonicalName () + "."
				+ field.getName () + messageEnd);
	}

	public static String makeClassMessage (String messageStart, Class<?> clazz,
			String messageEnd)
	{
		messageStart = (messageStart == null) ? "" : (messageStart + " ");
		messageEnd = (messageEnd == null) ? "" : (" " + messageEnd);
		return (messageStart + clazz.getCanonicalName () + messageEnd);
	}

	public static String makeConstructorMessage (String messageStart, Constructor<?> constructor,
			String messageEnd)
	{
		messageStart = (messageStart == null) ? "" : (messageStart + " ");
		messageEnd = (messageEnd == null) ? "" : (" " + messageEnd);
		return (messageStart + constructor + messageEnd);
	}

	public static String makeMethodMessage (String messageStart, Method method,
			String messageEnd)
	{
		messageStart = (messageStart == null) ? "" : (messageStart + " ");
		messageEnd = (messageEnd == null) ? "" : (" " + messageEnd);
		return (messageStart + method + messageEnd);
	}

	public static void reportInstanceCreationException (Class<?> cls, Exception e)
	{
		ReflectionException.reportException (cls, e,
				"Cannot create instance of " + cls.getCanonicalName ()
						+ ". Default constructor is unavailable");
	}
	
	public static void reportException (Class<?> annotatedClass, Exception e, String message)
	{
		new ReflectionException (message, e).reportException (annotatedClass);
	}

	public static void reportException (Class<?> annotatedClass, Exception e)
	{
		new ReflectionException (e.getLocalizedMessage (), e).reportException (annotatedClass);
	}

	public void reportException (Class<?> annotatedClass)
	{
		JOptionPane.showMessageDialog (
				null,
				getMessage ()
						+ "\nClick OK to see the traceback. You need to fix the reported UI annotation for class "
						+ annotatedClass.getCanonicalName (),
				"UIAnnotations.initialize () failure", JOptionPane.ERROR_MESSAGE);
		printStackTrace ();
	}
}
