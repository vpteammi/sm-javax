package com.sm.javax.langx.reflect;

public class MemberValueAccessor<T> extends ValueAccessor<T>
{
	public MemberValueAccessor(String accessSpec, ValueAccessor<?> member, ValueAccessor<T> memberValue)
	{
		super(accessSpec);
		this.member = member;
		this.memberValue = memberValue;
	}

	private final ValueAccessor<?> member;
	private final ValueAccessor<T> memberValue;

	@Override
	public Class<?> getType()
	{
		return memberValue.getType();
	}

	@Override
	public T getValue(Object obj, Object... args)
	{
		return (memberValue.getValue(member.getValue(obj), args));
	}

	@Override
	public boolean usesArgs ()
	{
		return memberValue.usesArgs ();
	}

	@Override
	public String toString ()
	{
		return (super.toString() + "=" + member.toString() + ":" + memberValue.toString ());
	}
}
