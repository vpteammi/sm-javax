package com.sm.javax.langx.reflect;

import java.util.ArrayList;
import java.util.Iterator;

public class MethodVariants<T> implements Iterable<MethodVariant<T>>
{
	private final Class<T> returnType;
	private final Class<?>[] argTypes;
	private final ArrayList<MethodVariant<T>> variants = new ArrayList<MethodVariant<T>> ();
	private MethodVariant<T>[] variantsArray = null;

	public MethodVariants (Class<T> returnType, Class<?>... argTypes)
	{
		this (returnType, false, argTypes);
	}

	public MethodVariants (Class<T> returnType, boolean allVariants,
			Class<?>... argTypes)
	{
		this.returnType = returnType;
		this.argTypes = argTypes;
		if (allVariants && (argTypes.length > 0))
		{
			boolean[] positions = new boolean[argTypes.length];
			add (positions, 0, true);
			add (positions, 0, false);
		}
		else
		{
			variants.add (new MethodVariant<T> (returnType, argTypes));
		}
	}

	private final void add (boolean[] positions, int index, boolean position)
	{
		positions[index] = position;
		index++;
		if (index == positions.length)
		{
			variant (positions);
		}
		else
		{
			add (positions, index, true);
			add (positions, index, false);
		}
	}

	public final MethodVariants<T> variant (boolean... usedArgs)
	{
		if (usedArgs.length == argTypes.length)
		{
			variants.add (new MethodVariant<T> (returnType, argTypes, usedArgs));
			variantsArray = null;
			return (this);
		}
		else
		{
			throw new IllegalArgumentException (
					"Variant spec has incorrect number of positions");
		}
	}

	@Override
	public Iterator<MethodVariant<T>> iterator ()
	{
		return (variants.iterator ());
	}

	@SuppressWarnings ("unchecked")
	MethodVariant<T>[] toArray ()
	{
		if (variantsArray == null)
		{
			variantsArray = variants.toArray (new MethodVariant[variants.size ()]);
		}
		return (variantsArray);
	}

	public Class<T> getReturnType ()
	{
		return (returnType);
	}
}
