package com.sm.javax.langx.reflect;

public class MethodSignature<T>
{
	private final Class<T> returnType;
	private final Class<?>[] parameterTypes;

	public MethodSignature (Class<T> returnType, Class<?>... parameterTypes)
	{
		super ();
		this.returnType = returnType;
		this.parameterTypes = parameterTypes;
	}

	public Class<T> getReturnType ()
	{
		return returnType;
	}

	public Class<?>[] getParameterTypes ()
	{
		return parameterTypes;
	}

	public boolean isVoid ()
	{
		return ((returnType == null) || returnType.equals (void.class) || returnType.equals (Void.class));
	}

	@Override
	public String toString ()
	{
		StringBuilder signature = new StringBuilder (returnType.getCanonicalName ());
		if (parameterTypes.length > 0)
		{
			String sep = " (";
			for (Class<?> type : parameterTypes)
			{
				signature.append (sep).append (type.getCanonicalName ());
				sep = ", ";
			}
			signature.append (")");
		}
		else
		{
			signature.append (" ()");
		}
		return (signature.toString ());
	}
}
