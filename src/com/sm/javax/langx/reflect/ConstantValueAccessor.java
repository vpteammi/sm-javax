package com.sm.javax.langx.reflect;


public class ConstantValueAccessor<T> extends ValueAccessor<T>
{
	public ConstantValueAccessor(T value)
	{
		super(String.valueOf(value));
		this.value = value;
	}

	@SuppressWarnings("unchecked")
	public static <T>ConstantValueAccessor<T> create (String valueRepr, Class<T> valueType)
	{
		if (valueType.isAssignableFrom(String.class))
		{
			return (ConstantValueAccessor<T>) (new ConstantValueAccessor<String>(valueRepr));
		}
		if (valueType.isAssignableFrom(Boolean.class) || valueType.isAssignableFrom(boolean.class))
		{
			return (ConstantValueAccessor<T>) (new ConstantValueAccessor<Boolean>(Boolean.valueOf(valueRepr)));
		}
		if (valueType.isAssignableFrom(Integer.class) || valueType.isAssignableFrom(int.class))
		{
			return (ConstantValueAccessor<T>) (new ConstantValueAccessor<Integer>(Integer.valueOf(valueRepr)));
		}
		if (valueType.isAssignableFrom(Long.class) || valueType.isAssignableFrom(long.class))
		{
			return (ConstantValueAccessor<T>) (new ConstantValueAccessor<Long>(Long.valueOf(valueRepr)));
		}
		if (valueType.isAssignableFrom(Short.class) || valueType.isAssignableFrom(short.class))
		{
			return (ConstantValueAccessor<T>) (new ConstantValueAccessor<Short>(Short.valueOf(valueRepr)));
		}
		if (valueType.isAssignableFrom(Double.class) || valueType.isAssignableFrom(double.class))
		{
			return (ConstantValueAccessor<T>) (new ConstantValueAccessor<Double>(Double.valueOf(valueRepr)));
		}
		throw new IllegalArgumentException ("Constants of type " + valueType.getName() + " not supported");
	}
	
	private final T value;

	@Override
	public T getValue(Object obj, Object... args)
	{
		return (value);
	}

	@Override
	public Class<?> getType()
	{
		return (Class<?>) (value.getClass());
	}

	@Override
	public boolean usesArgs ()
	{
		return (false);
	}

	@Override
	public String toString ()
	{
		return (super.toString() + "=" + value.toString());
	}
}
