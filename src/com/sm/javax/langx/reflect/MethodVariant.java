package com.sm.javax.langx.reflect;

import java.lang.reflect.Method;

public class MethodVariant<T> extends MethodSignature<T>
{
	private final boolean[] positions;
	private final boolean allPositionsUsed;
	private final boolean noPositionsUsed;

	public MethodVariant (Class<T> returnType, Class<?>[] parameterTypes,
			boolean[] positions)
	{
		super (returnType, filterParameterTypes (parameterTypes, positions));
		this.positions = positions;
		int usedPositions = 0;
		for (boolean pos : positions)
		{
			if (pos)
				usedPositions++;
		}
		allPositionsUsed = (usedPositions == positions.length);
		noPositionsUsed = (usedPositions == 0);
	}

	public MethodVariant (Class<T> returnType, Class<?>... parameterTypes)
	{
		this (returnType, parameterTypes, buildPositions (parameterTypes));
	}

	private static boolean[] buildPositions (Class<?>... parameterTypes)
	{
		boolean[] positions = new boolean[parameterTypes.length];
		int count = 0;
		for (Class<?> parameter : parameterTypes)
		{
			positions[count++] = (parameter != null);
		}
		return (positions);
	}

	private final static Class<?>[] emptyTypes = new Class<?>[0];

	private static Class<?>[] filterParameterTypes (Class<?>[] parameterTypes,
			boolean[] positions)
	{
		int count = 0;
		for (boolean pos : positions)
		{
			if (pos)
				count++;
		}

		if (count == parameterTypes.length)
			return (parameterTypes);

		if (count == 0)
			return (emptyTypes);

		Class<?>[] filteredTypes = new Class<?>[count];

		int argsCount = 0;
		int callCount = 0;

		for (boolean pos : positions)
		{
			if (pos)
				filteredTypes[callCount++] = parameterTypes[argsCount];
			argsCount++;
		}

		return (filteredTypes);
	}

	private final static Object[] emptyArgs = new Object [0];
	
	public Object[] getArgs (Object... args) throws ReflectionException
	{
		if (args.length == positions.length)
		{
			if (allPositionsUsed)
			{
				return (args);
			}
			else if (noPositionsUsed)
			{
				return (emptyArgs);
			}
			else
			{
				Object[] callArgs = new Object[getParameterTypes ().length];
				int argsCount = 0;
				int callCount = 0;
				for (boolean pos : positions)
				{
					if (pos)
						callArgs[callCount++] = args[argsCount];
					argsCount++;
				}
				return (callArgs);
			}
		}
		else
		{
			throw new ReflectionException (
					"Illegal number of parameters while calling " + this.toString () + ". Expected: " + positions.length
							+ ", supplied: " + args.length);
		}
	}

	public MethodValueAccessor<T> getAccessor (String spec, Method method)
	{
		method.setAccessible (true);
		return (new MethodValueAccessor<T> (spec, method, this));
	}
}
