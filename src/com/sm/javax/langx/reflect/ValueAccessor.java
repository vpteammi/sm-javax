package com.sm.javax.langx.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

import com.sm.javax.utilx.Tuple2;

public abstract class ValueAccessor<T>
{
	private final String accessSpec;

	protected void reportAccessException (Object obj, Throwable ex)
	{
		System.err.println (ex.getLocalizedMessage ()
				+ "\nwhen accessing value on object " + obj + " with spec "
				+ accessSpec);
		ex.printStackTrace ();
	}

	public abstract T getValue (Object obj, Object... args);

	/**
	 * Returns value type as declared in accessor. This type does not necessary
	 * match the type declared as generic parameter of UIValueAccessor but is
	 * assignable to it
	 * 
	 * @return
	 */
	public abstract Class<?> getType ();

	protected ValueAccessor (String accessSpec)
	{
		this.accessSpec = accessSpec;
	}

	public boolean usesArgs ()
	{
		return (true);
	}

	@Override
	public String toString ()
	{
		return (this.getClass ().getCanonicalName () + ":" + accessSpec + "("
				+ getType ().getCanonicalName () + ")");
	}

	private final static Pattern memberPattern = Pattern.compile ("\\$\\{.*\\}");

	public static String parseMemberReference (String accessSpec)
	{
		accessSpec = accessSpec.trim ();
		return (accessSpec.substring (2, accessSpec.length () - 1).trim ());
	}

	public static boolean isMemberSpec (String accessSpec)
	{
		return (memberPattern.matcher (accessSpec.trim ()).matches ());
	}

	public static <T> ValueAccessor<T> getAccessor (Field forField,
			boolean internalAccess, String name, MethodVariants<T> variants)
			throws MethodAmbiguityException
	{
		return (getAccessor (forField.getDeclaringClass (), internalAccess, name,
				variants));
	}

	private final static MethodVariants<Object> getterSignatures = new MethodVariants<Object> (Object.class);
	
	public static ValueAccessor<Object> getAccessor (Class<?> clazz,
					boolean internalAccess, String name)
			throws MethodAmbiguityException
	{
		return (getAccessor (clazz, internalAccess, name, getterSignatures));
	}

	@SuppressWarnings ("unchecked")
	public static <T> ValueAccessor<T> getAccessor (Class<?> clazz,
					boolean internalAccess, String name, MethodVariants<T> variants)
			throws MethodAmbiguityException
	{
		Tuple2<MethodVariant<T>, Method> method = Classes.getBestCompatibleMethod (
				clazz, internalAccess, name, variants);
		if (method != null)
		{
			return (method._1.getAccessor (name, method._2));
		}
		else
		{
			Class<?> accessorType = variants.getReturnType ();
			Field field = Classes.getFirstCompatibleField (clazz, internalAccess,
					name, accessorType);
			if (field != null)
			{
				field.setAccessible (true);
				return (new FieldValueAccessor<T> (name, field));
			}
			else if (Classes.isAssignableToFrom (boolean.class, accessorType))
			{
				if (name.equals ("true"))
				{
					return (ValueAccessor<T>) (new ConstantValueAccessor<Boolean> (
							Boolean.TRUE));
				}
				else if (name.equals ("false"))
				{
					return (ValueAccessor<T>) (new ConstantValueAccessor<Boolean> (
							Boolean.FALSE));
				}
			}
			else if (accessorType.equals (String.class))
			{
				return (ValueAccessor<T>) (new ConstantValueAccessor<String> (name));
			}
		}
		return (null);
	}
}
