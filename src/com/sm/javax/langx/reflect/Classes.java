package com.sm.javax.langx.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sm.javax.utilx.CollectionUtils;
import com.sm.javax.utilx.Tuple2;

@SuppressWarnings("unchecked")
public class Classes {
	private static boolean isAccessible(Member member, boolean thisClass, boolean internalAccess) {
		int modifiers = member.getModifiers();
		if (Modifier.isPublic(modifiers))
			return (true);
		if (Modifier.isPrivate(modifiers))
			return (thisClass && internalAccess);
		return (internalAccess);
	}

	/**
	 * Returns all the declared methods with a given name declared in this class
	 * 
	 * @param clazz
	 * @param name
	 * @return
	 */
	public static Method[] getDeclaredMethods(Class<?> clazz, boolean internalAccess, boolean thisClass, String name) {
		List<Method> methods = getDeclaredMethodsAsList(clazz, internalAccess, thisClass, name);
		return (methods.size() > 0 ? methods.toArray(new Method[methods.size()]) : emptyMethods);
	}

	private final static Method[] emptyMethods = new Method[0];

	private final static List<Method> emptyMethodsList = new ArrayList<Method>();

	public interface MethodFilter {
		boolean accept(Method m);
	}

	private static class NameFilter implements MethodFilter {
		private final String name;

		public NameFilter(String name) {
			this.name = name;
		}

		@Override
		public boolean accept(Method method) {
			return (method.getName().equals(name));
		}
	}

	public static List<Method> getDeclaredMethodsAsList(Class<?> clazz, boolean internalAccess, boolean thisClass,
			MethodFilter filter) {
		ArrayList<Method> methods = null;
		for (Method method : clazz.getDeclaredMethods()) {
			if (isAccessible(method, thisClass, internalAccess) && ((filter == null) || filter.accept(method))) {
				if (methods == null) {
					methods = new ArrayList<Method>();
				}
				methods.add(method);
			}
		}
		return (methods == null ? emptyMethodsList : methods);
	}

	public static List<Method> getDeclaredMethodsAsList(Class<?> clazz, boolean internalAccess, boolean thisClass,
			final String name) {
		return (getDeclaredMethodsAsList(clazz, internalAccess, thisClass, new NameFilter(name)));
	}

	/**
	 * Returns all the declared methods with a given name declared in this class
	 * and all its ancestors. The methods are ordered by hierarchy from bottom
	 * to top
	 * 
	 * @param clazz
	 * @param name
	 * @return
	 */
	public static Method[] getAllDeclaredMethods(Class<?> clazz, boolean internalAccess, String name) {
		List<Method> methods = getAllDeclaredMethodsAsList(clazz, internalAccess, name);
		return (methods.size() > 0 ? methods.toArray(new Method[methods.size()]) : emptyMethods);
	}

	public static List<Method> getAllDeclaredMethodsAsList(Class<?> clazz, boolean internalAccess,
			MethodFilter filter) {
		ArrayList<Method> methods = null;
		boolean thisClass = true;
		for (Class<?> cc = clazz; cc != null; cc = cc.getSuperclass()) {
			for (Method method : cc.getDeclaredMethods()) {
				if (isAccessible(method, thisClass, internalAccess) && filter.accept(method)) {
					if (methods == null) {
						methods = new ArrayList<Method>();
					}
					methods.add(method);
				}
			}
			thisClass = false;
		}
		return (methods == null ? emptyMethodsList : methods);
	}

	public static List<Method> getAllDeclaredMethodsAsList(Class<?> clazz, boolean internalAccess, String name) {
		return (getAllDeclaredMethodsAsList(clazz, internalAccess, new NameFilter(name)));
	}

	private final static Field[] emptyFields = new Field[0];
	private final static List<Field> emptyFieldsList = new ArrayList<Field>();

	public static Field[] getAllDeclaredFields(Class<?> clazz, boolean internalAccess, String name) {
		List<Field> fileds = getAllDeclaredFieldsAsList(clazz, internalAccess, name);
		return (fileds.size() > 0 ? fileds.toArray(new Field[fileds.size()]) : emptyFields);
	}

	static List<Field> getAllDeclaredFieldsAsList(Class<?> clazz, boolean internalAccess, String name) {
		ArrayList<Field> fields = null;
		boolean thisClass = true;
		for (Class<?> cc = clazz; cc != null; cc = cc.getSuperclass()) {
			for (Field field : cc.getDeclaredFields()) {
				if (isAccessible(field, thisClass, internalAccess) && field.getName().equals(name)) {
					if (fields == null) {
						fields = new ArrayList<Field>();
					}
					fields.add(field);
				}
			}
			thisClass = false;
		}
		return (fields == null ? emptyFieldsList : fields);
	}

	private final static Set<Class<?>> primitiveWrappersSet = new HashSet<>();
	private final static Map<Class<?>, Class<?>> primitiveWrappers = new HashMap<Class<?>, Class<?>>();

	static {
		CollectionUtils.addAll(primitiveWrappersSet, Boolean.class, Integer.class, Double.class, Long.class,
				Short.class, Byte.class, Character.class, Float.class, Void.class);
		primitiveWrappers.put(boolean.class, Boolean.class);
		primitiveWrappers.put(int.class, Integer.class);
		primitiveWrappers.put(double.class, Double.class);
		primitiveWrappers.put(long.class, Long.class);
		primitiveWrappers.put(short.class, Short.class);
		primitiveWrappers.put(byte.class, Byte.class);
		primitiveWrappers.put(char.class, Character.class);
		primitiveWrappers.put(float.class, Float.class);
		primitiveWrappers.put(void.class, Void.class);
	}

	public static Class<?> getPrimitiveWrapper(Class<?> primitiveClass) {
		Class<?> wrapper = primitiveWrappers.get(primitiveClass);
		return (wrapper == null ? primitiveClass : wrapper);
	}

	private static boolean primitiveEquals(Class<?> primitiveClass, Class<?> otherClass) {
		Class<?> wrapper = primitiveWrappers.get(primitiveClass);
		return ((wrapper != null) && wrapper.equals(otherClass));
	}

	public static boolean equals(Class<?> class1, Class<?> class2) {
		if (class1 == class2) {
			return (true);
		} else if (class1.equals(class2)) {
			return (true);
		} else if (class1.isPrimitive()) {
			return (primitiveEquals(class1, class2));
		} else if (class2.isPrimitive()) {
			return (primitiveEquals(class2, class1));
		}
		return (false);
	}

	public static boolean isPrimitive(Class<?> cls) {
		return (cls.isPrimitive() || primitiveWrappersSet.contains(cls));
	}

	public static boolean isBoolean(Class<?> cls) {
		return (equals(cls, Boolean.class));
	}

	public static boolean isAssignableToFrom(Class<?> to, Class<?> from) {
		if (equals(to, from)) {
			return (true);
		} else if (!to.isPrimitive() && !from.isPrimitive()) {
			return (to.isAssignableFrom(from));
		}
		return (to.equals(Object.class));
	}

	public static boolean isCompatible(Method method, Class<?> returnType, Class<?>... parameterTypes) {
		if (!returnType.equals(void.class) && !returnType.equals(Void.class)) {
			if (!isAssignableToFrom(returnType, method.getReturnType())) {
				return (false);
			}
		}
		Class<?>[] methodParameterTypes = method.getParameterTypes();
		int parametersCount = parameterTypes.length;
		if (parametersCount != methodParameterTypes.length) {
			return (false);
		} else {
			for (int i = 0; i < parametersCount; i++) {
				if (!isAssignableToFrom(methodParameterTypes[i], parameterTypes[i])) {
					return (false);
				}
			}
		}
		return (true);
	}

	private static int getCompatibilityDistance(Method method, Class<?> returnType, Class<?>... parameterTypes) {
		int distance = 0;

		if (!equals(returnType, method.getReturnType())) {
			distance++;
		}

		Class<?>[] methodParameterTypes = method.getParameterTypes();
		int parametersCount = Math.min(parameterTypes.length, methodParameterTypes.length);
		for (int i = 0; i < parametersCount; i++) {
			if (!equals(methodParameterTypes[i], parameterTypes[i])) {
				distance++;
			}
		}

		distance += Math.max(parameterTypes.length, methodParameterTypes.length) - parametersCount;

		return (distance);
	}

	private static <T extends MethodSignature<?>> int getCompatibilityDistance(Tuple2<T, Method> method) {
		return (getCompatibilityDistance(method._2, method._1.getReturnType(), method._1.getParameterTypes()));
	}

	public static Field getBestCompatibleField(Class<?> clazz, boolean internalAccess, String name, Class<?> type) {
		return (getFirstCompatibleField(clazz, internalAccess, name, type));
	}

	public static Field getFirstCompatibleField(Class<?> clazz, boolean internalAccess, String name, Class<?> type) {
		for (Field field : getAllDeclaredFields(clazz, internalAccess, name)) {
			if (isAssignableToFrom(type, field.getType())) {
				return (field);
			}
		}
		return (null);
	}

	@SuppressWarnings("rawtypes")
	public static <T extends MethodSignature> Tuple2<T, Method> getBestCompatibleMethod(Class<?> clazz,
			boolean internalAccess, String name, Iterable<T> signatures) throws MethodAmbiguityException {
		List<Tuple2<T, Method>> methods = getCompatibleMethodsAsList(clazz, internalAccess, name, signatures);

		int size = methods.size();

		if (size == 1) {
			return (methods.get(0));
		} else if (size > 1) {
			int bestDistance = Integer.MAX_VALUE;
			int bestCount = 0;
			int bestIndex = -1;
			int index = 0;
			for (Tuple2<T, Method> method : methods) {
				int distance = getCompatibilityDistance(method);
				if (distance < bestDistance) {
					bestDistance = distance;
					bestIndex = index;
					bestCount = 1;
				} else if (distance == bestDistance) {
					bestCount++;
				}
				index++;
			}

			if (bestCount == 1) {
				return (methods.get(bestIndex));
			}

			throw new MethodAmbiguityException(name, MethodAmbiguityException.toArray(methods));
		} else {
			return (null);
		}
	}

	@SuppressWarnings("rawtypes")
	public static <T extends MethodSignature> Tuple2<T, Method> getFirstCompatibleMethod(Class<?> clazz,
			boolean internalAccess, String name, T... signatures) {
		for (Method method : getAllDeclaredMethods(clazz, internalAccess, name)) {
			for (T signature : signatures) {
				if (isCompatible(method, signature.getReturnType(), signature.getParameterTypes())) {
					return new Tuple2<T, Method>(signature, method);
				}
			}
		}
		return (null);
	}

	private final static ArrayList<?> emptyList = new ArrayList<Object>();

	@SuppressWarnings({ "rawtypes" })
	static <T extends MethodSignature> List<Tuple2<T, Method>> getCompatibleMethodsAsList(Class<?> clazz,
			boolean internalAccess, String name, Iterable<T> signatures) {
		ArrayList<Tuple2<T, Method>> methods = null;
		boolean thisClass = true;
		for (Class<?> cc = clazz; cc != null; cc = cc.getSuperclass()) {
			for (Method method : getDeclaredMethods(cc, internalAccess, thisClass, name)) {
				for (T signature : signatures) {
					if (isCompatible(method, signature.getReturnType(), signature.getParameterTypes())) {
						if (methods == null) {
							methods = new ArrayList<Tuple2<T, Method>>();
						}
						methods.add(new Tuple2<T, Method>(signature, method));
					}
				}
			}

			if (methods != null) {
				return (methods);
			}

			thisClass = false;
		}

		return ((ArrayList<Tuple2<T, Method>>) emptyList);
	}

	public final static Class<?>[] EMPTY_CLASS_ARRAY = new Class<?>[0];

	public static Class<?>[] getAllImplementedInterfaces(Class<?> clazz) {
		if (clazz != null) {
			HashSet<Class<?>> interfaces = new HashSet<Class<?>>();

			while (clazz != null) {
				for (Class<?> intf : clazz.getInterfaces()) {
					interfaces.add(intf);
				}

				clazz = clazz.getSuperclass();
			}

			return (interfaces.toArray(EMPTY_CLASS_ARRAY));
		}

		return (EMPTY_CLASS_ARRAY);
	}

	public static Class<?>[] getAllSuperclasses(Class<?> clazz) {
		if ((clazz != null) && ((clazz = clazz.getSuperclass()) != null)) {
			ArrayList<Class<?>> superclasses = new ArrayList<Class<?>>();

			while (clazz != null) {
				superclasses.add(clazz);
				clazz = clazz.getSuperclass();
			}

			return (superclasses.toArray(EMPTY_CLASS_ARRAY));
		}

		return (EMPTY_CLASS_ARRAY);
	}
}
