package com.sm.javax.langx.reflect;

import java.lang.reflect.Field;


public class FieldValueAccessor<T> extends ValueAccessor<T>
{
	public FieldValueAccessor(String accessSpec, Field field)
	{
		super(accessSpec);
		this.field = field;
	}

	private final Field field;

	@SuppressWarnings("unchecked")
	@Override
	public T getValue(Object obj, Object ... args)
	{
		try
		{
			return (T) (field.get(obj));
		}
		catch (IllegalArgumentException e)
		{
			reportAccessException(obj, e);
		}
		catch (IllegalAccessException e)
		{
			reportAccessException(obj, e);
		}
		return (null);
	}

	@Override
	public Class<?> getType()
	{
		return (Class<?>) (field.getType());
	}

	@Override
	public boolean usesArgs ()
	{
		return (false);
	}

	@Override
	public String toString ()
	{
		return (super.toString() + "=" + field.toString());
	}
}
