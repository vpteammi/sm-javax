package com.sm.javax.langx;

@SuppressWarnings("serial")
public class ValueCastException extends ExceptionX {

	public ValueCastException() {
		super();
	}

	public ValueCastException(String format, Object... args) {
		super(format, args);
	}

	public ValueCastException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ValueCastException(String message, Throwable cause) {
		super(message, cause);
	}

	public ValueCastException(String message) {
		super(message);
	}

	public ValueCastException(Throwable cause, String format, Object... args) {
		super(cause, format, args);
	}

	public ValueCastException(Throwable cause) {
		super(cause);
	}
}
