package com.sm.javax.httpx;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sm.javax.langx.Strings;
import com.sm.javax.utilx.ArrayUtils;

public class HttpUtils
{
	public static class ParsedURI
	{
		public String authority;
		public String scheme;
		public String host;
		public String userInfo;
		public int port;
		public String path;
		public String query;
		public String fragment;

		public ParsedURI (String url) throws URISyntaxException, MalformedURLException
		{
			this (new URL (url));
		}

		public ParsedURI (URL url) throws URISyntaxException
		{
			this (url.toURI ());
			this.query = url.getQuery ();
		}

		public ParsedURI (URI uri)
		{
			this.scheme = uri.getScheme ();
			this.authority = uri.getAuthority ();
			this.host = uri.getHost ();
			this.port = uri.getPort ();
			this.path = uri.getPath ();
			this.query = uri.getQuery ();
			this.fragment = uri.getFragment ();
		}

		public String getFullQueryString ()
		{
			if (!Strings.isEmpty (query) || (!Strings.isEmpty (fragment)))
			{
				return (path + (!Strings.isEmpty (query) ? ("?" + query) : "") + (!Strings
						.isEmpty (fragment) ? ("#" + fragment) : ""));
			}
			else
				return (path);
		}

		public URL toURL () throws MalformedURLException, URISyntaxException,
				UnsupportedEncodingException
		{
			return (new URL (scheme, host, port, getFullQueryString ()));
		}

		public URI toURI () throws URISyntaxException, UnsupportedEncodingException
		{
			return (new URI (scheme, userInfo, host, port, path, decodeUrl (query),
					fragment));
		}

		void debug ()
		{
			System.out.println ("authority = " + authority);
			System.out.println ("scheme = " + scheme);
			System.out.println ("host = " + host);
			System.out.println ("userInfo = " + userInfo);
			System.out.println ("path = " + path);
			System.out.println ("port = " + port);
			System.out.println ("query = " + query);
			System.out.println ("fragment = " + fragment);
		}

		public void addToQuery (String[] parameters)
		{
			if (!ArrayUtils.isEmpty (parameters))
			{
				StringBuilder queryString = Strings.isEmpty (query) ? null
						: new StringBuilder (query);
				
				for (String parameter: parameters)
				{
					queryString = Strings.concat (queryString, "&", parameter);
				}
				
				query = queryString.toString ();
			}
		}
	}

	public static URL removeUrlParameters (URL url, String... exclParameters)
	{
		if ((url != null) && !ArrayUtils.isEmpty (exclParameters))
		{
			try
			{
				ParsedURI uri = new ParsedURI (url);
				String urlQuery = uri.query;
				if (!Strings.isEmpty (urlQuery))
				{
					Map<String, List<String>> parameters = splitUrlQuery (urlQuery);
					for (String parm : exclParameters)
					{
						parameters.remove (parm);
					}

					if (parameters.size () > 0)
					{
						uri.query = buildQuery (parameters);
					}
					else
					{
						uri.query = null;
					}
					url = uri.toURL ();
				}
			}
			catch (MalformedURLException | URISyntaxException
					| UnsupportedEncodingException e)
			{
				e.printStackTrace ();
			}
		}

		return (url);
	}

	public static ParsedURI parseUrl (String url)
	{
		try
		{
			return (new ParsedURI (url));
		}
		catch (MalformedURLException | URISyntaxException e)
		{
			return (null);
		}
	}

	public static URL removeUrlParameters (String url, String... exclParameters)
	{
		if (!Strings.isEmpty (url))
		{
			try
			{
				return (removeUrlParameters (new URL (url), exclParameters));
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace ();
			}
		}

		return (null);
	}

	public static String addUrlParameters (String url, String... parameters)
	{
		try
		{
			return (addUrlParameters (new URL (url), parameters).toExternalForm ());
		}
		catch (MalformedURLException e)
		{
			return (url);
		}
	}

	public static URL addUrlParameters (URL url, String... parameters)
	{
		if ((url != null) && (parameters.length > 0))
		{
			try
			{
				ParsedURI uri = new ParsedURI (url);
				uri.addToQuery (parameters);
				url = uri.toURL ();
			}
			catch (URISyntaxException | MalformedURLException | UnsupportedEncodingException e)
			{
				e.printStackTrace ();
			}
		}

		return (url);
	}

	public static String buildQuery (Map<String, List<String>> parameters)
	{
		try
		{
			return (buildQuery (parameters, "UTF-8"));
		}
		catch (UnsupportedEncodingException e)
		{
			try
			{
				return (buildQuery (parameters, null));
			}
			catch (UnsupportedEncodingException e1)
			{
				// This shouldn't happen
				return ("");
			}
		}
	}

	public static String encodeUrl (String text, String encoding)
			throws UnsupportedEncodingException
	{
		return (Strings.isEmpty (encoding) ? encodeUrl (text) : URLEncoder.encode (
				text, encoding));
	}

	public static String encodeUrl (String text)
			throws UnsupportedEncodingException
	{
		return (URLEncoder.encode (text, "UTF-8"));
	}

	public static String decodeUrl (String text, String encoding)
			throws UnsupportedEncodingException
	{
		return (Strings.isEmpty (encoding) ? decodeUrl (text) : URLDecoder.decode (
				text, encoding));
	}

	public static String decodeUrl (String text)
			throws UnsupportedEncodingException
	{
		return (Strings.isEmpty (text) ? text : URLDecoder.decode (text, "UTF-8"));
	}

	public static String buildQuery (Map<String, List<String>> parameters,
			String encoding) throws UnsupportedEncodingException
	{
		StringBuilder query = null;
		if ((parameters != null) && (parameters.size () > 0))
		{
			for (Entry<String, List<String>> parameter : parameters.entrySet ())
			{
				String key = encodeUrl (parameter.getKey (), "UTF-8");
				// String key = parameter.getKey ();
				for (String value : parameter.getValue ())
				{
					query = Strings.concat (query, "&", key);
					if (!Strings.isEmpty (value))
						// query.append ("=").append (value);
						query.append ("=").append (encodeUrl (value, "UTF-8"));
				}
			}
		}

		return (query == null ? null : query.toString ());
	}

	public static Map<String, List<String>> splitUrlQuery (URL url)
			throws UnsupportedEncodingException
	{
		return (splitUrlQuery (url.getQuery ()));
	}

	public static Map<String, List<String>> splitUrlQuery (String query)
	{
		try
		{
			return (splitUrlQuery (query, "UTF-8"));
		}
		catch (UnsupportedEncodingException e)
		{
			try
			{
				return (splitUrlQuery (query, null));
			}
			catch (UnsupportedEncodingException e1)
			{
				// This shouldn't happen
				return (null);
			}
		}
	}

	public static Map<String, List<String>> splitUrlQuery (String query,
			String encoding) throws UnsupportedEncodingException
	{
		final Map<String, List<String>> queryPairs = new LinkedHashMap<String, List<String>> ();
		final String[] pairs = Strings.noNull (query).split ("&");
		for (String pair : pairs)
		{
			final int idx = pair.indexOf ("=");
			final String key = idx > 0 ? decodeUrl (pair.substring (0, idx), encoding)
					: pair;
			// final String key = idx > 0 ? pair.substring (0, idx) : pair;
			if (!queryPairs.containsKey (key))
			{
				queryPairs.put (key, new LinkedList<String> ());
			}
			final String value = (idx > 0 && (pair.length () > idx + 1)) ? decodeUrl (
					pair.substring (idx + 1), encoding) : null;
			// final String value = (idx > 0 && (pair.length () > idx + 1)) ?
			// pair.substring (idx + 1) : null;
			queryPairs.get (key).add (value);
		}
		return queryPairs;
	}

	public static void setCookie (HttpServletResponse response, String name,
			Object value)
	{
		if (response != null && !Strings.isEmpty (name))
		{
			response.addCookie (new Cookie (name, Strings.noNull (value)));
		}
	}

	public static Cookie getCookie (HttpServletRequest request, String name)
	{
		if (request != null && !Strings.isEmpty (name))
		{
			Cookie[] cookies = request.getCookies ();
			if (!ArrayUtils.isEmpty (cookies))
			{
				for (Cookie cookie : cookies)
				{
					if (name.equals (cookie.getName ()))
						return (cookie);
				}
			}
		}

		return (null);
	}

	public static void main (String[] args)
	{
		try
		{
			ParsedURI aURL;
			URL url = new URL (
					"http://localhost:8080/TaskManagement/Creo4ReleaseThemes.jsp?ist=Creo+NC+%26+Tool+Design+%2F+Mold+Design+IST&query=Creo+Release+Themes+in+Analysis&refresh=iii");
			aURL = new ParsedURI (url);
			aURL.debug ();
			System.out.println (url.toExternalForm ());
			System.out.println ("remove refresh: "
					+ removeUrlParameters (url, "refresh").toExternalForm ());
			System.out.println ("remove refresh,list: "
					+ removeUrlParameters (url, "refresh", "list").toExternalForm ());
			url = new URL ("http://java.sun.com:80/docs/books/"
					+ "tutorial/index.html?xa=90#DOWNLOADING");
			aURL = new ParsedURI (url);
			aURL.debug ();
			System.out.println (url.toExternalForm ());
			System.out.println (aURL.toURL ().toExternalForm ());
			String emailUrl = "http://localhost:8080/TaskManagement/Creo4ReleaseThemes.jsp?user=vparfenov@ptc.com&list=view&refresh=yes";
			System.out.println ("Original: " + emailUrl);
			System.out.println ("Encoded: " + encodeUrl (emailUrl));
			System.out.println ("Decoded: " + decodeUrl (emailUrl));
			System.out.println ("Encoded [Creo NC & Tool Design / Mold Design IST]: "
					+ encodeUrl ("Creo NC & Tool Design / Mold Design IST"));

		}
		catch (MalformedURLException | URISyntaxException
				| UnsupportedEncodingException e)
		{
			e.printStackTrace ();
		}
	}
}
