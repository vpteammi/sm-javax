package com.sm.javax.httpx;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HttpResponse
{
	private final HttpURLConnection connection;

	HttpResponse (HttpURLConnection connection)
	{
		this.connection = connection;
	}
	
	public int getCode () throws IOException
	{
		return connection.getResponseCode ();
	}
	
	public String getMessage () throws IOException
	{
		return connection.getResponseMessage ();
	}
	
	public Map<String, List<String>> getHeaders ()
	{
		return connection.getHeaderFields ();
	}
	
	public List<String> getHeaders (String header)
	{
		return getHeaders ().get (header);
	}
	
	public String getHeader (String header)
	{
		List<String> values = getHeaders ().get (header);
		return (values == null) ? null : values.stream ().findAny ().orElse (null);
	}
	
	public String getContentAsString () throws IOException
	{
		return new BufferedReader(new InputStreamReader(connection.getInputStream ()))
			  .lines().collect(Collectors.joining("\n"));
	}

	public void saveContentToFile (File file) throws IOException
	{
		Files.copy(connection.getInputStream (), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}

	public void saveContentToFile (String path) throws IOException
	{
		saveContentToFile (new File (path));
	}

	public HttpURLConnection getConnection ()
	{
		return connection;
	}
}
