package com.sm.javax.httpx;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

public class HttpForm extends HttpRequest <HttpForm>
{
	private String boundary = "--------------------------" + 
								new Random (System.currentTimeMillis ()).ints (8, 1000, 9999)
									.mapToObj (v -> String.valueOf (v))
									.collect (Collectors.joining ());
	

	private final Buffer endOfForm = new Buffer (boundary.length () + 10).println ().print ("--").print (boundary).println ("--");

	private static class Buffer
	{
		private final ByteArrayOutputStream byteStream;
		private PrintStream printStream;

		public Buffer ()
		{
			this.byteStream = new ByteArrayOutputStream ();
		}

		public Buffer (int size)
		{
			this.byteStream = new ByteArrayOutputStream (size);
		}

		private Buffer initialize ()
		{
			if (printStream == null)
			{
				printStream = new PrintStream (byteStream);
			}
			return this;
		}

		public Buffer println ()
		{
			initialize ();
			printStream.print ("\r\n");
			return this;
		}

		public Buffer print (Object value)
		{
			if (value != null)
			{
				initialize ();
				printStream.print (String.valueOf (value));
			}
			return this;
		}

		public Buffer println (Object value)
		{
			return print (value).println ();
		}

		public long size ()
		{
			if (printStream != null)
			{
				printStream.flush ();
			}

			return byteStream.size ();
		}

		public void write (OutputStream os) throws IOException
		{
			byteStream.writeTo (os);
		}
	}

	private class Part extends Buffer
	{
		public Part (String name)
		{
			super ();
			println ().print ("--").println (boundary);
			print ("Content-Disposition: form-data; name=\"" + name + "\"");
		}
	}

	private class TextPart extends Part
	{
		public TextPart (String name, Object data)
		{
			super (name);
			println ();
			println ();
			print (data);
		}
	}

	private class FilePart extends Part
	{
		private final File file;

		public FilePart (String name, File file)
		{
			super (name);
			this.file = file;
			println ("; filename=\"" + file.getName () + "\"");
			println ("Content-Type: application/octet-stream");
			println ();
		}

		@Override
		public long size ()
		{
			return super.size () + file.length ();
		}

		@Override
		public void write (OutputStream os) throws IOException
		{
			super.write (os);
			Files.copy (file.toPath (), os);
			System.out.println (file.getAbsolutePath () + ": size=" + file.length ());
		}
	}

	private final ArrayList<Part> parts = new ArrayList<> ();
	
	public HttpForm (URL url) throws MalformedURLException
	{
		super (url);
		setMethod (Method.POST);
	}

	public HttpForm addPart (String name, Object data)
	{
		parts.add (new TextPart (name, data));
		return this;
	}

	public HttpForm addFile (String name, File file)
	{
		parts.add (new FilePart (name, file));
		return this;
	}

	public HttpResponse submit () throws IOException
	{
		setRequestProperty ("Content-Type", "multipart/form-data; boundary=" + boundary);
		setRequestProperty ("Content-Length", String.valueOf (contentLength ()));
		HttpResponse response = connect ();
		if (debug) System.out.println ("Content-Length:" + contentLength ());
		OutputStream os = response.getConnection ().getOutputStream ();
		FileOutputStream output = debug ? new FileOutputStream ("output.txt") : null;
		for (Part part : parts)
		{
			if (debug) part.write (output);
			part.write (os);
		}
		if (debug) endOfForm.write (output);
		endOfForm.write (os);
		if (debug) output.close ();
		return response;
	}

	private long contentLength ()
	{
		return parts.stream ().mapToLong (p -> p.size ()).sum () + endOfForm.size ();
	}

	@Override
	protected HttpForm getThis ()
	{
		return this;
	}
}
