package com.sm.javax.httpx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

import com.sm.javax.netx.NetException;
import com.sm.javax.netx.NetSimpleSocketServer;

public class HttpSimpleServer extends NetSimpleSocketServer
{

	public HttpSimpleServer () throws NetException
	{
		super ();
	}

	public URL getLocalHttpUrl ()
	{
		try
		{
			return (new URL ("http://localhost:" + getLocalPort ()));
		}
		catch (MalformedURLException e)
		{
			return (null);
		}
	}

	@Override
	protected void onAccept (InputStream is)
	{
			onAccept (new BufferedReader (new InputStreamReader (is)));
	}

	@Override
	protected void onAccept (OutputStream os)
	{
		PrintWriter writer = null;
		onAccept (writer = new PrintWriter (os));
		writer.close ();
	}
	
	protected void onAccept (BufferedReader reader)
	{
		try
		{
			String line = null;
			do
			{
				line = reader.readLine ();
				 System.out.println ("[" + line + "]");
			}
			while (line.length () > 0);
		}
		catch (IOException e)
		{
			catchException (e);
		}
	}

	protected void onAccept (PrintWriter writer)
	{
	}
}
