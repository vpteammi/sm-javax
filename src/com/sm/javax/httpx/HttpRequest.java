package com.sm.javax.httpx;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;

public abstract class HttpRequest<THIS extends HttpRequest<?>>
{
	protected boolean debug = false;
	
	private final HashMap<String, String> properties = new HashMap<> ();
	private final URL url;
	private Method method = Method.GET;

	public static enum Method
	{
		GET (false), PUT (true), POST (true), HEAD (false), DELETE (false);
		
		public final boolean output;

		private Method (boolean output)
		{
			this.output = output;
		}
	};

	public HttpRequest (URL url) throws MalformedURLException
	{
		String protocol = url.getProtocol ();
		if (!protocol.equals ("http") && !protocol.equals ("https"))
		{
			throw new MalformedURLException ("Expected http or https");
		}
		this.url = url;
	}

	public THIS debug (boolean debug)
	{
		this.debug = debug;
		return getThis ();
	}

	public THIS setMethod (String method) throws MalformedURLException
	{
		return setMethod (Method.valueOf (method));
	}

	public THIS setMethod (Method method)
	{
		this.method = method;
		return getThis ();
	}

	public THIS setBasicAuthorization (String username, String password)
	{
		return setRequestProperty ("Authorization",
				"Basic " + Base64.getEncoder ().encodeToString ((username + ":" + password).getBytes ()));
	}

	public THIS setRequestProperty (String name, Object value)
	{
		properties.put (name, String.valueOf (value));
		return getThis ();
	}

	public HttpResponse connect () throws IOException
	{
		HttpURLConnection connection = (HttpURLConnection) url.openConnection ();
		connection.setDoOutput (method.output);
		connection.setRequestMethod (method.name ());
		properties.entrySet ().stream ().forEach (p -> connection.setRequestProperty (p.getKey (), p.getValue ()));
		return new HttpResponse (connection);
	}

	protected abstract THIS getThis ();
}